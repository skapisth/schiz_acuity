function [mean_amp_t,theta,amp_1,...
        mean_dist_diff_bn_s_e_ms_to_center,...
        mean_l_d_c,mean_s_d_c,...
        x_all,y_all] = calc_amp_theta(msInfo,tasktype,scatPlot,title_str,x_all,y_all)
    
    [amp_1,theta] = deal([]);
    diff_bn_start_end_ms_dist_to_center = [];
    [land_dist_to_c,start_dist_to_c] = deal([]);
    [all_sx,all_sy,all_lx,all_ly,x_all,y_all] = deal([]);
    for ts = 1:length(msInfo.startPos)
        if strcmp(tasktype,'fix')
            for ms = 1:length(msInfo.startPos{ts}(1,:))
                startEndLocs = [msInfo.startPos{ts}(1,ms),msInfo.startPos{ts}(2,ms);...
                                msInfo.endPos{ts}(1,ms),msInfo.endPos{ts}(2,ms)];
                amp_1(1,end+1) = pdist(startEndLocs,'euclidean');
                
                sx = msInfo.startPos{ts}(1,ms);
                sy = msInfo.startPos{ts}(2,ms);
                lx = msInfo.endPos{ts}(1,ms);
                ly = msInfo.endPos{ts}(2,ms);
                
                x_all(1,end+1) = sx - lx;
                y_all(1,end+1) = sy - ly;
                
                theta(1,end+1) = atan2(sy - ly, sx - lx);
                
                
                
                s_center_locs = [msInfo.startPos{ts}(1,ms),msInfo.startPos{ts}(2,ms);...
                                   0,0];
                s_dist_center = pdist(s_center_locs,'euclidean');
                
                e_center_locs = [msInfo.endPos{ts}(1,ms),msInfo.endPos{ts}(2,ms);...
                                   0,0];
                e_dist_center = pdist(e_center_locs,'euclidean');
                
                diff_bn_start_end_ms_dist_to_center(1,end+1) = s_dist_center - e_dist_center;
                start_dist_to_c(1,end+1) = s_dist_center;
                land_dist_to_c(1,end+1) = e_dist_center;
                all_sx(1,end+1) = sx;
                all_sy(1,end+1) = sy;
                all_lx(1,end+1)  = lx;
                all_ly(1,end+1)  = ly;
                stimuliSize = 0;
            end
        
            
        else
             for ms = 1:length(msInfo.startPos)
               startP_x = msInfo.startPos(1,ms);
               startP_y = msInfo.startPos(2,ms);
               endP_x = msInfo.endPos(1,ms);
               endP_y = msInfo.endPos(2,ms);
           
               if ~isnan(startP_x)
                   startEndLocs = [startP_x,startP_y;...
                                    endP_x,endP_y];
                    amp_1(1,end+1) = pdist(startEndLocs,'euclidean');

                    sx = startP_x;
                    sy = startP_y;
                    lx = endP_x;
                    ly = endP_y;
                    theta(1,end+1) = atan2(sy - ly, sx - lx);
                    
                    x_all(1,end+1) = sx - lx;
                    y_all(1,end+1) = sy - ly;
                    
                    s_center_locs = [sx,sy;...
                                   0,0];
                    s_dist_center = pdist(s_center_locs,'euclidean');

                    e_center_locs = [lx,ly;...
                                       0,0];
                    e_dist_center = pdist(e_center_locs,'euclidean');

                    diff_bn_start_end_ms_dist_to_center(1,end+1) = s_dist_center - e_dist_center;
                    start_dist_to_c(1,end+1) = s_dist_center;
                    land_dist_to_c(1,end+1) = e_dist_center;
                    all_sx(1,end+1) = sx;
                    all_sy(1,end+1) = sy;
                    all_lx(1,end+1)  = lx;
                    all_ly(1,end+1)  = ly;
               end
            end
            stimuliSize = 4.2;
        end
            
    end
    mean_amp_t = mean(amp_1);
    mean_dist_diff_bn_s_e_ms_to_center = mean(diff_bn_start_end_ms_dist_to_center);
    idx_reqd = find(start_dist_to_c >= 10);
    mean_s_d_c = mean(start_dist_to_c(idx_reqd));
    mean_l_d_c = mean(land_dist_to_c(idx_reqd));
    if scatPlot
        figure()
        if strcmp(tasktype,'fix')
            centerX = 8;
            centerY = -8;
            width = 16;
            height = 16;
            rectangle('Position',[-centerX+stimuliSize, centerY, width, height],'LineWidth',3,'LineStyle','-')
            hold on
        else
            stimuliSize = stimuliSize/2;
            width = 2 * stimuliSize;
            height = width * 5;
            centerX = (-stimuliSize);
            centerY = (-stimuliSize*5);
            rectangle('Position',[centerX, centerY, width, height],'LineWidth',3)
            hold on
        end
        
%         scatter(all_sx,all_sy,'r')
%         hold on
        scatter(all_lx,all_ly)
        
        
        
        t_x = [all_sx; all_lx]';
        t_y = [all_sy; all_ly]';
%         for each = 1:length(all_lx)
%             plot(t_x(each,:),t_y(each,:), '-','color','g')
%             hold on
%         end
        xlim([-30,30])
        ylim([-30,30])
        xlabel('arcmins')
        ylabel('arcmins')
        axis square
        title(title_str)
%         legend('start location','end location')
    end
    
end
