function [ poiEM ] = plotColorsOnTraces( pptrials, currentTrialId, em, c, convert )
%Plots the color and location of the wanted EM in the gui
poiEM = [];

for i = 1:length(pptrials{currentTrialId}.(em).start)
    startTime = pptrials{currentTrialId}.(em).start(i) * convert;
    if isempty(pptrials{currentTrialId}.(em).duration)
        durationTime = 5;
    else
        durationTime = pptrials{currentTrialId}.(em).duration(i) * convert;
    end
    poiEM = fill([startTime, startTime ...
        startTime + durationTime, ...
        startTime + durationTime], ...
        [-35, 35, 35, -35], ...
        c, 'EdgeColor', c, 'LineWidth', 2, 'Clipping', 'On', 'FaceAlpha', 0.25);
end
end
