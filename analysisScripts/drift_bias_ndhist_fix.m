function drift_bias_ndhist_fix(patients,controls)
    
    cP = jet(length(patients));
    cC = winter(length(controls));

    for ii_p = 1:length(patients)
        [inst_sp_x_P,inst_sp_y_P] = deal([]);
        dir_path = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\patients\',patients{ii_p},'\matFiles\');
        file_to_read = strcat('fix_results_cut_drift_',patients{ii_p},'.mat');
        load(strcat(dir_path,file_to_read))
        for each = 1:length(fixation.instSpX)
            x = cell2mat(fixation.instSpX{each});
            y = cell2mat(fixation.instSpY{each});
            inst_sp_x_P = [inst_sp_x_P,x];
            inst_sp_y_P = [inst_sp_y_P,y];
        end
        clear em;
        temp_inst_sp_x_P = inst_sp_x_P(inst_sp_x_P > -200 & inst_sp_x_P < 200);
        temp_inst_sp_x_P = inst_sp_x_P(inst_sp_y_P > -200 & inst_sp_y_P < 200);
        temp_inst_sp_y_P = inst_sp_y_P(inst_sp_x_P > -200 & inst_sp_x_P < 200);
        temp_inst_sp_y_P = inst_sp_y_P(inst_sp_y_P > -200 & inst_sp_y_P < 200);
%         figure()
        ndhist(temp_inst_sp_x_P(~isnan(temp_inst_sp_x_P)), temp_inst_sp_y_P(~isnan(temp_inst_sp_y_P)), 'bins',3, 'radial','axis',[-200 200 -200 200],'nr','filt');
        figure()
        ndhist(inst_sp_x_P, inst_sp_y_P, 'bins', 2, 'radial','axis',[-200 200 -200 200],'nr','filt');
        title(patients{ii_p})
    end
    for ii_c = 1:length(controls)
        [inst_sp_x_C,inst_sp_y_C] = deal([]);
        dir_path = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\controls\',controls{ii_c},'\matFiles\');
        file_to_read = strcat('fix_results_cut_drift_',controls{ii_c},'.mat');
        load(strcat(dir_path,file_to_read))
        for each = 1:length(fixation.instSpX)
            x = cell2mat(fixation.instSpX{each});
            y = cell2mat(fixation.instSpY{each});
            inst_sp_x_C = [inst_sp_x_C,x];
            inst_sp_y_C = [inst_sp_y_C,y];
        end
        temp_inst_sp_x_C = inst_sp_x_C(inst_sp_x_C > -200 & inst_sp_x_C < 200);
        temp_inst_sp_x_C = inst_sp_x_C(inst_sp_y_C > -200 & inst_sp_y_C < 200);
        temp_inst_sp_y_C = inst_sp_y_C(inst_sp_x_C > -200 & inst_sp_x_C < 200);
        temp_inst_sp_y_C = inst_sp_y_C(inst_sp_y_C > -200 & inst_sp_y_C < 200);
        
        ndhist(temp_inst_sp_x_C(~isnan(temp_inst_sp_x_C)), temp_inst_sp_y_C(~isnan(temp_inst_sp_y_C)), 'bins',2, 'radial','axis',[-200 200 -200 200],'nr','filt');
        figure()
        ndhist(inst_sp_x_C, inst_sp_y_C, 'bins', 3, 'radial','axis',[-200 200 -200 200],'nr','filt');
%         title(controls{ii_c})
    end
   
    

end