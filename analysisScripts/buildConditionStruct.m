function dataStruct = buildConditionStruct(uEcc)

dataStruct(length(uEcc)) = struct(...;
    'thresh', NaN,...;
    'par', NaN,...;
    'threshB', NaN,...;
    'bootsMS', NaN(1,2),...;
    'logBootMS', NaN(1,2));

end
