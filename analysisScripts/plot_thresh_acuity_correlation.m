function plot_thresh_acuity_correlation(subjects,flag)
    machine = {'D'};
    figures = struct(...
                    'FIGURE_ON', 0,... %Show individual trial with traces in 2D space
                    'VIDEO', 0,... %Wills save video of FIGURE_ON figures
                    'FIXATION_ANALYSIS',0,... %Plots psychometric curves
                    'saveFixMat',1,...
                    'QUICK_ANALYSIS',0,... %Only plots psychometric curves
                    'CHECK_TRACES',0,... %will plot traces
                    'HUX_RUN',1,... %will discard trials differently
                    'FOLDED', 0,...
                    'FIXED_SIZE', 0,...
                    'COMPARE_SPANS',0); %0 = all spans, 1 = small spans, 2 = large spans

    params = struct(...
                    'patient',1,...
                    'hux',0,...
                    'nBoots', 1,... %Number of boots for psychometric fits
                    'crowded', false, ... %Gets rewritten based on currentCond
                    'DMS',true, ...
                    'D', false, ...
                    'MS', false,...
                    'fixation',false,...
                    'machine',machine, ... %DPI "A" vs dDPI "D"
                    'session', '1', ... %Which number session
                    'spanMax', 100,... %The Maximum Span
                    'spanMin', 0,...%The Minimum Span
                    'compSpans', 0,...
                    'ecc',0,...
                    'samplingRate' , 1000/341,...
                    'stabilization','Unstabilized');
   cP = jet(length(subjects));

   for ii_p = 1:length(subjects)
       if flag 
            params.samplingRate = 1000/1000;
            newEyeris = 1;
       else
           if params.patient
               if ii_p > 3
                   params.samplingRate = 1000/1000;
                   newEyeris = 1;
               else
                   params.samplingRate = 1000/341;
                   newEyeris = 0;
               end
           end
       end
       if params.hux == 1
            newEyeris = 0;
       else
           if ii_p == 1
                newEyeris = 0;
           else
               newEyeris = 1;
               params.samplingRate = 1000/1000;
           end
       end
       em = [];
       params.subject = subjects{ii_p};
       title_str = char(subjects{ii_p});
       if params.patient
           pathToFile =  strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\patients\',char(subjects{ii_p}),'\unCrowded');
       end
       if params.hux
           pathToFile =  strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\controls\HUX_C\',char(subjects{ii_p}),'\unCrowded');
       elseif params.hux == -1
            pathToFile =  strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\controls\',char(subjects{ii_p}),'\unCrowded');
       end
      
       load(fullfile(pathToFile,'pptrials.mat'), 'pptrials');
       for ii = 1:length(pptrials)
           if isfield(pptrials{ii}, 'pixelAngle')
               params.pixelAngle(ii) = pptrials{1,ii}.pixelAngle;
               pptrials{1,ii}.pxAngle = pptrials{1,ii}.pixelAngle;
            else
                params.pixelAngle(ii) = pptrials{1,ii}.pxAngle;
            end
       end
       trialChar = buildTrialCharStruct(pptrials,newEyeris);
       trialChar = add_fields_trialChar(trialChar,pptrials,params);
       if newEyeris
           [~, ~, valid, ~] = countingTrials_newEyeris(pptrials, em, params, figures);
       else
           [~, ~, valid, ~] = countingTrials(pptrials, em, params, figures);
       end
       condition = Psychometric_Graphing(params,params.ecc,...
                                  valid, trialChar, params.crowded, title_str);
       
       all_thresh_P(ii_p) = condition.thresh;
     
       
       
   end
   for ii_s = 1:length(subjects)
       if params.patient
           dir_path = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\patients\',subjects{ii_s},'\matFiles\');
           file_to_read = strcat('em_info_for_heatmaps_task_',subjects{ii_s},'.mat');
       end
       if params.hux
           dir_path = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\controls\HUX_C\',subjects{ii_s},'\matFiles\');
       else
           dir_path = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\controls\',subjects{ii_s},'\matFiles\');
       end
       file_to_read = strcat('em_info_for_heatmaps_',subjects{ii_s},'.mat');
     
       load(strcat(dir_path,file_to_read))
       avg_x = round(nanmean(em.allTraces.xALL),2);
       avg_y = round(nanmean(em.allTraces.yALL),2);
       s_e_locs = [avg_x,avg_y;0,0];
       euc_dist = pdist(s_e_locs,'euclidean');
       all_euc_dist(ii_s) = euc_dist;
       
   end
  
   
   figure()
   plot([all_thresh_P],[all_euc_dist],'o','MarkerSize',10,'MarkerFaceColor','k')
   xlabel('Acuity Threshold (arcmin)')
   ylabel('Gaze Offset (arcmin)')
   set(gca,'fontsize',27,'FontWeight','Bold')
%    xlim([2,4])
   ylim([0,13])
end