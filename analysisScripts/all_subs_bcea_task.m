function all_subs_bcea_task(patients,controls, HUX)
    
    cP = jet(length(patients));
    cC = winter(length(controls));
    chC = summer(length(controls));
    for ii_p = 1:length(patients)
        dir_path = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\patients\',patients{ii_p},'\matFiles\');
    
        
        file_to_read_avg = strcat('em_info_for_heatmaps_',patients{ii_p},'.mat');
        load(strcat(dir_path,file_to_read_avg))
        
        xT = em.allTraces.xALL;
        yT = em.allTraces.yALL;
        all_bcea_P(ii_p) = get_bcea(xT/60,yT/60);
        
        figure(3)
        plot([1],[all_bcea_P(ii_p)],'o','MarkerSize',10,'MarkerFaceColor','g')
        hold on
        
    end
    for ii_c = 1:length(controls)
        dir_path = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\controls\',controls{ii_c},'\matFiles\');
     
        file_to_read_avg = strcat('em_info_for_heatmaps_',controls{ii_c},'.mat');
        load(strcat(dir_path,file_to_read_avg))
        
        xT = em.allTraces.xALL;
        yT = em.allTraces.yALL;
        all_bcea_C(ii_c) = get_bcea(xT/60,yT/60);
        
        figure(3)
        plot([2],[all_bcea_C(ii_c)],'o','MarkerSize',10,'MarkerFaceColor','m')
        hold on
    end

    for ii_hc = 1:length(HUX)
        dir_path = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\controls\HUX_C',HUX{ii_hc},'\matFiles\');
     
        file_to_read_avg = strcat('em_info_for_heatmaps_',HUX{ii_hc},'.mat');
        load(strcat(dir_path,file_to_read_avg))
        
        xT = em.allTraces.xALL;
        yT = em.allTraces.yALL;
        all_bcea_hC(ii_hc) = get_bcea(xT/60,yT/60);
        
        figure(3)
        plot([3],[all_bcea_hC(ii_hc)],'o','MarkerSize',10,'MarkerFaceColor','b')
        hold on
    end
    
    
    figure(3)
    std_err_bcea_P = std(all_bcea_P)/sqrt(length(all_bcea_P));
    std_err_bcea_C = std(all_bcea_C)/sqrt(length(all_bcea_C));
    std_err_bcea_hC = std(all_bcea_hC)/sqrt(length(all_bcea_hC));
    errorbar([mean(all_bcea_P) mean(all_bcea_C(~isnan(all_bcea_C))), mean(all_bcea_hC(~isnan(all_bcea_hC)))],...
                  [std_err_bcea_P std_err_bcea_C std_err_bcea_hC],...
                   'o', 'Color', 'k', 'LineStyle', '-','LineWidth',3)
   set(gca, 'XTick', [1:1:3], 'XTickLabel', {'Patients'; 'Healthy controls'; 'HUX controls'}, 'FontSize', 12);
   xlim([0 4])
%    ylim([0 5])
   ylabel('bcea (deg^2)')
   set(gca,'fontsize',27,'FontWeight','Bold')
%    set(gca, 'YScale', 'log')
   

end
