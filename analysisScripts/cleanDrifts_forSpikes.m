


Fs = 341;
conversionFactor = 1000/341;
%% tune these peak params for individual subjects
MaxPeakWidth = 5;
MinPeakHeight = 0.8;
MinPeakDistance = 2;
%% Don't change anything beyond this point

%% clean up drift trials
for ti = 1:length(pptrials)
    timeOn = round(pptrials{ti}.TimeTargetON/(conversionFactor));
    timeOff = round(min(pptrials{ti}.TimeTargetOFF/(conversionFactor)-1, pptrials{ti}.ResponseTime/(conversionFactor)));   
    all_ds = length(find(pptrials{ti}.drifts.start >= timeOn & pptrials{ti}.drifts.start <= timeOff));
    if all_ds > 0
        pause = 0;
    end
%     for si = 1:length(pptrials{ti}.drifts.start)
%         driftStart = pptrials{ti}.drifts.start(si);
%         if pptrials{ti}.drifts.duration(si) == 0
%             continue
%         else
%             if driftStart == 0
%                 driftStart = 1;
%             end
%             driftEnd = pptrials{ti}.drifts.start(si) + pptrials{ti}.drifts.duration(si) - 1;
%             driftSegment_X = pptrials{ti}.x.position(driftStart:driftEnd);
%             driftSegment_Y = pptrials{ti}.y.position(driftStart:driftEnd);
%             temp = abs(diff(driftSegment_X));
% 
%             [~,peakidxX] = findpeaks(temp,'MaxPeakWidth',MaxPeakWidth, ...
%                 'MinPeakHeight',MinPeakHeight, ...
%                 'MinPeakDistance', MinPeakDistance);
%             %'Threshold',0.7,
%             temp= [0 temp];% append a value for equal length for plottng
% 
%             multiple_peak_index_start = [];
%             multiple_peak_index_end = [];
% 
%             figure();
%             s1 = subplot(2,1,1);
%             plot(temp);
%             hold on
%             plot(peakidxX,temp(peakidxX+1),'o')
%             title(sprintf('peaks, pptrial %i',ti));
%             s2 = subplot(2,1,2);
%             %[~,peakidxX] = findpeaks(abs(diff(X_FULL)),'MaxPeakWidth',3,'MinPeakHeight',5);
% 
%             plot(peakidxX,driftSegment_X(peakidxX),'o');hold on;
%             plot(driftSegment_X);
% 
%             title(sprintf('old trace, pptrial %i',ti));
%             linkaxes([s1 s2], 'x');
% 
% % check if all good, otherwise fix and then save to another
%             prompt = "Are all spikes detected? Y/N: ";
%             txt = input(prompt,"s");
%             if isempty(txt)
%                 txt = 'y';
%             end
%             if strcmp(txt,'y') % no need for fixing
%                 smoothedDriftSegment_X = driftSegment_X;
%                 smoothedDriftSegment_Y = driftSegment_Y;
%                 pptrials{ti}.smoothed_drifts{si}.x = smoothedDriftSegment_X;
%                 pptrials{ti}.smoothed_drifts{si}.y = smoothedDriftSegment_Y;
%             else
%             while strcmp(txt,'y') ~= 1
%      
%                 prompt = "Fix peak index [1] or add new one[2] or multiple samples[3] or remove peak[4]: ";
%                 txt2 = input(prompt);
%                 if txt2 == 1
%                     prompt = "which peak: ";
%                     pkIdx = input(prompt);
%                     prompt = "which direction [1] or [-1]";
%                     sign = input(prompt);
%                     peakidxX(pkIdx) =  peakidxX(pkIdx) + sign;
%                 elseif txt2 == 2
%                     prompt = "where is the peak";
%                     pk = input(prompt);
%                     peakidxX =  [peakidxX, pk];
%                     peakidxX = sort(peakidxX);
%                 elseif txt2 == 3
%                     % remove peak first
%                     prompt = "remove which peak: ";
%                     pkIdx = input(prompt);
%                     peakidxX(pkIdx) = nan;
%                     peakidxX = sort(peakidxX);
%                     peakidxX = peakidxX(~isnan(peakidxX));
%                     % then add new
%                     startIdx = input("start index of peak:");
%                     endIdx = input("end index of peak:");
%                     multiple_peak_index_start = [multiple_peak_index_start startIdx];
%                     multiple_peak_index_end = [multiple_peak_index_end endIdx];
%                 elseif txt2 == 4
%                     prompt = "remove which peak: ";
%                     pkIdx = input(prompt);
%                     peakidxX(pkIdx) = nan;
%                     peakidxX = sort(peakidxX);
%                     peakidxX = peakidxX(~isnan(peakidxX));
%                 end
%             figure();
%             s1 = subplot(2,1,1);
%             plot(temp);
%             hold on
%             plot(peakidxX,temp(peakidxX+1),'o')
%             title(sprintf('old trace, pptrial %i',ti));
% 
%             s2 = subplot(2,1,2);
%             %[~,peakidxX] = findpeaks(abs(diff(X_FULL)),'MaxPeakWidth',3,'MinPeakHeight',5);
% 
%             plot(peakidxX,driftSegment_X(peakidxX),'o');hold on;
%             
%             plot(driftSegment_X);
% 
% 
%             linkaxes([s1 s2], 'x');
%                 prompt = "Are all spikes detected? Y/N: ";
%                 txt = input(prompt,"s");
%                 if isempty(txt)
%                 txt = 'y';
%                 end
%             end
%             end
% 
%             
% 
%             % smooth the peak
%             smoothedDriftSegment_X = driftSegment_X;
%             smoothedDriftSegment_Y = driftSegment_Y;
%             for peaki = 1:length(peakidxX)
%                  
%                 idx = peakidxX(peaki);
%                     
%                 smoothedDriftSegment_X(idx) = (driftSegment_X(idx-1) +driftSegment_X(idx+1)) /2; %use average
%                 smoothedDriftSegment_Y(idx) = (driftSegment_Y(idx-1) +driftSegment_Y(idx+1)) /2; %use average
%             end
%             
%             for mi = 1:length(multiple_peak_index_start)
%                 % interpolation
%                 tmp = [multiple_peak_index_start(mi):multiple_peak_index_end(mi)]; % the array of the consecutive peaks
%                 x = [tmp(1) - 1, tmp(end) + 1]; % the index with good values
%                 driftX = [driftSegment_X(tmp(1) - 1), driftSegment_X(tmp(end) + 1)];
%                 driftY = [driftSegment_Y(tmp(1) - 1), driftSegment_Y(tmp(end) + 1)];
%                 x_interp = interp1(x, driftX, tmp, 'linear');
%                 y_interp = interp1(x, driftY, tmp, 'linear');
% 
%                 smoothedDriftSegment_X(tmp) = x_interp;
%                 smoothedDriftSegment_Y(tmp) = y_interp;
%             end
%             
% 
% 
%             % plot trace before and after smoothing
%             figure();
%             s1 = subplot(2,1,1);
%             plot(peakidxX,driftSegment_X(peakidxX),'o');hold on;
%             plot(driftSegment_X);
%             title(sprintf('old trace, pptrial %i',ti));
% 
%             s2 = subplot(2,1,2);
%             %[~,peakidxX] = findpeaks(abs(diff(X_FULL)),'MaxPeakWidth',3,'MinPeakHeight',5);
% 
%             plot(peakidxX,smoothedDriftSegment_X(peakidxX),'o');hold on;
%             plot(smoothedDriftSegment_X);
%             title(sprintf('cleaned trace, pptrial %i',ti));
% 
%             linkaxes([s1 s2], 'x');
%             pause();
% 
%             % if all looks good, save new drift
%             pptrials{ti}.smoothed_drifts{si}.x = smoothedDriftSegment_X;
%             pptrials{ti}.smoothed_drifts{si}.y = smoothedDriftSegment_Y;
% 
% 
%             
%         end
%     end
end
%% save pptrials
filepath = strcat('C:\Users\ruccilab\Documents\AO_processing\',subject,'_',date,'\allCombined\');
save(strcat(filepath,'pptrials_drift_filtered.mat'), 'pptrials', '-mat');

%% visualization to tune peak height
[~,peakidxX] = findpeaks(abs(diff(driftSegment_X)),'MaxPeakWidth',3,'MinPeakHeight',3.5);


figure();
plot(abs(diff(driftSegment_X)))
hold on;
plot(driftSegment_X)
plot(peakidxX,driftSegment_X(peakidxX),'o');hold on;
