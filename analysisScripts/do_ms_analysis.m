function [msRates,startTime,endTime,...
        startPos_x,startPos_y,...
        endPos_x,endPos_y] = do_ms_analysis(pptrials,num_ts,msRates,startTime,endTime,...
                                            startPos_x,startPos_y,...
                                            endPos_x,endPos_y,params)
    
%     valid_dms_ts = find(valid.dms == 1);
%     total_trials = length(valid_dms_ts);
    
%     for i = 1:length(total_trials)
    if params.newEyeris
        timeOn = 1;
        timeOff = length(pptrials{num_ts}.x.position);
    else
        if params.blankScreenAnalysis
            timeOn = round(pptrials{num_ts}.TimeTargetON/params.samplingRate) - 400/params.samplingRate;
            timeOff = timeOn + 400/params.samplingRate;
        else
             timeOn = round(pptrials{num_ts}.TimeTargetON/params.samplingRate);
             timeOff = floor(min(pptrials{num_ts}.TimeTargetOFF/params.samplingRate, pptrials{num_ts}.ResponseTime/params.samplingRate));
        end
            
    end
        findms = find(pptrials{num_ts}.microsaccades.start > timeOn & pptrials{num_ts}.microsaccades.start < timeOff);
        
        if ~isempty(findms)
            numEM = sum(pptrials{num_ts}.microsaccades.start > timeOn & pptrials{num_ts}.microsaccades.start < timeOff);
            msRates(1,end+1) = (numEM/timeOff)*1000;
            for ms_l = 1:length(findms)
                endTime_temp = pptrials{num_ts}.microsaccades.start(findms(ms_l)) + pptrials{num_ts}.microsaccades.duration(findms(ms_l));
                if (endTime_temp == length(pptrials{num_ts}.x.position) +1)
                    endTime_temp = length(pptrials{num_ts}.x.position);
                end
                if (endTime_temp > length(pptrials{num_ts}.y.position))
                    msRates(1,end+1) = 0;
                    startTime(1,end+1) = NaN;
                    endTime(1,end+1) = NaN;
                    startPos_x(1,end+1) = NaN;
                    startPos_y(1,end+1) = NaN;
                    endPos_x(1,end+1) = NaN;
                    endPos_y(1,end+1) = NaN;
                else    
                    if params.newEyeris
                        startTime(1,end+1) = pptrials{num_ts}.microsaccades.start(findms(ms_l));
                        endTime(1,end+1) = pptrials{num_ts}.microsaccades.start(findms(ms_l)) + pptrials{num_ts}.microsaccades.duration(findms(ms_l));
                    else
                        startTime(1,end+1) = (pptrials{num_ts}.microsaccades.start(findms(ms_l)) - timeOn) * params.samplingRate;
                        endTime(1,end+1) = ((pptrials{num_ts}.microsaccades.start(findms(ms_l)) + pptrials{num_ts}.microsaccades.duration(findms(ms_l))) - timeOff)* params.samplingRate;
                    end
                    sX = pptrials{num_ts}.x.position(pptrials{num_ts}.microsaccades.start(findms(ms_l)));
                   
                    sY = pptrials{num_ts}.y.position(pptrials{num_ts}.microsaccades.start(findms(ms_l)));
                   
                    eX = pptrials{num_ts}.x.position(endTime_temp);
                   
                    eY = pptrials{num_ts}.y.position(endTime_temp);
                    
                    
                    if (sX > 60 || sY > 60 || eX > 60 || eY > 60)
%                         msRates(1,end+1) = 0;
                        startTime(1,end+1) = NaN;
                        endTime(1,end+1) = NaN;
                        startPos_x(1,end+1) = NaN;
                        startPos_y(1,end+1) = NaN;
                        endPos_x(1,end+1) = NaN;
                        endPos_y(1,end+1) = NaN;
                    else
                         startPos_x(1,end+1) = sX;
                         startPos_y(1,end+1) = sY;
                         endPos_x(1,end+1) = eX;
                         endPos_y(1,end+1) = eY;
                    end
                end
            end
        else
            msRates(1,end+1) = 0;
            startTime(1,end+1) = NaN;
            endTime(1,end+1) = NaN;
            startPos_x(1,end+1) = NaN;
            startPos_y(1,end+1) = NaN;
            endPos_x(1,end+1) = NaN;
            endPos_y(1,end+1) = NaN;
        end
        
        
%     end
    
    
    
end