clc
clear all
close all

em = [];
newEyeris = 0;

machine = {'D'};

subject = {'PC-S01'};
condition = {'uncrowded'};

figures = struct(...
    'FIGURE_ON', 0,... %Show individual trial with traces in 2D space
    'VIDEO', 0,... %Wills save video of FIGURE_ON figures
    'FIXATION_ANALYSIS',0,... %Plots psychometric curves
    'saveFixMat',0,...
    'QUICK_ANALYSIS',0,... %Only plots psychometric curves
    'CHECK_TRACES',0,... %will plot traces
    'HUX_RUN',1,... %will discard trials differently
    'FOLDED', 0,...
    'FIXED_SIZE', 0,...
    'COMPARE_SPANS',0); %0 = all spans, 1 = small spans, 2 = large spans

params = struct(...
    'newEyeris',newEyeris,...
    'subject',subject,...
    'patient',1,...
    'nBoots', 1,... %Number of boots for psychometric fits
    'crowded', false, ... %Gets rewritten based on currentCond
    'blankScreenAnalysis',0,...
    'DMS',true, ...
    'D', false, ...
    'MS', false,...
    'fixation',false,...
    'machine',machine, ... %DPI "A" vs dDPI "D"
    'session', '1', ... %Which number session
    'spanMax', 100,... %The Maximum Span
    'spanMin', 0,...%The Minimum Span
    'compSpans', 0,...
    'ecc',0,...
    'samplingRate' , 1000/341,... 
    'stabilization','Unstabilized');

if params.patient
    if params.blankScreenAnalysis
        if newEyeris
            pathToFile = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\newEyeris_analysisScripts\patients\',char(subject),'\unCrowded\blankScreenData');
        else
            pathToFile =  strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\patients\',char(subject),'\unCrowded');
        end
    else
        pathToFile =  strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\patients\',char(subject),'\unCrowded');
    end
else
    pathToFile =  strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\controls\',char(subject),'\unCrowded');
end

if strcmp(subject{1}, 'PC-S16' )|| strcmp(subject{1}, 'PC-S07')
    if params.DMS
        load(fullfile(pathToFile,'task_clean.mat'), 'all_clean');
    elseif params.fixation
        load(fullfile(pathToFile,'fix_clean.mat'), 'all_clean');
    end
   
end
load(fullfile(pathToFile,'pptrials.mat'), 'pptrials');

for ii = 1:length(pptrials)
    if isfield(pptrials{ii}, 'pixelAngle')
        params.pixelAngle(ii) = pptrials{1,ii}.pixelAngle;
        pptrials{1,ii}.pxAngle = pptrials{1,ii}.pixelAngle;
    else
        params.pixelAngle(ii) = pptrials{1,ii}.pxAngle;
    end
    if newEyeris
        pptrials{ii}.x.velocity = p_calculateVelocity(pptrials{ii}.x.position, 'prefilter', true, ...
          'postfilter', true, 'postvalue', true, 'order', 3, 'smoothing', bitor( double(round(51/1000*pptrials{ii}.eye_spf)), 1)  ) * pptrials{ii}.eye_spf / 1000;
        pptrials{ii}.y.velocity = p_calculateVelocity(pptrials{ii}.y.position, 'prefilter', true, ...
          'postfilter', true, 'postvalue', true, 'order', 3, 'smoothing', bitor( double(round(51/1000*pptrials{ii}.eye_spf)), 1)  ) * pptrials{ii}.eye_spf / 1000;
    end
end
trialChar = buildTrialCharStruct(pptrials,newEyeris);
trialChar = add_fields_trialChar(trialChar,pptrials,params);



if newEyeris
    params.samplingRate = 1000/pptrials{1}.eye_spf;
    [traces, counter, valid, em] = countingTrials_newEyeris(pptrials, em, params, figures);
else
    %params.samplingRate = 1000/341;
    [traces, counter, valid, em] = countingTrials(pptrials, em, params, figures);
end

if strcmp(subject{1}, 'PC-S16') || strcmp(subject{1}, 'PC-S07')
    valid.dms = all_clean;
end

old_pptrials = pptrials;
clean_pptrials = {};

needed_ts = find(valid.dms == 1);
for all = 1:length(needed_ts)
   clean_pptrials{1,end+1} = pptrials{needed_ts(all)};
end

pptrials = clean_pptrials;