function all_sub_ms_analysis(patients,controls,HUX,tasktype)
    
    [all_rates_P,all_rates_C,all_rates_hC,...
     std_err_rate_P,std_err_rate_C,std_err_rate_hC] = deal([]);
    
    ampAndRate = 1;
    pPlot = 0;
    scatPlot = 0;
    dPlot = 0;
    [all_theta_p,all_theta_c] = deal([]);
    cP = jet(length(patients));
    cC = winter(length(controls));
    chC = winter(length(HUX));
    for ii_p = 1:length(patients)
        [x_all,y_all] = deal([]);
        dir_path = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\patients\',patients{ii_p},'\matFiles\');
        if strcmp(tasktype,'fix')
            file_to_read = strcat('ms_info_fix_500ms_',patients{ii_p},'.mat');
        else
            file_to_read = strcat('ms_info_task_',patients{ii_p},'.mat');
        end
        
        load(strcat(dir_path,file_to_read))
        
        all_rates_P(1,end+1) = mean(msInfo.msRates);
        title_str = patients{ii_p};
        [mean_amp_t,theta,amp_1,...
            mean_dist_diff_bn_s_e_ms_to_center,...
            mean_l_d_c,mean_s_d_c,...
            x_all,y_all] = calc_amp_theta(msInfo,tasktype,scatPlot,title_str,x_all,y_all);
        all_amp_P(ii_p) = mean_amp_t;
        all_mean_dist_diff_bn_s_e_ms_to_center_P(ii_p) = mean_dist_diff_bn_s_e_ms_to_center;
        mean_l_d_c_P(ii_p) = mean_l_d_c;
        mean_s_d_c_P(ii_p) = mean_s_d_c;
        
        msInfo.x_all = x_all;
        msInfo.y_all = y_all;
        if strcmp(tasktype,'fix')
             Fname = sprintf(strcat('ms_info_fix_500ms_vectors_',patients{ii_p},'.mat'));
        else
            Fname = sprintf(strcat('ms_info_task_vectors_',patients{ii_p},'.mat'));
        end
        pathToSub = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\patients\',patients{ii_p},'\','matFiles');
        if ~exist(pathToSub,'dir')
            mkdir(pathToSub);
        end

%         save(fullfile(pathToSub,Fname),'msInfo');
   
        if pPlot
            all_theta_p(1,end+1) = mean(theta);
            Fname = sprintf(strcat('all_theta_p','.mat'));
            pathToSub = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\patients\theta\');
            if ~exist(pathToSub,'dir')
                mkdir(pathToSub);
            end

            save(fullfile(pathToSub,Fname),'all_theta_p');
%             figure()
%             %Here -- take average of theta using circ_mean and then use
%             %that to plot the vector plots..[mu ul ll] = circ_mean([3.14,3.14,0,0])
%             polarplot(theta,amp_1,'.','MarkerSize',9)
%             rlim([0 30])
%             title(patients{ii_p})
%             pathToSave = 'C:/Users/sanjana/OneDrive - University of Rochester/Documents/Sanjana/Projects/schizophrenia/Figures/ms_polarplots_task';
%             
%             if ~exist(strcat(pathToSave), 'dir')
%             mkdir(strcat(pathToSave))
%             end
%             fullPath = strcat(pathToSave,'/%s');
%             strfName = patients{ii_p};
%             saveas(gcf,sprintf(fullPath, strfName), 'png')
        end
        
        clear msInfo;
        if ampAndRate
            figure(1)
            plot([1],[all_rates_P(ii_p)],'o','MarkerSize',10,'MarkerFaceColor',cP(ii_p,:))
            hold on
            figure(2)
            plot([1],[all_amp_P(ii_p)],'o','MarkerSize',10,'MarkerFaceColor',cP(ii_p,:))
            hold on
            
        end
        if dPlot
            figure(1)
            plot([1],[all_mean_dist_diff_bn_s_e_ms_to_center_P(ii_p)],'o','MarkerSize',10,'MarkerFaceColor',cP(ii_p,:))
            hold on
            figure(2)
            plot([1],mean_l_d_c_P(ii_p),'o','MarkerSize',10,'MarkerFaceColor',cP(ii_p,:))
            hold on
            figure(3)
            plot([1],mean_s_d_c_P(ii_p),'o','MarkerSize',10,'MarkerFaceColor',cP(ii_p,:))
            hold on
        end
    end
     for ii_c = 1:length(controls)
         [x_all,y_all] = deal([]);
         if ii_c > 10
            dir_path = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\controls\',controls{ii_c},'\matFiles\');
         else
             dir_path = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\controls\HUX_C\',controls{ii_c},'\matFiles\');
         end
        if strcmp(tasktype,'fix')
            file_to_read = strcat('ms_info_fix_500ms_',controls{ii_c},'.mat');
        else
            file_to_read = strcat('ms_info_task_',controls{ii_c},'.mat');
        end
        load(strcat(dir_path,file_to_read))
        
        all_rates_C(1,end+1) = mean(msInfo.msRates);
        title_str = controls{ii_c};
        [mean_amp_t,theta,amp_1,...
            mean_dist_diff_bn_s_e_ms_to_center,...
            mean_l_d_c,mean_s_d_c,...
            x_all,y_all] = calc_amp_theta(msInfo,tasktype,scatPlot,title_str,x_all,y_all);
        mean_s_d_c_C(ii_c) = mean_s_d_c;
        mean_l_d_c_C(ii_c) = mean_l_d_c;
        all_amp_C(ii_c) = mean(mean_amp_t);
        all_mean_dist_diff_bn_s_e_ms_to_center_C(ii_c) = mean_dist_diff_bn_s_e_ms_to_center;
        
        pathToSub = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\controls\',controls{ii_c},'\','matFiles');
        if ~exist(pathToSub,'dir')
            mkdir(pathToSub);
        end

        msInfo.x_all = x_all;
        msInfo.y_all = y_all;
        if strcmp(tasktype,'fix')
             Fname = sprintf(strcat('ms_info_fix_vectors_',controls{ii_c},'.mat'));
        else
            Fname = sprintf(strcat('ms_info_task_vectors_',controls{ii_c},'.mat'));
        end
%         save(fullfile(pathToSub,Fname),'msInfo');
        if pPlot
            all_theta_c(1,end+1) = mean(theta);
            Fname = sprintf(strcat('all_theta_c','.mat'));
            pathToSub = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\controls\theta\');
            if ~exist(pathToSub,'dir')
                mkdir(pathToSub);
            end

            save(fullfile(pathToSub,Fname),'all_theta_c');
%             figure()
%             polarplot(theta,amp_1,'.','MarkerSize',9)
%             rlim([0 30])
%             title(controls{ii_c})
        end
        
        clear msInfo;
        if ampAndRate
            figure(1)
            plot([2],[all_rates_C(ii_c)],'o','MarkerSize',10,'MarkerFaceColor',cC(ii_c,:))
            hold on
            figure(2)
            plot([2],[all_amp_C(ii_c)],'o','MarkerSize',10,'MarkerFaceColor',cC(ii_c,:))
            hold on
        end
         if dPlot
            figure(1)
            plot([2],[all_mean_dist_diff_bn_s_e_ms_to_center_C(ii_c)],'o','MarkerSize',10,'MarkerFaceColor',cC(ii_c,:))
            hold on
            figure(2)
            plot([2],mean_l_d_c_C(ii_c),'o','MarkerSize',10,'MarkerFaceColor',cC(ii_c,:))
            hold on
            figure(3)
            plot([2],mean_s_d_c_C(ii_c),'o','MarkerSize',10,'MarkerFaceColor',cC(ii_c,:))
            hold on
         end
     end
    for ii_hc = 1:length(HUX)
         [x_all,y_all] = deal([]);
        dir_path = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\controls\HUX_C\',HUX{ii_hc},'\matFiles\');
        if strcmp(tasktype,'fix')
            file_to_read = strcat('ms_info_fix_500ms_',HUX{ii_hc},'.mat');
        else
            file_to_read = strcat('ms_info_task_',HUX{ii_hc},'.mat');
        end
        load(strcat(dir_path,file_to_read))
        
        all_rates_hC(1,end+1) = mean(msInfo.msRates);
        title_str = HUX{ii_hc};
        [mean_amp_t,theta,amp_1,...
            mean_dist_diff_bn_s_e_ms_to_center,...
            mean_l_d_c,mean_s_d_c,...
            x_all,y_all] = calc_amp_theta(msInfo,tasktype,scatPlot,title_str,x_all,y_all);
        mean_s_d_hc_C(ii_hc) = mean_s_d_c;
        mean_l_d_hc_C(ii_hc) = mean_l_d_c;
        all_amp_hC(ii_hc) = mean(mean_amp_t);
        all_mean_dist_diff_bn_s_e_ms_to_center_hC(ii_hc) = mean_dist_diff_bn_s_e_ms_to_center;
        
        pathToSub = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\controls\HUX_C\',HUX{ii_hc},'\','matFiles');
        if ~exist(pathToSub,'dir')
            mkdir(pathToSub);
        end

        msInfo.x_all = x_all;
        msInfo.y_all = y_all;
        if strcmp(tasktype,'fix')
             Fname = sprintf(strcat('ms_info_fix_vectors_',HUX{ii_hc},'.mat'));
        else
            Fname = sprintf(strcat('ms_info_task_vectors_',HUX{ii_hc},'.mat'));
        end
        save(fullfile(pathToSub,Fname),'msInfo');
        if pPlot
            all_theta_c(1,end+1) = mean(theta);
            Fname = sprintf(strcat('all_theta_c','.mat'));
            pathToSub = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\controls\theta\');
            if ~exist(pathToSub,'dir')
                mkdir(pathToSub);
            end

            save(fullfile(pathToSub,Fname),'all_theta_c');
%             figure()
%             polarplot(theta,amp_1,'.','MarkerSize',9)
%             rlim([0 30])
%             title(controls{ii_c})
        end
        
        clear msInfo;
        if ampAndRate
            figure(1)
            plot([3],[all_rates_hC(ii_hc)],'o','MarkerSize',10,'MarkerFaceColor',chC(ii_hc,:))
            hold on
            figure(2)
            plot([3],[all_amp_hC(ii_hc)],'o','MarkerSize',10,'MarkerFaceColor',chC(ii_hc,:))
            hold on
        end
         if dPlot
            figure(1)
            plot([2],[all_mean_dist_diff_bn_s_e_ms_to_center_C(ii_c)],'o','MarkerSize',10,'MarkerFaceColor',cC(ii_c,:))
            hold on
            figure(2)
            plot([2],mean_l_d_c_C(ii_c),'o','MarkerSize',10,'MarkerFaceColor',cC(ii_c,:))
            hold on
            figure(3)
            plot([2],mean_s_d_c_C(ii_c),'o','MarkerSize',10,'MarkerFaceColor',cC(ii_c,:))
            hold on
         end
     end
    if dPlot
        figure(1)
        std_err_diff_P = std(all_mean_dist_diff_bn_s_e_ms_to_center_P)/sqrt(length(all_mean_dist_diff_bn_s_e_ms_to_center_P));
        std_err_diff_C = std(all_mean_dist_diff_bn_s_e_ms_to_center_C)/sqrt(length(all_mean_dist_diff_bn_s_e_ms_to_center_C));
        errorbar([mean(all_mean_dist_diff_bn_s_e_ms_to_center_P) mean(all_mean_dist_diff_bn_s_e_ms_to_center_C)],...
                  [std_err_diff_P std_err_diff_C],...
                  'o', 'Color', 'k', 'LineStyle', '-','LineWidth',3)
        set(gca, 'XTick', [1:1:2], 'XTickLabel', {'Patients'; 'Controls'}, 'FontSize', 12);
        ylabel('Diff in distances')
        set(gca,'fontsize',27,'FontWeight','Bold')
        xlim([0 3])
        
        figure(2)
        std_err_l_diff_P = std(mean_l_d_c_P)/sqrt(length(mean_l_d_c_P));
        std_err_l_diff_C = std(mean_l_d_c_C)/sqrt(length(mean_l_d_c_C));
        errorbar([mean(mean_l_d_c_P) mean(mean_l_d_c_C)],...
                  [std_err_l_diff_P std_err_l_diff_C],...
                  'o', 'Color', 'k', 'LineStyle', '-','LineWidth',3)
        set(gca, 'XTick', [1:1:2], 'XTickLabel', {'Patients'; 'Controls'}, 'FontSize', 12);
        ylabel('Landing distance from center')
        set(gca,'fontsize',27,'FontWeight','Bold')
        xlim([0 3])
        
        figure(3)
        std_err_s_diff_P = std(mean_s_d_c_P)/sqrt(length(mean_s_d_c_P));
        std_err_s_diff_C = std(mean_s_d_c_C)/sqrt(length(mean_s_d_c_C));
        errorbar([mean(mean_s_d_c_P) mean(mean_s_d_c_C)],...
                  [std_err_s_diff_P std_err_s_diff_C],...
                  'o', 'Color', 'k', 'LineStyle', '-','LineWidth',3)
        set(gca, 'XTick', [1:1:2], 'XTickLabel', {'Patients'; 'Controls'}, 'FontSize', 12);
        ylabel('Starting distance from center')
        set(gca,'fontsize',27,'FontWeight','Bold')
        xlim([0 3])
    end
    if ampAndRate
        figure(1)
        hold on
        std_err_rate_P = std(all_rates_P)/sqrt(length(all_rates_P));
        std_err_rate_C = std(all_rates_C)/sqrt(length(all_rates_C));
        std_err_rate_hC = std(all_rates_hC)/sqrt(length(all_rates_hC));
        errorbar([mean(all_rates_P) mean(all_rates_C) mean(all_rates_hC)],...
                  [std_err_rate_P std_err_rate_C std_err_rate_hC],...
                  'o', 'Color', 'k', 'LineStyle', '-','LineWidth',3)
        set(gca, 'XTick', [1:1:3], 'XTickLabel', {'Patients'; 'Healthy controls'; 'Hux controls'}, 'FontSize', 12);
        ylabel('microsaccade rate(num ms/s)')
        set(gca,'fontsize',15,'FontWeight','Bold')
        xlim([0 3])
         errorbar([mean(all_rates_P) mean(all_rates_C)],...
                  [std_err_rate_P std_err_rate_C ],...
                  'o', 'Color', 'k', 'LineStyle', '-','LineWidth',3)
        set(gca, 'XTick', [1:1:2], 'XTickLabel', {'Patients'; 'Healthy controls'}, 'FontSize', 12);
    %     saveas(gcf,'C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\Figures\msPlots\msRate_task_p_c.png')
    %     [p_r h_r stats] = ranksum(all_rates_P,all_rates_C);
    %     if h_r == 1
    %         text(1.5, mean(all_rates_P)+1, '*', 'FontSize', 45,'FontWeight','bold') %0.94
    %     end

        figure(2)
        hold on
        std_err_amp_P = std(all_amp_P)/sqrt(length(all_amp_P));
        std_err_amp_C = std(all_amp_C)/sqrt(length(all_amp_C));
        std_err_amp_hC = std(all_amp_hC)/sqrt(length(all_amp_hC));
        errorbar([mean(all_amp_P) mean(all_amp_C) mean(all_amp_hC)],...
                  [std_err_amp_P std_err_amp_C std_err_amp_hC],...
                  'o', 'Color', 'k', 'LineStyle', '-','LineWidth',3)
        set(gca, 'XTick', [1:1:3], 'XTickLabel', {'Patients'; 'Healthy controls'; 'Hux controls'}, 'FontSize', 12);
        ylabel('microsaccade amplitude')
        set(gca,'fontsize',27,'FontWeight','Bold')
        xlim([0 4])
        errorbar([mean(all_amp_P) mean(all_amp_C) ],...
                  [std_err_amp_P std_err_amp_C ],...
                  'o', 'Color', 'k', 'LineStyle', '-','LineWidth',3)
        set(gca, 'XTick', [1:1:2], 'XTickLabel', {'Patients'; 'Healthy controls'}, 'FontSize', 12);
        ylabel('microsaccade amplitude [arcmin]')
        set(gca,'fontsize',15,'FontWeight','Bold')
        xlim([0 3])
%         saveas(gcf,'C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\Figures\msPlots\msAmp_task_p_c.png')
    %     [p_amp h_amp stats] = ranksum(all_amp_P,all_amp_C);
    %     if h_amp == 1
    %         text(1.5, mean(all_amp_P)+1, '*', 'FontSize', 45,'FontWeight','bold') %0.94
    %     end
    end
end

