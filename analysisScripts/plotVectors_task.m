function plotVectors_task(patients,controls)
    
    cP = jet(length(patients));
    cC = winter(length(controls));
    fig = 0;
    for ii_p = 1:length(patients)
        statsData_p(ii_p).name = patients{ii_p};
        [inst_sp_x_P,inst_sp_y_P,raw_x_P,raw_y_P] = deal([]);
        dir_path = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\patients\',patients{ii_p},'\matFiles\');
        file_to_read = strcat('em_info_for_drift_bias_task_',patients{ii_p},'.mat');
        load(strcat(dir_path,file_to_read))
        ln_em = numel(fieldnames(em.ecc_0));
        for t_size = 1:ln_em
            f_name = fieldnames(em.ecc_0);

            all_chars = eval(strcat('em.ecc_0.',f_name{t_size}));
            if ~isfield(all_chars,'instSpX')
                continue
            else
                for all = 1:length(all_chars.instSpX)
                    x = all_chars.instSpX{all};
                    y = all_chars.instSpY{all};
                    inst_sp_x_P = [inst_sp_x_P,x];
                    inst_sp_y_P = [inst_sp_y_P,y];
                    
                    rawX_P = all_chars.x_d_bias{all};
                    rawY_P = all_chars.y_d_bias{all};
                    raw_x_P = [raw_x_P,rawX_P];
                    raw_y_P = [raw_y_P,rawY_P];
                end
            end

        end
        clear em;
        gazeX = mean(raw_x_P(~isnan(raw_x_P)));
        gazeY = mean(raw_y_P(~isnan(raw_y_P)));
        
        [vecGaze] = createUnitVector([gazeX,gazeY]);
        
        statsData_p(ii_p).gazeX = vecGaze(1,1);
        statsData_p(ii_p).gazeY = vecGaze(1,2);
        
        velX = mean(inst_sp_x_P(~isnan(inst_sp_x_P)));
        velY = mean(inst_sp_y_P(~isnan(inst_sp_y_P)));
        
        [vecVel] = createUnitVector([velX,velY]);
        statsData_p(ii_p).velX = vecVel(1,1);
        statsData_p(ii_p).velY = vecVel(1,2);
        
        file_to_read = strcat('ms_info_task_vectors_',patients{ii_p},'.mat');
        load(strcat(dir_path,file_to_read))
        
        x_ms = mean(msInfo.x_all);
        y_ms = mean(msInfo.y_all);
        
        [vecMS] = createUnitVector([x_ms,y_ms]);
        
        statsData_p(ii_p).msX = vecMS(1,1);
        statsData_p(ii_p).msY = vecMS(1,2);
        
        Fname = sprintf(strcat('statsData_p','.mat'));
        pathToSub = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\patients\theta\');
        if ~exist(pathToSub,'dir')
            mkdir(pathToSub);
        end
        
        save(fullfile(pathToSub,Fname),'statsData_p');
        
        
        
        if fig
            plots(vecGaze,vecVel,vecMS,patients{ii_p})
        end
    end
    
    for ii_c = 1:length(controls)
        statsData_c(ii_c).name = controls{ii_c};
        [inst_sp_x_C,inst_sp_y_C,raw_x_C,raw_y_C] = deal([]);
        dir_path = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\controls\',controls{ii_c},'\matFiles\');
        file_to_read = strcat('em_info_for_drift_bias_task_',controls{ii_c},'.mat');
        load(strcat(dir_path,file_to_read))
        ln_em = numel(fieldnames(em.ecc_0));
        for t_size = 1:ln_em
            f_name = fieldnames(em.ecc_0);

            all_chars = eval(strcat('em.ecc_0.',f_name{t_size}));
            if ~isfield(all_chars,'instSpX')
                continue
            else
                for all = 1:length(all_chars.instSpX)
                    x = all_chars.instSpX{all};
                    y = all_chars.instSpY{all};
                    inst_sp_x_C = [inst_sp_x_C,x];
                    inst_sp_y_C = [inst_sp_y_C,y];
                    
                    rawX_C = all_chars.x_d_bias{all};
                    rawY_C = all_chars.y_d_bias{all};
                    raw_x_C = [raw_x_C,rawX_C];
                    raw_y_C = [raw_y_C,rawY_C];
                end
            end

        end
        clear em;
        gazeX = mean(raw_x_C(~isnan(raw_x_C)));
        gazeY = mean(raw_y_C(~isnan(raw_y_C)));
        
        [vecGaze] = createUnitVector([gazeX,gazeY]);
        statsData_c(ii_c).gazeX = vecGaze(1,1);
        statsData_c(ii_c).gazeY = vecGaze(1,2);
       
        
        velX = mean(inst_sp_x_C(~isnan(inst_sp_x_C)));
        velY = mean(inst_sp_y_C(~isnan(inst_sp_y_C)));
        
        [vecVel] = createUnitVector([velX,velY]);
        statsData_c(ii_c).velX = vecVel(1,1);
        statsData_c(ii_c).velY = vecVel(1,2);
        
        
        file_to_read = strcat('ms_info_task_vectors_',controls{ii_c},'.mat');
        load(strcat(dir_path,file_to_read))
        
        x_ms = mean(msInfo.x_all);
        y_ms = mean(msInfo.y_all);
        
        [vecMS] = createUnitVector([x_ms,y_ms]);
        
        statsData_c(ii_c).msX = vecMS(1,1);
        statsData_c(ii_c).msY = vecMS(1,2);
        
        Fname = sprintf(strcat('statsData_c','.mat'));
        pathToSub = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\controls\theta\');
        if ~exist(pathToSub,'dir')
            mkdir(pathToSub);
        end
        
       save(fullfile(pathToSub,Fname),'statsData_c');
       
       if fig
           plots(vecGaze,vecVel,vecMS,controls{ii_c})
       end
    end
end

function plots(vecGaze,vecVel,vecMS,subject)
    
    h1 = plot([0,vecGaze(1,1)],[0,vecGaze(1,2)],'r');
    hold on
    
    p1 = [0 0];                         % First Point
    p2 = [vecGaze(1,1) vecGaze(1,2)];                         % Second Point
    dp = p2-p1;                         % Difference
    quiver(p1(1),p1(2),dp(1),dp(2),0,'color','r','Linewidth',2)
    text(p1(1),p1(2), sprintf('(%.2f,%.2f)',p1))
    text(double(p2(1)),double(p2(2)), sprintf('(%.2f,%.2f)',p2))
    
    hold on
    
    h2 = plot([0,vecVel(1,1)],[0,vecVel(1,2)],'b');
    hold on
    p1 = [0 0];                         % First Point
    p2 = [vecVel(1,1) vecVel(1,2)];                         % Second Point
    dp = p2-p1;                         % Difference
    quiver(p1(1),p1(2),dp(1),dp(2),0,'color','b','Linewidth',2)
    text(p1(1),p1(2), sprintf('(%.2f,%.2f)',p1))
    text(double(p2(1)),double(p2(2)), sprintf('(%.2f,%.2f)',p2))
    hold on
    
    h3 = plot([0,vecMS(1,1)],[0,vecMS(1,2)],'k');
    hold on
    
    p1 = [0 0];                         % First Point
    p2 = [vecMS(1,1) vecMS(1,2)];                         % Second Point
    dp = p2-p1;                         % Difference
    quiver(p1(1),p1(2),dp(1),dp(2),0,'color','k','Linewidth',2)
    text(p1(1),p1(2), sprintf('(%.2f,%.2f)',p1))
    text(double(p2(1)),double(p2(2)), sprintf('(%.2f,%.2f)',p2))
    xlim([-1 1])
    ylim([-1 1])
    legend([h1(1), h2(1), h3(1)],'Avg gaze vector','Avg velocity vector','Avg ms vector')
    title(patient)
    pathToSave = 'C:/Users/sanjana/OneDrive - University of Rochester/Documents/Sanjana/Projects/schizophrenia/Figures/vectorPlots/patients';
    
    if ~exist(strcat(pathToSave), 'dir')
        mkdir(strcat(pathToSave))
    end
    fullPath = strcat(pathToSave,'/%s');
    strfName = patients{ii_p};
    saveas(gcf,sprintf(fullPath, strfName), 'png')
    
end

 
