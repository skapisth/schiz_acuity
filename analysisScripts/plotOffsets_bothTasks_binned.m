function plotOffsets_bothTasks_binned(patients,controls,HUX)
    
    for ii_p = 1:length(patients)
        dir_path = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\patients\',patients{ii_p},'\matFiles\');
        file_to_read = strcat('em_info_for_heatmaps_task_',patients{ii_p},'.mat');
        load(strcat(dir_path,file_to_read))
        avg_x_b1 = round(nanmean(em.allTraces.xBinned.bin1),2);
        avg_y_b1 = round(nanmean(em.allTraces.yBinned.bin1),2);
        s_e_locs_b1 = [avg_x_b1,avg_y_b1;0,0];
        euc_dist_b1 = pdist(s_e_locs_b1,'euclidean');
        euc_off_p_b1(ii_p) = euc_dist_b1;
        
        avg_x_b2 = round(nanmean(em.allTraces.xBinned.bin2),2);
        avg_y_b2 = round(nanmean(em.allTraces.yBinned.bin2),2);
        s_e_locs_b2 = [avg_x_b2,avg_y_b2;0,0];
        euc_dist_b2 = pdist(s_e_locs_b2,'euclidean');
        euc_off_p_b2(ii_p) = euc_dist_b2;
        
        avg_x_b3 = round(nanmean(em.allTraces.xBinned.bin3),2);
        avg_y_b3 = round(nanmean(em.allTraces.yBinned.bin3),2);
        s_e_locs_b3 = [avg_x_b3,avg_y_b3;0,0];
        euc_dist_b3 = pdist(s_e_locs_b3,'euclidean');
        euc_off_p_b3(ii_p) = euc_dist_b3;
        
        avg_x_b4 = round(nanmean(em.allTraces.xBinned.bin4),2);
        avg_y_b4 = round(nanmean(em.allTraces.yBinned.bin4),2);
        s_e_locs_b4 = [avg_x_b4,avg_y_b4;0,0];
        euc_dist_b4 = pdist(s_e_locs_b4,'euclidean');
        euc_off_p_b4(ii_p) = euc_dist_b4;
        
        avg_x_b5 = round(nanmean(em.allTraces.xBinned.bin5),2);
        avg_y_b5 = round(nanmean(em.allTraces.yBinned.bin5),2);
        s_e_locs_b5 = [avg_x_b5,avg_y_b5;0,0];
        euc_dist_b5 = pdist(s_e_locs_b5,'euclidean');
        euc_off_p_b5(ii_p) = euc_dist_b5;
        
        clear em;
%         figure(1)
%         plot([1,2,3,4,5],[euc_off_p_b1(ii_p),euc_off_p_b2(ii_p),...
%             euc_off_p_b3(ii_p),euc_off_p_b4(ii_p),euc_off_p_b5(ii_p)],'-o','color','g')
%         hold on
%         
        file_to_read_BSA = strcat('em_info_for_BSA_heatmaps_',patients{ii_p},'.mat');
        load(strcat(dir_path,file_to_read_BSA))
        avg_x_BSA_b1 = round(nanmean(em.allTraces.xBinned.bin1),2);
        avg_y_BSA_b1 = round(nanmean(em.allTraces.yBinned.bin1),2);
        s_e_locs_b1 = [avg_x_BSA_b1,avg_y_BSA_b1;0,0];
        euc_dist_b1 = pdist(s_e_locs_b1,'euclidean');
        euc_off_BSA_p_b1(ii_p) = euc_dist_b1;
        
        avg_x_BSA_b2 = round(nanmean(em.allTraces.xBinned.bin2),2);
        avg_y_BSA_b2 = round(nanmean(em.allTraces.yBinned.bin2),2);
        s_e_locs_b2 = [avg_x_BSA_b2,avg_y_BSA_b2;0,0];
        euc_dist_b2 = pdist(s_e_locs_b2,'euclidean');
        euc_off_BSA_p_b2(ii_p) = euc_dist_b2;
        
        avg_x_BSA_b3 = round(nanmean(em.allTraces.xBinned.bin3),2);
        avg_y_BSA_b3 = round(nanmean(em.allTraces.yBinned.bin3),2);
        s_e_locs_b3 = [avg_x_BSA_b3,avg_y_BSA_b3;0,0];
        euc_dist_b3 = pdist(s_e_locs_b3,'euclidean');
        euc_off_BSA_p_b3(ii_p) = euc_dist_b3;
        
        avg_x_BSA_b4 = round(nanmean(em.allTraces.xBinned.bin4),2);
        avg_y_BSA_b4 = round(nanmean(em.allTraces.yBinned.bin4),2);
        s_e_locs_b4 = [avg_x_BSA_b4,avg_y_BSA_b4;0,0];
        euc_dist_b4 = pdist(s_e_locs_b4,'euclidean');
        euc_off_BSA_p_b4(ii_p) = euc_dist_b4;
        
%         avg_x_b5 = round(nanmean(em.allTraces.xBinned.bin5),2);
%         avg_y_b5 = round(nanmean(em.allTraces.yBinned.bin5),2);
%         s_e_locs_b5 = [avg_x_b5,avg_y_b5;0,0];
%         euc_dist_b5 = pdist(s_e_locs_b5,'euclidean');
%         euc_off_p_b5(ii_p) = euc_dist_b5;
%         
        clear em;
%         figure(1)
%         plot([1,2,3,4,5,6,7,8,9],...
%             [euc_off_BSA_p_b1(ii_p),euc_off_BSA_p_b2(ii_p),...
%             euc_off_BSA_p_b3(ii_p),euc_off_BSA_p_b4(ii_p),...
%             euc_off_p_b1(ii_p),euc_off_p_b2(ii_p),...
%             euc_off_p_b3(ii_p),euc_off_p_b4(ii_p),...
%             euc_off_p_b5(ii_p)],'-o','color','g')

%         plot([1,2,3,4,5],[euc_off_task_p_b1(ii_p),euc_off_task_p_b2(ii_p),...
%             euc_off_task_p_b3(ii_p),euc_off_task_p_b4(ii_p),euc_off_task_p_b5(ii_p)],'-o','color','g')
        hold on
        
    end
    for ii_c = 1:length(controls)
        if ii_c < 11
            dir_path = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\controls\HUX_C\',controls{ii_c},'\matFiles\');
         file_to_read = strcat('em_info_for_heatmaps_',controls{ii_c},'.mat');
        else
            dir_path = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\controls\',controls{ii_c},'\matFiles\');
         file_to_read = strcat('em_info_for_heatmaps_task_',controls{ii_c},'.mat');
        end
       
        load(strcat(dir_path,file_to_read))
        avg_x_b1 = round(nanmean(em.allTraces.xBinned.bin1),2);
        avg_y_b1 = round(nanmean(em.allTraces.yBinned.bin1),2);
        s_e_locs_b1 = [avg_x_b1,avg_y_b1;0,0];
        euc_dist_b1 = pdist(s_e_locs_b1,'euclidean');
        euc_off_c_b1(ii_c) = euc_dist_b1;
        
        avg_x_b2 = round(nanmean(em.allTraces.xBinned.bin2),2);
        avg_y_b2 = round(nanmean(em.allTraces.yBinned.bin2),2);
        s_e_locs_b2 = [avg_x_b2,avg_y_b2;0,0];
        euc_dist_b2 = pdist(s_e_locs_b2,'euclidean');
        euc_off_c_b2(ii_c) = euc_dist_b2;
        
        avg_x_b3 = round(nanmean(em.allTraces.xBinned.bin3),2);
        avg_y_b3 = round(nanmean(em.allTraces.yBinned.bin3),2);
        s_e_locs_b3 = [avg_x_b3,avg_y_b3;0,0];
        euc_dist_b3 = pdist(s_e_locs_b3,'euclidean');
        euc_off_c_b3(ii_c) = euc_dist_b3;
        
        avg_x_b4 = round(nanmean(em.allTraces.xBinned.bin4),2);
        avg_y_b4 = round(nanmean(em.allTraces.yBinned.bin4),2);
        s_e_locs_b4 = [avg_x_b4,avg_y_b4;0,0];
        euc_dist_b4 = pdist(s_e_locs_b4,'euclidean');
        euc_off_c_b4(ii_c) = euc_dist_b4;
        
        avg_x_b5 = round(nanmean(em.allTraces.xBinned.bin5),2);
        avg_y_b5 = round(nanmean(em.allTraces.yBinned.bin5),2);
        s_e_locs_b5 = [avg_x_b5,avg_y_b5;0,0];
        euc_dist_b5 = pdist(s_e_locs_b5,'euclidean');
        euc_off_c_b5(ii_c) = euc_dist_b5;
        
        clear em;
%         figure(1)
%         plot([1,2,3,4,5],[euc_off_c_b1(ii_c),euc_off_c_b2(ii_c),...
%             euc_off_c_b3(ii_c),euc_off_c_b4(ii_c),euc_off_c_b5(ii_c)],'o','color','m')
%         hold on
        
        
        file_to_read_BSA = strcat('em_info_for_BSA_heatmaps_',controls{ii_c},'.mat');
        load(strcat(dir_path,file_to_read_BSA))
        avg_x_BSA_b1 = round(nanmean(em.allTraces.xBinned.bin1),2);
        avg_y_BSA_b1 = round(nanmean(em.allTraces.yBinned.bin1),2);
        s_e_locs_b1 = [avg_x_BSA_b1,avg_y_BSA_b1;0,0];
        euc_dist_b1 = pdist(s_e_locs_b1,'euclidean');
        euc_off_BSA_c_b1(ii_c) = euc_dist_b1;
        
        avg_x_BSA_b2 = round(nanmean(em.allTraces.xBinned.bin2),2);
        avg_y_BSA_b2 = round(nanmean(em.allTraces.yBinned.bin2),2);
        s_e_locs_b2 = [avg_x_BSA_b2,avg_y_BSA_b2;0,0];
        euc_dist_b2 = pdist(s_e_locs_b2,'euclidean');
        euc_off_BSA_c_b2(ii_c) = euc_dist_b2;
        
        avg_x_BSA_b3 = round(nanmean(em.allTraces.xBinned.bin3),2);
        avg_y_BSA_b3 = round(nanmean(em.allTraces.yBinned.bin3),2);
        s_e_locs_b3 = [avg_x_b3,avg_y_BSA_b3;0,0];
        euc_dist_b3 = pdist(s_e_locs_b3,'euclidean');
        euc_off_BSA_c_b3(ii_c) = euc_dist_b3;
        
        avg_x_BSA_b4 = round(nanmean(em.allTraces.xBinned.bin4),2);
        avg_y_BSA_b4 = round(nanmean(em.allTraces.yBinned.bin4),2);
        s_e_locs_b4 = [avg_x_BSA_b4,avg_y_BSA_b4;0,0];
        euc_dist_b4 = pdist(s_e_locs_b4,'euclidean');
        euc_off_BSA_c_b4(ii_c) = euc_dist_b4;
        
%         avg_x_b5 = round(nanmean(em.allTraces.xBinned.bin5),2);
%         avg_y_b5 = round(nanmean(em.allTraces.yBinned.bin5),2);
%         s_e_locs_b5 = [avg_x_b5,avg_y_b5;0,0];
%         euc_dist_b5 = pdist(s_e_locs_b5,'euclidean');
%         euc_off_c_b5(ii_c) = euc_dist_b5;
%         
        clear em;
%         figure(1)
%         plot([1,2,3,4,5,6,7,8,9],...
%             [euc_off_BSA_c_b1(ii_c),euc_off_BSA_c_b2(ii_c),...
%             euc_off_BSA_c_b3(ii_c),euc_off_BSA_c_b4(ii_c),...
%             euc_off_c_b1(ii_c),euc_off_c_b2(ii_c),...
%             euc_off_c_b3(ii_c),euc_off_c_b4(ii_c),...
%             euc_off_c_b5(ii_c)],'o','color','m')
        hold on
    end
    
    for ii_hc = 1:length(HUX)
        dir_path = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\controls\HUX_C\',HUX{ii_hc},'\matFiles\');
        file_to_read = strcat('em_info_for_heatmaps_',HUX{ii_hc},'.mat');
        load(strcat(dir_path,file_to_read))
        avg_x_b1 = round(nanmean(em.allTraces.xBinned.bin1),2);
        avg_y_b1 = round(nanmean(em.allTraces.yBinned.bin1),2);
        s_e_locs_b1 = [avg_x_b1,avg_y_b1;0,0];
        euc_dist_b1 = pdist(s_e_locs_b1,'euclidean');
        euc_off_hc_b1(ii_hc) = euc_dist_b1;
        
        avg_x_b2 = round(nanmean(em.allTraces.xBinned.bin2),2);
        avg_y_b2 = round(nanmean(em.allTraces.yBinned.bin2),2);
        s_e_locs_b2 = [avg_x_b2,avg_y_b2;0,0];
        euc_dist_b2 = pdist(s_e_locs_b2,'euclidean');
        euc_off_hc_b2(ii_hc) = euc_dist_b2;
        
        avg_x_b3 = round(nanmean(em.allTraces.xBinned.bin3),2);
        avg_y_b3 = round(nanmean(em.allTraces.yBinned.bin3),2);
        s_e_locs_b3 = [avg_x_b3,avg_y_b3;0,0];
        euc_dist_b3 = pdist(s_e_locs_b3,'euclidean');
        euc_off_hc_b3(ii_hc) = euc_dist_b3;
        
        avg_x_b4 = round(nanmean(em.allTraces.xBinned.bin4),2);
        avg_y_b4 = round(nanmean(em.allTraces.yBinned.bin4),2);
        s_e_locs_b4 = [avg_x_b4,avg_y_b4;0,0];
        euc_dist_b4 = pdist(s_e_locs_b4,'euclidean');
        euc_off_hc_b4(ii_hc) = euc_dist_b4;
        
        avg_x_b5 = round(nanmean(em.allTraces.xBinned.bin5),2);
        avg_y_b5 = round(nanmean(em.allTraces.yBinned.bin5),2);
        s_e_locs_b5 = [avg_x_b5,avg_y_b5;0,0];
        euc_dist_b5 = pdist(s_e_locs_b5,'euclidean');
        euc_off_hc_b5(ii_hc) = euc_dist_b5;
        
        clear em;

        
        file_to_read_BSA = strcat('em_info_for_BSA_heatmaps_',HUX{ii_hc},'.mat');
        load(strcat(dir_path,file_to_read_BSA))
        avg_x_BSA_b1 = round(nanmean(em.allTraces.xBinned.bin1),2);
        avg_y_BSA_b1 = round(nanmean(em.allTraces.yBinned.bin1),2);
        s_e_locs_b1 = [avg_x_BSA_b1,avg_y_BSA_b1;0,0];
        euc_dist_b1 = pdist(s_e_locs_b1,'euclidean');
        euc_off_BSA_hc_b1(ii_hc) = euc_dist_b1;
        
        avg_x_BSA_b2 = round(nanmean(em.allTraces.xBinned.bin2),2);
        avg_y_BSA_b2 = round(nanmean(em.allTraces.yBinned.bin2),2);
        s_e_locs_b2 = [avg_x_BSA_b2,avg_y_BSA_b2;0,0];
        euc_dist_b2 = pdist(s_e_locs_b2,'euclidean');
        euc_off_BSA_hc_b2(ii_hc) = euc_dist_b2;
        
        avg_x_BSA_b3 = round(nanmean(em.allTraces.xBinned.bin3),2);
        avg_y_BSA_b3 = round(nanmean(em.allTraces.yBinned.bin3),2);
        s_e_locs_b3 = [avg_x_b3,avg_y_BSA_b3;0,0];
        euc_dist_b3 = pdist(s_e_locs_b3,'euclidean');
        euc_off_BSA_hc_b3(ii_hc) = euc_dist_b3;
        
        avg_x_BSA_b4 = round(nanmean(em.allTraces.xBinned.bin4),2);
        avg_y_BSA_b4 = round(nanmean(em.allTraces.yBinned.bin4),2);
        s_e_locs_b4 = [avg_x_BSA_b4,avg_y_BSA_b4;0,0];
        euc_dist_b4 = pdist(s_e_locs_b4,'euclidean');
        euc_off_BSA_hc_b4(ii_hc) = euc_dist_b4;
        

%         
        clear em;
        figure(1)
        plot([1,2,3,4,5,6,7,8,9],...
            [euc_off_BSA_hc_b1(ii_hc),euc_off_BSA_hc_b2(ii_hc),...
            euc_off_BSA_hc_b3(ii_hc),euc_off_BSA_hc_b4(ii_hc),...
            euc_off_hc_b1(ii_hc),euc_off_hc_b2(ii_hc),...
            euc_off_hc_b3(ii_hc),euc_off_hc_b4(ii_hc),...
            euc_off_hc_b5(ii_hc)],'o','color','b')

        hold on
    end
    
%     figure(1)
    std_err_P_b1 = std(euc_off_p_b1)/sqrt(length(euc_off_p_b1));
    std_err_C_b1 = std(euc_off_c_b1)/sqrt(length(euc_off_c_b1));
%     std_err_hC_b1 = std(euc_off_hc_b1)/sqrt(length(euc_off_hc_b1));
    
    std_err_P_b2 = std(euc_off_p_b2)/sqrt(length(euc_off_p_b2));
    std_err_C_b2 = std(euc_off_c_b2)/sqrt(length(euc_off_c_b2));
%     std_err_hC_b2 = std(euc_off_hc_b2)/sqrt(length(euc_off_hc_b2));
    
    std_err_P_b3 = std(euc_off_p_b3)/sqrt(length(euc_off_p_b3));
    std_err_C_b3 = std(euc_off_c_b3)/sqrt(length(euc_off_c_b3));
%     std_err_hC_b3 = std(euc_off_hc_b3)/sqrt(length(euc_off_hc_b3));
    
    std_err_P_b4 = std(euc_off_p_b4)/sqrt(length(euc_off_p_b4));
    std_err_C_b4 = std(euc_off_c_b4)/sqrt(length(euc_off_c_b4));
%     std_err_hC_b4 = std(euc_off_hc_b4)/sqrt(length(euc_off_hc_b4));
    
    std_err_P_b5 = std(euc_off_p_b5)/sqrt(length(euc_off_p_b5));
    std_err_C_b5 = std(euc_off_c_b5)/sqrt(length(euc_off_c_b5));
%     std_err_hC_b5 = std(euc_off_hc_b5)/sqrt(length(euc_off_hc_b5));
    
    std_err_BSA_P_b1 = std(euc_off_BSA_p_b1)/sqrt(length(euc_off_BSA_p_b1));
    std_err_BSA_C_b1 = std(euc_off_BSA_c_b1)/sqrt(length(euc_off_BSA_c_b1));
%     std_err_BSA_hC_b1 = std(euc_off_BSA_hc_b1)/sqrt(length(euc_off_BSA_hc_b1));
    
    std_err_BSA_P_b2 = std(euc_off_BSA_p_b2)/sqrt(length(euc_off_BSA_p_b2));
    std_err_BSA_C_b2 = std(euc_off_BSA_c_b2)/sqrt(length(euc_off_BSA_c_b2));
%     std_err_BSA_hC_b2 = std(euc_off_BSA_hc_b2)/sqrt(length(euc_off_BSA_hc_b2));
    
    
    std_err_BSA_P_b3 = std(euc_off_BSA_p_b3)/sqrt(length(euc_off_BSA_p_b3));
    std_err_BSA_C_b3 = std(euc_off_BSA_c_b3)/sqrt(length(euc_off_BSA_c_b3));
%     std_err_BSA_hC_b3 = std(euc_off_BSA_hc_b3)/sqrt(length(euc_off_BSA_hc_b3));
    
    std_err_BSA_P_b4 = std(euc_off_BSA_p_b4)/sqrt(length(euc_off_BSA_p_b4));
    std_err_BSA_C_b4 = std(euc_off_BSA_c_b4)/sqrt(length(euc_off_BSA_c_b4));
%     std_err_BSA_hC_b4 = std(euc_off_BSA_hc_b4)/sqrt(length(euc_off_BSA_hc_b4));
    
    
%     std_err_P_b5 = std(euc_off_p_b5)/sqrt(length(euc_off_p_b5));
%     std_err_C_b5 = std(euc_off_c_b5)/sqrt(length(euc_off_c_b5));
%     figure(1)
%     errorbar([mean(euc_off_p_b1) mean(euc_off_p_b2) mean(euc_off_p_b3)...
%                mean(euc_off_p_b4) mean(euc_off_p_b5)],...
%               [std_err_fix_P_b1 std_err_fix_P_b2 std_err_fix_P_b3 std_err_fix_P_b4 std_err_fix_P_b5],...
%               'o', 'Color', 'g', 'LineStyle', '-','LineWidth',3)
%     hold on
%     errorbar([mean(euc_off_c_b1) mean(euc_off_c_b2) mean(euc_off_c_b3)...
%                mean(euc_off_c_b4) mean(euc_off_c_b5)],...
%               [std_err_fix_C_b1 std_err_fix_C_b2 std_err_fix_C_b3 std_err_fix_C_b4 std_err_fix_C_b5],...
%               'o', 'Color', 'm', 'LineStyle', '-','LineWidth',3)   
%     set(gca, 'XTick', [1:1:5], 'XTickLabel', {'0-100'; '100-200';'200-300';'300-400';'400-500'}, 'FontSize', 12);
%     ylabel('Euclidean offset [arcmin]')
%     set(gca,'fontsize',27,'FontWeight','Bold')
%     xlim([0 6])
    figure(1)
    hold on
    errorbar([mean(euc_off_BSA_c_b1) mean(euc_off_BSA_c_b2)...
            mean(euc_off_BSA_c_b3) mean(euc_off_BSA_c_b4),...
            mean(euc_off_c_b1),mean(euc_off_c_b2),...
            mean(euc_off_c_b3),mean(euc_off_c_b4),...
            mean(euc_off_c_b5)],...
              [std_err_BSA_C_b1 std_err_BSA_C_b2...
              std_err_BSA_C_b3 std_err_BSA_C_b4...
              std_err_C_b1 std_err_C_b2...
              std_err_C_b3 std_err_C_b4...
              std_err_C_b5],...
              'o', 'Color', 'm', 'LineStyle', '-','LineWidth',3)
    hold on
    errorbar([mean(euc_off_BSA_hc_b1) mean(euc_off_BSA_hc_b2)...
            mean(euc_off_BSA_hc_b3) mean(euc_off_BSA_hc_b4),...
            mean(euc_off_hc_b1),mean(euc_off_hc_b2),...
            mean(euc_off_hc_b3),mean(euc_off_hc_b4),...
            mean(euc_off_hc_b5)],...
              [std_err_BSA_hC_b1 std_err_BSA_hC_b2...
              std_err_BSA_hC_b3 std_err_BSA_hC_b4...
              std_err_hC_b1 std_err_hC_b2...
              std_err_hC_b3 std_err_hC_b4...
              std_err_hC_b5],...
              'o', 'Color', 'b', 'LineStyle', '-','LineWidth',3)
    hold on
    errorbar([mean(euc_off_BSA_p_b1) mean(euc_off_BSA_p_b2)...
              mean(euc_off_BSA_p_b3) mean(euc_off_BSA_p_b4),...
              mean(euc_off_p_b1) mean(euc_off_p_b2)...
              mean(euc_off_p_b3) mean(euc_off_p_b4),...
              mean(euc_off_p_b5)],...
              [std_err_BSA_P_b1 std_err_BSA_P_b2...
              std_err_BSA_P_b3 std_err_BSA_P_b4...
              std_err_P_b1 std_err_P_b2...
              std_err_P_b3 std_err_P_b4...
              std_err_P_b5],...
              'o', 'Color', 'g', 'LineStyle', '-','LineWidth',3)
    set(gca, 'XTick', [1:1:9],...
        'XTickLabel', {'50'; '150';'250';'350';...
                        '450';'550';'650';'750';'850'}, 'FontSize', 12);
%     errorbar([mean(euc_off_task_c_b1) mean(euc_off_task_c_b2)...
%             mean(euc_off_task_c_b3) mean(euc_off_task_c_b4) mean(euc_off_task_c_b5)],...
%               [std_err_task_C_b1 std_err_task_C_b2 std_err_task_C_b3 std_err_task_C_b4 std_err_task_C_b5],...
%               'o', 'Color', 'm', 'LineStyle', '-','LineWidth',3)
%     set(gca, 'XTick', [1:1:5], 'XTickLabel', {'0-100'; '100-200';'200-300';'300-400';'400-500'}, 'FontSize', 12);
    xlabel('Time (ms)')
    ylabel('Euclidean offset [arcmin]')
    set(gca,'fontsize',13,'FontWeight','Bold')
    xlim([0 10])
    xline(4.5,'--k','LineWidth',3)
    legend('Healthy controls','Patients');
    legend('Healthy controls','HUX controls','Patients');
end