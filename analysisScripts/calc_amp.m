function [mean_amp_t] = calc_amp(msInfo)
    
    for ts = 1:length(msInfo.startPos)
        amp_1 = deal([]);
        for ms = 1:length(msInfo.startPos{ts}(1))
            startEndLocs = [msInfo.startPos{ts}(1,ms),msInfo.startPos{ts}(2,ms);...
                            msInfo.endPos{ts}(1,ms),msInfo.endPos{ts}(2,ms)];
            amp_1(1,end+1) = pdist(startEndLocs,'euclidean');
        end
        mean_amp_t(ts) = mean(amp_1);
    end
    
    
end