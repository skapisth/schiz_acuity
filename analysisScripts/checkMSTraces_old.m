function pptrials = checkMSTraces_old(trialId, pptrials, params, filepath, singleSegs, nameFile)


% microfaces data cleaning
% MAC - 4/17/2020 by JI - mostly good, some quantization errors
% M008 - 4/17/2020 by JI - some good, many quantization errors


if strcmp('D',params.machine)
    filepath = filepath;
else
    filepath = filepath;
end
%test

if isempty(trialId)
    trialId = 1:length(pptrials);
end

PLOT_DRIFTS = input('Plot individual segments (y/n): ','s');

if PLOT_DRIFTS == 'y'
    
    %     snellen = input('Is this the snellen task? (y/n)','s');
    figure('position',[2300, 100, 1500, 800])
    
    trialCounter = 1;
    for driftIdx = 1:inf
        if (trialCounter) > length(trialId)
            return;
        end
        currentTrialId = trialId(trialCounter);
        
        
        subplot(2,2,3:4)
        
        hold off
        %         figure;
        %         if isempty('params')
        if params.fixation
            poiStart = pptrials{currentTrialId}.TimeFixationON;
            poiEnd = min(pptrials{currentTrialId}.TimeFixationOFF);
            axisWindow = 60;
        else
            if pptrials{currentTrialId}.TimeTargetOFF <= 0
                return;
            end
            poiStart = pptrials{currentTrialId}.TimeTargetON;
            poiEnd = min(pptrials{currentTrialId}.TimeTargetOFF);
            axisWindow = 40;
        end
        
        
        poi = fill([poiStart, poiStart ...
            poiEnd, poiEnd], ...
            [-50, 50, 50, -50], ...
            'g', 'EdgeColor', 'g', 'LineWidth', 2, 'Clipping', 'On', 'FaceAlpha', 0);
        
        xTrace = pptrials{currentTrialId}.x.position + pptrials{currentTrialId}.xoffset * pptrials{currentTrialId}.pixelAngle;
        yTrace = pptrials{currentTrialId}.y.position + pptrials{currentTrialId}.yoffset * pptrials{currentTrialId}.pixelAngle;
        
        hold on
        if strcmp('D',params.machine)
            hx = plot(1:(params.samplingRate):length(xTrace)*(params.samplingRate),xTrace, 'Color', [0 0 200] / 255, 'HitTest', 'off', 'LineWidth', 2);
            hy = plot(1:(params.samplingRate):length(yTrace)*(params.samplingRate),yTrace, 'Color', [0 180 0] / 255, 'HitTest', 'off', 'LineWidth', 2);
            sampling = params.samplingRate;
            axis([0 180 0 150])
        else
            hx = plot(xTrace, 'Color', [0 0 200] / 255, 'HitTest', 'off', 'LineWidth', 2);
            hy = plot(yTrace, 'Color', [0 180 0] / 255, 'HitTest', 'off', 'LineWidth', 2);
            sampling = 1;
            axis([0 180 0 150])
        end
        if params.fixation
            axis([poiStart, poiEnd, -axisWindow, axisWindow])
        else
            axis([poiStart-400, poiEnd + 400, -axisWindow, axisWindow])
        end
        xlabel('Time','FontWeight','bold')
        ylabel('arcmin','FontWeight','bold')
        title(sprintf('Trial: %i', currentTrialId)); %
        
        if strcmp('Stabilized', params.stabilization)
            %             for ii = 1:length(pptrials)
            if isfield(pptrials{currentTrialId},'XStab')
                hold on
                plot(pptrials{currentTrialId}.XStab.ts*(params.samplingRate),...
                    pptrials{currentTrialId}.XStab.stream,'-*','Color',[0 0 200] / 255, 'LineWidth', 2);
                
                hold on
                plot(pptrials{currentTrialId}.YStab.ts*(params.samplingRate),...
                    pptrials{currentTrialId}.YStab.stream,'-*','Color',[0 180 0] / 255, 'LineWidth', 2);
            end
        end
        
        subplot(2,2,1)
        
        [~,~,dc,~,~,singleSegs,~,~] = ...
            CalculateDiffusionCoef(sampling,struct('x',xTrace(poiStart/sampling:poiEnd/sampling), 'y',yTrace(poiStart/sampling:poiEnd/sampling)));
        %         [singleSegs,x,y] = getDriftSegmentInfo (msTRIALS, pptrials, params);
        plot(singleSegs);
        ylim([0 400]);
        title(dc);
        if singleSegs(140) > 300
            warning('This trial has large DC!')
        end
        subplot(2,2,3:4)
        
        
        set(gca, 'FontSize', 12)
        poiMS = [1 0 0];
        poiS = [1 0 0];
        poiD = [0 1 0];
        poiN = [0 0 0];
        poiI = [0 .423 .521];
        poiB = [.749 .019 1];
        if strcmp('D',params.machine)
            poiMS = plotColorsOnTraces(pptrials, currentTrialId, 'microsaccades', [1 0 0], params.samplingRate); %red
            poiS = plotColorsOnTraces(pptrials, currentTrialId, 'saccades', [1 0 0], params.samplingRate); %red
            poiD = plotColorsOnTraces(pptrials, currentTrialId, 'drifts', [0 1 0], params.samplingRate); %green
            poiN = plotColorsOnTraces(pptrials, currentTrialId, 'notracks', [0 0 0], params.samplingRate); %black
            poiI = plotColorsOnTraces(pptrials, currentTrialId, 'invalid', [0 0 1], params.samplingRate); %blue
            poiB = plotColorsOnTraces(pptrials, currentTrialId, 'blinks', [.749 .019 1], params.samplingRate); %pink
        else
            for i = 1:length(pptrials{currentTrialId}.microsaccades.start)
                msStartTime = pptrials{currentTrialId}.microsaccades.start(i);
                msDurationTime = pptrials{currentTrialId}.microsaccades.duration(i);
                poiMS = fill([msStartTime, msStartTime ...
                    msStartTime + msDurationTime, ...
                    msStartTime + msDurationTime], ...
                    [-35, 35, 35, -35], ...
                    'r', 'EdgeColor', 'r', 'LineWidth', 2, 'Clipping', 'On', 'FaceAlpha', 0.25);
            end
        end
        %         if ~isempty(poiMS) || ~isempty(poiS) || ~isempty(poiD) || ~isempty(poiI)
        % figure;
        if isempty(poiMS)
            legend([hx, hy, poiMS, poiD, poiN, poiI, poiB], ...
            {'X','Y','D','NoTrack','Invalid','Blink'},'FontWeight','bold')
        else
            legend([hx, hy, poiMS, poiD, poiN, poiI, poiB], ...
                {'X','Y','MS/S','D','NoTrack','Invalid','Blink'},'FontWeight','bold')
        end
        %         end
        
        contType = input...
            ('Saccade(1), Microsaccade(2), Drift(3), \n No Track(4), Invalid(5), Blinks(6), \n Back a trial(7), Stop(0), AutoPrune(8), Clear All (9), \n Fill In With Drift (10) \n Next Trial(enter)?\n');
        cont = [];
        if contType == 1
            cont = input('Tag a Saccade(a) or Change Saccade Labelled(b)?','s');
            em = 'saccades'; % Post analysis will check amps and organize as MS or S
        elseif contType == 2
            cont = input('Tag a microsaccade(a) or Change Microsaccade Labelled(b)?','s');
            em = 'microsaccades'; % +- 10msec
        elseif contType == 3
            cont = input('Tag a Drift(a) or Change Drift Labelled(b)?','s');
            em = 'drifts';
        elseif contType == 4
            cont = input('Tag a No Track(a) or Change no track Labelling?(b)','s');
            em = 'notracks'; %flat no tracks
        elseif contType == 5
            cont = input('Tag a Invalid(a) or Change invalid Labelling?(b)','s');
            em = 'invalid'; %bad drifts
        elseif contType == 6
            cont = input('Tag a Blink(a) or Change a Blink Labelling?(b)','s');
            em = 'blinks';
        elseif contType == 7
            cont = 'z';
        elseif contType == 0
            cont = 's';
        elseif contType == 8
            pptrials = autoPrune( pptrials, currentTrialId, 'drifts', 75);
            pptrials = autoPrune( pptrials, currentTrialId, 'microsaccades', 30);
            pptrials = autoPrune( pptrials, currentTrialId, 'saccades', 30);
            pptrials = autoPrune( pptrials, currentTrialId, 'blinks', 10);
            continue;
        elseif contType == 9
            pptrials = autoPrune( pptrials, currentTrialId, 'drifts', 10000);
            pptrials = autoPrune( pptrials, currentTrialId, 'microsaccades', 10000);
            pptrials = autoPrune( pptrials, currentTrialId, 'saccades', 10000);
            pptrials = autoPrune( pptrials, currentTrialId, 'blinks', 10000);
            pptrials = autoPrune( pptrials, currentTrialId, 'invalid', 10000);
            pptrials = autoPrune( pptrials, currentTrialId, 'notracks', 10000);
            continue;
        elseif contType == 10
            
            Trial = pptrials{currentTrialId};
            AllMovements = joinEvents(Trial.saccades, Trial.microsaccades);
            AllMovements = joinEvents(AllMovements, Trial.blinks);
            AllMovements = joinEvents(AllMovements, Trial.notracks);
            AllMovements = joinEvents(AllMovements, Trial.invalid);
            Events = invertEvents(AllMovements, Trial.samples);
            Events = updateAmplitudeAngle(Trial, Events);
            pptrials{currentTrialId}.drifts  = Events;
            cont = 'f';
        else
            trialCounter = trialCounter + 1;
            continue;
        end
        
        if cont == 's'
            save(fullfile(sprintf('%s',filepath), nameFile), 'pptrials')
            break;
        elseif cont == 'a'
            numTaggedInTrace = length(pptrials{currentTrialId}.(em).start)+1;%input('Which ones (in order) do you want to tag?');
            startTime = input('What is the start time?');
            duration = input('What is the end time?');
            if strcmp('D',params.machine)
                pptrials{currentTrialId}.(em).start(numTaggedInTrace) = round(startTime/(params.samplingRate));
                pptrials{currentTrialId}.(em).duration(numTaggedInTrace) = round((duration/(params.samplingRate)) - round(startTime/(params.samplingRate)));
            else
                pptrials{currentTrialId}.(em).start(numTaggedInTrace) = round(startTime);
                pptrials{currentTrialId}.(em).duration(numTaggedInTrace) = round((duration) - round(startTime));
            end
            fprintf('Saving pptrials\n')
            save(fullfile(sprintf('%s',filepath), nameFile), 'pptrials')
            continue;
        elseif cont == 'b'
            fprintf('Total start times: %.0f \n', round(pptrials{currentTrialId}.(em).start, 3)*(params.samplingRate))
            startTime = input('When does the wrong EM start?');
            newStartTime = input('When does the EM actually start? (set to 0 if not present)');
            newDurationTime = input('When does the EM end? (set to 0 if not present)');
            for numW = 1:length(startTime)
                wrong = find((pptrials{currentTrialId}.(em).start) == round(startTime(numW)/(params.samplingRate)));
                pptrials{currentTrialId}.(em).start(wrong) = round(newStartTime(numW)/(params.samplingRate));
                pptrials{currentTrialId}.(em).duration(wrong) = ...
                    (round(newDurationTime(numW)/(params.samplingRate))) - round(newStartTime(numW)/(params.samplingRate));
            end
            fprintf('Saving pptrials\n')
            save(fullfile(sprintf('%s',filepath), nameFile), 'pptrials')
            continue;
        elseif cont == 'z'
            trialCounter = trialCounter-1;
            continue;
        elseif cont == 'f'
            save(fullfile(sprintf('%s',filepath), nameFile), 'pptrials')
            continue;
        end
        save(fullfile(sprintf('%s',filepath), nameFile), 'pptrials')
        trialCounter = trialCounter + 1;
    end
end

close

end
