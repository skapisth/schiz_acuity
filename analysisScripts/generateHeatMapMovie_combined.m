function generateHeatMapMovie_combined(subject,blankS_posX,blankS_posY,blankS_timeStamps,posX, posY,timeStamps)

    
    v_trial = VideoWriter(sprintf('./movies/%s',subject{1}), 'UnCompressed AVI');
    v_trial.FrameRate = 5;
    % v_trial.CompressionRatio = 2;
    open(v_trial);
    NBins = 15;
    % temporal bins
    TBins = 20; %ms
    TimeIntervals = linspace(1, 400, 400/TBins);
    limit = 30;
    Alph = 0.25;
    frame_count = 0;
    
    %% for the blank screen
    % get all data in a vector format
    blankS_posX = reshape(blankS_posX, [1, size(blankS_posX,1)*size(blankS_posX,2)]);
    blankS_posY = reshape(blankS_posY, [1, size(blankS_posY,1)*size(blankS_posY,2)]);
    
    
    blankS_timeStamps = reshape(blankS_timeStamps, [1, size(blankS_timeStamps,1)*size(blankS_timeStamps,2)]);
    
    load('./MyColormaps.mat')
    if ~exist('movies', 'dir')
        mkdir('movies')
    end
    
%     figure()
    for t = 1:length(TimeIntervals)-1
        idx = find(blankS_timeStamps<TimeIntervals(t+1) & blankS_timeStamps>=TimeIntervals(t));
        
        result = MyHistogram2(blankS_posX(idx), blankS_posY(idx),[-limit,limit,NBins;-limit,limit,NBins]);
        %     result = result.Values;
        result = result./(max(max(result)));
        
        
        
        figure()
%         subplot(1,2,1) 
        hold on
        pH = pcolor(linspace(-limit,limit,size(result,1)),linspace(-limit,limit,size(result,1)),...
            result');
        pH.FaceAlpha = Alph;
        set(gcf, 'Colormap', mycmap)
        set(gca, 'Visible', 'off','FontSize', 16)
        %     caxis([lL uL])
        axis tight
        axis square
        axis off
        shading interp;
%         axis([-axisValue axisValue -axisValue axisValue]);
        hold on
        text(10,22,sprintf('time: %dms', round(TimeIntervals(t+1))), 'FontWeight','bold')
        hold on
        frame_count = frame_count + 1;
        movie_trial(frame_count) = getframe(gcf);
        writeVideo(v_trial, movie_trial(frame_count));
    end
    hold on
    %% for the task
    % spatial bins for the heatmaps
%     NBins = 15;
%     % temporal bins
%     TBins = 20; %ms
    TimeIntervals = linspace(1, 500, 500/TBins);
%     limit = 30;
%     Alph = 0.25;
%     frame_count = 0;
    % get all data in a vector format
    posX = reshape(posX, [1, size(posX,1)*size(posX,2)]);
    posY = reshape(posY, [1, size(posY,1)*size(posY,2)]);
    
    
    timeStamps = reshape(timeStamps, [1, size(timeStamps,1)*size(timeStamps,2)]);
    
    load('./MyColormaps.mat')
    if ~exist('movies', 'dir')
        mkdir('movies')
    end
    
    
    for t = 1:length(TimeIntervals)-1
        idx = find(timeStamps<TimeIntervals(t+1) & timeStamps>=TimeIntervals(t));
        
        result = MyHistogram2(posX(idx), posY(idx),[-limit,limit,NBins;-limit,limit,NBins]);
        %     result = result.Values;
        result = result./(max(max(result)));
        
        
        
        figure()
%         subplot(1,2,2) 
        hold on
        pH = pcolor(linspace(-limit,limit,size(result,1)),linspace(-limit,limit,size(result,1)),...
            result');
        pH.FaceAlpha = Alph;
        set(gcf, 'Colormap', mycmap)
        set(gca, 'Visible', 'off','FontSize', 16)
        %     caxis([lL uL])
        axis tight
        axis square
        axis off
        shading interp;
        stimuliSize = 4.2;offset = 0;
        stimuliSize = stimuliSize/2;
        axisValue = 40;
        width = 2 * stimuliSize;
        height = width * 5;
        centerX = (-stimuliSize+offset);
        centerY = (-stimuliSize*5);
        rectangle('Position',[centerX, centerY, width, height],'LineWidth',2)
        
        
%         axis([-axisValue axisValue -axisValue axisValue]);
        hold on
        text(10,22,sprintf('time: %dms', round(TimeIntervals(t+1))), 'FontWeight','bold')
        hold on
        frame_count = frame_count + 1;
        movie_trial(frame_count) = getframe(gcf);
        writeVideo(v_trial, movie_trial(frame_count));
    end
    
    
    close(v_trial);
    close all;
end

























