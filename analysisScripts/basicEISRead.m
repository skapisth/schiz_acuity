%% load in some data

subjects = {'2'};
% subjects = {'Janis', 'Giorgio'};

pt ='Y:\SchizVIs\RealData\patients\PC-S04\unCrowded\';

% pt = 'C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\priorityMaps2.0\scriptsAndData\data\mp\';
for si = 1:length(subjects)
    
    pathtodata = fullfile(pt, subjects{si});
%     [data, labels] = ddpi_readDEBUG(si)
       
    fname1 = sprintf('pptrials.mat');
    
    fprintf('%s\n', fname1);
    
    % this is the raw read in of the data
    data = readdata(pathtodata, CalListDDPI_Schiz()); %% comment out last line in eis_openTrial.m for without eye tracking data
%     plotBlinksAndNoTracksUsingRawData(data);
%     data = fixAlternatingNoTrackAndBlinkIssue(data);
    
    pptrials =  preprocessingDDPI(data,30, 340);
%     
%     pptrials = data;
    save(fullfile(pathtodata, fname1), 'pptrials', 'data');
    
end

