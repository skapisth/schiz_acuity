clc;clear all;
% subjects = {'PC-S01','PC-S02','PC-S04','PC-S07','PC-S12','PC-S16','FE-S01','FE-S08','FE-S09'};
subjects = {'HUX4','HUX10','HUX17','HUX18','HUX22','HUX23','Hux24','MP','PC-C06','FE-C19','FE-C05','FE-C17'};
subjects = {'HUX10','HUX17','HUX18','HUX22','HUX23','Hux24','MP','FE-C19','FE-C05','FE-C17'};
% subjects = {'PC-S04'};
patient = 0;
newEyeris = 0;
% load('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\patients\acuityThreshMats\all_thresh_.mat');

for ii_s = 1:length(subjects)
    
    if patient
        load(strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\patients\cleanDrifts\','cleanD_struct_',subjects{ii_s},'.mat'));
         if length(subjects) > 1
            if ii_s > 3
                newEyeris = 1;
                Fs = 1000;
                params.samplingRate = 1000/Fs;
            else
                newEyeris = 0;
                Fs = 341;
                params.samplingRate = 1000/Fs;
            end
        else
            if newEyeris
                Fs = 1000;
                params.samplingRate = 1000/Fs;
            else
                Fs = 341;
                params.samplingRate = 1000/Fs;
            end
         end
    else
        load(strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\controls\cleanDrifts\','cleanD_struct_',subjects{ii_s},'.mat'));
        if length(subjects) > 1
            if ii_s > 7
                newEyeris = 1;
                Fs = 1000;
                params.samplingRate = 1000/Fs;
            else
                newEyeris = 0;
                Fs = 341;
                params.samplingRate = 1000/Fs;
            end
        else
            if newEyeris
                Fs = 1000;
                params.samplingRate = 1000/Fs;
            else
                Fs = 341;
                params.samplingRate = 1000/Fs;
            end

         end
    end
    [DCoef_DeTrended_AO Bias_AO DCoef_Dsq_AO DsqDetrended_AO Dsq_AO SingleSegmentDsq_AO Time_AO NFix_AO] = ...
    CalculateDiffusionCoef_only(all_long_drifts_clean, ...
    'Fsampling', Fs,...
    'RegressionExclusionLag', 100/1000*Fs, ...%% check this
    'MaxTime', round(250 / 1000 * Fs));

all_DC(ii_s) = DCoef_Dsq_AO;
%% plot Diff Constant plot

% % tdsq_AO = (1:length(Dsq_AO)) / Fs * 1000; % ms
% % 
% % figure(); clf; % plot information about drift diffusion
% % subplot(1, 2, 1);
% % plot(tdsq_AO, SingleSegmentDsq_AO);
% % xlim(tdsq_AO([1, end]));
% % xlabel('time lag (ms)');
% % ylabel('variance of displacement (arcmin^2)');
% % title('single segments');
% % 
% % subplot(1, 2, 2); hold on;
% % plot(tdsq_AO, Dsq_AO, 'k-', 'linewidth', 2);
% % plot(tdsq_AO, (4 * tdsq_AO) * DCoef_Dsq_AO / 1000, 'b--', 'linewidth', 2)
% % xlim(tdsq_AO([1, end]));
% % xlabel('time lag (ms)');
% % ylabel('variance of displacement (arcmin^2)');
% % legend({'data', 'brownian motion'}, 'Location', 'northwest');
% % title(sprintf('Diffusion constant = %1.2f arcmin^2/s', DCoef_Dsq_AO'));
% % pathToSave = 'C:/Users/sanjana/OneDrive - University of Rochester/Documents/Sanjana/Projects/schizophrenia/Figures/DiffusionConstant_clean/controls';
% % fullPath = strcat(pathToSave,'/%s');
% % strfName = subjects{ii_s};
% % saveas(gcf,sprintf(fullPath, strfName), 'png')
% % saveas(gcf,sprintf(fullPath, strfName), 'eps')
% close all

end
FName = sprintf(strcat('all_DC_C', '.mat'));
save(fullfile('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\controls\acuityThreshMats\', FName), 'all_DC');

figure()
plot([all_thresh],[all_DC],'o','MarkerSize',10,'MarkerFaceColor','k')
xlabel('Acuity Threshold (arcmin)')
ylabel('Diffusion constant (arcmin^2/sec)')
set(gca,'fontsize',27,'FontWeight','Bold')
%    xlim([2,4])
% ylim([0,13])

clear all

load('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\controls\acuityThreshMats\all_DC_C.mat')

allDC_C = all_DC;

load('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\patients\acuityThreshMats\all_DC_P.mat')

allDC_P = all_DC;
figure()
for allP = 1:length(allDC_P)
    plot([1],[allDC_P(allP)],'o','MarkerSize',10,'MarkerFaceColor','g')
    hold on
end

for allC = 1:length(allDC_C)
    plot([2],[allDC_C(allC)],'o','MarkerSize',10,'MarkerFaceColor','r')
    hold on
end

std_err_DC_P = std(allDC_P)/sqrt(length(allDC_P));
std_err_DC_C = std(allDC_C)/sqrt(length(allDC_C));

errorbar([mean(allDC_P) mean(allDC_C) ],...
       [std_err_DC_P  std_err_DC_C],...
       '-o', 'Color', 'k','LineWidth',3)

set(gca, 'XTick', [1:1:2], 'XTickLabel', {'Patients'; 'Healthy controls'}, 'FontSize', 12);
ylabel('Diffusion constant (arcmin^2/sec)')
set(gca,'fontsize',15,'FontWeight','Bold')
xlim([0 3])

[p,h] = ranksum(allDC_P,allDC_C);

cP = distinguishable_colors(length(dms));
reqd_dms = [];
reqd_ms = [];
for allP = 1:length(dms)
    if ms(allP) < 0
        continue
    else
        reqd_dms(1,end+1) = dms(allP);
        reqd_ms(1,end+1) = ms(allP);
        plot([1,2],[dms(allP),ms(allP)],'-o','MarkerSize',8,'MarkerFaceColor',cP(allP,:))
        hold on
    end
end



std_err_dms_P = std(reqd_dms)/sqrt(length(reqd_dms));
std_err_ms_P = std(reqd_ms)/sqrt(length(reqd_ms));

errorbar([mean(reqd_dms) mean(reqd_ms) ],...
       [std_err_dms_P  std_err_ms_P],...
       '-o', 'Color', 'k','LineWidth',3)

set(gca, 'XTick', [1:1:2], 'XTickLabel', {'d+ms'; 'ms only'}, 'FontSize', 12);
ylabel('acuity threshold (arcmins)')
set(gca,'fontsize',27,'FontWeight','Bold')
xlim([0 3])

[p,h] = ranksum(reqd_dms,reqd_ms);