function all_sub_d_char_analysis_fix(patients,controls)
    
    [all_span_P,all_curv_P,all_PLF_P,all_mn_speed_P,all_amp_P] = deal([]);
    [all_span_C,all_curv_C,all_PLF_C,all_mn_speed_C,all_amp_C] = deal([]);
    cP = jet(length(patients));
    cC = winter(length(controls));
    for ii_p = 1:length(patients)
        dir_path = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\patients\',patients{ii_p},'\matFiles\');
        file_to_read = strcat('fix_results_cut_drift_',patients{ii_p},'.mat');
        load(strcat(dir_path,file_to_read))

        fixation.span = cell2mat(fixation.span);
        span_P = mean(fixation.span(~isnan(fixation.span)));
        fixation.curvature = cell2mat(fixation.curvature);
        curv_P = mean(fixation.curvature(~isnan(fixation.curvature)));
        fixation.prlDistance = cell2mat(fixation.prlDistance);
        PLF_P = mean(fixation.prlDistance(~isnan(fixation.prlDistance)));
        fixation.mn_speed = cell2mat(fixation.mn_speed);
%         fixation.mn_speed = fixation.mn_speed(fixation.mn_speed <=350);
        mn_speed_P = mean(fixation.mn_speed(~isnan(fixation.mn_speed)));
        fixation.amplitude = cell2mat(fixation.amplitude);
        amp_P = mean(fixation.amplitude(~isnan(fixation.amplitude)));
           
        all_span_P(1,end+1) = span_P;
        all_curv_P(1,end+1) = curv_P;
        all_PLF_P(1,end+1) = PLF_P;
        all_mn_speed_P(1,end+1) = mn_speed_P;
        all_amp_P(1,end+1) = amp_P;
        
        clear fixation
        figure(1)
        plot([1],[all_span_P(ii_p)],'o','MarkerSize',10,'MarkerFaceColor',cP(ii_p,:))
        hold on
        figure(2)
        plot([1],[all_curv_P(ii_p)],'o','MarkerSize',10,'MarkerFaceColor',cP(ii_p,:))
        hold on
        figure(3)
        plot([1],[all_PLF_P(ii_p)],'o','MarkerSize',10,'MarkerFaceColor',cP(ii_p,:))
        hold on
        figure(4)
        plot([1],[all_mn_speed_P(ii_p)],'o','MarkerSize',10,'MarkerFaceColor',cP(ii_p,:))
        hold on
        figure(5)
        plot([1],[all_amp_P(ii_p)],'o','MarkerSize',10,'MarkerFaceColor',cP(ii_p,:))
        hold on
    end
    
    for ii_c = 1:length(controls)
        dir_path = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\controls\',controls{ii_c},'\matFiles\');
        file_to_read = strcat('fix_results_cut_drift_',controls{ii_c},'.mat');
        load(strcat(dir_path,file_to_read))
        
        fixation.span = cell2mat(fixation.span);
        span_C = mean(fixation.span(~isnan(fixation.span)));
        fixation.curvature = cell2mat(fixation.curvature);
        curv_C = mean(fixation.curvature(~isnan(fixation.curvature)));
        fixation.prlDistance = cell2mat(fixation.prlDistance);
        PLF_C = mean(fixation.prlDistance(~isnan(fixation.prlDistance)));
        fixation.mn_speed = cell2mat(fixation.mn_speed);
%         fixation.mn_speed = fixation.mn_speed(fixation.mn_speed<=350);
        mn_speed_C = mean(fixation.mn_speed(~isnan(fixation.mn_speed)));
        fixation.amplitude = cell2mat(fixation.amplitude);
        amp_C = mean(fixation.amplitude(~isnan(fixation.amplitude)));
        
        
        all_span_C(1,end+1) = span_C;
        all_curv_C(1,end+1) = curv_C;
        all_PLF_C(1,end+1) = PLF_C;
        all_mn_speed_C(1,end+1) = mn_speed_C;
        all_amp_C(1,end+1) = amp_C;
        
        clear fixation
        figure(1)
        plot([2],[all_span_C(ii_c)],'o','MarkerSize',10,'MarkerFaceColor',cC(ii_c,:))
        hold on
        figure(2)
        plot([2],[all_curv_C(ii_c)],'o','MarkerSize',10,'MarkerFaceColor',cC(ii_c,:))
        hold on
        figure(3)
        plot([2],[all_PLF_C(ii_c)],'o','MarkerSize',10,'MarkerFaceColor',cC(ii_c,:))
        hold on
        figure(4)
        plot([2],[all_mn_speed_C(ii_c)],'o','MarkerSize',10,'MarkerFaceColor',cC(ii_c,:))
        hold on
        figure(5)
        plot([2],[all_amp_C(ii_c)],'o','MarkerSize',10,'MarkerFaceColor',cC(ii_c,:))
        hold on
    
    end
    
    
    figure(1)
    std_err_span_P = std(all_span_P)/sqrt(length(all_span_P));
    std_err_span_C = std(all_span_C)/sqrt(length(all_span_C));
    errorbar([mean(all_span_P) mean(all_span_C)],...
              [std_err_span_P std_err_span_C],...
              'o', 'Color', 'k', 'LineStyle', '-','LineWidth',3)
    set(gca, 'XTick', [1:1:2], 'XTickLabel', {'Patients'; 'Controls'}, 'FontSize', 12);
    ylabel('Drift span [arcmin]')
    set(gca,'fontsize',27,'FontWeight','Bold')
    xlim([0 3])
%     saveas(gcf,'C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\Figures\d_span_task_p_c.png')
%     [p_sp h_sp stats] = ranksum(all_span_P,all_span_C);
%     if h_sp == 1
%         text(2.1, mean(all_span_P)+1, '*', 'FontSize', 45,'FontWeight','bold') %0.94
%     end

    figure(2)
    std_err_curv_P = std(all_curv_P)/sqrt(length(all_curv_P));
    std_err_curv_C = std(all_curv_C)/sqrt(length(all_curv_C));
    errorbar([mean(all_curv_P) mean(all_curv_C)],...
              [std_err_curv_P std_err_curv_C],...
              'o', 'Color', 'k', 'LineStyle', '-','LineWidth',3)
    set(gca, 'XTick', [1:1:2], 'XTickLabel', {'Patients'; 'Controls'}, 'FontSize', 12);
    ylabel('Drift curvature [arcmin^-1]')
    set(gca,'fontsize',27,'FontWeight','Bold')
    xlim([0 3])
%     [p_curv h_curv stats] = ranksum(all_curv_P,all_curv_C);
%     if h_curv == 1
%         text(2.1, mean(all_curv_P)+1, '*', 'FontSize', 45,'FontWeight','bold') %0.94
%     end
    
    figure(3)
    std_err_PLF_P = std(all_PLF_P)/sqrt(length(all_PLF_P));
    std_err_PLF_C = std(all_PLF_C)/sqrt(length(all_PLF_C));
    errorbar([mean(all_PLF_P) mean(all_PLF_C)],...
              [std_err_PLF_P std_err_PLF_C],...
              'o', 'Color', 'k', 'LineStyle', '-','LineWidth',3)
    set(gca, 'XTick', [1:1:2], 'XTickLabel', {'Patients'; 'Controls'}, 'FontSize', 12);
    ylabel('PLF [arcmin]')
    set(gca,'fontsize',27,'FontWeight','Bold')
    xlim([0 3])
%     [p_PLF h_PLF stats] = ranksum(all_PLF_P,all_PLF_C);
%     if h_PLF == 1
%         text(2.1, mean(all_PLF_P)+1, '*', 'FontSize', 45,'FontWeight','bold') %0.94
%     end
    
    figure(4)
    std_err_mn_speed_P = std(all_mn_speed_P)/sqrt(length(all_mn_speed_P));
    std_err_mn_speed_C = std(all_mn_speed_C)/sqrt(length(all_mn_speed_C));
    errorbar([mean(all_mn_speed_P) mean(all_mn_speed_C)],...
              [std_err_mn_speed_P std_err_mn_speed_C],...
              'o', 'Color', 'k', 'LineStyle', '-','LineWidth',3)
    set(gca, 'XTick', [1:1:2], 'XTickLabel', {'Patients'; 'Controls'}, 'FontSize', 12);
    ylabel('Drift speed [arcmin/s]')
    set(gca,'fontsize',27,'FontWeight','Bold')
    xlim([0 3])
%     [p_mn_s h_mn_s stats] = ranksum(all_mn_speed_P,all_mn_speed_C);
%     if h_mn_s == 1
%         text(2.1, mean(all_mn_speed_P)+1, '*', 'FontSize', 45,'FontWeight','bold') %0.94
%     end
    
    figure(5)
    std_err_amp_P = std(all_amp_P)/sqrt(length(all_amp_P));
    std_err_amp_C = std(all_amp_C)/sqrt(length(all_amp_C));
    errorbar([mean(all_amp_P) mean(all_amp_C)],...
              [std_err_amp_P std_err_amp_C],...
              'o', 'Color', 'k', 'LineStyle', '-','LineWidth',3)
    set(gca, 'XTick', [1:1:2], 'XTickLabel', {'Patients'; 'Controls'}, 'FontSize', 12);
    ylabel('Drift amplitude [arcmin]')
    set(gca,'fontsize',27,'FontWeight','Bold')
    xlim([0 3])
%     [p_amp h_amp stats] = ranksum(all_amp_P,all_amp_C);
%     if h_amp == 1
%         text(2.1, mean(all_amp_P)+1, '*', 'FontSize', 45,'FontWeight','bold') %0.94
%     end
    
    
end