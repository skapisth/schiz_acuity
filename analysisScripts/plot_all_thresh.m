function plot_all_thresh(SZ_patients,BP_patients,controls,hux_controls)
    machine = {'D'};
    figures = struct(...
                    'FIGURE_ON', 0,... %Show individual trial with traces in 2D space
                    'VIDEO', 0,... %Wills save video of FIGURE_ON figures
                    'FIXATION_ANALYSIS',0,... %Plots psychometric curves
                    'saveFixMat',1,...
                    'QUICK_ANALYSIS',0,... %Only plots psychometric curves
                    'CHECK_TRACES',0,... %will plot traces
                    'HUX_RUN',1,... %will discard trials differently
                    'FOLDED', 0,...
                    'FIXED_SIZE', 0,...
                    'COMPARE_SPANS',0); %0 = all spans, 1 = small spans, 2 = large spans

    params = struct(...
                    'patient',1,...
                    'nBoots', 1,... %Number of boots for psychometric fits
                    'crowded', false, ... %Gets rewritten based on currentCond
                    'DMS',true, ...
                    'D', false, ...
                    'MS', false,...
                    'fixation',false,...
                    'machine',machine, ... %DPI "A" vs dDPI "D"
                    'session', '1', ... %Which number session
                    'spanMax', 100,... %The Maximum Span
                    'spanMin', 0,...%The Minimum Span
                    'compSpans', 0,...
                    'ecc',0,...
                    'samplingRate' , 1000/341,...
                    'stabilization','Unstabilized');
   cP = jet(length(SZ_patients));
   cC = winter(length(hux_controls));

   for ii_p = 1:length(SZ_patients)
       if ii_p > 3
           params.samplingRate = 1000/1000;
           newEyeris = 1;
       else
           params.samplingRate = 1000/341;
           newEyeris = 0;
       end
       em = [];
       params.subject = SZ_patients{ii_p};
       title_str = char(SZ_patients{ii_p});
       pathToFile =  strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\patients\',char(SZ_patients{ii_p}),'\unCrowded');
       load(fullfile(pathToFile,'pptrials.mat'), 'pptrials');
       for ii = 1:length(pptrials)
           if isfield(pptrials{ii}, 'pixelAngle')
               params.pixelAngle(ii) = pptrials{1,ii}.pixelAngle;
               pptrials{1,ii}.pxAngle = pptrials{1,ii}.pixelAngle;
            else
                params.pixelAngle(ii) = pptrials{1,ii}.pxAngle;
            end
       end
       trialChar = buildTrialCharStruct(pptrials,newEyeris);
       trialChar = add_fields_trialChar(trialChar,pptrials,params);
       if newEyeris
           [~, ~, valid, ~] = countingTrials_newEyeris(pptrials, em, params, figures);
       else
           [~, ~, valid, ~] = countingTrials(pptrials, em, params, figures);
       end
       condition = Psychometric_Graphing(params,params.ecc,...
                                  valid, trialChar, params.crowded, title_str);
       
       all_thresh_P(ii_p) = condition.thresh;
       figure(1)
       plot([1],[all_thresh_P(ii_p)],'o','MarkerSize',10,'MarkerFaceColor','g')
       hold on
   end
   
%    for ii_bp = 1:length(BP_patients)
%        
%        params.samplingRate = 1000/1000;
%        newEyeris = 1;
%        
%        em = [];
%        params.subject = BP_patients{ii_bp};
%        title_str = char(BP_patients{ii_bp});
%        pathToFile =  strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\patients\',char(BP_patients{ii_bp}),'\unCrowded');
%        load(fullfile(pathToFile,'pptrials.mat'), 'pptrials');
%        for ii = 1:length(pptrials)
%            if isfield(pptrials{ii}, 'pixelAngle')
%                params.pixelAngle(ii) = pptrials{1,ii}.pixelAngle;
%                pptrials{1,ii}.pxAngle = pptrials{1,ii}.pixelAngle;
%             else
%                 params.pixelAngle(ii) = pptrials{1,ii}.pxAngle;
%            end
%            pptrials{ii}.Unstabilized = 0;
%        end
%        trialChar = buildTrialCharStruct(pptrials,newEyeris);
%        trialChar = add_fields_trialChar(trialChar,pptrials,params);
%        if newEyeris
%            [~, ~, valid, ~] = countingTrials_newEyeris(pptrials, em, params, figures);
%        else
%            [~, ~, valid, ~] = countingTrials(pptrials, em, params, figures);
%        end
%        condition = Psychometric_Graphing(params,params.ecc,...
%                                   valid, trialChar, params.crowded, title_str);
%        
%        all_thresh_bP(ii_bp) = condition.thresh;
%        figure(1)
%        plot([2],[all_thresh_bP(ii_bp)],'o','MarkerSize',10,'MarkerFaceColor','b')
%        hold on
%    end
%    
   for ii_c = 1:length(controls)
       em = [];
       if ii_c < 10
           newEyeris = 0;
           params.samplingRate = 1000/341;
       else
           newEyeris = 1;
           params.samplingRate = 1000/1000;
       end
       params.subjects = controls{ii_c};
       title_str = char(controls{ii_c});
       if ii_c > 9
            pathToFile =  strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\controls\',char(controls{ii_c}),'\unCrowded');
       else
           pathToFile =  strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\controls\HUX_C\',char(controls{ii_c}),'\unCrowded');
       end
       load(fullfile(pathToFile,'pptrials.mat'), 'pptrials');
       for ii = 1:length(pptrials)
           if isfield(pptrials{ii}, 'pixelAngle')
               params.pixelAngle(ii) = pptrials{1,ii}.pixelAngle;
               pptrials{1,ii}.pxAngle = pptrials{1,ii}.pixelAngle;
            else
                params.pixelAngle(ii) = pptrials{1,ii}.pxAngle;
           end
           pptrials{ii}.Unstabilized = 0;
       end
       trialChar = buildTrialCharStruct(pptrials,newEyeris);
       trialChar = add_fields_trialChar(trialChar,pptrials,params);
       if newEyeris
           [~, ~, valid, ~] = countingTrials_newEyeris(pptrials, em, params, figures);
       else
           [~, ~, valid, ~] = countingTrials(pptrials, em, params, figures);
       end
       condition = Psychometric_Graphing(params,params.ecc,...
                                  valid, trialChar, params.crowded, title_str);
                              
       all_thresh_C(ii_c) = condition.thresh;
       figure(1)
       plot([2],[all_thresh_C(ii_c)],'o','MarkerSize',10,'MarkerFaceColor','r')
       hold on
   end
   
   for ii_huxc = 1:length(hux_controls)
       em = [];
       newEyeris = 0;
       params.samplingRate = 1000/341;
       params.subjects = hux_controls{ii_huxc};
       title_str = char(hux_controls{ii_huxc});
       pathToFile =  strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\controls\HUX_C\',char(hux_controls{ii_huxc}),'\unCrowded');
       load(fullfile(pathToFile,'pptrials.mat'), 'pptrials');
       for ii = 1:length(pptrials)
           if isfield(pptrials{ii}, 'pixelAngle')
               params.pixelAngle(ii) = pptrials{1,ii}.pixelAngle;
               pptrials{1,ii}.pxAngle = pptrials{1,ii}.pixelAngle;
            else
                params.pixelAngle(ii) = pptrials{1,ii}.pxAngle;
            end
       end
       trialChar = buildTrialCharStruct(pptrials,newEyeris);
       trialChar = add_fields_trialChar(trialChar,pptrials,params);
       [~, ~, valid, ~] = countingTrials(pptrials, em, params, figures);
       condition = Psychometric_Graphing(params,params.ecc,...
                                  valid, trialChar, params.crowded, title_str);
                              
       all_thresh_huxC(ii_huxc) = condition.thresh;
       figure(1)
       plot([3],[all_thresh_huxC(ii_huxc)],'o','MarkerSize',10,'MarkerFaceColor','m')
       hold on
   end
   
   
   
   figure(1)
   std_err_thresh_P = std(all_thresh_P)/sqrt(length(all_thresh_P));
   std_err_thresh_bP = std(all_thresh_bP)/sqrt(length(all_thresh_bP));
   std_err_thresh_huxC = std(all_thresh_huxC)/sqrt(length(all_thresh_huxC));
   std_err_thresh_C = std(all_thresh_C)/sqrt(length(all_thresh_C));
   errorbar([mean(all_thresh_P) mean(all_thresh_bP) mean(all_thresh_C) mean(all_thresh_huxC)],...
       [std_err_thresh_P std_err_thresh_bP std_err_thresh_C std_err_thresh_huxC],...
       'o', 'Color', 'k','LineWidth',3)
   set(gca, 'XTick', [1:1:4], 'XTickLabel', {'SZ'; 'BP'; 'HC';'HUX-C'}, 'FontSize', 12);
   ylabel('Threshold [arcmin]')
   set(gca,'fontsize',27,'FontWeight','Bold')
   xlim([0 5])
   
   figure()
   hold on
   errorbar([mean(all_thresh_P) mean(all_thresh_C) ],...
       [std_err_thresh_P  std_err_thresh_C],...
       'o', 'Color', 'k','LineWidth',3)
   set(gca, 'XTick', [1:1:2], 'XTickLabel', {'SZ'; 'HC'}, 'FontSize', 12);
   ylabel('Threshold [arcmin]')
   set(gca,'fontsize',15,'FontWeight','Bold')
   xlim([0 3])
   
   [p,h] = ranksum(all_thresh_P,all_thresh_C);
end