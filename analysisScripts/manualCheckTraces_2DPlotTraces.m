em = [];
newEyeris = 1;
figures = struct(...
    'FIGURE_ON', 0,... %Show individual trial with traces in 2D space
    'VIDEO', 0,... %Wills save video of FIGURE_ON figures
    'FIXATION_ANALYSIS',0,... %Plots psychometric curves
    'saveFixMat',0,...
    'QUICK_ANALYSIS',0,... %Only plots psychometric curves
    'CHECK_TRACES',1,... %will plot traces
    'HUX_RUN',1,... %will discard trials differently
    'FOLDED', 0,...
    'FIXED_SIZE', 0,...
    'COMPARE_SPANS',0); %0 = all spans, 1 = small spans, 2 = large spans

machine = {'D'};

subject = {'FE-C05'};
condition = {'uncrowded'};

params = struct(...
    'newEyeris',newEyeris,...
    'subject',subject,...
    'patient',0,...
    'blankScreenAnalysis',0,...
    'nBoots', 1,... %Number of boots for psychometric fits
    'crowded', false, ... %Gets rewritten based on currentCond
    'DMS',true, ...
    'D', false, ...
    'MS', false,...
    'fixation',false,...
    'machine',machine, ... %DPI "A" vs dDPI "D"
    'session', '1', ... %Which number session
    'spanMax', 100,... %The Maximum Span
    'spanMin', 0,...%The Minimum Span
    'compSpans', 0,...
    'ecc',0,...
    'samplingRate' , 1000/341,... % 1000/341 for old eyeris
    'stabilization','Unstabilized');
if params.patient
    if params.blankScreenAnalysis
        if newEyeris
            pathToFile = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\newEyeris_analysisScripts\patients\',char(subject),'\unCrowded\blankScreenData');
        else
            pathToFile =  strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\patients\',char(subject),'\unCrowded');
        end
    else
        pathToFile =  strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\patients\',char(subject),'\unCrowded');
    end
else
    if params.blankScreenAnalysis
        pathToFile = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\newEyeris_analysisScripts\controls\',char(subject),'\unCrowded\blankScreenData');
    else
        pathToFile =  strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\controls\',char(subject),'\unCrowded');
    end
end

if ~params.blankScreenAnalysis
    if strcmp(subject{1}, 'PC-S16')% || strcmp(subject{1}, 'PC-S07')
        if params.DMS
            load(fullfile(pathToFile,'task_clean.mat'), 'all_clean');
        elseif params.fixation
            load(fullfile(pathToFile,'fix_clean.mat'), 'all_clean');
        end

    end
end

load(fullfile(pathToFile,'pptrials.mat'), 'pptrials');

for ii = 1:length(pptrials)
    if isfield(pptrials{ii}, 'pixelAngle')
        params.pixelAngle(ii) = pptrials{1,ii}.pixelAngle;
        pptrials{1,ii}.pxAngle = pptrials{1,ii}.pixelAngle;
    else
        params.pixelAngle(ii) = pptrials{1,ii}.pxAngle;
    end
    if strcmp(subject{1}, 'FE-B01') 
        pptrials{ii}.Unstabilized = 0;
    end
    if newEyeris
        pptrials{ii}.x.velocity = p_calculateVelocity(pptrials{ii}.x.position, 'prefilter', true, ...
          'postfilter', true, 'postvalue', true, 'order', 3, 'smoothing', bitor( double(round(51/1000*pptrials{ii}.eye_spf)), 1)  ) * pptrials{ii}.eye_spf / 1000;
        pptrials{ii}.y.velocity = p_calculateVelocity(pptrials{ii}.y.position, 'prefilter', true, ...
          'postfilter', true, 'postvalue', true, 'order', 3, 'smoothing', bitor( double(round(51/1000*pptrials{ii}.eye_spf)), 1)  ) * pptrials{ii}.eye_spf / 1000;
    end
end

trialChar = buildTrialCharStruct(pptrials,newEyeris);
trialChar = add_fields_trialChar(trialChar,pptrials,params);

if newEyeris
    params.samplingRate = 1000/pptrials{1}.eye_spf;
    [traces, counter, valid, em] = countingTrials_newEyeris(pptrials, em, params, figures);
else
    params.samplingRate = 1000/341;
    [traces, counter, valid, em] = countingTrials(pptrials, em, params, figures);
end

if figures.CHECK_TRACES
    if params.patient
        filepath = 'C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\patients\manMark';
    else
        filepath = 'C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\controls\manMark';
    end
    if params.D
        nameFile = 'pptrials.mat';
        driftTRIALS = find(valid.d > 0 & trialChar.TimeFixationON == 0);
        [singleSegs,x,y] = getDriftSegmentInfo (driftTRIALS, pptrials, params);
        pptrials = checkMSTraces_new(driftTRIALS, pptrials, params, filepath, singleSegs, nameFile);
        %         save(fullfile(filepath, 'pptrials.mat'), 'pptrials')
    elseif params.DMS
        nameFile = 'pptrials.mat';
        dms_TRIALS = find(valid.dms > 0 );
        [singleSegs,x,y] = getDriftSegmentInfo (dms_TRIALS, pptrials, params);
        pptrials = checkMSTraces_new(dms_TRIALS, pptrials, params, filepath, singleSegs, nameFile,newEyeris);
        fprintf('Saving pptrials\n')
        %         save(fullfile(filepath, 'pptrials.mat'), 'pptrials')
    elseif params.fixation
        nameFile = 'pptrials.mat';
        fixTRIALS = find(valid.fixation == 1);
        [singleSegs,x,y] = getDriftSegmentInfo(fixTRIALS, pptrials, params);
        pptrials = checkMSTraces_new(fixTRIALS, pptrials, params, filepath, singleSegs, nameFile,newEyeris);
        pptrials_fix = pptrials;
        save(fullfile(filepath, 'pptrials.mat'), 'pptrials')
    end
    
   
end

if params.plot_2D_traces
    if params.DMS
        valid_dms_ts = find(valid.dms == 1);
    elseif params.fixation
            valid_dms_ts = find(valid.fixation == 1);
    end
    for each_t = 1:length(valid_dms_ts)
        id = valid_dms_ts(each_t);
        if newEyeris
             timeOn = 1;
             timeOff = length(pptrials{id}.x.position);
        else
            if params.blankScreenAnalysis
                timeOn = round((pptrials{id}.TimeTargetON-400)/params.samplingRate);
                timeOff = round(pptrials{id}.TimeTargetON/params.samplingRate);
            else
                timeOn = round(pptrials{id}.TimeTargetON/params.samplingRate);
                timeOff = floor(min(pptrials{id}.TimeTargetOFF/params.samplingRate, pptrials{id}.ResponseTime/params.samplingRate));
            end
        end
        x = pptrials{id}.x.position(ceil(timeOn):floor(timeOff));
        y = pptrials{id}.y.position(ceil(timeOn):floor(timeOff));
%         x = x(x>-30&x<30);y = y(x>-30&x<30);x = x(y>-30&y<30);y = y(y>-30&y<30);
        
        [x_all,y_all] = deal([]);
        for all = 1:length(x)
            if all == 1
                diff_x = x(1);
                diff_y = y(1);
                x(1) = 0;
                y(1) = 0;
                x_all(1,end+1) = x(all);
                y_all(1,end+1) = y(all);
            else
                x_all(1,end+1) = x(all)-diff_x;
                y_all(1,end+1) = y(all)-diff_y;
            end
            
        end
        if each_t >= 70
            pause = 0;
        end
        subplot(2,1,1,'align')
        plot(x,y)
        xlim([-20 20])
        ylim([-20 20])
        
        hold on
        subplot(2,1,2,'align')
        plot(x,'r')
        hold on
        plot(y,'b')
        title(string(each_t))
        
%         contType = input('Trash trial(1)? \n Next Trial(enter)');
%         cont = [];
%         if contType == 1
%             mark = input('which trial to trash?');
%             id_0 = valid_dms_ts(mark);
%             if params.DMS
%                 valid.dms(id_0) = 0;
%             elseif params.fixation
%                 valid.fixation(id_0) = 0;
%             end
%         end
        
        
        input ''
        clf
        
        
    end
if params.patient
    pathToSub = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\patients\',params.subject,'\','unCrowded');
else
    pathToSub = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\controls\',params.subject,'\','unCrowded');
end
if ~exist(pathToSub,'dir')
    mkdir(pathToSub);
end
Fname = sprintf(strcat('clean_data_',params.subject,'.mat'));
if params.DMS
    all_clean = valid.dms;
elseif params.fixation
    all_clean = valid.fixation;
end
save(fullfile(pathToSub,Fname),'all_clean');


end

if params.scanpath_analysis
    valid_dms_ts = find(valid.dms == 1);
    total_trials = length(valid_dms_ts);
    fix_idx = find(valid.dms >0 & trialChar.TimeFixationON ~= 0);
    trial_list = {};
    for num_ts = 1:total_trials
        idx = valid_dms_ts(num_ts);
        trial = pptrials{num_ts};
        if newEyeris
            timeOn = 1;
            timeOff = length(pptrials{id}.x.position);
        else
            timeOn = round(trial.TimeTargetON/params.samplingRate);
            timeOff = floor(min(trial.TimeTargetOFF/params.samplingRate, trial.ResponseTime/params.samplingRate));
        end
        trial_list{end+1} = trial;
        trial.sRate = 1000;
        Events = findEvents(trial.velocity, ...
                            'minvel', 120, ...
                            'minterval', 15, ...
                            'mduration', 15, ...
                            'sRate', trial.sRate );
%% use eval_length
%         [Length,emaps] = evalLength(trial,Events,'Start', 'timeOn','Duration', 'timeOff-timeOn')
    end
    [Length,emaps] = evalLength(trial_list,Events);
end

function length = calc_distance(xs,yx,xe,ye)
    startEndLocs = [xs,ys;...
                    xe,ye];
    length = pdist(startEndLocs,'euclidean');

end