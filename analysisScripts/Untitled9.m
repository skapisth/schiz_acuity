function bcea = get_bcea(x, y)
% smoothing: 41
% cutseg: how much of the drift do you wanna cut from the beginning and end
% maxSpeed (arcmin): 180 

% compute drift span
% mx = mean(x(cutseg:end-cutseg));
% my = mean(y(cutseg:end-cutseg));
% spanV1 = max(sqrt((x - mx).^2 + (y - my).^2));
x = x(~isnan(x));
y = y(~isnan(y));
x2 = 2.291; %chi-square variable with two degrees of freedom, encompasing 68% of the highest density points
bceaMatrix =  2*x2*pi * std(x) * std(y) * (1-corrcoef(x,y).^2).^0.5; %smaller BCEA correlates to more stable fixation 
bcea = abs(bceaMatrix(1,2));

end
