function [condition] = Psychometric_Graphing(params, uEcc, valid, data, ~,  title_str)

fprintf('%s: psychometric fitting (normal)\n', datestr(now));

uEcc = round(uEcc);
condition = buildConditionStruct(uEcc);
xl = [0.1, 11];
yl = [0, 1.05];
gamma = 0.25;
performanceX = [];
%         if any(data.Response == 3 | data.Response == 4) %%NOTE: Why do this check
%             gamma = 0.25; % 4-afc
%         end

if params.DMS
    emType = valid.dms;
elseif params.MS
    emType = valid.ms;
else
    emType = valid.d;
end

for ui = 1:length(uEcc) %9 eccentricity for crowded condition
    useTrials = emType & (data.TargetEccentricity == uEcc(ui));
    % Define parameters
%     logmar = log10(params.pixelAngle(ii) * double(data.TargetStrokewidth(useTrials)));
%     mar = 10.^logmar;
    corr = data.Correct(useTrials);
    
%     tarSW = data.TargetStrokewidth(useTrials);
%     corrSW4 = corr(tarSW == 4);
    
    if sum(useTrials) < 10
        warning('There are too few trials to graph!')
        fprintf('Not Enough Trials for Graph!')
        condition(ui).thresh = NaN;
        condition(ui).threshSW4 = NaN;
        condition(ui).sizesTestedX = NaN;
        condition(ui).performanceAtSizesY = NaN;
        condition(ui).par = NaN;
        condition(ui).threshB = NaN;
        condition(ui).bootMS = [NaN, NaN];
        condition(ui).logBootMS = [NaN,NaN];
        condition(ui).Condition = title_str;
        return;
    end
    if params.crowded == 1 %% crowded condition "true"
        flankerdis = 1.4;
        ccdist = (data.TargetSize(useTrials)*flankerdis);
        [~, ~, ~] = psyfitCrowding(ccdist, corr, 'DistType', 'Normal',...
            'PlotHandle', ui, 'Xlim', xl, 'Ylim', yl, 'Boots', params.nBoots,...
            'Chance', gamma, 'Extra');
        graphTitle = title_str;
        title(graphTitle,'Interpreter','none');
        xlabel('Flanker Distance C-C (arcmin)'); % Flanker Distance instead of sw
        set(gcf, 'Color', 'w');
        set(gca, 'FontSize', 12, 'FontWeight', 'bold');
        
    end
    
    if sum(useTrials) > 1
        sw = round(data.TargetSize(useTrials),1);
        
        [thresh, par, threshB, xValues, yValues, chiSq] = psyfitCrowding(sw, corr, 'DistType', 'Normal',...
            'PlotHandle', 10+ui, 'Xlim', xl, 'Ylim', yl, 'Boots', 2,...
            'Chance', gamma, 'Extra');
            performanceX = [];%yValues(xSize);
            actualSize = [];

        x = round(double(unique(sw)),1);
        set(gca, 'XTick', x, 'xticklabel', {x})
        
        xlimit = max(x)+1;
        xlim([0,xlimit])
        
        title_str = strcat(title_str,sprintf('; Threshold - %f',thresh),...
                    sprintf('; N - %i', length(sw)),sprintf('; p - %f',chiSq))
        graphTitle = title_str;
        title(graphTitle,'Interpreter','none');
        
        xlabel('SW in arcmin');
        ylabel('Proportion Correct');
        set(gcf, 'Color', 'w');
        set(gca, 'FontSize', 16);
        
        
        condition(ui).thresh = thresh;
        condition(ui).threshSW4 = performanceX;
        condition(ui).actualSizeSW4 = actualSize;
        condition(ui).sizesTestedX = xValues;
        condition(ui).performanceAtSizesY = yValues;
        condition(ui).par = par;
        condition(ui).threshB = threshB;
        condition(ui).bootMS = [nanmean(threshB), nanstd(threshB)];
        condition(ui).logBootMS = [nanmean(log10(threshB)), nanstd(log10(threshB))];
        condition(ui).Condition = title_str;
    elseif sum(useTrials) <= 1
        fprintf('Not Enough Trials for Graph!')
        condition(ui).thresh = NaN;
        condition(ui).threshSW4 = NaN;
        condition(ui).sizesTestedX = NaN;
        condition(ui).performanceAtSizesY = NaN;
        condition(ui).par = NaN;
        condition(ui).threshB = NaN;
        condition(ui).bootMS = [NaN, NaN];
        condition(ui).logBootMS = [NaN,NaN];
        condition(ui).Condition = title_str;
        return;
    else
        graphTitle = title_str;
        title(graphTitle,'Interpreter','none');
    end
    
end

data.psyfitsNorm.uncrowded = condition;

end

