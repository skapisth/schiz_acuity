%% params
clc;clear all;
subjects = {'PC-S01','PC-S02','PC-S04','PC-S07','PC-S12','PC-S16','FE-S01','FE-S02','FE-S09'};
% subjects = {'HUX4','HUX10','HUX12','HUX18','HUX21','HUX22','HUX23','Hux24','MP'};
% subjects = {'PC-S04'};
patient = 1;
huxC = 0;
newEyeris = 0;
%% subtract the mean from x and y
%% use 300ms chunks instead of 250
time_threshold = 250;
machine = {'D'};
figures = struct(...
                    'FIGURE_ON', 0,... %Show individual trial with traces in 2D space
                    'VIDEO', 0,... %Wills save video of FIGURE_ON figures
                    'FIXATION_ANALYSIS',0,... %Plots psychometric curves
                    'saveFixMat',1,...
                    'QUICK_ANALYSIS',0,... %Only plots psychometric curves
                    'CHECK_TRACES',0,... %will plot traces
                    'HUX_RUN',1,... %will discard trials differently
                    'FOLDED', 0,...
                    'FIXED_SIZE', 0,...
                    'COMPARE_SPANS',0); %0 = all spans, 1 = small spans, 2 = large spans

params = struct(...
                    'patient',1,...
                    'nBoots', 1,... %Number of boots for psychometric fits
                    'crowded', false, ... %Gets rewritten based on currentCond
                    'DMS',false, ...
                    'D', false, ...
                    'MS', true,...
                    'fixation',false,...
                    'machine',machine, ... %DPI "A" vs dDPI "D"
                    'session', '1', ... %Which number session
                    'spanMax', 100,... %The Maximum Span
                    'spanMin', 0,...%The Minimum Span
                    'compSpans', 0,...
                    'ecc',0,...
                    'samplingRate' , 1000/341,...
                    'stabilization','Unstabilized');
      
for ii_s = 1:length(subjects)
    params.subject = subjects{ii_s};
    title_str = char(subjects{ii_s});
    if patient
        pathToFile =  strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\patients\',char(subjects{ii_s}),'\unCrowded');     
        
        if length(subjects) > 1
            if ii_s > 3
                newEyeris = 1;
                Fs = 1000;
                params.samplingRate = 1000/Fs;
            else
                newEyeris = 0;
                Fs = 341;
                params.samplingRate = 1000/Fs;
            end
        else
            if newEyeris
                Fs = 1000;
                params.samplingRate = 1000/Fs;
            else
                Fs = 341;
                params.samplingRate = 1000/Fs;
            end

        end
    end
    if (patient == 0 && huxC == 1)
         pathToFile =  strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\controls\HUX_C\',char(subjects{ii_s}),'\unCrowded');
         newEyeris = 0;
         Fs = 341;
         params.samplingRate = 1000/Fs;
    end
    if (patient == 0 && huxC == 0)
         pathToFile =  strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\controls\',char(subjects{ii_s}),'\unCrowded');
         if ii_s > 1
            newEyeris = 1;
            Fs = 1000;
            params.samplingRate = 1000/Fs;
        else
            newEyeris = 0;
            Fs = 341;
            params.samplingRate = 1000/Fs;
        end
    end
    if newEyeris
        sample_threshold = time_threshold ;
    else
        sample_threshold = time_threshold / (1000/Fs); % use drift segments longer than this threshold in samples;
    end
%     if (strcmp(subjects{ii_s}, 'PC-S16') || strcmp(subjects{1}, 'PC-S07'))
%         if params.DMS
%             load(fullfile(pathToFile,'task_clean.mat'), 'all_clean');
%         elseif params.fixation
%             load(fullfile(pathToFile,'fix_clean.mat'), 'all_clean');
%         end
%         
%     end
    load(fullfile(pathToFile,'pptrials.mat'), 'pptrials');
    for ii = 1:length(pptrials)
        if isfield(pptrials{ii}, 'pixelAngle')
            params.pixelAngle(ii) = pptrials{1,ii}.pixelAngle;
            pptrials{1,ii}.pxAngle = pptrials{1,ii}.pixelAngle;
        else
            params.pixelAngle(ii) = pptrials{1,ii}.pxAngle;
        end
        pptrials{ii}.Unstabilized = 0;
    end
%     if (strcmp(subjects{1}, 'PC-S16')  || strcmp(subjects{1}, 'PC-S07'))
%         valid.dms = all_clean;
%     end
    
    trialChar = buildTrialCharStruct(pptrials,newEyeris);
    trialChar = add_fields_trialChar(trialChar,pptrials,params);
    em = [];
    if newEyeris
        [~, ~, valid, ~] = countingTrials_newEyeris(pptrials, em, params, figures);
    else
        [~, ~, valid, ~] = countingTrials(pptrials, em, params, figures);
    end
    
    idx = find(valid.fixation == 0);
    
    for val = 1:length(idx)
        pptrials_new{val} = pptrials{idx(val)};
    end
    
    for ts = 1:length(pptrials_new)
        plot(pptrials_new(ts).x.position,'r')
        hold on
        plot(pptrials_new(ts).y.position,'b')
        title(string(all))

        contType = input('drift trial(1)? ms trials (2)? \n Next Trial(enter)');
        cont = [];
        if contType == 1
            valid.(counter_save).x = all_long_drifts(all).x;
            all_long_drifts_clean(counter_save).y = all_long_drifts(all).y;
        end
        clf
    end
    
end