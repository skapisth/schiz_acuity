function plotOffsets_bothTasks(patients,controls)
    
    for ii_p = 1:length(patients)
        dir_path = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\patients\',patients{ii_p},'\matFiles\');
        file_to_read_fix = strcat('em_info_for_heatmaps_fix_',patients{ii_p},'.mat');
%         load(strcat(dir_path,file_to_read_fix))
%         avg_x = round(nanmean(em.allTraces.xALL),2);
%         avg_y = round(nanmean(em.allTraces.yALL),2);
%         s_e_locs = [avg_x,avg_y;0,0];
%         euc_dist = pdist(s_e_locs,'euclidean');
%         euc_offset_fix_p(ii_p) = euc_dist;
%         clear em;
        
        file_to_read_task = strcat('em_info_for_heatmaps_task_',patients{ii_p},'.mat');
        load(strcat(dir_path,file_to_read_task))
        avg_x = round(nanmean(em.allTraces.xALL),2);
        avg_y = round(nanmean(em.allTraces.yALL),2);
        s_e_locs = [avg_x,avg_y;0,0];
        euc_dist = pdist(s_e_locs,'euclidean');
        euc_offset_task_p(ii_p) = euc_dist;
        clear em;
%         figure(1)
%         plot([1,2],[euc_offset_fix_p(ii_p),euc_offset_task_p(ii_p)],'-o','color','g')
%         hold on
        
    end
    for ii_c = 1:length(controls)
        dir_path = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\controls\',controls{ii_c},'\matFiles\');
        file_to_read_fix = strcat('em_info_for_heatmaps_fix_',controls{ii_c},'.mat');
%         load(strcat(dir_path,file_to_read_fix))
%         avg_x = round(nanmean(em.allTraces.xALL),2);
%         avg_y = round(nanmean(em.allTraces.yALL),2);
%         s_e_locs = [avg_x,avg_y;0,0];
%         euc_dist = pdist(s_e_locs,'euclidean');
%         euc_offset_fix_c(ii_c) = euc_dist;
%         clear em;
%         plot([1],[euc_offset_fix_c(ii_c)],'o','color','b')
        
        file_to_read_task = strcat('em_info_for_heatmaps_',controls{ii_c},'.mat');
        load(strcat(dir_path,file_to_read_task))
        avg_x = round(nanmean(em.allTraces.xALL),2);
        avg_y = round(nanmean(em.allTraces.yALL),2);
        s_e_locs = [avg_x,avg_y;0,0];
        euc_dist = pdist(s_e_locs,'euclidean');
        euc_offset_task_c(ii_c) = euc_dist;
        clear em;
        plot([2],[euc_offset_task_c(ii_c)],'o','color','m')
        hold on
    end
%     figure(1)
%     std_err_fix_P = std(euc_offset_fix_p)/sqrt(length(euc_offset_fix_p));
%     std_err_fix_C = std(euc_offset_fix_c)/sqrt(length(euc_offset_fix_c));
%     
    std_err_task_P = std(euc_offset_task_p)/sqrt(length(euc_offset_task_p));
    std_err_task_C = std(euc_offset_task_c)/sqrt(length(euc_offset_task_c));
%     errorbar([mean(euc_offset_fix_p) mean(euc_offset_task_p)],...
%               [std_err_fix_P std_err_task_P],...
%               'o', 'Color', 'k', 'LineStyle', '-','LineWidth',3)
    hold on
%     errorbar([mean(euc_offset_fix_c) mean(euc_offset_task_c)],...
%               [std_err_fix_C std_err_task_C],...
%               'o', 'Color', 'b', 'LineStyle', '-','LineWidth',3)
    set(gca, 'XTick', [1:1:2], 'XTickLabel', {'Fixation'; 'Task'}, 'FontSize', 12);
    ylabel('Euclidean offset [arcmin]')
    set(gca,'fontsize',27,'FontWeight','Bold')
    xlim([0 3])
    
    [p_task,h_task,stats1] = ranksum(euc_offset_task_p,euc_offset_task_c);
    [p_fix,h_fix,stats2] = ranksum(euc_offset_fix_p,euc_offset_fix_c);
    
end