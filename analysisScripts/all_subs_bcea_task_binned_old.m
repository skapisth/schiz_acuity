function all_subs_bcea_task_binned_old(patients,controls)
    
    cP = jet(length(patients));
    cC = winter(length(controls));
    for ii_p = 1:length(patients)
        dir_path = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\patients\',patients{ii_p},'\matFiles\');
        file_to_read = strcat('em_info_cut_drift_',patients{ii_p},'.mat');
        load(strcat(dir_path,file_to_read))
        
        all_bcea_P_b1(ii_p) = abs(log(em.bcea_b1/60));
        all_bcea_P_b2(ii_p) = abs(log(em.bcea_b2/60));
        all_bcea_P_b3(ii_p) = abs(log(em.bcea_b3/60));
        all_bcea_P_b4(ii_p) = abs(log(em.bcea_b4/60));
        all_bcea_P_b5(ii_p) = abs(log(em.bcea_b5/60));
        clear em
        figure(1)
%         plot([1,2,3,4,5],[all_bcea_P_b1(ii_p),all_bcea_P_b2(ii_p),...
%               all_bcea_P_b3(ii_p),all_bcea_P_b4(ii_p),all_bcea_P_b5(ii_p)],'-o','color''g')
       plot([1,2,3,4,5],[all_bcea_P_b1(ii_p),all_bcea_P_b2(ii_p),...
            all_bcea_P_b3(ii_p),all_bcea_P_b4(ii_p),all_bcea_P_b5(ii_p)],'-o','color','g')
        hold on
    end
    for ii_c = 1:length(controls)
        dir_path = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\controls\',controls{ii_c},'\matFiles\');
        file_to_read = strcat('em_info_cut_drift_',controls{ii_c},'.mat');
        load(strcat(dir_path,file_to_read))
        
        all_bcea_C_b1(ii_c) = abs(log(em.bcea_b1/60));
        all_bcea_C_b2(ii_c) = abs(log(em.bcea_b2/60));
        all_bcea_C_b3(ii_c) = abs(log(em.bcea_b3/60));
        all_bcea_C_b4(ii_c) = abs(log(em.bcea_b4/60));
        all_bcea_C_b5(ii_c) = abs(log(em.bcea_b5/60));
        
        clear em
        figure(1)
        plot([1,2,3,4,5],[all_bcea_C_b1(ii_c),all_bcea_C_b2(ii_c),...
                          all_bcea_C_b3(ii_c),all_bcea_C_b4(ii_c),all_bcea_C_b5(ii_c)],...
                          'o','color','m')
        hold on
    end
    
    
    figure(1)
    std_err_bcea_C_b1 = std(all_bcea_C_b1)/sqrt(length(all_bcea_C_b1));
    std_err_bcea_C_b2 = std(all_bcea_C_b2)/sqrt(length(all_bcea_C_b2));
    std_err_bcea_C_b3 = std(all_bcea_C_b3)/sqrt(length(all_bcea_C_b3));
    std_err_bcea_C_b4 = std(all_bcea_C_b4)/sqrt(length(all_bcea_C_b4));
    std_err_bcea_C_b5 = std(all_bcea_C_b5)/sqrt(length(all_bcea_C_b5));
    
    errorbar([mean(all_bcea_C_b1) mean(all_bcea_C_b2)...
              mean(all_bcea_C_b3) mean(all_bcea_C_b4) mean(all_bcea_C_b5)],...
                  [std_err_bcea_C_b1 std_err_bcea_C_b2...
                   std_err_bcea_C_b3 std_err_bcea_C_b4 std_err_bcea_C_b5],...
                   'o', 'Color', 'm', 'LineStyle', '-','LineWidth',3)
   set(gca, 'XTick', [1:1:5], 'XTickLabel', {'0-100'; '100-200';'200-300';'300-400';'400-500'}, 'FontSize', 12);
   xlim([0 6])
%    ylim([0 5])
   ylabel('bcea (deg^2)')
   set(gca,'fontsize',27,'FontWeight','Bold')
   set(gca, 'YScale', 'log')
   

end
