
function plotHistograms(em,title_str)
    
    [all_span,all_curv,all_PRL,all_mn_speed,all_vel,all_stim_size,all_amp] = deal([]);
    
    ln_em = numel(fieldnames(em.ecc_0));
    
    
    for t_size = 1:ln_em
        f_name = sprintf('strokeWidth_%i',t_size);
        if ~isfield(em.ecc_0,f_name)
            ln_em = ln_em+1;
            continue
        end
        
        all_chars = eval(strcat('em.ecc_0.',f_name));
        
       
        
        for all = 1:length(all_chars.response)
            all_span(1,end+1) = all_chars.span(all);
            all_curv(1,end+1) = all_chars.curvature{all};
            all_PRL(1,end+1) = all_chars.prlDistance(all);
            all_mn_speed(1,end+1) = all_chars.mn_speed{all};
            all_vel(1,end+1) = all_chars.velocity(all);
            all_stim_size(1,end+1) = all_chars.stimulusSize;
            all_amp(1,end+1) = all_chars.amplitude(all);
            
        end
        
    end
    
   [uniqueA1,~,k] = unique(all_stim_size(1,:));
    numberUniqueA1 = numel(uniqueA1);
    mean_span = zeros(size(all_stim_size,1),numberUniqueA1);
    stdErr_span = zeros(size(all_stim_size,1),numberUniqueA1);
    
    mean_curv = zeros(size(all_stim_size,1),numberUniqueA1);
    stdErr_curv = zeros(size(all_stim_size,1),numberUniqueA1);
    
    mean_speed = zeros(size(all_stim_size,1),numberUniqueA1);
    stdErr_speed = zeros(size(all_stim_size,1),numberUniqueA1);
    
    mean_vel = zeros(size(all_stim_size,1),numberUniqueA1);
    stdErr_vel = zeros(size(all_stim_size,1),numberUniqueA1);
    
    mean_amp = zeros(size(all_stim_size,1),numberUniqueA1);
    stdErr_amp = zeros(size(all_stim_size,1),numberUniqueA1);
    
    mean_prl = zeros(size(all_stim_size,1),numberUniqueA1);
    stdErr_prl = zeros(size(all_stim_size,1),numberUniqueA1);
    
    
    for nu = 1:numberUniqueA1
        indexToThisUniqueValue = (nu==k)';
        
        val_span = all_span(:,indexToThisUniqueValue);
        val_span = val_span(~isnan(val_span));
        val_curv = all_curv(:,indexToThisUniqueValue);
        val_curv = val_curv(~isnan(val_curv));
        val_speed = all_mn_speed(:,indexToThisUniqueValue);
        val_speed = val_speed(~isnan(val_speed));
        val_vel = all_vel(:,indexToThisUniqueValue);
        val_vel = val_vel(~isnan(val_vel));
        val_amp = all_amp(:,indexToThisUniqueValue);
        val_amp = val_amp(~isnan(val_amp));
        val_prl = all_PRL(:,indexToThisUniqueValue);
        val_prl = val_prl(~isnan(val_prl));
        
        mean_span(:,nu) = mean(val_span,2);
        stdErr_span(:,nu) = mean(sqrt(val_span .* (1-val_span)/length(val_span)));
        
        mean_curv(:,nu) = mean(val_curv,2);
        stdErr_curv(:,nu) = mean(sqrt(val_curv .* (1-val_curv)/length(val_curv)));
        
        if ~isempty(val_speed)
        
            mean_speed(:,nu) = mean(val_speed,2);
            stdErr_speed(:,nu) = mean(sqrt(val_speed .* (1-val_speed)/length(val_speed)));
%         else
%             continue
        end
        
        mean_vel(:,nu) = mean(val_vel,2);
        stdErr_vel(:,nu) = mean(sqrt(val_vel .* (1-val_vel)/length(val_vel)));
        
        mean_amp(:,nu) = mean(val_amp,2);
        stdErr_amp(:,nu) = mean(sqrt(val_amp .* (1-val_amp)/length(val_amp)));
        
        mean_prl(:,nu) = mean(val_prl,2);
        stdErr_prl(:,nu) = mean(sqrt(val_prl .* (1-val_prl)/length(val_prl)));
    end
    figure()
    errorbar(unique(all_stim_size),mean_span,stdErr_span,'o', 'Color', 'k', 'LineStyle', '-','LineWidth',3)
    xlabel('Stim Size')
    ylabel('Drift span [arcmin]')
    set(gca,'fontsize',27,'FontWeight','Bold')
    graphTitle = title_str;
    title(graphTitle,'Interpreter','none');
    set(gca,'fontsize',27,'FontWeight','Bold')
    
    figure()
%     plot(all_stim_size,all_curv,'o')
    errorbar(unique(all_stim_size),mean_curv,stdErr_curv,'o', 'Color', 'k', 'LineStyle', '-','LineWidth',3)
    xlabel('Stim Size')
    ylabel('Drift curvature [arcmin^-1]')
    set(gca,'fontsize',27,'FontWeight','Bold')
    graphTitle = title_str;
    title(graphTitle,'Interpreter','none');
    set(gca,'fontsize',27,'FontWeight','Bold')
    
    figure()
%     plot(all_stim_size,all_mn_speed,'o')
    errorbar(unique(all_stim_size),mean_speed,stdErr_speed,'o', 'Color', 'k', 'LineStyle', '-','LineWidth',3)
    xlabel('Stim Size')
    ylabel('Mean drift speed [arcmins/s]')
    set(gca,'fontsize',27,'FontWeight','Bold')
    graphTitle = title_str;
    title(graphTitle,'Interpreter','none');
    
    figure()
%     plot(all_stim_size,all_vel,'o')
    errorbar(unique(all_stim_size),mean_vel,stdErr_vel,'o', 'Color', 'k', 'LineStyle', '-','LineWidth',3)
    xlabel('Stim Size')
    ylabel('Drift velocity ')
    set(gca,'fontsize',27,'FontWeight','Bold')
    graphTitle = title_str;
    title(graphTitle,'Interpreter','none');
    
    figure()
%     plot(all_stim_size,all_amp,'o')
    errorbar(unique(all_stim_size),mean_amp,stdErr_amp,'o', 'Color', 'k', 'LineStyle', '-','LineWidth',3)
    xlabel('Stim Size')
    ylabel('Drift amplitude [arcmin]')
    set(gca,'fontsize',27,'FontWeight','Bold')
    graphTitle = title_str;
    title(graphTitle,'Interpreter','none');
    
    figure()
%     plot(all_stim_size,all_PRL,'o')
    errorbar(unique(all_stim_size),mean_prl,stdErr_prl,'o', 'Color', 'k', 'LineStyle', '-','LineWidth',3)
    xlabel('Stim Size')
    ylabel('PRL')
    set(gca,'fontsize',27,'FontWeight','Bold')
    graphTitle = title_str;
    title(graphTitle,'Interpreter','none');
    
    
end