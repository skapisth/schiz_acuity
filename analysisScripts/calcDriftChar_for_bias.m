function [em] = calcDriftChar_for_bias(valueGroup,params,pptrials,em)

idx = valueGroup.idx;
strokeWidth = valueGroup.strokeWidth;
ecc = valueGroup.ecc;
drift_dur_limit_ms = 250;

counter = 1;
count_empty_drifts = 0;
for idxValue = 1:length(idx)
    i = idx(idxValue);
    dataStruct.id(counter) = i;
    if params.newEyeris
        timeOn = 1;
        timeOff = length(pptrials{i}.x.position);
    else
        timeOn = round(pptrials{i}.TimeTargetON/params.samplingRate);
        timeOff = floor(min(pptrials{i}.TimeTargetOFF/params.samplingRate, pptrials{i}.ResponseTime/params.samplingRate));
    end
    findms = find(pptrials{i}.microsaccades.start > timeOn & pptrials{i}.microsaccades.start < timeOff);
    if isempty(findms)
        find_d_segs = find(pptrials{i}.drifts.start <= timeOff);
        if isempty(find_d_segs)
            count_empty_drifts = count_empty_drifts+1;
            flag = 0;
            continue;
        end
        for each = 1:length(find_d_segs)
            d_dur = pptrials{i}.drifts.duration(find_d_segs(each));
            d_dur_ms = d_dur*params.samplingRate;
            if d_dur_ms >=drift_dur_limit_ms
                time_drift_start = pptrials{i}.drifts.start(find_d_segs(each));
                if (time_drift_start+d_dur > length(pptrials{i}.x.position))
                    time_corr = length(pptrials{i}.x.position);
                    x = pptrials{i}.x.position(ceil(time_drift_start):floor(time_corr)) + pptrials{i}.xoffset * params.pixelAngle(i);
                    y = pptrials{i}.y.position(ceil(time_drift_start):floor(time_corr)) + pptrials{i}.yoffset * params.pixelAngle(i);
                    dataStruct.x_d_bias{counter} = x;
                    dataStruct.y_d_bias{counter} = y;
                else
                    x = pptrials{i}.x.position(ceil(time_drift_start):floor(time_drift_start+d_dur)) + pptrials{i}.xoffset * params.pixelAngle(i);
                    y = pptrials{i}.y.position(ceil(time_drift_start):floor(time_drift_start+d_dur)) + pptrials{i}.yoffset * params.pixelAngle(i);
                    dataStruct.x_d_bias{counter} = x;
                    dataStruct.y_d_bias{counter} = y;
                end
                [ x, y] = checkXYArt(x, y, min(round(length(x)/100)),31);
                [ x, y] = checkXYBound(x, y, 60);
                [~, dataStruct.instSpX{counter},...
                    dataStruct.instSpY{counter},...
                    dataStruct.mn_speed{counter},...
                    dataStruct.driftAngle{counter},...
                    dataStruct.curvature{counter},...
                    dataStruct.varx{counter},...
                    dataStruct.vary{counter},...
                    dataStruct.span(counter), ...
                    dataStruct.amplitude(counter)] = getDriftChar(x, y, 41, 1, 250);
                counter = counter + 1;
            else
                continue
            end
        end
    else
        ms_start_sample = pptrials{i}.microsaccades.start(findms);
        samples_before_ms = ms_start_sample - timeOn;
        samples_after_ms = timeOff - ms_start_sample;
        find_d_segs = find(pptrials{i}.drifts.start <= timeOff);
        if isempty(find_d_segs)
            count_empty_drifts = count_empty_drifts+1;
            flag = 0;
            continue;
        end
        for each = 1:length(find_d_segs)
            d_dur = pptrials{i}.drifts.duration(find_d_segs(each));
            d_dur_ms = d_dur*params.samplingRate;
            needed_d_seg = drift_dur_limit_ms/params.samplingRate; %(300ms converted to samples)
            if samples_before_ms >= needed_d_seg
                if d_dur_ms >=drift_dur_limit_ms
                    time_drift_start = pptrials{i}.drifts.start(find_d_segs(each));
                    if (time_drift_start+d_dur > length(pptrials{i}.x.position))
                        time_corr = length(pptrials{i}.x.position);
                        x = pptrials{i}.x.position(ceil(time_drift_start):floor(time_corr)) + pptrials{i}.xoffset * params.pixelAngle(i);
                        y = pptrials{i}.y.position(ceil(time_drift_start):floor(time_corr)) + pptrials{i}.yoffset * params.pixelAngle(i);
                        
                        dataStruct.x_d_bias{counter} = x;
                        dataStruct.y_d_bias{counter} = y;
                    else
                        x = pptrials{i}.x.position(ceil(time_drift_start):floor(time_drift_start+d_dur)) + pptrials{i}.xoffset * params.pixelAngle(i);
                        y = pptrials{i}.y.position(ceil(time_drift_start):floor(time_drift_start+d_dur)) + pptrials{i}.yoffset * params.pixelAngle(i);
                        
                        dataStruct.x_d_bias{counter} = x;
                        dataStruct.y_d_bias{counter} = y;
                    end
                    [ x, y] = checkXYArt(x, y, min(round(length(x)/100)),31);
                    [ x, y] = checkXYBound(x, y, 60);
                    [~, dataStruct.instSpX{counter},...
                        dataStruct.instSpY{counter},...
                        dataStruct.mn_speed{counter},...
                        dataStruct.driftAngle{counter},...
                        dataStruct.curvature{counter},...
                        dataStruct.varx{counter},...
                        dataStruct.vary{counter},...
                        dataStruct.span(counter), ...
                        dataStruct.amplitude(counter)] = getDriftChar(x, y, 41, 1, 250);
                    counter = counter + 1;
                else
                    continue
                end
            end
            if samples_after_ms >= needed_d_seg
                if d_dur_ms >=drift_dur_limit_ms
                    time_drift_start = pptrials{i}.drifts.start(find_d_segs(each));
                    if (time_drift_start+d_dur > length(pptrials{i}.x.position))
                        time_corr = length(pptrials{i}.x.position);
                        x = pptrials{i}.x.position(ceil(time_drift_start):floor(time_corr)) + pptrials{i}.xoffset * params.pixelAngle(i);
                        y = pptrials{i}.y.position(ceil(time_drift_start):floor(time_corr)) + pptrials{i}.yoffset * params.pixelAngle(i);
                        
                        dataStruct.x_d_bias{counter} = x;
                        dataStruct.y_d_bias{counter} = y;
                    else
                        x = pptrials{i}.x.position(ceil(time_drift_start):floor(time_drift_start+d_dur)) + pptrials{i}.xoffset * params.pixelAngle(i);
                        y = pptrials{i}.y.position(ceil(time_drift_start):floor(time_drift_start+d_dur)) + pptrials{i}.yoffset * params.pixelAngle(i);
                        
                        dataStruct.x_d_bias{counter} = x;
                        dataStruct.y_d_bias{counter} = y;
                    end
                    [ x, y] = checkXYArt(x, y, min(round(length(x)/100)),31);
                    [ x, y] = checkXYBound(x, y, 60);
                    [~, dataStruct.instSpX{counter},...
                        dataStruct.instSpY{counter},...
                        dataStruct.mn_speed{counter},...
                        dataStruct.driftAngle{counter},...
                        dataStruct.curvature{counter},...
                        dataStruct.varx{counter},...
                        dataStruct.vary{counter},...
                        dataStruct.span(counter), ...
                        dataStruct.amplitude(counter)] = getDriftChar(x, y, 41, 1, 250);
                counter = counter + 1;
                else
                    continue
                end
            end
        end
    end
end
em.(ecc).(strokeWidth) = dataStruct;
end
