function all_subs_bcea_fix(patients,controls)
    
    cP = jet(length(patients));
    cC = winter(length(controls));
    for ii_p = 1:length(patients)
        dir_path = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\patients\',patients{ii_p},'\matFiles\');
        file_to_read = strcat('fix_results_cut_drift_',patients{ii_p},'.mat');
        load(strcat(dir_path,file_to_read))
        
        all_bcea_P(ii_p) = abs(log(fixation.bcea/60));
        clear fixation
        figure(1)
        plot([1],[all_bcea_P(ii_p)],'o','MarkerSize',10,'MarkerFaceColor',cP(ii_p,:))
        hold on
    end
    for ii_c = 1:length(controls)
        dir_path = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\controls\',controls{ii_c},'\matFiles\');
        file_to_read = strcat('fix_results_cut_drift_',controls{ii_c},'.mat');
        load(strcat(dir_path,file_to_read))
        
        all_bcea_C(ii_c) = abs(log(fixation.bcea/60));
        clear fixation
        figure(1)
        plot([2],[all_bcea_C(ii_c)],'o','MarkerSize',10,'MarkerFaceColor',cC(ii_c,:))
        hold on
    end
    figure(1)
    std_err_bcea_P = std(all_bcea_P)/sqrt(length(all_bcea_P));
    std_err_bcea_C = std(all_bcea_C)/sqrt(length(all_bcea_C));
    errorbar([mean(all_bcea_P) mean(all_bcea_C)],...
                  [std_err_bcea_P std_err_bcea_C],...
                   'o', 'Color', 'k', 'LineStyle', '-','LineWidth',3)
    set(gca, 'XTick', [1:1:2], 'XTickLabel', {'Patients'; 'Controls'}, 'FontSize', 12);
    xlim([0 3])
    ylim([0 5])
    ylabel('bcea (deg^2)')
    set(gca,'fontsize',27,'FontWeight','Bold')
    set(gca, 'YScale', 'log')
    

end