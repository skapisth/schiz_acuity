function bcea = get_bcea(x, y)
newX = x;
newY = y;
x = x(~isnan(newX) & ~isnan(newY));
y = y(~isnan(newX) & ~isnan(newY));

% x = x./60;
% y = y./60;
x2 = 1.13; %chi-square variable with two degrees of freedom, encompasing 68% of the highest density points
bceaMatrix =  2*x2*pi * std(x) * std(y) * (1-corrcoef(x,y).^2).^0.5; %smaller BCEA correlates to more stable fixation 
bcea = abs(bceaMatrix(1,2));

end
