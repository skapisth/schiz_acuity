function drift_bias_ndhist_task(patients,controls)
    
    cP = jet(length(patients));
    cC = winter(length(controls));

    for ii_p = 1:length(patients)
        [inst_sp_x_P,inst_sp_y_P,raw_x,raw_y] = deal([]);
        dir_path = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\patients\',patients{ii_p},'\matFiles\');
        file_to_read = strcat('em_info_for_drift_bias_task_',patients{ii_p},'.mat');
        load(strcat(dir_path,file_to_read))
        ln_em = numel(fieldnames(em.ecc_0));
        for t_size = 1:ln_em
            f_name = fieldnames(em.ecc_0);

            all_chars = eval(strcat('em.ecc_0.',f_name{t_size}));
            if ~isfield(all_chars,'instSpX')
                continue
            else
                for all = 1:length(all_chars.instSpX)
                    x = all_chars.instSpX{all};
                    y = all_chars.instSpY{all};
                    rawX = all_chars.x_d_bias{all};
                    rawY = all_chars.y_d_bias{all};
                    inst_sp_x_P = [inst_sp_x_P,x];
                    inst_sp_y_P = [inst_sp_y_P,y];
                    raw_x = [raw_x,rawX];
                    raw_y = [raw_y,rawY];
                end
            end

        end
        clear em;
        temp_inst_sp_x_P = inst_sp_x_P(inst_sp_x_P > -200 & inst_sp_x_P < 200);
        temp_inst_sp_x_P = inst_sp_x_P(inst_sp_y_P > -200 & inst_sp_y_P < 200);
        temp_inst_sp_y_P = inst_sp_y_P(inst_sp_x_P > -200 & inst_sp_x_P < 200);
        temp_inst_sp_y_P = inst_sp_y_P(inst_sp_y_P > -200 & inst_sp_y_P < 200);
%         ellipseXY(inst_sp_x_P(~isnan(inst_sp_x_P)), inst_sp_y_P(~isnan(inst_sp_x_P)), 68, 'b', 0)
%         figure()
% ndhist(temp_inst_sp_x_P(~isnan(temp_inst_sp_x_P)), temp_inst_sp_y_P(~isnan(temp_inst_sp_x_P)), 'bins',1, 'radial','axis',[-200 200 -200 200],'nr','filt');
%         ndhist(inst_sp_x_P(~isnan(inst_sp_x_P)), inst_sp_y_P(~isnan(inst_sp_x_P)),  'radial','axis',[-200 200 -200 200],'nr','filt');
% 
%         title(patients{ii_p})

%         hold on
%         file_to_read_avg = strcat('em_info_for_heatmaps_',patients{ii_p},'.mat');
%         load(strcat(dir_path,file_to_read_avg))
%         avg_x = round(nanmean(em.allTraces.xALL),2);
%         avg_y = round(nanmean(em.allTraces.yALL),2);
%         plot(avg_x,avg_y,'kp','MarkerSize',10,'MarkerFaceColor','g')
%         hold on
%         ellipseXY(em.allTraces.xALL(~isnan(em.allTraces.xALL)), em.allTraces.yALL(~isnan(em.allTraces.yALL)), 68, 'g', 3)
%         pathToSave = 'C:/Users/sanjana/OneDrive - University of Rochester/Documents/Sanjana/Projects/schizophrenia/Figures/Drift_velocity_task';
%             
%         if ~exist(strcat(pathToSave), 'dir')
%             mkdir(strcat(pathToSave))
%         end
%         fullPath = strcat(pathToSave,'/%s');
%         strfName = patients{ii_p};
%         saveas(gcf,sprintf(fullPath, strfName), 'png')
    end
    for ii_c = 1:length(controls)
        [inst_sp_x_C,inst_sp_y_C] = deal([]);
        dir_path = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\controls\',controls{ii_c},'\matFiles\');
        file_to_read = strcat('em_info_for_drift_bias_task_',controls{ii_c},'.mat');
        load(strcat(dir_path,file_to_read))
        ln_em = numel(fieldnames(em.ecc_0));
        for t_size = 1:ln_em
            f_name = fieldnames(em.ecc_0);

            all_chars = eval(strcat('em.ecc_0.',f_name{t_size}));
            if ~isfield(all_chars,'instSpX')
                continue
            else
                for all = 1:length(all_chars.instSpX)
                    x = all_chars.instSpX{all};
                    y = all_chars.instSpY{all};
                    inst_sp_x_C = [inst_sp_x_C,x];
                    inst_sp_y_C = [inst_sp_y_C,y];
                end
            end

        end
        clear em;
        temp_inst_sp_x_C = inst_sp_x_C(inst_sp_x_C > -200 & inst_sp_x_C < 200);
        temp_inst_sp_x_C = inst_sp_x_C(inst_sp_y_C > -200 & inst_sp_y_C < 200);
        temp_inst_sp_y_C = inst_sp_y_C(inst_sp_x_C > -200 & inst_sp_x_C < 200);
        temp_inst_sp_y_C = inst_sp_y_C(inst_sp_y_C > -200 & inst_sp_y_C < 200);
        
        ndhist(temp_inst_sp_x_C(~isnan(temp_inst_sp_x_C)), temp_inst_sp_y_C(~isnan(temp_inst_sp_y_C)), 'bins',1.5, 'radial','axis',[-200 200 -200 200],'nr','filt');
        title(controls{ii_c})
%         figure()
%         ndhist(inst_sp_x_C, inst_sp_y_C, 'bins',1.5, 'radial','axis',[-200 200 -200 200],'nr','filt');
        
        
        pathToSave = 'C:/Users/sanjana/OneDrive - University of Rochester/Documents/Sanjana/Projects/schizophrenia/Figures/Drift_velocity_task';
%             
        if ~exist(strcat(pathToSave), 'dir')
            mkdir(strcat(pathToSave))
        end
        fullPath = strcat(pathToSave,'/%s');
        strfName = controls{ii_c};
        saveas(gcf,sprintf(fullPath, strfName), 'png')
        
%         hold on
%         file_to_read_avg = strcat('em_info_for_heatmaps_',controls{ii_c},'.mat');
%         load(strcat(dir_path,file_to_read_avg))
%         avg_x = round(nanmean(em.allTraces.xALL),2);
%         avg_y = round(nanmean(em.allTraces.yALL),2);
%         plot(avg_x,avg_y,'kp','MarkerSize',25,'MarkerFaceColor','m')
%          hold on
%         ellipseXY(em.allTraces.xALL(~isnan(em.allTraces.xALL)), em.allTraces.yALL(~isnan(em.allTraces.yALL)), 68, 'm', 3)
    end
   
end

