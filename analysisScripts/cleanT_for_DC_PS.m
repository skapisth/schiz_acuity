%% params
clc;clear all;
% subjects = {'PC-S01','PC-S02','PC-S04','PC-S07','PC-S12','PC-S16','FE-S01','FE-S08'};
% subjects = {'HUX4','HUX10','HUX12','HUX18','HUX21','HUX22','HUX23','Hux24','MP'};
subjects = {'PC-S16'};
patient = 1;
huxC = 0;

%% subtract the mean from x and y
%% use 300ms chunks instead of 250
time_threshold = 250;
machine = {'D'};
figures = struct(...
                    'FIGURE_ON', 0,... %Show individual trial with traces in 2D space
                    'VIDEO', 0,... %Wills save video of FIGURE_ON figures
                    'FIXATION_ANALYSIS',0,... %Plots psychometric curves
                    'saveFixMat',1,...
                    'QUICK_ANALYSIS',0,... %Only plots psychometric curves
                    'CHECK_TRACES',0,... %will plot traces
                    'HUX_RUN',1,... %will discard trials differently
                    'FOLDED', 0,...
                    'FIXED_SIZE', 0,...
                    'COMPARE_SPANS',0); %0 = all spans, 1 = small spans, 2 = large spans

params = struct(...
                    'patient',1,...
                    'nBoots', 1,... %Number of boots for psychometric fits
                    'crowded', false, ... %Gets rewritten based on currentCond
                    'DMS',true, ...
                    'D', false, ...
                    'MS', false,...
                    'fixation',false,...
                    'machine',machine, ... %DPI "A" vs dDPI "D"
                    'session', '1', ... %Which number session
                    'spanMax', 100,... %The Maximum Span
                    'spanMin', 0,...%The Minimum Span
                    'compSpans', 0,...
                    'ecc',0,...
                    'samplingRate' , 1000/341,...
                    'stabilization','Unstabilized');
      
for ii_s = 1:length(subjects)
    
   
    
    params.subject = subjects{ii_s};
    title_str = char(subjects{ii_s});
    if patient
        pathToFile =  strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\patients\',char(subjects{ii_s}),'\unCrowded');     
        
        if length(subjects) > 1
            if ii_s > 3
                newEyeris = 1;
                Fs = 1000;
                params.samplingRate = 1000/Fs;
            else
                newEyeris = 0;
                Fs = 341;
                params.samplingRate = 1000/Fs;
            end
        else
            newEyeris = 1;
            Fs = 1000;
            params.samplingRate = 1000/Fs;
        end
    end
    if (patient == 0 && huxC == 1)
         pathToFile =  strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\controls\HUX_C\',char(subjects{ii_s}),'\unCrowded');
         newEyeris = 0;
         Fs = 341;
         params.samplingRate = 1000/Fs;
    end
    if (patient == 0 && huxC == 0)
         pathToFile =  strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\controls\',char(subjects{ii_s}),'\unCrowded');
         if ii_s > 1
            newEyeris = 1;
            Fs = 1000;
            params.samplingRate = 1000/Fs;
        else
            newEyeris = 0;
            Fs = 341;
            params.samplingRate = 1000/Fs;
        end
    end
    if newEyeris
        sample_threshold = time_threshold ;
    else
        sample_threshold = time_threshold / (1000/Fs); % use drift segments longer than this threshold in samples;
    end
    if (strcmp(subjects{ii_s}, 'PC-S16') || strcmp(subjects{1}, 'PC-S07'))
        if params.DMS
            load(fullfile(pathToFile,'task_clean.mat'), 'all_clean');
        elseif params.fixation
            load(fullfile(pathToFile,'fix_clean.mat'), 'all_clean');
        end
        
    end
    load(fullfile(pathToFile,'pptrials.mat'), 'pptrials');
    for ii = 1:length(pptrials)
        if isfield(pptrials{ii}, 'pixelAngle')
            params.pixelAngle(ii) = pptrials{1,ii}.pixelAngle;
            pptrials{1,ii}.pxAngle = pptrials{1,ii}.pixelAngle;
        else
            params.pixelAngle(ii) = pptrials{1,ii}.pxAngle;
        end
        pptrials{ii}.Unstabilized = 0;
    end
    if (strcmp(subjects{1}, 'PC-S16')  || strcmp(subjects{1}, 'PC-S07'))
        valid.dms = all_clean;
    end
    
    trialChar = buildTrialCharStruct(pptrials,newEyeris);
    trialChar = add_fields_trialChar(trialChar,pptrials,params);
    em = [];
    if newEyeris
        [~, ~, valid, ~] = countingTrials_newEyeris(pptrials, em, params, figures);
    else
        [~, ~, valid, ~] = countingTrials(pptrials, em, params, figures);
    end
    
    
    
%     condition = Psychometric_Graphing(params,params.ecc,...
%         valid, trialChar, params.crowded, title_str);
%     close all
%     all_thresh(ii_s) = condition.thresh;
    
    all_long_drifts = struct();
    segidx = 0;
    idx = find(valid.fixation == 0);
    
    for val = 1:length(idx)
        pptrials_new{val} = pptrials{idx(val)};
    end
    for nT = 1:length(pptrials_new)
        for di = 1:length(pptrials_new{1,nT}.drifts.duration)
            if pptrials_new{1,nT}.drifts.duration(di) >= sample_threshold
                tmp_start = pptrials_new{1,nT}.drifts.start(di);
                tmp_end = tmp_start + sample_threshold;
                ms_in_trial = find(pptrials_new{1,nT}.microsaccades.start >= tmp_start & pptrials_new{1,nT}.microsaccades.start <= tmp_end);
                if ~isempty(ms_in_trial) > 0
                    continue
                end
                if tmp_end > length(pptrials_new{1,nT}.x.position)
                    continue
                end
                if tmp_end > length(pptrials_new{1,nT}.y.position)
                    continue
                end
                tmp_x = pptrials_new{1,nT}.x.position(tmp_start:tmp_end);
                tmp_y = pptrials_new{1,nT}.y.position(tmp_start:tmp_end);
                
                euc_dist = sqrt((tmp_x-0).^2 + (tmp_y-0).^2);
                mean_euc_dist = mean(euc_dist);
                
                if mean_euc_dist > 30
                    continue;
                end
                
                % check if there is nan value in the trace
                if sum(isnan(tmp_x)) ~= 0 ||  sum(isnan(tmp_y)) ~= 0
                    [startIndex, stopIndex] = getIndicesFromBin(~isnan(tmp_x));
                    durations = stopIndex - startIndex;
                    use_subtrace = durations > sample_threshold; % which subtrace to use after removing nan
                    for ui = 1:length(use_subtrace)
                        if use_subtrace(ui) % save the subtrace
                            tmp_start = startIndex(ui);
                            tmp_end = stopIndex(ui);
                            tmp_x = pptrials_new{1,nT}.x.position(tmp_start:tmp_end);
                            tmp_y = pptrials_new{1,nT}.y.position(tmp_start:tmp_end);
                            
                            segidx = segidx+1;
                        end
                    end
                else
                    
                    % save the trace to the struct
                    segidx = segidx+1;
                    
                    all_long_drifts(segidx).x = tmp_x;
                    all_long_drifts(segidx).y = tmp_y;
                    all_long_drifts(segidx).recordingIdx = nT;
                    all_long_drifts(segidx).duration = pptrials_new{1,nT}.drifts.duration(di);
                end
            end
        end
    end
 
%% any drift seg after 20ms..check if it goes above 200..
% for all = 1:length(SingleSegmentDsq_AO)
%     subplot(2,1,1,'align')
%     plot(SingleSegmentDsq_AO(all,:))
%     hold on 
%     subplot(2,1,2,'align')
%     plot(all_long_drifts(all).x,'r')
%     hold on
%     plot(all_long_drifts(all).y,'b')
%     title(string(all))
%     input ''
%     clf
% end

close all
end












%