function plotVectors_fix(patients,controls)
    
    cP = jet(length(patients));
    cC = winter(length(controls));

    for ii_p = 1:length(patients)
        [inst_sp_x_P,inst_sp_y_P,raw_x_P,raw_y_P] = deal([]);
        dir_path = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\patients\',patients{ii_p},'\matFiles\');
        file_to_read = strcat('fix_results_cut_drift_',patients{ii_p},'.mat');
        load(strcat(dir_path,file_to_read))
        for each = 1:length(fixation.instSpX)
            x = cell2mat(fixation.instSpX{each});
            y = cell2mat(fixation.instSpY{each});
            inst_sp_x_P = [inst_sp_x_P,x];
            inst_sp_y_P = [inst_sp_y_P,y];
        end
        file_to_read = strcat('em_info_for_heatmaps_fix_',patients{ii_p},'.mat');
        load(strcat(dir_path,file_to_read))
        
        raw_x_P = [raw_x_P,em.allTraces.xALL];
        raw_y_P = [raw_y_P,em.allTraces.yALL];
        figure()
        gazeX = mean(raw_x_P(~isnan(raw_x_P)));
        gazeY = mean(raw_y_P(~isnan(raw_y_P)));
        [vecGaze] = createUnitVector([gazeX,gazeY]);
        h1 = plot([0,vecGaze(1,1)],[0,vecGaze(1,2)],'r');
        hold on
        
        p1 = [0 0];                         % First Point
        p2 = [vecGaze(1,1) vecGaze(1,2)];                         % Second Point
        dp = p2-p1;                         % Difference
        quiver(p1(1),p1(2),dp(1),dp(2),0,'color','r','Linewidth',2)
        text(p1(1),p1(2), sprintf('(%.2f,%.2f)',p1))
        text(double(p2(1)),double(p2(2)), sprintf('(%.2f,%.2f)',p2))
        
        hold on
        velX = mean(inst_sp_x_P(~isnan(inst_sp_x_P)));
        velY = mean(inst_sp_y_P(~isnan(inst_sp_y_P)));
        [vecVel] = createUnitVector([velX,velY]);
        h2 = plot([0,vecVel(1,1)],[0,vecVel(1,2)],'b');
        hold on
        p1 = [0 0];                         % First Point
        p2 = [vecVel(1,1) vecVel(1,2)];                         % Second Point
        dp = p2-p1;                         % Difference
        quiver(p1(1),p1(2),dp(1),dp(2),0,'color','b','Linewidth',2)
        text(p1(1),p1(2), sprintf('(%.2f,%.2f)',p1))
        text(double(p2(1)),double(p2(2)), sprintf('(%.2f,%.2f)',p2))
        
        clear em;
        
        file_to_read = strcat('ms_info_fix_500ms_vectors_',patients{ii_p},'.mat');
        load(strcat(dir_path,file_to_read))
        
        x_ms = mean(msInfo.x_all);
        y_ms = mean(msInfo.y_all);
        
        [vecMS] = createUnitVector([x_ms,y_ms]);
        h3 = plot([0,vecMS(1,1)],[0,vecMS(1,2)],'k');
        hold on
        
        p1 = [0 0];                         % First Point
        p2 = [vecMS(1,1) vecMS(1,2)];                         % Second Point
        dp = p2-p1;                         % Difference
        quiver(p1(1),p1(2),dp(1),dp(2),0,'color','k','Linewidth',2)
        text(p1(1),p1(2), sprintf('(%.2f,%.2f)',p1))
        text(double(p2(1)),double(p2(2)), sprintf('(%.2f,%.2f)',p2))
         xlim([-1 1])
        ylim([-1 1])
        legend([h1(1), h2(1), h3(1)],'Avg gaze vector','Avg velocity vector','Avg ms vector')
        title(patients{ii_p})
       

    end
    for ii_c = 1:length(controls)
        [inst_sp_x_C,inst_sp_y_C,raw_x_C,raw_y_C] = deal([]);
        dir_path = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\controls\',controls{ii_c},'\matFiles\');
        file_to_read = strcat('fix_results_cut_drift_',controls{ii_c},'.mat');
        load(strcat(dir_path,file_to_read))
        for each = 1:length(fixation.instSpX)
            x = cell2mat(fixation.instSpX{each});
            y = cell2mat(fixation.instSpY{each});
            inst_sp_x_C = [inst_sp_x_C,x];
            inst_sp_y_C = [inst_sp_y_C,y];
        end
        file_to_read = strcat('em_info_for_heatmaps_fix_',controls{ii_c},'.mat');
        load(strcat(dir_path,file_to_read))
        
        raw_x_C = [raw_x_C,em.allTraces.xALL];
        raw_y_C = [raw_y_C,em.allTraces.yALL];  
        gazeX = mean(raw_x_C(~isnan(raw_x_C)));
        gazeY = mean(raw_y_C(~isnan(raw_y_C)));
        [vecGaze] = createUnitVector([gazeX,gazeY]);
        figure()
        h1 = plot([0,vecGaze(1,1)],[0,vecGaze(1,2)],'r')
        hold on
        
        p1 = [0 0];                         % First Point
        p2 = [vecGaze(1,1) vecGaze(1,2)];                         % Second Point
        dp = p2-p1;                         % Difference
        quiver(p1(1),p1(2),dp(1),dp(2),0,'color','r','Linewidth',2)
        text(p1(1),p1(2), sprintf('(%.2f,%.2f)',p1))
        text(double(p2(1)),double(p2(2)), sprintf('(%.2f,%.2f)',p2))
        
        velX = mean(inst_sp_x_C(~isnan(inst_sp_x_C)));
        velY = mean(inst_sp_y_C(~isnan(inst_sp_y_C)));
        [vecVel] = createUnitVector([velX,velY]);
        h2 = plot([0,vecVel(1,1)],[0,vecVel(1,2)],'b')
        hold on
        
        p1 = [0 0];                         % First Point
        p2 = [vecVel(1,1) vecVel(1,2)];                         % Second Point
        dp = p2-p1;                         % Difference
        quiver(p1(1),p1(2),dp(1),dp(2),0,'color','b','Linewidth',2)
        text(p1(1),p1(2), sprintf('(%.0f,%.0f)',p1))
        text(double(p2(1)),double(p2(2)), sprintf('(%.0f,%.0f)',p2))
        
        clear em
        
        file_to_read = strcat('ms_info_fix_500ms_vectors_',controls{ii_c},'.mat');
        load(strcat(dir_path,file_to_read))
        
        x_ms = mean(msInfo.x_all);
        y_ms = mean(msInfo.y_all);
        
        [vecMS] = createUnitVector([x_ms,y_ms]);
        h3 = plot([0,vecMS(1,1)],[0,vecMS(1,2)],'k');
        hold on
        
        p1 = [0 0];                         % First Point
        p2 = [vecMS(1,1) vecMS(1,2)];                         % Second Point
        dp = p2-p1;                         % Difference
        quiver(p1(1),p1(2),dp(1),dp(2),0,'color','k','Linewidth',2)
        text(p1(1),p1(2), sprintf('(%.2f,%.2f)',p1))
        text(double(p2(1)),double(p2(2)), sprintf('(%.2f,%.2f)',p2))
        xlim([-1 1])
        ylim([-1 1])
        legend([h1(1), h2(1), h3(1)],'Avg gaze vector','Avg velocity vector','Avg ms vector')
        title(controls{ii_c})
        clear em
        
    end
   
    

end