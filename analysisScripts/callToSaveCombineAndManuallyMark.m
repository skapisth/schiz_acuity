function [pptrials] = callToSaveCombineAndManuallyMark(userVariables, userFlags)
if userFlags.saveFileFlag == '1'
    pptrials = validTrials;
    if ~exist(userVariables.pathToSave, 'dir')
       mkdir(userVariables.pathToSave)
    end
    if ~exist(strcat(userVariables.pathToSave, userVariables.folderName), 'dir')
       mkdir(strcat(userVariables.pathToSave, string(userVariables.folderName)))
    end
    fName = sprintf('pptrials.mat');
    save(fullfile(strcat(userVariables.pathToSave, string(userVariables.folderName)), fName), 'pptrials')
end

if userFlags.combinePPTrials == '1'
    if ~exist(strcat(userVariables.pathToFile, '\allCombined'), 'dir')
       mkdir(strcat(userVariables.pathToFile, '\allCombined'))
    end
    dirToLook = userVariables.pathToSave;
    fName = sprintf('pptrials.mat');
    [ allPPTrials ] = reBuildPPtrials (dirToLook);
    pptrials = allPPTrials;
    save(fullfile(strcat(userVariables.pathToFile, '\allCombined'), fName), 'pptrials')   
end

% if userFlags.manMark == '1' 
%     for i = 1:length(validTrials)
%         xyPositions(i).x = validTrials{i}.x.position;
%         xyPositions(i).y = validTrials{i}.y.position; 
%     end
%  
% 
%     [DCoef_DeTrended Bias DCoef_Dsq DsqDetrended Dsq SingleSegmentDsq Time NFix] =  CalculateDiffusionCoef(xyPositions);
% 
% 
%      pptrials = checkTraces(validTrials, SingleSegmentDsq, strcat(userVariables.pathToSaveTaggedData,userVariables.folderName,'\'));

end
