function ms_start_end_scatter_plot(msInfo,tasktype,scatPlot,title_str)
    
    [all_sx,all_sy,all_lx,all_ly] = deal([]);
    for ts = 1:length(msInfo.startPos)
        if strcmp(tasktype,'fix')
            for ms = 1:length(msInfo.startPos{ts}(1,:))
                
                sx = msInfo.startPos{ts}(1,ms);
                sy = msInfo.startPos{ts}(2,ms);
                lx = msInfo.endPos{ts}(1,ms);
                ly = msInfo.endPos{ts}(2,ms);
                
                all_sx(1,end+1) = sx;
                all_sy(1,end+1) = sy;
                all_lx(1,end+1)  = lx;
                all_ly(1,end+1)  = ly;
               
                stimuliSize = 0;
            end          
        else
             for ms = 1:length(msInfo.startPos)
               startP_x = msInfo.startPos(1,ms);
               startP_y = msInfo.startPos(2,ms);
               endP_x = msInfo.endPos(1,ms);
               endP_y = msInfo.endPos(2,ms);
           
               if ~isnan(startP_x)
                    sx = startP_x;
                    sy = startP_y;
                    lx = endP_x;
                    ly = endP_y;
                    
                    all_sx(1,end+1) = sx;
                    all_sy(1,end+1) = sy;
                    all_lx(1,end+1)  = lx;
                    all_ly(1,end+1)  = ly;
               end
            end
            stimuliSize = 4.2;
        end
            
    end
   
    if scatPlot
        figure()
        if strcmp(tasktype,'fix')
            centerX = 8;
            centerY = -8;
            width = 16;
            height = 16;
            rectangle('Position',[-centerX+stimuliSize, centerY, width, height],'LineWidth',3,'LineStyle','-')
            hold on
        else
            stimuliSize = stimuliSize/2;
            width = 2 * stimuliSize;
            height = width * 5;
            centerX = (-stimuliSize);
            centerY = (-stimuliSize*5);
            rectangle('Position',[centerX, centerY, width, height],'LineWidth',3)
            hold on
        end
        
%         scatter(all_sx,all_sy,'r')
%         hold on
%         scatter(all_lx,all_ly)
        
        
        
        t_x = [all_sx; all_lx]';
        t_y = [all_sy; all_ly]';
        
        s_dist_center = sqrt((all_sx-0).^2 + (all_sy-0).^2);
        idx_reqd = find(s_dist_center >= 10);
%         for each = 1:length(all_lx)
%             plot(t_x(each,:),t_y(each,:), '-','color','g')
%             hold on
%         end
%         figure()
        x_req_S = all_sx(idx_reqd);
        y_req_S = all_sy(idx_reqd);
        x_req_E = all_lx(idx_reqd);
        y_req_E = all_ly(idx_reqd);
        s1 = scatter(mean(x_req_S),mean(y_req_S),'r','filled')
        s1.LineWidth = 2.0;
        hold on
        s2 = scatter(mean(x_req_E),mean(y_req_E),'b','filled')
        s2.LineWidth = 2.0;
        hold on
        plot(mean(t_x(idx_reqd)),mean(t_y(idx_reqd)),'-','color','g','LineWidth',2.5)
        xlim([-30,30])
        ylim([-30,30])
        xlabel('arcmins')
        ylabel('arcmins')
        axis square
        title(title_str)
        legend('avg start location','avg end location')
    end
    
end