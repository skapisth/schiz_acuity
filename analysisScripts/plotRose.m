function plotRose(patients,controls,tasktype)
    cP = distinguishable_colors(length(patients));%parula(length(patients));
    cC = distinguishable_colors(length(controls));
   
    [all_b_ms_P, all_b0_ms_P,all_b_ms_C, all_b0_ms_C] = deal([]);
    [all_b_d_P, all_b0_d_P,all_b_d_C, all_b0_d_C] = deal([]);
    
    scatPlot= 0;
    
    for ii_p = 1:length(patients)
        dir_path = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\patients\',patients{ii_p},'\matFiles\');

        if strcmp(tasktype,'fix')
            file_to_read = strcat('ms_info_fix_500ms_',controls{ii_p},'.mat');
        else
            file_to_read = strcat('ms_info_task_',patients{ii_p},'.mat');
        end
        load(strcat(dir_path,file_to_read))
        title_str = patients{ii_p};
        [~,theta_P,amp_1_P,...
            ~,...
            ~,~] = calc_amp_theta(msInfo,tasktype,scatPlot,title_str);
        
        
        deg_theta_ms_C = rad2deg(theta_P);
        idx = find(amp_1_P> 5);
        amp_1_P = amp_1_P(idx);
        theta_P = theta_P(idx);
        
        polarhistogram(theta_P,'Normalization','pdf','FaceColor','r','FaceAlpha',0.3);
% %         rlim([0 1])
%         hold on
        
        [inst_sp_x_P,inst_sp_y_P,raw_x,raw_y] = deal([]);
        dir_path = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\patients\',patients{ii_p},'\matFiles\');
        
        file_to_read = strcat('em_info_for_drift_bias_task_',patients{ii_p},'.mat');
        load(strcat(dir_path,file_to_read))
        ln_em = numel(fieldnames(em.ecc_0));
        for t_size = 1:ln_em
            f_name = fieldnames(em.ecc_0);

            all_chars = eval(strcat('em.ecc_0.',f_name{t_size}));
            if ~isfield(all_chars,'instSpX')
                continue
            else
                for all = 1:length(all_chars.instSpX)
                    x = all_chars.instSpX{all};
                    y = all_chars.instSpY{all};
                    rawX = all_chars.x_d_bias{all};
                    rawY = all_chars.y_d_bias{all};
                    inst_sp_x_P = [inst_sp_x_P,x];
                    inst_sp_y_P = [inst_sp_y_P,y];
                    raw_x = [raw_x,rawX];
                    raw_y = [raw_y,rawY];
                end
            end

        end
        clear em;
        temp_inst_sp_x_P = inst_sp_x_P(inst_sp_x_P > -200 & inst_sp_x_P < 200);
        temp_inst_sp_x_P = inst_sp_x_P(inst_sp_y_P > -200 & inst_sp_y_P < 200);
        temp_inst_sp_y_P = inst_sp_y_P(inst_sp_x_P > -200 & inst_sp_x_P < 200);
        temp_inst_sp_y_P = inst_sp_y_P(inst_sp_y_P > -200 & inst_sp_y_P < 200);

        nd_X_P = temp_inst_sp_x_P(~isnan(temp_inst_sp_x_P));
        nd_Y_P = temp_inst_sp_y_P(~isnan(temp_inst_sp_x_P));
        
        
        rad_theta_vel_P = atan2(nd_Y_P,nd_X_P);
%         polarhistogram(rad_theta_vel_P,'Normalization','probability','FaceColor','y','FaceAlpha',0.2)
%         rlim([0 1])
        title(patients{ii_p})
        set(gca,'fontsize',13,'FontWeight','Bold')
%         legend('microsaccade direction','drift direction')
%         legend box off
        pathToSave = 'C:/Users/sanjana/OneDrive - University of Rochester/Documents/Sanjana/Projects/schizophrenia/Figures/rosePlots_task/msOnly';
            
        if ~exist(strcat(pathToSave), 'dir')
            mkdir(strcat(pathToSave))
        end
        fullPath = strcat(pathToSave,'/%s');
        strfName = patients{ii_p};
        saveas(gcf,sprintf(fullPath, strfName), 'png')
        saveas(gcf,sprintf(fullPath, strfName), 'eps')
        close all
        
    end
    
    for ii_c = 1:length(controls)
        
        if ii_c > 10
            dir_path = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\controls\',controls{ii_c},'\matFiles\');
        else
            dir_path = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\controls\HUX_C\',controls{ii_c},'\matFiles\');
        end
        if strcmp(tasktype,'fix')
            file_to_read = strcat('ms_info_fix_500ms_',controls{ii_c},'.mat');
        else
            file_to_read = strcat('ms_info_task_',controls{ii_c},'.mat');
        end
        load(strcat(dir_path,file_to_read))
        title_str = controls{ii_c};
        [~,theta_C,amp_1_C,...
            ~,...
            ~,~] = calc_amp_theta(msInfo,tasktype,scatPlot,title_str);
        
        deg_theta_ms_C = rad2deg(theta_C);
        idx_C = find(amp_1_C> 5);
        amp_1_C = amp_1_C(idx_C);
        theta_C = theta_C(idx_C);
        
        polarhistogram(theta_C,'Normalization','probability','FaceColor','r','FaceAlpha',0.3);
% %         rlim([0 1])
%         hold on
%         
    [inst_sp_x_C,inst_sp_y_C,raw_x,raw_y] = deal([]);
    if ii_c > 10
       dir_path = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\controls\',controls{ii_c},'\matFiles\');
    else
       dir_path = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\controls\HUX_C\',controls{ii_c},'\matFiles\');
    end
    
    file_to_read = strcat('em_info_for_drift_bias_task_',controls{ii_c},'.mat');
    load(strcat(dir_path,file_to_read))
    ln_em = numel(fieldnames(em.ecc_0));
    for t_size = 1:ln_em
        f_name = fieldnames(em.ecc_0);
        
        all_chars = eval(strcat('em.ecc_0.',f_name{t_size}));
        if ~isfield(all_chars,'instSpX')
            continue
        else
            for all = 1:length(all_chars.instSpX)
                x = all_chars.instSpX{all};
                y = all_chars.instSpY{all};
                rawX = all_chars.x_d_bias{all};
                rawY = all_chars.y_d_bias{all};
                inst_sp_x_C = [inst_sp_x_C,x];
                inst_sp_y_C = [inst_sp_y_C,y];
                raw_x = [raw_x,rawX];
                raw_y = [raw_y,rawY];
            end
        end
        
    end
    clear em;
    temp_inst_sp_x_C = inst_sp_x_C(inst_sp_x_C > -200 & inst_sp_x_C < 200);
    temp_inst_sp_x_C = inst_sp_x_C(inst_sp_y_C > -200 & inst_sp_y_C < 200);
    temp_inst_sp_y_C = inst_sp_y_C(inst_sp_x_C > -200 & inst_sp_x_C < 200);
    temp_inst_sp_y_C = inst_sp_y_C(inst_sp_y_C > -200 & inst_sp_y_C < 200);
    
    nd_X_C = temp_inst_sp_x_C(~isnan(temp_inst_sp_x_C));
    nd_Y_C = temp_inst_sp_y_C(~isnan(temp_inst_sp_x_C));
    
    rad_theta_vel_C = atan2(nd_Y_C,nd_X_C);
    
%     polarhistogram(rad_theta_vel_C,'Normalization','probability','FaceColor','y','FaceAlpha',0.2)
%     rlim([0 1])
    title(controls{ii_c})
    set(gca,'fontsize',13,'FontWeight','Bold')
%     legend('microsaccade direction','drift direction')
%     legend box off
    pathToSave = 'C:/Users/sanjana/OneDrive - University of Rochester/Documents/Sanjana/Projects/schizophrenia/Figures/rosePlots_task/msOnly';
    
    if ~exist(strcat(pathToSave), 'dir')
        mkdir(strcat(pathToSave))
    end
    fullPath = strcat(pathToSave,'/%s');
    strfName = controls{ii_c};
    saveas(gcf,sprintf(fullPath, strfName), 'png')
    saveas(gcf,sprintf(fullPath, strfName), 'eps')
    close all
    
    end
    
    
    
end

