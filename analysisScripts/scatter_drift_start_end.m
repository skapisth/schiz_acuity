function scatter_drift_start_end(patients,controls)
    
    cP = jet(length(patients));
    cC = winter(length(controls));

    for ii_p = 1:length(patients)
%         [xStart,yStart,xEnd,yEnd] = deal([]);
        dir_path = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\patients\',patients{ii_p},'\matFiles\');
        file_to_read = strcat('em_info_for_drift_bias_task_',patients{ii_p},'.mat');
        load(strcat(dir_path,file_to_read))
        ln_em = numel(fieldnames(em.ecc_0));
        for t_size = 1:ln_em
            f_name = fieldnames(em.ecc_0);

            all_chars = eval(strcat('em.ecc_0.',f_name{t_size}));
            
            if ~isfield(all_chars,'x_d_bias')
                continue
            else
                for all = 1:length(all_chars.x_d_bias)
                    x = all_chars.x_d_bias{all};
                    y = all_chars.y_d_bias{all};
                    
                    x = x(x>-30&x<30);y = y(x>-30&x<30);x = x(y>-30&y<30);y = y(y>-30&y<30);
                   
                    val_x = all_chars.x_d_bias{all}(1) - 0;
                    val_y = all_chars.y_d_bias{all}(1) - 0;
                    
                    xC = all_chars.x_d_bias{all} - val_x;
                    yC = all_chars.y_d_bias{all} - val_y;
                    
                    xC = xC(xC>-30&xC<30);yC = yC(xC>-30&xC<30);xC = xC(yC>-30&yC<30);yC = yC(yC>-30&yC<30);
                    
                    figure(1)
                    plot(x,y,'b')
                    hold on
                    if isempty(x) || isempty(y)
                        continue
                    else
                        plot(x(1),y(1),'r*')
                        hold on
                        plot(x(end),y(end),'g*')
                    end
                    figure(2)
                    plot(xC,yC,'b')
                    hold on
                    if isempty(xC) || isempty(yC)
                        continue
                    else
                        plot(xC(1),yC(1),'r*')
                        hold on
                        plot(xC(end),yC(end),'g*')
                    end
                    
                end
            end

        end
        clear em;
        t_x = [xStart; xEnd]';
        t_y = [yStart; yEnd]';
        figure()
        scatter(xStart,yStart,'r')
        hold on
        scatter(xEnd,yEnd)
        hold on
        for each = 1:length(t_x)
            plot(t_x(each,:),t_y(each,:), '-','color','g')
            hold on
        end
        xlim([-30,30])
        ylim([-30,30])
        xlabel('arcmins')
        ylabel('arcmins')
        legend('start location','end location')
        axis square
        title(patients{ii_p})
       
    end
    for ii_c = 1:length(controls)
        [xStart,yStart,xEnd,yEnd] = deal([]);
        dir_path = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\controls\',controls{ii_c},'\matFiles\');
        file_to_read = strcat('em_info_for_drift_bias_task_',controls{ii_c},'.mat');
        load(strcat(dir_path,file_to_read))
        ln_em = numel(fieldnames(em.ecc_0));
        for t_size = 1:ln_em
            f_name = fieldnames(em.ecc_0);

            all_chars = eval(strcat('em.ecc_0.',f_name{t_size}));
            
            for all = 1:length(all_chars.x_d_bias)
                xStart = [xStart,all_chars.x_d_bias{all}(1)];
                xEnd = [xEnd,all_chars.x_d_bias{all}(end)];
                yStart = [yStart,all_chars.y_d_bias{all}(1)];
                yEnd = [yEnd,all_chars.y_d_bias{all}(end)];
            end

        end
        clear em;
        t_x = [xStart; xEnd]';
        t_y = [yStart; yEnd]';
        figure()
        scatter(xStart,yStart,'r')
        hold on
        scatter(xEnd,yEnd)
        hold on
        for each = 1:length(t_x)
            plot(t_x(each,:),t_y(each,:), '-','color','g')
            hold on
        end
        xlim([-30,30])
        ylim([-30,30])
        xlabel('arcmins')
        ylabel('arcmins')
        legend('start location','end location')
        axis square
        title(controls{ii_c})
    end
   
    
end