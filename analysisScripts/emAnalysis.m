function [em] = emAnalysis(em, pptrials, valid, trialChar,params, uEcc,figures,subject)
    
    uST = unique(round(trialChar.TargetSize,1));
    valueGroup.ecc = sprintf('ecc_%i', uEcc);
    
    if params.DMS
        em.trialCount = length(find(valid.dms == 1));
    elseif params.D
        em.trialCount = length(find(valid.d == 1));
    elseif params.MS
        em.trialCount = length(find(valid.ms == 1));
    end
    
    counter = 1;
    [x_all_bcea,y_all_bcea] = deal([]);
    [x_all_bcea_b1,y_all_bcea_b1,...
        x_all_bcea_b2,y_all_bcea_b2,...
        x_all_bcea_b3,y_all_bcea_b3,...
        x_all_bcea_b4,y_all_bcea_b4,...
        x_all_bcea_b5,y_all_bcea_b5] = deal([]);
    
    ii_new = 1;
    for ii = 1:length(uST)
        
        trialIdx = find(round(trialChar.TargetSize,1) == uST(ii));
        partMSIdx = intersect(find(valid.partialMS == 1), trialIdx);
        sIdx = intersect(find(valid.s == 1), trialIdx);
        fixIdx = find(trialChar.TimeFixationON > 0);
        
        
        if params.D
            idx = intersect(find(valid.d == 1 ), trialIdx);
        elseif params.MS
            idx = intersect(find(valid.ms == 1), trialIdx);
        elseif params.DMS
            idx = intersect(find(valid.dms == 1), trialIdx);
        end
        
        valueGroup.idx = idx;
        valueGroup.strokeWidth = sprintf('strokeWidth_%i', (ii));
        
        valueGroup.stimulusSize = uST(ii);
        numberTrials(ii) = numel(idx);
        if numberTrials(ii) < 1
            spnAmpEvaluate.ccDist(ii) = 0;
            spnAmpEvaluate.stimulusSize(ii) = 0;
            spnAmpEvaluate.SW(ii) = 0;
            continue;
        end
        
        if params.drift_bias_mats
            [em] = calcDriftChar_for_bias(valueGroup,params,pptrials,em);
        else
            [em,x_all_bcea,y_all_bcea,...
                x_all_bcea_b1,y_all_bcea_b1,...
                x_all_bcea_b2,y_all_bcea_b2,...
                x_all_bcea_b3,y_all_bcea_b3,...
                x_all_bcea_b4,y_all_bcea_b4,...
                x_all_bcea_b5,y_all_bcea_b5] = calculateDriftChar(valueGroup,params,pptrials,...
                em,x_all_bcea,y_all_bcea,...
                x_all_bcea_b1,y_all_bcea_b1,...
                x_all_bcea_b2,y_all_bcea_b2,...
                x_all_bcea_b3,y_all_bcea_b3,...
                x_all_bcea_b4,y_all_bcea_b4,...
                x_all_bcea_b5,y_all_bcea_b5);
        end
        if ~params.short_300ms_analysis
            if params.save_heatmap_mats
                [em,~,ii_new] = vals_for_heatmaps_task(em,counter,valueGroup,params,ii_new);
            end
            
            
        end
    end
    if params.drift_bias_mats
        pathToSub = getPathToSaveMatfiles(params);
        em_Fname = sprintf(strcat('em_info_for_drift_bias_task_',subject,'.mat'));
        save(fullfile(pathToSub,em_Fname),'em');
    end
    if ~params.blankScreenAnalysis
        [bcea] = get_bcea(x_all_bcea(~isnan(x_all_bcea)), y_all_bcea(~isnan(y_all_bcea)));
        [bcea_b1] = get_bcea(x_all_bcea_b1(~isnan(x_all_bcea_b1)), y_all_bcea_b1(~isnan(y_all_bcea_b1)));
        [bcea_b2] = get_bcea(x_all_bcea_b2(~isnan(x_all_bcea_b2)), y_all_bcea_b2(~isnan(y_all_bcea_b2)));
        [bcea_b3] = get_bcea(x_all_bcea_b3(~isnan(x_all_bcea_b3)), y_all_bcea_b3(~isnan(y_all_bcea_b3)));
        [bcea_b4] = get_bcea(x_all_bcea_b4(~isnan(x_all_bcea_b4)), y_all_bcea_b4(~isnan(y_all_bcea_b4)));
        [bcea_b5] = get_bcea(x_all_bcea_b5(~isnan(x_all_bcea_b5)), y_all_bcea_b5(~isnan(y_all_bcea_b5)));
        em.bcea = bcea;
        em.bcea_b1 = bcea_b1;
        em.bcea_b2 = bcea_b2;
        em.bcea_b3 = bcea_b3;
        em.bcea_b4 = bcea_b4;
        em.bcea_b5 = bcea_b5;
        pathToSub = getPathToSaveMatfiles(params);
        em_Fname = sprintf(strcat('em_info_cut_drift_',subject,'.mat'));
        save(fullfile(pathToSub,em_Fname),'em');
    end
    
    if ~params.short_300ms_analysis
        if params.save_heatmap_mats
            pathToSub = getPathToSaveMatfiles(params);
            if params.blankScreenAnalysis
                em_Fname = sprintf(strcat('em_info_for_BSA_heatmaps_',subject,'.mat'));
            else
                em_Fname = sprintf(strcat('em_info_for_heatmaps_task_',subject,'.mat'));
            end
            save(fullfile(pathToSub,em_Fname),'em');
        end
    end
end


