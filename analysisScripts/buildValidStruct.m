%Sorting only valid trials from pptrials. Excluding invlaid trials from
%data set.

function [validStructure] = buildValidStruct (pptrials)
fprintf('%s: getting valid trials\n', datestr(now));
validStructure = struct(...;
    'gazeoffcenter', false(size(pptrials)),...;
    'bigoffset', false(size(pptrials)),...;
    'bigoffsetfixation', false(size(pptrials)),...;
    'tooshort', false(size(pptrials)),...;
    'blink', false(size(pptrials)),...;
    'notrack', false(size(pptrials)),...;
    'noresponse', false(size(pptrials)),...;
    'manualdiscard', false(size(pptrials)),...;
    's', false(size(pptrials)),...;
    'fixation', false(size(pptrials)),...;
    'span', false(size(pptrials)),...;
    'dms', false(size(pptrials)),...;
    'ms', false(size(pptrials)),...;
    'timesOFF', false(size(pptrials)),...
    'unknownToss', false(size(pptrials)),...
    'partialMS', false(size(pptrials)),...
    'lookingattarget', false(size(pptrials)),...
    'wrongecc',false(size(pptrials)),...
    'd', false(size(pptrials)));

end
