% patients = {'PC-S12','PC-S16','FE-S01','FE-S08'};
%'FE-S08','Hux24','FE-C19','PC-C06'
% 'PC-S04','FE-S09'
patients = {'PC-S01','PC-S02','PC-S04','PC-S07','PC-S12','PC-S16','FE-S01','FE-S08','FE-S09','FE-S17','FE-S18','FE-S31'};
% patients = {'PC-S01','PC-S02','PC-S07','PC-S12','PC-S16','FE-S01','FE-S08'};
% patients = {'FE-S01'};
% b_patients = {'FE-B01'};
%'HUX18', pc-c06
controls = {'HUX4','HUX10','HUX12','HUX17','HUX18','HUX21','HUX22','HUX23','MP','HUX24','FE-C05','FE-C17','FE-C19','FE-C33','FE-C36','FE-C44','FE-C48'};
% controls = {'HUX4','HUX10','HUX12','HUX17','HUX18','HUX21','HUX22','HUX23','MP','HUX24','PC-C06','FE-C19','FE-C05','FE-C17'};
% controls = {'HUX4','HUX10','HUX12','HUX18','HUX21','HUX22','HUX23','MP','HUX24'};
% controls = {'FE-C17'};
HUX = {'HUX4','HUX10','HUX12','HUX18','HUX21','HUX22','HUX23','MP','HUX24'};
% 'PC-C06','FE-C05','FE-C17','FE-C19',
params = struct(...
    'patient',1,...
    'binned',0,...
    'full',1,...
    'withOffset',0);

% plotRose(patients,controls,'task')
%  plotSkewness(patients,controls,'task')
% get_bias_prob(patients,controls,'task')
% get_offsets_in_x_y(patients,controls)
% plot_polar_vectors_task(patients,controls)
% plotVectors_task(patients,controls)
% plotVectors_fix(patients,controls)
% plot_thresh_acuity_correlation(patients,0) %1 for bp patients
% plot_polar_with_ellipse(patients,controls,'task')
plot_all_thresh(patients,b_patients,controls,HUX) %plot 1
% drift_bias_ndhist_task(patients,controls)
% scatter_drift_start_end(patients,controls)
% drift_bias_ndhist_fix(patients,controls)
% plotOffsets_bothTasks(patients,controls)
% plotOffsets_bothTasks_binned(patients,controls,HUX) 
% plotHeatmaps(patients,params)
% all_sub_d_char_analysis_task_allComb(patients,controls)
% all_sub_d_char_analysis_task(patients,controls,HUX)
% all_sub_d_char_analysis_fix(patients,controls)
% all_subs_bcea_fix(patients,controls)
% all_subs_bcea_task_binned(patients,controls,HUX)
% all_subs_bcea_task(patients,controls)
% all_sub_ms_analysis(patients,controls,HUX,'task')
% call_ms_analysis(patients,controls)
% call_to_plot_ms_start_end(patients,controls,'task')
% plot_polar_with_ellipse(patients,controls,'task')
% msLatency(patients,controls)
% plot_ms_start_end_ellipses(patients,controls)