function plot_polar_vectors_task(patients,controls)
    rotate = 1;
    avg = 1;
    [rad_theta_vel_all,amp_vel_all] = deal([]);
    [rad_theta_ms_all,amp_ms_all] = deal([]);
    for ii_p = 1:length(patients)
        dir_path = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\patients\theta\');
        file_to_read = strcat('statsData_p','.mat');
        load(strcat(dir_path,file_to_read))
        
        %% 1 = gaze, 2 = vel, 3 = ms
        [rad_theta_gaze,amp_gaze] = getValsForPolarPlot(statsData_p,1,ii_p,rotate);
        [rad_theta_vel,amp_vel] = getValsForPolarPlot(statsData_p,2,ii_p,rotate);
        [rad_theta_ms,amp_ms] = getValsForPolarPlot(statsData_p,3,ii_p,rotate);
        
        rad_theta_vel_all(1,end+1) = rad_theta_vel;
        amp_vel_all(1,end+1) = amp_vel;
        
        rad_theta_ms_all(1,end+1) = rad_theta_ms;
        amp_ms_all(1,end+1) = amp_ms;
        
        if rotate == 1 && avg == 1
            g = polarplot(rad_theta_gaze,amp_gaze,'o');
            g.Color = 'red';g.MarkerSize = 12;
            hold on
            if ii_p == length(patients)
%                 v = polarplot(rad_theta_vel_all,amp_vel_all,'o');
%                 v.Color = 'blue';v.Marker = 'square';v.MarkerSize = 12;
%                 hold on
%                 clear v;
%                 legend(["Avg Gaze","Drift Velocity"],'Location','northwest')
                  m = polarplot(rad_theta_ms_all,amp_ms_all,'*');
                  m.Color = 'green';m.MarkerSize = 20;
                  hold on
                  clear m;
                  legend(["Avg Gaze","Microsaccade direction"],'Location','northwest')
            end
            
        else
             g = polarplot(rad_theta_gaze,amp_gaze,'o');
             g.Color = 'red';g.MarkerSize = 12;
             
             
             hold on
             v = polarplot(rad_theta_vel,amp_vel);
             v.Color = 'blue';v.Marker = 'square';v.MarkerSize = 12;
             
             
             hold on
             m = polarplot(rad_theta_ms,amp_ms,'*');
             m.Color = 'green';m.MarkerSize = 12;
             
             legend(["Avg Gaze","Drift Velocity","Microsaccade direction"],'Location','northwest')
             title(patients{ii_p})
        end
        
       
        if (rotate == 1 && ii_p == length(patients))
            pathToSave = 'C:/Users/sanjana/OneDrive - University of Rochester/Documents/Sanjana/Projects/schizophrenia/Figures/polarVectorPlots/patients/rotated';
            if ~exist(strcat(pathToSave), 'dir')
                mkdir(strcat(pathToSave))
            end
            fullPath = strcat(pathToSave,'/%s');
            saveas(gcf,sprintf(fullPath, 'all_patients_ms_bias'), 'png')
            close all
        else
            pathToSave = 'C:/Users/sanjana/OneDrive - University of Rochester/Documents/Sanjana/Projects/schizophrenia/Figures/polarVectorPlots/patients/notRotated';
            if ~exist(strcat(pathToSave), 'dir')
                mkdir(strcat(pathToSave))
            end
            fullPath = strcat(pathToSave,'/%s');
            strfName = patients{ii_p};
            saveas(gcf,sprintf(fullPath, strfName), 'png')
            close all
        end
        
    end
    clear g; clear m; clear v;
    clear rad_theta_gaze; clear rad_theta_vel; clear rad_theta_ms; 
    clear amp_gaze;clear amp_vel;clear amp_ms;
    
    [rad_theta_vel_all,amp_vel_all] = deal([]);
    [rad_theta_ms_all,amp_ms_all] = deal([]);
    for ii_c = 1:length(controls)
        dir_path = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\controls\theta\');
        file_to_read = strcat('statsData_c','.mat');
        load(strcat(dir_path,file_to_read))
        
        [rad_theta_gaze,amp_gaze] = getValsForPolarPlot(statsData_c,1,ii_c,rotate);
        [rad_theta_vel,amp_vel] = getValsForPolarPlot(statsData_c,2,ii_c,rotate);
        [rad_theta_ms,amp_ms] = getValsForPolarPlot(statsData_c,3,ii_c,rotate);
        
        rad_theta_vel_all(1,end+1) = rad_theta_vel;
        amp_vel_all(1,end+1) = amp_vel;
        
        rad_theta_ms_all(1,end+1) = rad_theta_ms;
        amp_ms_all(1,end+1) = amp_ms;
         if rotate == 1 && avg == 1
            g = polarplot(rad_theta_gaze,amp_gaze,'o');
            g.Color = 'red';g.MarkerSize = 12;
            hold on
            if ii_c == length(controls)
%                 v = polarplot(rad_theta_vel_all,amp_vel_all,'o');
%                 v.Color = 'blue';v.Marker = 'square';v.MarkerSize = 12;
%                 hold on
%                 clear v;
%                 legend(["Avg Gaze","Drift Velocity"],'Location','northwest')
                  m = polarplot(rad_theta_ms_all,amp_ms_all,'*');
                  m.Color = 'green';m.MarkerSize = 20;
                  hold on
                  clear m;
                  legend(["Avg Gaze","Microsaccade direction"],'Location','northwest')
            end
         else
        
            g = polarplot(rad_theta_gaze,amp_gaze,'o');
            g.Color = 'red';g.MarkerSize = 12;


            hold on
            v = polarplot(rad_theta_vel,amp_vel);
            v.Color = 'blue';v.Marker = 'square';v.MarkerSize = 12;


            hold on
            m = polarplot(rad_theta_ms,amp_ms,'*');
            m.Color = 'green';m.MarkerSize = 12;

            legend(["Avg Gaze","Drift Velocity","Microsaccade direction"],'Location','northwest')
            title(controls{ii_c})
         end
        
        if (rotate == 1 && ii_c == length(controls))
            pathToSave = 'C:/Users/sanjana/OneDrive - University of Rochester/Documents/Sanjana/Projects/schizophrenia/Figures/polarVectorPlots/controls/rotated';
            if ~exist(strcat(pathToSave), 'dir')
                mkdir(strcat(pathToSave))
            end
            fullPath = strcat(pathToSave,'/%s');
            strfName = 'all_controls_ms_bias';
            saveas(gcf,sprintf(fullPath, strfName), 'png')
            close all
        else
            pathToSave = 'C:/Users/sanjana/OneDrive - University of Rochester/Documents/Sanjana/Projects/schizophrenia/Figures/polarVectorPlots/controls/notRotated';
            if ~exist(strcat(pathToSave), 'dir')
                mkdir(strcat(pathToSave))
            end
            fullPath = strcat(pathToSave,'/%s');
            strfName = controls{ii_c};
            saveas(gcf,sprintf(fullPath, strfName), 'png')
            close all
        end
       
        
    end
    
end

function [rad_theta,amp] = getValsForPolarPlot(statsData,flag,idx,rotate)
    
  %% gaze angle calculation
        y_gaze = statsData(idx).gazeY;
        x_gaze = statsData(idx).gazeX;
        
        rad_theta_gaze = atan2(y_gaze,x_gaze);
        deg_theta_gaze = rad2deg(rad_theta_gaze);
        deg_rot_val = 90 - (deg_theta_gaze);
        rad_rot_val = deg2rad(deg_rot_val);
  %% vel angle calculation  
        y_vel = statsData(idx).velY;
        x_vel = statsData(idx).velX;
        rad_theta_vel = atan2(y_vel,x_vel);
  %% ms angle calculation
        y_ms = statsData(idx).msY;
        x_ms = statsData(idx).msX;
        rad_theta_ms = atan2(y_ms,x_ms);
  
  %% get vals depending on flags 1 for gaze, 2 for vel and 3 for ms
  %% use the same rotation obtained from gaze angle rotation
    if flag == 1 
        if rotate
            rad_theta = rad_theta_gaze + rad_rot_val;
        else
            rad_theta = rad_theta_gaze;
        end
        amp = sqrt(x_gaze^2+y_gaze^2);
    end
    if flag == 2
        if rotate
            rad_theta = rad_theta_vel + rad_rot_val;
        else
            rad_theta = rad_theta_vel;
        end
        amp = sqrt(x_vel^2+y_vel^2);
    end
    if flag == 3
        if rotate
            rad_theta = rad_theta_ms + rad_rot_val;
        else
            rad_theta = rad_theta_ms;
        end
        amp = sqrt(x_ms^2+y_ms^2);
    end
    
    amp = 1;
    
end