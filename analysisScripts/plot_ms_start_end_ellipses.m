function plot_ms_start_end_ellipses(patients,controls)
 
    scatPlot = 0;
    cP = distinguishable_colors(length(patients));
    cC = distinguishable_colors(length(controls));
    [all_dist_start_toC, all_dist_end_toC,...
        rate_all,mean_amp_all] = deal([]);
    [all_dist_start_toC_P_fill,all_dist_start_toC_C_fill] = deal([]);
    for ii_p = 1:length(patients)
        dir_path = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\patients\',patients{ii_p},'\matFiles\');
%         
        
%         file_to_read = strcat('ms_info_blankS_',patients{ii_p},'.mat');
        file_to_read = strcat('ms_info_task_',patients{ii_p},'.mat');
        load(strcat(dir_path,file_to_read))
        
       
        title_str = patients{ii_p};
       
        [all_dist_start_toC,all_dist_end_toC,...
            rate_all,mean_amp_all] = ms_Analysis_2023(msInfo,scatPlot,title_str,cP,ii_p,all_dist_start_toC,all_dist_end_toC,...
                                    rate_all,mean_amp_all);
        
       
    end
    hold on
    std_err_start_toC = std(all_dist_start_toC)/sqrt(length(all_dist_start_toC));
    std_err_end_toC = std(all_dist_end_toC)/sqrt(length(all_dist_end_toC));
    
    errorbar([mean(all_dist_start_toC) mean(all_dist_end_toC) ],...
        [std_err_start_toC  std_err_end_toC ],...
        '-o', 'Color', 'k','LineWidth',3)
%     [p,h] = ranksum(all_dist_start_toC,all_dist_end_toC);
%     ylim([5 40])
    
    for ii_c = 1:length(controls)
        if ii_c < 10
            dir_path = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\controls\HUX_C\',controls{ii_c},'\matFiles\');
        else
            dir_path = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\controls\',controls{ii_c},'\matFiles\');
        end
%         file_to_read = strcat('ms_info_blankS_',controls{ii_c},'.mat');
        file_to_read = strcat('ms_info_task_',controls{ii_c},'.mat');
        load(strcat(dir_path,file_to_read))
        title_str = controls{ii_c};
         
        [all_dist_start_toC,all_dist_end_toC,...
            rate_all,mean_amp_all] = ms_Analysis_2023(msInfo,scatPlot,title_str,cC,ii_c,all_dist_start_toC,all_dist_end_toC,...
                                    rate_all,mean_amp_all);
                             
       
        
        
    end
    
    hold on

    std_err_start_toC = std(all_dist_start_toC)/sqrt(length(all_dist_start_toC));
    std_err_end_toC = std(all_dist_end_toC)/sqrt(length(all_dist_end_toC));
    
    errorbar([mean(all_dist_start_toC) mean(all_dist_end_toC) ],...
        [std_err_start_toC  std_err_end_toC ],...
        '-o', 'Color', 'k','LineWidth',3)
%     ylim([5 40])
%     [p,h] = ranksum(all_dist_start_toC,all_dist_end_toC);
        
end