function [Speed,Velocity, vel_x, vel_y] = CalculateDriftVelocity(drifts,TimeInterval)

% Calculates drift velocity and speed given x and y positions and a time
% interval. Velocity and speed are calculated using sgfilt.


if ~( length(TimeInterval) == 2 || length(TimeInterval) == 1 )
    error('Time interval not valid')
end
vel_x = []; vel_y = [];
%for ii = 1:length(drifts(1))
    
    if length(TimeInterval) == 2
        tmp_x = sgfilt(drifts.x(TimeInterval(1):TimeInterval(2)),3,41,1);
        tmp_y = sgfilt(drifts.y(TimeInterval(1):TimeInterval(2)),3,41,1);
        x = tmp_x(floor((41/2)+10):end-floor(41/2));
        y = tmp_y(floor((41/2)+10):end-floor(41/2));
        vel_tmp = sqrt(x .^2 + y .^ 2) * 1000;
        speed_tmp(ii,:) = sqrt(x .^ 2+ y .^ 2) * 1000;
        vel_x = [ vel_x x];
        vel_y = [ vel_y y];
    else
        tmp_x = sgfilt(drifts.x(TimeInterval(1):end),3,41,1);
        tmp_y = sgfilt(drifts.y(TimeInterval(1):end),3,41,1);
        x = tmp_x(floor((41/2)+10):end-floor(41/2));
        y = tmp_y(floor((41/2)+10):end-floor(41/2));
        vel_tmp = sqrt(x .^2 + y .^ 2) * 1000;
        vel_x = [ vel_x x];
        vel_y = [ vel_y y];
        
    end
    
%end

Velocity = nanmean(vel_tmp);%(cell2mat(vel_tmp));

if length(TimeInterval) == 2
    Speed.avg = nanmean(speed_tmp);
    Speed.SE = nanstd(speed_tmp)/sqrt(length(speed_tmp));
else
    Speed = [];
end

end
