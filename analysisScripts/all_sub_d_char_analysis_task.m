function all_sub_d_char_analysis_task(patients,controls,HUX_C)
    
    [all_span_P,all_curv_P,all_PLF_P,all_mn_speed_P,all_vel_P,all_amp_P,all_bcea_P] = deal([]);
    [all_span_C,all_curv_C,all_PLF_C,all_mn_speed_C,all_vel_C,all_amp_C,all_bcea_C] = deal([]);
    [all_span_HC,all_curv_HC,all_PLF_HC,all_mn_speed_HC,all_vel_HC,all_amp_HC,all_bcea_HC] = deal([]);
    cP = jet(length(patients));
    cC = winter(length(controls));
    cH = summer(length(HUX_C));
    for ii_p = 1:length(patients)
        dir_path = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\patients\',patients{ii_p},'\matFiles\');
        file_to_read = strcat('em_info_cut_drift_',patients{ii_p},'.mat');
        load(strcat(dir_path,file_to_read))
        ln_em = numel(fieldnames(em.ecc_0));
        
        [all_span,all_curv,all_PLF,all_mn_speed,all_vel,all_amp,all_bcea] = deal([]);
        for t_size = 1:ln_em
            f_name = fieldnames(em.ecc_0);

            all_chars = eval(strcat('em.ecc_0.',f_name{t_size}));
            for all = 1:length(all_chars.response)
                if all_chars.span(all) > 40
                    continue
                end
%                 if all_chars.curvature{all} > 40
%                     continue
%                 end
                if all_chars.prlDistance(all) > 40
                    continue
                end
%                 if all_chars.mn_speed{all} > 40
%                     continue
%                 end
%                 if all_chars.velocity(all) > 40
%                     continue
%                 end
%                 if all_chars.amplitude(all) > 40
%                     continue
%                 end
                all_span(1,end+1) = all_chars.span(all);
                all_curv(1,end+1) = all_chars.curvature{all};
                all_PLF(1,end+1) = all_chars.prlDistance(all);
                all_mn_speed(1,end+1) = all_chars.mn_speed{all};
                all_vel(1,end+1) = all_chars.velocity(all);
                all_amp(1,end+1) = all_chars.amplitude(all);
%                 all_bcea(1,end+1) = all_chars.bcea{all};
            end
        end
        clear em
        all_span_P(ii_p) = mean(all_span(~isnan(all_span)));
        all_curv_P(ii_p)  = mean(all_curv(~isnan(all_curv)));
        all_PLF_P(ii_p) = mean(all_PLF(~isnan(all_PLF)));
        all_mn_speed_P(ii_p) = mean(all_mn_speed(~isnan(all_mn_speed)));
        all_vel_P(ii_p) = mean(all_vel(~isnan(all_vel)));
        all_amp_P(ii_p) = mean(all_amp(~isnan(all_amp)));
%         all_bcea_P(ii_p) = mean(all_bcea(~isnan(all_bcea)));
        
        figure(1)
        plot([1],[all_span_P(ii_p)],'o','MarkerSize',10,'MarkerFaceColor',cP(ii_p,:))
        hold on
        figure(2)
        plot([1],[all_curv_P(ii_p)],'o','MarkerSize',10,'MarkerFaceColor',cP(ii_p,:))
        hold on
        figure(3)
        plot([1],[all_PLF_P(ii_p)],'o','MarkerSize',10,'MarkerFaceColor',cP(ii_p,:))
        hold on
        figure(4)
        plot([1],[all_mn_speed_P(ii_p)],'o','MarkerSize',10,'MarkerFaceColor',cP(ii_p,:))
        hold on
        figure(5)
        plot([1],[all_vel_P(ii_p)],'o','MarkerSize',10,'MarkerFaceColor',cP(ii_p,:))
        hold on
        figure(6)
        plot([1],[all_amp_P(ii_p)],'o','MarkerSize',10,'MarkerFaceColor',cP(ii_p,:))
        hold on
        
    end
    
    for ii_c = 1:length(controls)
        dir_path = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\controls\',controls{ii_c},'\matFiles\');
        file_to_read = strcat('em_info_cut_drift_',controls{ii_c},'.mat');
        load(strcat(dir_path,file_to_read))
        ln_em = numel(fieldnames(em.ecc_0));
        
        [all_span,all_curv,all_PLF,all_mn_speed,all_vel,all_amp,all_bcea] = deal([]);
        for t_size = 1:ln_em
            f_name = fieldnames(em.ecc_0);
            all_chars = eval(strcat('em.ecc_0.',f_name{t_size}));
            for all = 1:length(all_chars.response)
                if all_chars.span(all) > 40
                    continue
                end
%                 if all_chars.curvature{all} > 40
%                     continue
%                 end
                if all_chars.prlDistance(all) > 40
                    continue
                end
%                 if all_chars.mn_speed{all} > 40
%                     continue
%                 end
%                 if all_chars.velocity(all) > 40
%                     continue
%                 end
%                 if all_chars.amplitude(all) > 40
%                     continue
%                 end
                all_span(1,end+1) = all_chars.span(all);
                all_curv(1,end+1) = all_chars.curvature{all};
                all_PLF(1,end+1) = all_chars.prlDistance(all);
                all_mn_speed(1,end+1) = all_chars.mn_speed{all};
                all_vel(1,end+1) = all_chars.velocity(all);
                all_amp(1,end+1) = all_chars.amplitude(all);
%                 all_bcea(1,end+1) = all_chars.bcea{all};
            end
        end
        clear em
        all_span_C(ii_c) = mean(all_span(~isnan(all_span)));
        all_curv_C(ii_c)  = mean(all_curv(~isnan(all_curv)));
        all_PLF_C(ii_c) = mean(all_PLF(~isnan(all_PLF)));
        all_mn_speed_C(ii_c) = mean(all_mn_speed(~isnan(all_mn_speed)));
        all_vel_C(ii_c) = mean(all_vel(~isnan(all_vel)));
        all_amp_C(ii_c) = mean(all_amp(~isnan(all_amp)));
%         all_bcea_C(ii_c) = mean(all_bcea(~isnan(all_bcea)));
        
        figure(1)
        plot([2],[all_span_C(ii_c)],'o','MarkerSize',10,'MarkerFaceColor',cC(ii_c,:))
        hold on
        figure(2)
        plot([2],[all_curv_C(ii_c)],'o','MarkerSize',10,'MarkerFaceColor',cC(ii_c,:))
        hold on
        figure(3)
        plot([2],[all_PLF_C(ii_c)],'o','MarkerSize',10,'MarkerFaceColor',cC(ii_c,:))
        hold on
        figure(4)
        plot([2],[all_mn_speed_C(ii_c)],'o','MarkerSize',10,'MarkerFaceColor',cC(ii_c,:))
        hold on
        figure(5)
        plot([2],[all_vel_C(ii_c)],'o','MarkerSize',10,'MarkerFaceColor',cC(ii_c,:))
        hold on
        figure(6)
        plot([2],[all_amp_C(ii_c)],'o','MarkerSize',10,'MarkerFaceColor',cC(ii_c,:))
        hold on
       
    end
    
     for ii_hc = 1:length(HUX_C)
        dir_path = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\controls\HUX_C\',HUX_C{ii_hc},'\matFiles\');
        file_to_read = strcat('em_info_cut_drift_',HUX_C{ii_hc},'.mat');
        load(strcat(dir_path,file_to_read))
        ln_em = numel(fieldnames(em.ecc_0));
        
        [all_span,all_curv,all_PLF,all_mn_speed,all_vel,all_amp,all_bcea] = deal([]);
        for t_size = 1:ln_em
            f_name = fieldnames(em.ecc_0);
            all_chars = eval(strcat('em.ecc_0.',f_name{t_size}));
            for all = 1:length(all_chars.response)
                if all_chars.span(all) > 40
                    continue
                end
%                 if all_chars.curvature{all} > 40
%                     continue
%                 end
                if all_chars.prlDistance(all) > 40
                    continue
                end
%                 if all_chars.mn_speed{all} > 40
%                     continue
%                 end
%                 if all_chars.velocity(all) > 40
%                     continue
%                 end
%                 if all_chars.amplitude(all) > 40
%                     continue
%                 end
                all_span(1,end+1) = all_chars.span(all);
                all_curv(1,end+1) = all_chars.curvature{all};
                all_PLF(1,end+1) = all_chars.prlDistance(all);
                all_mn_speed(1,end+1) = all_chars.mn_speed{all};
                all_vel(1,end+1) = all_chars.velocity(all);
                all_amp(1,end+1) = all_chars.amplitude(all);
%                 all_bcea(1,end+1) = all_chars.bcea{all};
            end
        end
        clear em
        all_span_HC(ii_hc) = mean(all_span(~isnan(all_span)));
        all_curv_HC(ii_hc)  = mean(all_curv(~isnan(all_curv)));
        all_PLF_HC(ii_hc) = mean(all_PLF(~isnan(all_PLF)));
        all_mn_speed_HC(ii_hc) = mean(all_mn_speed(~isnan(all_mn_speed)));
        all_vel_HC(ii_hc) = mean(all_vel(~isnan(all_vel)));
        all_amp_HC(ii_hc) = mean(all_amp(~isnan(all_amp)));
%         all_bcea_C(ii_c) = mean(all_bcea(~isnan(all_bcea)));
        
        figure(1)
        plot([3],[all_span_HC(ii_hc)],'o','MarkerSize',10,'MarkerFaceColor',cH(ii_hc,:))
        hold on
        figure(2)
        plot([3],[all_curv_HC(ii_hc)],'o','MarkerSize',10,'MarkerFaceColor',cH(ii_hc,:))
        hold on
        figure(3)
        plot([3],[all_PLF_HC(ii_hc)],'o','MarkerSize',10,'MarkerFaceColor',cH(ii_hc,:))
        hold on
        figure(4)
        plot([3],[all_mn_speed_HC(ii_hc)],'o','MarkerSize',10,'MarkerFaceColor',cH(ii_hc,:))
        hold on
        figure(5)
        plot([3],[all_vel_HC(ii_hc)],'o','MarkerSize',10,'MarkerFaceColor',cH(ii_hc,:))
        hold on
        figure(6)
        plot([3],[all_amp_HC(ii_hc)],'o','MarkerSize',10,'MarkerFaceColor',cH(ii_hc,:))
        hold on
       
    end
    
    
    figure(1)
    std_err_span_P = std(all_span_P)/sqrt(length(all_span_P));
    std_err_span_C = std(all_span_C)/sqrt(length(all_span_C));
    std_err_span_HC = std(all_span_HC)/sqrt(length(all_span_HC));
    errorbar([mean(all_span_P) mean(all_span_C) mean(all_span_HC)],...
              [std_err_span_P std_err_span_C std_err_span_HC],...
              'o', 'Color', 'k', 'LineStyle', '-','LineWidth',3)
    set(gca, 'XTick', [1:1:3], 'XTickLabel', {'SZ'; 'HC'; 'HUX-C'}, 'FontSize', 12);
    ylabel('Drift span [arcmin]')
    set(gca,'fontsize',27,'FontWeight','Bold')
    xlim([0 4])
%     saveas(gcf,'C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\Figures\driftPlots\d_span_task_p_c.png')
%     [p_sp h_sp stats] = ranksum(all_span_P,all_span_C);
%     if h_sp == 1
%         text(2.1, mean(all_span_P)+1, '*', 'FontSize', 45,'FontWeight','bold') %0.94
%     end
% 
    figure(2)
    std_err_curv_P = std(all_curv_P)/sqrt(length(all_curv_P));
    std_err_curv_C = std(all_curv_C)/sqrt(length(all_curv_C));
    std_err_curv_HC = std(all_curv_HC)/sqrt(length(all_curv_HC));
    errorbar([mean(all_curv_P) mean(all_curv_C) mean(all_curv_HC)],...
              [std_err_curv_P std_err_curv_C std_err_curv_HC],...
              'o', 'Color', 'k', 'LineStyle', '-','LineWidth',3)
    set(gca, 'XTick', [1:1:3], 'XTickLabel', {'SZ'; 'HC'; 'HUX-C'}, 'FontSize', 12);
    ylabel('Drift curvature [arcmin^-1]')
    set(gca,'fontsize',27,'FontWeight','Bold')
    xlim([0 4])
% %     saveas(gcf,'C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\Figures\driftPlots\d_curvature_task_p_c.png')
% %     [p_curv h_curv stats] = ranksum(all_curv_P,all_curv_C);
%     if h_curv == 1
%         text(1.5, mean(all_curv_P)+1, '*', 'FontSize', 45,'FontWeight','bold') %0.94
%     end
%     
    figure(3)
    std_err_PLF_P = std(all_PLF_P)/sqrt(length(all_PLF_P));
    std_err_PLF_C = std(all_PLF_C)/sqrt(length(all_PLF_C));
    std_err_PLF_HC = std(all_PLF_HC)/sqrt(length(all_PLF_HC));
    errorbar([mean(all_PLF_P) mean(all_PLF_C) mean(all_PLF_HC)],...
              [std_err_PLF_P std_err_PLF_C std_err_PLF_HC],...
              'o', 'Color', 'k', 'LineStyle', '-','LineWidth',3)
    set(gca, 'XTick', [1:1:3], 'XTickLabel', {'SZ'; 'HC'; 'HUX-C'}, 'FontSize', 12);
    ylabel('PLF [arcmin]')
    set(gca,'fontsize',27,'FontWeight','Bold')
    xlim([0 4])
%     saveas(gcf,'C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\Figures\driftPlots\PLF_task_p_c.png')
% %     [p_PLF h_PLF stats] = ranksum(all_PLF_P,all_PLF_C);
% %     if h_PLF == 1
% %         text(2.1, mean(all_PLF_P)+1, '*', 'FontSize', 45,'FontWeight','bold') %0.94
% %     end
%     
    figure(4)
    std_err_mn_speed_P = std(all_mn_speed_P)/sqrt(length(all_mn_speed_P));
    std_err_mn_speed_C = std(all_mn_speed_C)/sqrt(length(all_mn_speed_C));
    std_err_mn_speed_HC = std(all_mn_speed_HC)/sqrt(length(all_mn_speed_HC));
    errorbar([mean(all_mn_speed_P(~isnan(all_mn_speed_P))) mean(all_mn_speed_C) mean(all_mn_speed_HC)],...
              [std_err_mn_speed_P std_err_mn_speed_C std_err_mn_speed_HC],...
              'o', 'Color', 'k', 'LineStyle', '-','LineWidth',3)
    set(gca, 'XTick', [1:1:3], 'XTickLabel', {'SZ'; 'HC'; 'HUX-C'}, 'FontSize', 12);
    ylabel('Drift speed [arcmin/s]')
    set(gca,'fontsize',27,'FontWeight','Bold')
    xlim([0 4])
%     saveas(gcf,'C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\Figures\driftPlots\d_speed_task_p_c.png')
% %     [p_mn_s h_mn_s stats] = ranksum(all_mn_speed_P,all_mn_speed_C);
% %     if h_mn_s == 1
% %         text(2.1, mean(all_mn_speed_P)+1, '*', 'FontSize', 45,'FontWeight','bold') %0.94
% %     end
%     
    figure(5)
    std_err_vel_P = std(all_vel_P)/sqrt(length(all_vel_P));
    std_err_vel_C = std(all_vel_C)/sqrt(length(all_vel_C));
    std_err_vel_HC = std(all_vel_HC)/sqrt(length(all_vel_HC));
    errorbar([mean(all_vel_P) mean(all_vel_C) mean(all_vel_HC)],...
              [std_err_vel_P std_err_vel_C std_err_vel_HC],...
              'o', 'Color', 'k', 'LineStyle', '-','LineWidth',3)
    set(gca, 'XTick', [1:1:3], 'XTickLabel', {'SZ'; 'HC'; 'HUX-C'}, 'FontSize', 12);
    ylabel('drift velocity')
    set(gca,'fontsize',27,'FontWeight','Bold')
    xlim([0 4])
% %     [p_vel h_vel stats] = ranksum(all_vel_P,all_vel_C);
% %     if h_vel == 1
% %         text(2.1, mean(all_vel_P)+1, '*', 'FontSize', 45,'FontWeight','bold') %0.94
% %     end
%     
    figure(6)
    std_err_amp_P = std(all_amp_P)/sqrt(length(all_amp_P));
    std_err_amp_C = std(all_amp_C)/sqrt(length(all_amp_C));
    std_err_amp_HC = std(all_amp_C)/sqrt(length(all_amp_HC));
    errorbar([mean(all_amp_P) mean(all_amp_C) mean(all_amp_HC)],...
              [std_err_amp_P std_err_amp_C std_err_amp_HC],...
              'o', 'Color', 'k', 'LineStyle', '-','LineWidth',3)
    set(gca, 'XTick', [1:1:3], 'XTickLabel', {'SZ'; 'HC'; 'HUX-C'}, 'FontSize', 12);
    ylabel('Drift amplitude [arcmin]')
    set(gca,'fontsize',27,'FontWeight','Bold')
    xlim([0 4])
%     saveas(gcf,'C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\Figures\driftPlots\d_amplitude_task_p_c.png')
% %     [p_amp h_amp stats] = ranksum(all_amp_P,all_amp_C);
% %     if h_amp == 1
% %         text(2.1, mean(all_amp_P)+1, '*', 'FontSize', 45,'FontWeight','bold') %0.94
% %     end

              
end