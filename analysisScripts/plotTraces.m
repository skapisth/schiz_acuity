directory = 'C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\controls\PC-C08\';
folder = 'unCrowded\';

fullpath = strcat(directory,folder);

% load([fullpath sprintf('ms_data.mat')]);
load([strcat(fullpath,'pptrials.mat')])

data.user = pptrials;
% data.user = ms_data;
for all = 1:length(data.user)

    tagTimeStamps(data.user{all},all)
    
end


function tagTimeStamps(trial,all)
    
    
    x_offset = trial.xoffset * trial.pixelAngle;
    y_offset = trial.yoffset * trial.pixelAngle;
    x_pos_arcmin = trial.x.position;
    y_pos_arcmin = trial.y.position;
    x_trace = x_pos_arcmin(:) + x_offset; %x trace with offset in arcmin
    y_trace = y_pos_arcmin(:) + y_offset; %y trace with offset in arcmin
    
    tm = (1:(1000/330):length(x_trace)*1000/330); % (ms/sample) for DDPI

    a = plot(tm,x_trace,'r'); hold on; % in arcmin 
    b = plot(tm,y_trace,'b'); hold on;% in arcmin
    
    
    if (trial.TimeFixationON~= 0)
        fixOn = plot([(trial.TimeFixationON),(trial.TimeFixationON)],ylim, 'k'); hold on;
        fixOff = plot([(trial.TimeFixationOFF),(trial.TimeFixationOFF)],ylim, '-k'); hold on;
    else
        fixOn = plot([(trial.TimeTargetON),(trial.TimeTargetON)],ylim, 'k'); hold on;
        fixOff = plot([(trial.TimeTargetOFF),(trial.TimeTargetOFF)],ylim, '-k'); hold on;
    end
    
    
    
    vals = [a, b, fixOn,fixOff];
%     'Delay On', 'Delay Off',
    if (trial.TimeFixationON~= 0)
        legend(vals,{'xTrace', 'yTrace','Fixation On', 'Fixation Off'});
    else
        legend(vals,{'xTrace', 'yTrace','Target On', 'Target Off'});
    end
    if (trial.TimeFixationON== 0)
        xlim([0 1500]);
    end
    ylim([-30 30]);

    title(sprintf("Trial #: %i", all));
    xlabel('Time (ms)')
    ylabel('Position in arc mins')
    input ''
    clf
    
end