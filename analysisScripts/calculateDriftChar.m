function [em,x_all_bcea,y_all_bcea,...
        x_all_bcea_b1,y_all_bcea_b1,...
        x_all_bcea_b2,y_all_bcea_b2,...
        x_all_bcea_b3,y_all_bcea_b3,...
        x_all_bcea_b4,y_all_bcea_b4,...
        x_all_bcea_b5,y_all_bcea_b5] = calculateDriftChar(valueGroup,params,pptrials,...
                                                         em,x_all_bcea,y_all_bcea,...
                                                         x_all_bcea_b1,y_all_bcea_b1,...
                                                         x_all_bcea_b2,y_all_bcea_b2,...
                                                         x_all_bcea_b3,y_all_bcea_b3,...
                                                         x_all_bcea_b4,y_all_bcea_b4,...
                                                         x_all_bcea_b5,y_all_bcea_b5)

idx = valueGroup.idx;
strokeWidth = valueGroup.strokeWidth;
ecc = valueGroup.ecc;
stimulusSize = valueGroup.stimulusSize;

drift_dur_limit_ms = 250;

counter = 1;
[xValues,yValues] = deal([]);
[xOffset, yOffset] = deal([]);

bin1 = 100*(1/params.samplingRate);
bin2 = 200*(1/params.samplingRate);
bin3 = 300*(1/params.samplingRate);
bin4 = 400*(1/params.samplingRate);
bin5 = 500*(1/params.samplingRate);

count_empty_drifts = 0;
for idxValue = 1:length(idx)
    i = idx(idxValue);
    dataStruct.id(counter) = i;
    
    if params.newEyeris
        timeOn = 1;
        timeOff = length(pptrials{i}.x.position);
    else
        timeOn = round(pptrials{i}.TimeTargetON/params.samplingRate);
        timeOff = floor(min(pptrials{i}.TimeTargetOFF/params.samplingRate, pptrials{i}.ResponseTime/params.samplingRate));
        if params.blankScreenAnalysis
            timeOn = 1;%round(pptrials{i}.TimeTargetON - 400/params.samplingRate);
            timeOff = floor(min(pptrials{i}.TimeTargetON/params.samplingRate, pptrials{i}.ResponseTime/params.samplingRate));
        end
    end
    x_full = pptrials{i}.x.position(ceil(timeOn):floor(timeOff)) + pptrials{i}.xoffset * params.pixelAngle(i);
    y_full = pptrials{i}.y.position(ceil(timeOn):floor(timeOff)) + pptrials{i}.yoffset * params.pixelAngle(i);
    
    x_all_bcea = [x_all_bcea,x_full];
    y_all_bcea = [y_all_bcea,y_full];
    
    x_all_bcea_b1 = [x_all_bcea_b1,x_full(1:bin1)];
    y_all_bcea_b1 = [y_all_bcea_b1,y_full(1:bin1)];
    
    x_all_bcea_b2 = [x_all_bcea_b2,x_full(bin1:bin2)];
    y_all_bcea_b2 = [y_all_bcea_b2,y_full(bin1:bin2)];
    
    x_all_bcea_b3 = [x_all_bcea_b3,x_full(bin2:bin3)];
    y_all_bcea_b3 = [y_all_bcea_b3,y_full(bin2:bin3)];
    
    
    
    if params.blankScreenAnalysis
        x_all_bcea_b4 = [x_all_bcea_b4,x_full(bin3:end)];
        y_all_bcea_b4 = [y_all_bcea_b4,y_full(bin3:end)];
        x_all_bcea_b5 = 0;
        y_all_bcea_b5 = 0;
    else
        x_all_bcea_b4 = [x_all_bcea_b4,x_full(bin3:bin4)];
        y_all_bcea_b4 = [y_all_bcea_b4,y_full(bin3:bin4)];
        x_all_bcea_b5 = [x_all_bcea_b5,x_full(bin4:end)];
        y_all_bcea_b5 = [y_all_bcea_b5,y_full(bin4:end)];
    end

    
    if params.short_300ms_analysis
        findms = find(pptrials{i}.microsaccades.start > timeOn & pptrials{i}.microsaccades.start < timeOff);
        if isempty(findms)
            find_d_segs = find(pptrials{i}.drifts.start <= timeOn);
            if isempty(find_d_segs)
                count_empty_drifts = count_empty_drifts+1;
                flag = 0;
                continue;
            else
                find_d_segs = find_d_segs(end);
            end
            d_dur = pptrials{i}.drifts.duration(find_d_segs);
            d_dur_ms = d_dur*params.samplingRate;
            needed_d_seg = drift_dur_limit_ms/params.samplingRate; %(300ms converted to samples)
            if d_dur_ms >=drift_dur_limit_ms
                time_drift_start = pptrials{i}.drifts.start(find_d_segs);
                x = pptrials{i}.x.position(ceil(time_drift_start):floor(time_drift_start+needed_d_seg)) + pptrials{i}.xoffset * params.pixelAngle(i);
                y = pptrials{i}.y.position(ceil(time_drift_start):floor(time_drift_start+needed_d_seg)) + pptrials{i}.yoffset * params.pixelAngle(i);
                flag = 1;
            else
                flag = 0;
                continue
            end
        else
            ms_start_sample = pptrials{i}.microsaccades.start(findms);
            samples_before_ms = ms_start_sample - timeOn;
            samples_after_ms = timeOff - ms_start_sample;
            find_d_segs = find(pptrials{i}.drifts.start <= timeOn);
            if isempty(find_d_segs)
                count_empty_drifts = count_empty_drifts+1;
                flag = 0;
                continue;
            else
                find_d_segs = find_d_segs(end);
            end
            d_dur = pptrials{i}.drifts.duration(find_d_segs);
            d_dur_ms = d_dur*params.samplingRate;
            needed_d_seg = drift_dur_limit_ms/params.samplingRate; %(300ms converted to samples)
            if samples_before_ms >= needed_d_seg
                if d_dur_ms >=drift_dur_limit_ms
                    time_drift_start = pptrials{i}.drifts.start(find_d_segs);
                    x = pptrials{i}.x.position(ceil(time_drift_start):floor(time_drift_start+needed_d_seg)) + pptrials{i}.xoffset * params.pixelAngle(i);
                    y = pptrials{i}.y.position(ceil(time_drift_start):floor(time_drift_start+needed_d_seg)) + pptrials{i}.yoffset * params.pixelAngle(i);
                    flag = 1;
                else
                    flag = 0;
                end
    %         elseif samples_after_ms >= needed_d_seg
    %             if d_dur_ms >=drift_dur_limit_ms
    %                 x = pptrials{i}.x.position(ceil(timeOn):floor(timeOn+needed_d_seg)) + pptrials{i}.xoffset * params.pixelAngle(i);
    %                 y = pptrials{i}.y.position(ceil(timeOn):floor(timeOn+needed_d_seg)) + pptrials{i}.yoffset * params.pixelAngle(i);
    %                 flag = 1;
    %             else
    %                 flag = 0;
    %             end
            else
                flag = 0;
                continue;
            end
        end
    else
        x =  pptrials{i}.x.position(ceil(timeOn):floor(timeOff)) + pptrials{i}.xoffset * params.pixelAngle(i);
        y = pptrials{i}.y.position(ceil(timeOn):floor(timeOff)) + pptrials{i}.yoffset * params.pixelAngle(i);
        flag = 1;
    end
    
    if flag
        dataStruct.position(counter).x = x;
        dataStruct.position(counter).y = y;

        dataStruct.recenteredposition(counter).x = x-x(1);
        dataStruct.recenteredposition(counter).y = y-y(1);

        xValues = [xValues, x];
        yValues = [yValues, y];

        xOffset = [xOffset, pptrials{i}.xoffset * params.pixelAngle(i)];
        xOffset = [xOffset, pptrials{i}.yoffset * params.pixelAngle(i)];

%         dataStruct.flankers(counter) = pptrials{i}.FlankerOrientations;
        dataStruct.response(counter) = pptrials{i}.Response;
        dataStruct.responseTime(counter) = pptrials{i}.ResponseTime;
        dataStruct.target(counter) = pptrials{i}.TargetOrientation;
        dataStruct.correct(counter) = double(pptrials{i}.Correct);


        [~, dataStruct.velocity(counter), ...
            velX{counter}, ...
            velY{counter}] = ...
            CalculateDriftVelocity(dataStruct.position(counter), 2);

        [~, dataStruct.instSpX{counter},...
            dataStruct.instSpY{counter},...
            dataStruct.mn_speed{counter},...
            dataStruct.driftAngle{counter},...
            dataStruct.curvature{counter},...
            dataStruct.varx{counter},...
            dataStruct.vary{counter},...
            dataStruct.span(counter), ...
            dataStruct.amplitude(counter)] = getDriftChar(x, y, 41, 1, 250);
        
        [dataStruct.prlDistance(counter),...
         dataStruct.prlDistanceX(counter),...
         dataStruct.prlDistanceY(counter)] = get_PRL(x_full, y_full);
        counter = counter + 1;
    end
end
if flag
    dataStruct.ccDistance = unique(stimulusSize)*1.4 - stimulusSize/2;
    dataStruct.stimulusSize = unique(stimulusSize);
    dataStruct.performanceAtSize = round(sum(dataStruct.correct)/length(dataStruct.correct),2);

    %Full Length
    forDSQCalc.x = {dataStruct.position.x};
    forDSQCalc.y = {dataStruct.position.y};

    [~,dataStruct.Bias,dataStruct.dCoefDsq, ~, dataStruct.Dsq, ...
        dataStruct.SingleSegmentDsq,~,~] = ...
        CalculateDiffusionCoef(params.samplingRate, struct('x',forDSQCalc.x, 'y', forDSQCalc.y));


    [ xValues, yValues] = checkXYArt(xValues, yValues, min(round(length(xValues)/100)),31); %Checks strange artifacts
    [ xValues, yValues] = checkXYBound(xValues, yValues, 30); %Checks boundries

    limit.xmin = floor(min(xValues));
    limit.xmax = ceil(max(xValues));
    limit.ymin = floor(min(yValues));
    limit.ymax = ceil(max(yValues));

    n_bins = 20;

    result = MyHistogram2(xValues, yValues, [limit.xmin,limit.xmax,n_bins;limit.ymin,limit.ymax,n_bins]);
    dataStruct.result = result./(max(max(result)));

    dataStruct.xAll = xValues;
    dataStruct.yAll = yValues;

    dataStruct.xOffset = xOffset;
    dataStruct.yOffset = yOffset;
    %bias - how brownian DC is; AC sugg - plot dCoefDsq - higher - drift is
    %larger, more motion..DC - potentially combining, speed and
    %curvature..think aboout moving slower but less curved, moving faster, more
    %curved..
    em.(ecc).(strokeWidth) = dataStruct;
end

end
