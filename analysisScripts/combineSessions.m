userFlags.combinePPTrials = '1';
userFlags.saveFileFlag = '0';

subjects = {'HUX18'};

for si = 1:length(subjects)

    userVariables.pathToFile = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\controls\AC_controls\',subjects{si});
    userVariables.pathToSave = userVariables.pathToFile;
    [pptrials] = callToSaveCombineAndManuallyMark(userVariables, userFlags);
    
end