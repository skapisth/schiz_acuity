clear all;
clc;clear all;
% subjects = {'PC-S01','PC-S02','PC-S04','PC-S07','PC-S12','PC-S16','FE-S01','FE-S08','FE-S09'};
subjects = {'HUX4','HUX10','HUX12','HUX18','HUX21','HUX22','HUX23','Hux24','MP','PC-C06','FE-C19','FE-C05','FE-C17'};
% subjects = {'PC-S04'};
patient = 0;
newEyeris = 0;
% load('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\patients\acuityThreshMats\all_thresh_.mat');

for ii_s = 1:length(subjects)
    
    if patient
        load(strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\patients\cleanDrifts\','cleanD_struct_',subjects{ii_s},'.mat'));
         if length(subjects) > 1
            if ii_s > 3
                newEyeris = 1;
                Fs = 1000;
                params.samplingRate = 1000/Fs;
            else
                newEyeris = 0;
                Fs = 341;
                params.samplingRate = 1000/Fs;
            end
        else
            if newEyeris
                Fs = 1000;
                params.samplingRate = 1000/Fs;
            else
                Fs = 341;
                params.samplingRate = 1000/Fs;
            end
         end
    else
         load(strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\controls\cleanDrifts\','cleanD_struct_',subjects{ii_s},'.mat'));
        if length(subjects) > 1
            if ii_s > 10
                newEyeris = 1;
                Fs = 1000;
                params.samplingRate = 1000/Fs;
            else
                newEyeris = 0;
                Fs = 341;
                params.samplingRate = 1000/Fs;
            end
        else
            if newEyeris
                Fs = 1000;
                params.samplingRate = 1000/Fs;
            else
                Fs = 341;
                params.samplingRate = 1000/Fs;
            end

         end
    end
    all_long_drifts = all_long_drifts_clean;
    %% powerspectrum without any filtering - adapted from scripts by JI
    nfft = 64; %set it to 64 and replot everything sk july 7
    p_welch = struct(); % for x trace
    for di = 1:length(all_long_drifts)
        [ps, f] = pwelch(all_long_drifts(di).x - nanmean(all_long_drifts(di).x), hann(nfft), nfft/2, nfft, Fs);
        ps_tmp(di, :) = ps;
    end
    p_welch.x = ps_tmp;
    p_welch_x.freq = f;
    
    for di = 1:length(all_long_drifts)
        [ps, f] = pwelch(all_long_drifts(di).y - nanmean(all_long_drifts(di).y), hann(nfft), nfft/2, nfft, Fs);
        ps_tmp(di, :) = ps;
    end
    p_welch.y = ps_tmp;
    p_welch_y.freq = f;
    
    
    %% plotting
    flim = 10;
    f_use_x = p_welch_x.freq > flim;
    f_use_y = p_welch_y.freq > flim;
    
    m_ps_x = nanmean(p_welch.x, 1);
    se_ps_x = nanstd(p_welch.x, [], 1) / sqrt(size(p_welch.x, 1));
    m_ps_y = nanmean(p_welch.y, 1);
    se_ps_y = nanstd(p_welch.y, [], 1) / sqrt(size(p_welch.y, 1));
    
    figure(); clf; hold on;
    ax(1) = subplot(1,1,1);
    [hl1, hp1] = boundedline(p_welch_x.freq(f_use_x), m_ps_x(f_use_x), se_ps_x(f_use_x));
    [hl2, hp2] = boundedline(p_welch_y.freq(f_use_y), m_ps_y(f_use_y), se_ps_y(f_use_y));
    
    set(hl1, 'Color', 'k', 'linewidth', 2);
    set(hp1, 'FaceColor', 'k', 'FaceAlpha', .3);
    set(hl2, 'Color', 'r', 'linewidth', 2);
    set(hp2, 'FaceColor', 'r', 'FaceAlpha', .3);
    yd = get(hp2, 'YData');
    set(hp2, 'YData', max(yd, eps));
    
    legend([hl1 hl2],'x', 'y');
    
    title(sprintf('%s powerspectrum of drifts, %i drifts', subjects{ii_s}, length(all_long_drifts)));
    ylabel('PSD (db)');
    % ylim([.9 * min(m_ps_x(f_use)), 1.3 * max(m_ps_x(f_use))]);
    set(gca, 'YScale', 'log');
    yt = yticks;
    yticklabels(10 * log10(yt));
    
    
    
    grid(ax, 'on');
    xlabel('frequency (Hz)');
    xlim([flim, 200]);
    xticks([flim, 30, 50, 60, 90, 120, 150, 180]);
    set(ax, 'XScale', 'log', 'FontSize', 12);
    
    
    imagePath = 'C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\Figures\powerSpectrum\';
    saveas(gcf,strcat(imagePath, subjects{ii_s}), 'png')
end


