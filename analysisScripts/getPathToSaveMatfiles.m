function pathToSub = getPathToSaveMatfiles(params)
    
    if params.patient
        pathToSub = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\patients\',params.subject,'\','matFiles');
        if ~exist(pathToSub,'dir')
            mkdir(pathToSub);
        end
    else
        pathToSub = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\controls\',params.subject,'\','matFiles');
        if ~exist(pathToSub,'dir')
            mkdir(pathToSub);
        end
    end
    
    
end