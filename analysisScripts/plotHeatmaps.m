function plotHeatmaps(subjects,params)
    
    for ii_s = 1:length(subjects)
        if params.patient
            dir_path = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\patients\',subjects{ii_s},'\matFiles\');
            file_to_read = strcat('em_info_for_heatmaps_',subjects{ii_s},'.mat');
        else
            dir_path = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\controls\',subjects{ii_s},'\matFiles\');
            file_to_read = strcat('em_info_for_heatmaps_',subjects{ii_s},'.mat');
        end
        
        load(strcat(dir_path,file_to_read))
        
        if params.full
%             figure(ii_s)
%             h = scatterhist(em.allTraces.xALL,em.allTraces.yALL,'Color','k','Kernel','on','Location','SouthWest',...
%                 'Direction','out');
%             line(h(2),[nanmean(em.allTraces.xALL) nanmean(em.allTraces.xALL)],[0 100],'Color','r','LineWidth',3,'LineStyle','-')
%             line(h(3),[nanmean(em.allTraces.yALL) nanmean(em.allTraces.yALL)],[0 100],'Color','r','LineWidth',3,'LineStyle','-')
%             
%             line(h(2),[0 0],[0 100],'Color','k','LineWidth',2,'LineStyle',':')
%             line(h(3),[0 0],[0 100],'Color','k','LineWidth',2,'LineStyle',':')
%             hold on
%             plot(em.allTraces.xALL,em.allTraces.yALL,'o',...
%                 'LineWidth',2,...
%                 'MarkerSize',10,...
%                 'MarkerEdgeColor','w',...
%                 'MarkerFaceColor','w')
%             
%             hold on
%             generateHeatMapSimple(em.allTraces.xALL, ...
%                 em.allTraces.yALL, ...
%                 'Bins', 30,...
%                 'StimulusSize', 4.2,...
%                 'AxisValue', 30,...
%                 'Uncrowded', 1,...
%                 'Borders', 1);
% %             colorbar
%             hold on
%             axisVal = 45;
%             line(h(1),[0 0],[-axisVal axisVal],'Color','k','LineWidth',1,'LineStyle',':')
%             line(h(1),[-axisVal axisVal],[0 0],'Color','k','LineWidth',1,'LineStyle',':')
%             
%             axis square;
%             set(gca,'XTick',[],'YTick',[],'xlabel',[],'ylabel',[])
%             set(gcf,'renderer','painters')
            avg_x = round(nanmean(em.allTraces.xALL),2);
            avg_y = round(nanmean(em.allTraces.yALL),2);
%             figure(1)
            if (length(subjects) < 8)
                color = 'g'
            else
                color = 'm';
            end
            hold on
            plot(avg_x,avg_y,'kp','MarkerSize',20,'MarkerFaceColor',color)
            hold on
            s_e_locs = [avg_x,avg_y;0,0];
            euc_dist = pdist(s_e_locs,'euclidean');
            ylim([-40 40])
            xlim([-40 40])
            xlabel('arcmin')
            ylabel('arcmin')
            axis square
%             pathToSave = 'C:/Users/sanjana/OneDrive - University of Rochester/Documents/Sanjana/Projects/schizophrenia/Figures/Task_Heatmaps';
%             
%             if ~exist(strcat(pathToSave), 'dir')
%             mkdir(strcat(pathToSave))
%             end
%             fullPath = strcat(pathToSave,'/%s');
%             strfName = strcat('Task_heatmap_',subjects{ii_s});
%             saveas(gcf,sprintf(fullPath, strfName), 'png')
        end
        
        if params.binned
            figure(ii_s)
            subplot(1,5,1)
            generateHeatMapSimple(em.allTraces.xBinned.bin1, ...
                                    em.allTraces.yBinned.bin1, ...
                                    'Bins', 30,...
                                    'StimulusSize', 4.2,...
                                    'AxisValue', 30,...
                                    'Uncrowded', 1,...
                                    'Borders', 1);
            title('0-100 ms')
            subplot(1,5,2)
            generateHeatMapSimple(em.allTraces.xBinned.bin2, ...
                                    em.allTraces.yBinned.bin2, ...
                                    'Bins', 30,...
                                    'StimulusSize', 4.2,...
                                    'AxisValue', 30,...
                                    'Uncrowded', 1,...
                                    'Borders', 1);
            title('100-200 ms')
            subplot(1,5,3)
            generateHeatMapSimple(em.allTraces.xBinned.bin3, ...
                                    em.allTraces.yBinned.bin3, ...
                                    'Bins', 30,...
                                    'StimulusSize', 4.2,...
                                    'AxisValue', 30,...
                                    'Uncrowded', 1,...
                                    'Borders', 1);
            title('200-300 ms')
            subplot(1,5,4)
            generateHeatMapSimple(em.allTraces.xBinned.bin4, ...
                                    em.allTraces.yBinned.bin4, ...
                                    'Bins', 30,...
                                    'StimulusSize', 4.2,...
                                    'AxisValue', 30,...
                                    'Uncrowded', 1,...
                                    'Borders', 1);
            title('300-400 ms')
            subplot(1,5,5)
            generateHeatMapSimple(em.allTraces.xBinned.bin5, ...
                                    em.allTraces.yBinned.bin5, ...
                                    'Bins', 30,...
                                    'StimulusSize', 4.2,...
                                    'AxisValue', 30,...
                                    'Uncrowded', 1,...
                                    'Borders', 1);
            title('400-500 ms')
            sgtitle(strcat(subjects{ii_s},strcat('; num trials = ',string(em.trialCount)),strcat('; num samples = ',string(length(em.allTraces.xBinned.bin1))))) 
        end
                            
    end
        
        
        
        
end