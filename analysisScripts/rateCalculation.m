function emRate = rateCalculation(pptrial, emType)

if pptrial.TimeFixationOFF > 0 %FIXATION TRIAL
    endTrial = pptrial.TimeFixationOFF;
elseif pptrial.ResponseTime == 0
    endTrial = pptrial.TimeTargetOFF;
else
    endTrial = min(pptrial.TimeTargetOFF, pptrial.ResponseTime);
end

switch emType
    case 's'
        numEM = sum(pptrial.saccades.start < endTrial);
    case 'ms'
        numEM = sum(pptrial.microsaccades.start < endTrial);
end

emRate = (numEM/endTrial)*1000;

end
