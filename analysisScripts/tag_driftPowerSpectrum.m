%% use same drift length (200ms drifts)
tag = 1;
nameFile = 'pptrials';
filepath = 'C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\controls\HUX21\unCrowded\clean_ps_drifts';
%% drift length threshold
ms_threshold = 200; % use drift segments longer than this threshold;
Fs = 341;%pptrials{1}.eye_spf; % 341;960Hz recordings
sample_threshold = ms_threshold / (1000/Fs); % use drift segments longer than this threshold in samples;
%% Don't change anything beyond this point
all_long_drifts = struct();
segidx = 0;
figure()
for nT = 1:length(pptrials)
%     timeOn = round(pptrials{nT}.TimeTargetON/(1000/Fs));
%     timeOff = round(min(pptrials{nT}.TimeTargetOFF/(1000/Fs)-1, pptrials{nT}.ResponseTime/(1000/Fs)));   
    x_vals = pptrials{nT}.x.position + pptrials{nT}.xoffset * pptrials{1,nT}.pixelAngle;
    y_vals = pptrials{nT}.y.position + pptrials{nT}.yoffset * pptrials{1,nT}.pixelAngle;
    if tag 
        plot(x_vals,'r')
        hold on
        plot(y_vals,'b')
        ylim([-40 40]);
        title(string(nT))
        contType = input('add drift(1)? Trash trial (2) \n Next Trial(enter)');
        cont = [];
        if contType == 1
            pptrials{nT}.drifts.start = [];
            pptrials{nT}.drifts.duration = [];
            numTaggedInTrace = length(pptrials{nT}.drifts.start)+1;
            startTime = input('What is the start time?');
            duration = input('What is the end time?');
            pptrials{nT}.drifts.start(numTaggedInTrace) = ...
                round(startTime);
            pptrials{nT}.drifts.duration(numTaggedInTrace) = ...
                round(duration) - round(startTime);
            
            fprintf('Saving pptrials\n')
            save(fullfile(sprintf('%s',filepath), nameFile), 'pptrials')
            contType = input('is there another drift(3)?');
            if contType == 3
                numTaggedInTrace = length(pptrials{nT}.drifts.start)+1;
                startTime = input('What is the start time?');
                duration = input('What is the end time?');
                pptrials{nT}.drifts.start(numTaggedInTrace) = ...
                    round(startTime);
                pptrials{nT}.drifts.duration(numTaggedInTrace) = ...
                    round(duration) - round(startTime);
                
                fprintf('Saving pptrials\n')
                save(fullfile(sprintf('%s',filepath), nameFile), 'pptrials')
            end
        else
            pptrials{nT}.drifts.start = [];
            pptrials{nT}.drifts.duration = [];
            fprintf('Saving pptrials\n')
            save(fullfile(sprintf('%s',filepath), nameFile), 'pptrials')
        end
        input ''
        clf
    else
        all_ds = length(pptrials{nT}.drifts.start);
        if all_ds > 0
            ids_ds = find(pptrials{nT}.drifts.start >= timeOn & pptrials{nT}.drifts.start <= timeOff);
            for di = 1:all_ds
                if pptrials{1,nT}.drifts.duration(di) > sample_threshold
                    tmp_start = pptrials{1,nT}.drifts.start(di);
                    tmp_end = pptrials{1,nT}.drifts.start(di) + pptrials{1,nT}.drifts.duration(di) - 1;
                    if tmp_end > length(pptrials{1,nT}.x.position)
                        continue
                    end
                    tmp_x = pptrials{1,nT}.x.position(tmp_start:tmp_end)+ pptrials{nT}.xoffset * pptrials{1,nT}.pixelAngle;
                    tmp_y = pptrials{1,nT}.y.position(tmp_start:tmp_end)+ pptrials{nT}.yoffset * pptrials{1,nT}.pixelAngle;
                    plot(tmp_x)
                    hold on
                    % check if there is nan value in the trace
                    if sum(isnan(tmp_x)) ~= 0 ||  sum(isnan(tmp_y)) ~= 0
                        [startIndex, stopIndex] = getIndicesFromBin(~isnan(tmp_x));
                        durations = stopIndex - startIndex;
                        use_subtrace = durations > sample_threshold; % which subtrace to use after removing nan
                        for ui = 1:length(use_subtrace)
                            if use_subtrace(ui) % save the subtrace
                                tmp_start = startIndex(ui);
                                tmp_end = stopIndex(ui);
                                tmp_x = pptrials{1,nT}.x.position(tmp_start:tmp_end);
                                tmp_y = pptrials{1,nT}.y.position(tmp_start:tmp_end);

                                segidx = segidx+1;
                                all_long_drifts(segidx).x = tmp_x;
                                all_long_drifts(segidx).y = tmp_y;
                                all_long_drifts(segidx).recordingIdx = nT;
                                all_long_drifts(segidx).duration = pptrials{1,nT}.drifts.duration(di);

                            end
                        end
                    else

                        % save the trace to the struct
                        segidx = segidx+1;

                        all_long_drifts(segidx).x = tmp_x;
                        all_long_drifts(segidx).y = tmp_y;
                        all_long_drifts(segidx).recordingIdx = nT;
                        all_long_drifts(segidx).duration = pptrials{1,nT}.drifts.duration(di);
                    end
                end
            end
        end
    end
end

%% powerspectrum without any filtering - adapted from scripts by JI
nfft = 60;
p_welch = struct(); % for x trace
for di = 1:length(all_long_drifts)
        [ps, f] = pwelch(all_long_drifts(di).x - nanmean(all_long_drifts(di).x), hann(nfft), nfft/2, nfft, Fs);
        ps_tmp(di, :) = ps;
end
p_welch.x = ps_tmp;
p_welch_x.freq = f;

for di = 1:length(all_long_drifts)
        [ps, f] = pwelch(all_long_drifts(di).y - nanmean(all_long_drifts(di).y), hann(nfft), nfft/2, nfft, Fs);
        ps_tmp(di, :) = ps;
end
p_welch.y = ps_tmp;
p_welch_y.freq = f;


%% plotting
flim = 10;
f_use_x = p_welch_x.freq > flim;
f_use_y = p_welch_y.freq > flim;
    
    m_ps_x = nanmean(p_welch.x, 1);
    se_ps_x = nanstd(p_welch.x, [], 1) / sqrt(size(p_welch.x, 1));
    m_ps_y = nanmean(p_welch.y, 1);
    se_ps_y = nanstd(p_welch.y, [], 1) / sqrt(size(p_welch.y, 1));
    
    figure(); clf; hold on;
    ax(1) = subplot(1,1,1);
    [hl1, hp1] = boundedline(p_welch_x.freq(f_use_x), m_ps_x(f_use_x), se_ps_x(f_use_x));
    [hl2, hp2] = boundedline(p_welch_y.freq(f_use_y), m_ps_y(f_use_y), se_ps_y(f_use_y));

    set(hl1, 'Color', 'k', 'linewidth', 2);
    set(hp1, 'FaceColor', 'k', 'FaceAlpha', .3);
    set(hl2, 'Color', 'r', 'linewidth', 2);
    set(hp2, 'FaceColor', 'r', 'FaceAlpha', .3);
    yd = get(hp2, 'YData');
    set(hp2, 'YData', max(yd, eps));
    
    legend([hl1 hl2],'x', 'y');
    
    title(sprintf('%s powerspectrum of drifts, %i drifts', subject{1}, length(all_long_drifts)));
    ylabel('PSD (db)');
    % ylim([.9 * min(m_ps_x(f_use)), 1.3 * max(m_ps_x(f_use))]);
    set(gca, 'YScale', 'log');
    yt = yticks;
    yticklabels(10 * log10(yt));
    
    
    
    grid(ax, 'on');
    xlabel('frequency (Hz)');
    xlim([flim, 200]);
    xticks([flim, 30, 50, 60, 90, 120, 150, 180]);
    set(ax, 'XScale', 'log', 'FontSize', 12);
 

imagePath = 'C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\Figures\powerSpectrum\';
saveas(gcf,strcat(imagePath, subject{1}), 'png')

