em = [];
figures = struct(...
    'FIGURE_ON', 0,... %Show individual trial with traces in 2D space
    'VIDEO', 0,... %Wills save video of FIGURE_ON figures
    'FIXATION_ANALYSIS',0,... %Plots psychometric curves
    'saveFixMat',0,...
    'QUICK_ANALYSIS',0,... %Only plots psychometric curves
    'CHECK_TRACES',0,... %will plot traces
    'HUX_RUN',1,... %will discard trials differently
    'FOLDED', 0,...
    'FIXED_SIZE', 0,...
    'COMPARE_SPANS',0); %0 = all spans, 1 = small spans, 2 = large spans

machine = {'D'};

% subject = {'PC-S01','PC-S02','PC-S04','PC-S07','PC-S12','PC-S16','FE-S01','FE-S08','FE-S09'};
% subject = {'PC-C06','FE-C05','FE-C17','FE-C19'};
% subject = {'HUX4','HUX10','HUX12','HUX18','HUX21','HUX22','HUX23','HUX24','MP'};;
condition = {'uncrowded'};
subject = {'HUX17'};
params = struct(...
    'patient',0,...
    'hux',1,...
    'blankScreenAnalysis',0,...
    'nBoots', 1,... %Number of boots for psychometric fits
    'crowded', false, ... %Gets rewritten based on currentCond
    'DMS',true, ...
    'D', false, ...
    'MS', false,...
    'fixation',false,...
    'machine',machine, ... %DPI "A" vs dDPI "D"
    'session', '1', ... %Which number session
    'spanMax', 100,... %The Maximum Span
    'spanMin', 0,...%The Minimum Span
    'compSpans', 0,...
    'ecc',0,...
    'samplingRate' , 1000/341,... % 1000/341 for old eyeris
    'stabilization','Unstabilized');


for ii_s = 1:length(subject)
    if params.patient
        if length(subject) > 1
            if ii_s < 4
                newEyeris = 0;
                params.newEyeris = 0;
            else
                newEyeris = 1;
                params.newEyeris = 1;
                newEyeris = 0;
                params.newEyeris = 0;
            end
        end
        if params.blankScreenAnalysis
            if newEyeris
                pathToFile = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\newEyeris_analysisScripts\patients\',char(subject{ii_s}),'\unCrowded\blankScreenData');
            else
                pathToFile =  strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\patients\',char(subject{ii_s}),'\unCrowded');
            end
        else
            pathToFile =  strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\patients\',char(subject{ii_s}),'\unCrowded');
        end
    else
        if params.hux 
            newEyeris = 0;
            params.newEyeris = 0;
        elseif length(subject) > 1
            if ii_s == 1
                newEyeris = 0;
                params.newEyeris = 0;
            else
                newEyeris = 1;
                params.newEyeris = 1;
            end
        end
        if params.blankScreenAnalysis
            if (newEyeris == 1 && params.hux == 0)
                pathToFile = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\newEyeris_analysisScripts\controls\',char(subject{ii_s}),'\unCrowded\blankScreenData');
            elseif (newEyeris == 0 && params.hux == 1)
                pathToFile =  strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\controls\HUX_C\',char(subject{ii_s}),'\unCrowded');
            elseif (newEyeris == 0 && params.hux == 0)
                 pathToFile =  strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\controls\',char(subject{ii_s}),'\unCrowded');
            end
        else
            if params.hux
                pathToFile =  strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\controls\HUX_C\',char(subject{ii_s}),'\unCrowded');
            else
                pathToFile =  strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\controls\',char(subject{ii_s}),'\unCrowded');
            end
        end
    end

    if ~params.blankScreenAnalysis
        if strcmp(subject{ii_s}, 'PC-S16')|| (strcmp(subject{ii_s}, 'PC-S07'))
            if params.DMS
                load(fullfile(pathToFile,'task_clean.mat'), 'all_clean');
            elseif params.fixation
                load(fullfile(pathToFile,'fix_clean.mat'), 'all_clean');
            end

        end
    end
    load(fullfile(pathToFile,'pptrials.mat'), 'pptrials');



    for ii = 1:length(pptrials)
        if isfield(pptrials{ii},'eye_spf')
            newEyeris = 1;
            if pptrials{ii}.eye_spf > 1000
                params.samplingRate = 1 ;
            else
                params.samplingRate = 1000/pptrials{ii}.eye_spf;
            end
            pptrials{ii}.x.velocity = p_calculateVelocity(pptrials{ii}.x.position, 'prefilter', true, ...
              'postfilter', true, 'postvalue', true, 'order', 3, 'smoothing', bitor( double(round(51/1000*pptrials{ii}.eye_spf)), 1)  ) * pptrials{ii}.eye_spf / 1000;
            pptrials{ii}.y.velocity = p_calculateVelocity(pptrials{ii}.y.position, 'prefilter', true, ...
              'postfilter', true, 'postvalue', true, 'order', 3, 'smoothing', bitor( double(round(51/1000*pptrials{ii}.eye_spf)), 1)  ) * pptrials{ii}.eye_spf / 1000;
        else
            newEyeris = 0;
            params.newEyeris = 0;
            params.samplingRate = params.samplingRate ;
        end
        if isfield(pptrials{ii}, 'pixelAngle')
            params.pixelAngle(ii) = pptrials{1,ii}.pixelAngle;
            pptrials{1,ii}.pxAngle = pptrials{1,ii}.pixelAngle;
        else
            params.pixelAngle(ii) = pptrials{1,ii}.pxAngle;
        end
        if strcmp(subject{ii_s}, 'FE-B01') 
            pptrials{ii}.Unstabilized = 0;
        end
    end

    trialChar = buildTrialCharStruct(pptrials,newEyeris);
    trialChar = add_fields_trialChar(trialChar,pptrials,params);



    if newEyeris
        [traces, counter, valid, em] = countingTrials_newEyeris(pptrials, em, params, figures);
    else
        [traces, counter, valid, em] = countingTrials(pptrials, em, params, figures);
    end
    % 



    traces.swIdx = trialChar.TargetStrokewidth(valid.d);
    trialChar.Subject = char(subject);

    title_str = char(subject{ii_s});
    
    valid_dms_ts = find(valid.dms == 1);
    total_trials = length(valid_dms_ts);
    for val = 1:length(valid_dms_ts)
        pptrials_new{val} = pptrials{valid_dms_ts(val)};
    end
    [msRates,startTime,endTime,...
        startPos_x,startPos_y,...
        endPos_x,endPos_y] = deal([]);
    for num_ts = 1:total_trials
        [msRates,startTime,endTime,...
            startPos_x,startPos_y,...
            endPos_x,endPos_y] = do_ms_analysis(pptrials_new,num_ts,msRates,startTime,endTime,...
            startPos_x,startPos_y,...
            endPos_x,endPos_y,params);
        
    end
    for num_rs = 1:length(msRates)
        msInfo.msRates(1,num_rs) = msRates(num_rs);
    end
    for all = 1:length(startPos_x)
        msInfo.startPos(1,all) = startPos_x(all);
        msInfo.startPos(2,all) = startPos_y(all);
        msInfo.endPos(1,all) = endPos_x(all);
        msInfo.endPos(2,all) = endPos_y(all);
        
        msInfo.startTime(1,all) = startTime(all);
        msInfo.endTime(all) = endTime(all);
    end
    if params.patient
        pathToSub = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\patients\',subject{ii_s},'\','matFiles');
        if ~exist(pathToSub,'dir')
            mkdir(pathToSub);
        end
    elseif params.hux
        pathToSub = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\controls\HUX_C\',subject{ii_s},'\','matFiles');
        if ~exist(pathToSub,'dir')
            mkdir(pathToSub);
        end
    else
        pathToSub = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\controls\',subject{ii_s},'\','matFiles');
        if ~exist(pathToSub,'dir')
            mkdir(pathToSub);
        end    
    end
    if params.blankScreenAnalysis
        msFname = sprintf(strcat('ms_info_blankS_',subject{ii_s},'.mat'));
    else
        msFname = sprintf(strcat('ms_info_task_',subject{ii_s},'.mat'));
    end
    save(fullfile(pathToSub,msFname),'msInfo');
    clear msInfo;
    
end
