function generateHeatMapMovie_one(posX, posY,timeStamps)

% get all data in a vector format
posX = reshape(posX, [1, size(posX,1)*size(posX,2)]);
posY = reshape(posY, [1, size(posY,1)*size(posY,2)]);


timeStamps = reshape(timeStamps, [1, size(timeStamps,1)*size(timeStamps,2)]);

load('./MyColormaps.mat')
if ~exist('movies', 'dir')
    mkdir('movies')
end
% initialize movie
v_trial = VideoWriter(sprintf('./movies/%s', 'trial'), 'UnCompressed AVI');
v_trial.FrameRate = 5;
% v_trial.CompressionRatio = 2;
open(v_trial);
% spatial bins for the heatmaps
NBins = 15;
% temporal bins
TBins = 20; %ms
TimeIntervals = linspace(1, 500, 500/TBins);
limit = 30;
Alph = 0.25;
frame_count = 0;
% uL = 1;
% lL = 0.2;
figure()
for t = 1:length(TimeIntervals)-1
    idx = find(timeStamps<TimeIntervals(t+1) & timeStamps>=TimeIntervals(t));
    
    result = MyHistogram2(posX(idx), posY(idx),[-limit,limit,NBins;-limit,limit,NBins]);
%     result = result.Values;
    result = result./(max(max(result)));   
    
    
    
    figure()
    stimuliSize = 4.2;offset = 0;
    stimuliSize = stimuliSize/2;
    axisValue = 40;
    width = 2 * stimuliSize;
    height = width * 5;
    centerX = (-stimuliSize+offset);
    centerY = (-stimuliSize*5);
    rectangle('Position',[centerX, centerY, width, height],'LineWidth',2)
    axis square
%     caxis([0 ceil(max(max(result)))])
    shading interp;

    axis([-axisValue axisValue -axisValue axisValue]);
    
    hold on
    pH = pcolor(linspace(-limit,limit,size(result,1)),linspace(-limit,limit,size(result,1)),...
        result');
    pH.FaceAlpha = Alph;
    set(gcf, 'Colormap', mycmap)
    set(gca, 'Visible', 'off','FontSize', 16)
%     caxis([lL uL])
    axis tight
    axis square
    axis off
    shading interp;
    hold on
    text(10,22,sprintf('time: %dms', round(TimeIntervals(t+1))), 'FontWeight','bold')
    hold on
    frame_count = frame_count + 1;
    movie_trial(frame_count) = getframe(gcf);
    writeVideo(v_trial, movie_trial(frame_count));
end


close(v_trial);
close all;
end

























