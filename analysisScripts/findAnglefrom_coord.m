function mean_theta = findAnglefrom_coord(msInfo,tasktype)
%     theta = atan2(sy - ly, sx - lx);
    theta = [];
    for ts = 1:length(msInfo.startPos)
        if strcmp(tasktype,'fix')
            for ms = 1:length(msInfo.startPos{ts}(1,:))
                sx = msInfo.startPos{ts}(1,ms);
                sy = msInfo.startPos{ts}(2,ms);
                lx = msInfo.endPos{ts}(1,ms);
                ly = msInfo.endPos{ts}(2,ms);
                theta(1,end+1) = atan2(sy - ly, sx - lx);
            end
        end
    end
    mean_theta = mean(theta);
end