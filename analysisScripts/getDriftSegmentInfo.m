function [singleSegs,x,y] = getDriftSegmentInfo (trials, pptrials, params)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here



x = {length(trials)};
y = {length(trials)};
counter = 1;
for ii = trials
    if params.fixation
        timeOn = round(pptrials{ii}.TimeFixationON);
        timeOff = round(min(pptrials{ii}.TimeFixationOFF));
        if timeOff > length(pptrials{ii}.x.position)
            timeOff = length(pptrials{ii}.x.position);
        end
        if timeOn == 0 && timeOff > 0
            timeOn = 1;
        end
    else
        if params.newEyeris
            timeOn = 1;
            timeOff = length(pptrials{ii}.x.position);
        else
            timeOn = round(pptrials{ii}.TimeTargetON);
            timeOff = round(min(pptrials{ii}.TimeTargetOFF, pptrials{ii}.ResponseTime));
        end
    end
    if strcmp('D',params.machine)
        x{counter} = pptrials{ii}.x.position(ceil(timeOn/(params.samplingRate)):floor(timeOff/(params.samplingRate))) + pptrials{ii}.pixelAngle * pptrials{ii}.xoffset;
        y{counter} = pptrials{ii}.y.position(ceil(timeOn/(params.samplingRate)):floor(timeOff/(params.samplingRate))) + pptrials{ii}.pixelAngle * pptrials{ii}.yoffset;
        counter = counter+1;
    else
        x{counter} = pptrials{ii}.x.position(timeOn:timeOff) + pptrials{ii}.pixelAngle * pptrials{ii}.xoffset;
        y{counter} = pptrials{ii}.y.position(timeOn:timeOff) + pptrials{ii}.pixelAngle * pptrials{ii}.yoffset;
        counter = counter+1;
    end
end

if strcmp('D',params.machine)
    for ii = 1:length(x)
        
        [~,~,~,~,~,singleSegs{ii},~,~] = ...
            CalculateDiffusionCoef(341,struct('x',x{ii}, 'y', y{ii}));
    end
else
    for ii = 1:length(x)
        [~,~,~,~,~,singleSegs{ii},~,~] = ...
            CalculateDiffusionCoef(1000,struct('x',x{ii}, 'y', y{ii}));
    end
end
end

