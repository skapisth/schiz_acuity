function call_to_plot_ms_start_end(patients,controls,tasktype)
    
    scatPlot = 1;
    
    for ii_p = 1:length(patients)
        
        dir_path = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\patients\',patients{ii_p},'\matFiles\');
        if strcmp(tasktype,'fix')
            file_to_read = strcat('ms_info_fix_500ms_',patients{ii_p},'.mat');
        else
            file_to_read = strcat('ms_info_task_',patients{ii_p},'.mat');
        end
        load(strcat(dir_path,file_to_read))
        title_str = patients{ii_p};
        
        ms_start_end_scatter_plot(msInfo,tasktype,scatPlot,title_str);
        pathToSave = 'C:/Users/sanjana/OneDrive - University of Rochester/Documents/Sanjana/Projects/schizophrenia/Figures/ms_start_end_line_plots/patients/avg_line';
        if ~exist(strcat(pathToSave), 'dir')
            mkdir(strcat(pathToSave))
        end
        fullPath = strcat(pathToSave,'/%s');
        saveas(gcf,sprintf(fullPath, patients{ii_p}), 'png')
        saveas(gcf,sprintf(fullPath, patients{ii_p}), 'eps')
        close all
        clear msInfo;
    end
     for ii_c = 1:length(controls)
        
        dir_path = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\controls\HUX_C\',controls{ii_c},'\matFiles\');
        if strcmp(tasktype,'fix')
            file_to_read = strcat('ms_info_fix_500ms_',controls{ii_c},'.mat');
        else
            file_to_read = strcat('ms_info_task_',controls{ii_c},'.mat');
        end
        load(strcat(dir_path,file_to_read))

        title_str = controls{ii_c};
        
        ms_start_end_scatter_plot(msInfo,tasktype,scatPlot,title_str);
        pathToSave = 'C:/Users/sanjana/OneDrive - University of Rochester/Documents/Sanjana/Projects/schizophrenia/Figures/ms_start_end_line_plots/controls/avg_line';
        if ~exist(strcat(pathToSave), 'dir')
            mkdir(strcat(pathToSave))
        end
        fullPath = strcat(pathToSave,'/%s');
        saveas(gcf,sprintf(fullPath, controls{ii_c}), 'png')
        saveas(gcf,sprintf(fullPath, controls{ii_c}), 'eps')
        close all
        clear msInfo;
        
        
     end
   
end

