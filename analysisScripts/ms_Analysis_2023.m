    function [all_dist_start_toC,all_dist_end_toC,...
            rate_all,mean_amp_all]=ms_Analysis_2023(msInfo,scatPlot,title_str,cP,ii_p,all_dist_start_toC,all_dist_end_toC,...
                                                    rate_all,mean_amp_all)
    
    
   
    all_ms_sX = msInfo.startPos(1,:);
    all_ms_sY = msInfo.startPos(2,:);
    sX = all_ms_sX(~isnan(all_ms_sX));
    sY = all_ms_sY(~isnan(all_ms_sY));
    
    all_ms_eX = msInfo.endPos(1,:);
    all_ms_eY = msInfo.endPos(2,:);
    eX = all_ms_eX(~isnan(all_ms_eX));
    eY = all_ms_eY(~isnan(all_ms_eY));
    
%     s_center_locs = [sX,sY;0,0];
    s_dist_center = sqrt((sX-0).^2 + (sY-0).^2);
    e_dist_center = sqrt((eX-0).^2 + (eY-0).^2);           
    amp_1 = sqrt((eX-sX).^2 + (eY-sY).^2);
    
    idx_reqd = find(s_dist_center >= 10);
    
    
    sX_needed = sX(idx_reqd);
    sY_needed = sY(idx_reqd);
    
    eX_needed = eX(idx_reqd);
    eY_needed = eY(idx_reqd);
    
  

    ms_start_to_center_dist = s_dist_center; 
    ms_end_to_center_dist =   e_dist_center;
    
    
%     hold on
% %     ellipseXY(sX_needed, sY_needed, 68, cP(ii_p,:), 0,1)
%     hold on; plot(mean(sX_needed),mean(sY_needed),'o','color','r')
%     hold on;
% %     ellipseXY(eX_needed,eY_needed, 68, cP(ii_p,:), 0,0)
%     hold on; plot(mean(eX_needed),mean(eY_needed),'o','color','b')
%     plot([mean(sX_needed),mean(eX_needed)],[mean( sY_needed),mean(eY_needed)],'-','color','g','LineWidth',1.5)
%     xlim([-30,30])
%     ylim([-30,30])
%     xlabel('arcmins')
%     ylabel('arcmins')
%     axis square
% %     title(sprintf('%s num trials, %i microsaccades', title_str, length(idx_reqd)));
%     legend('start location','end location')
%     set(gca,'fontsize',13,'FontWeight','Bold')
%     legend box off
%    amp_1 = sqrt((eX-sX).^2 + (eY-sY).^2);
    ms_start_to_center_dist = s_dist_center(idx_reqd); 
    ms_end_to_center_dist =   e_dist_center(idx_reqd);
    hold on
%     pathToSave = 'C:/Users/sanjana/OneDrive - University of Rochester/Documents/Sanjana/Projects/schizophrenia/Figures/ms_ellipses_task';
% %             
%     if ~exist(strcat(pathToSave), 'dir')
%         mkdir(strcat(pathToSave))
%     end
%     fullPath = strcat(pathToSave,'/%s');
%     strfName = title_str;
%     saveas(gcf,sprintf(fullPath, strfName), 'png')
%     saveas(gcf,sprintf(fullPath, strfName), 'eps')
%     close all
    
%     ms_start_to_center_dist =  sqrt((sX-0).^2 + (sY-0).^2);      
%     ms_end_to_center_dist =  sqrt((eX-0).^2 + (eY-0).^2); 
   
    mean_amp_all(1,end+1) = mean(amp_1);
   
    if scatPlot
        figure()
     
        stimuliSize = 4.2/2;
        width = 2 * stimuliSize;
        height = width * 5;
        centerX = (-stimuliSize);
        centerY = (-stimuliSize*5);
        rectangle('Position',[centerX, centerY, width, height],'LineWidth',3)
        hold on
        
        
        scatter(sX,sY,'r')
        hold on
        scatter(eX,eY)
        
        
        
        t_x = [sX; eX]';
        t_y = [sY; eY]';
        for each = 1:length(sX)
            plot(t_x(each,:),t_y(each,:), '-','color','g')
            hold on
        end
        xlim([-30,30])
        ylim([-30,30])
        xlabel('arcmins')
        ylabel('arcmins')
        axis square
        title(title_str)
        legend('start location','end location')
        
        pathToSave = 'C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\Figures\ms_start_end_line_plots\patients';
%             
        if ~exist(strcat(pathToSave), 'dir')
            mkdir(strcat(pathToSave))
        end
        fullPath = strcat(pathToSave,'/%s');
        strfName = title_str;
        saveas(gcf,sprintf(fullPath, strfName), 'png')
    end
    
    all_dist_start_toC(1,end+1) = mean(ms_start_to_center_dist);
    all_dist_end_toC(1,end+1) = mean(ms_end_to_center_dist);
   
    figure(1)
    plot([1,2],[mean(ms_start_to_center_dist),mean(ms_end_to_center_dist)],'-o','MarkerSize',6,'MarkerFaceColor',cP(ii_p,:),'LineStyle', '-','LineWidth',1.5)
    hold on
    set(gca, 'XTick', [1:1:2], 'XTickLabel', {'ms start'; 'ms land'}, 'FontSize', 13);
    ylabel('Distance to center [arcmin]')
    set(gca,'fontsize',13,'FontWeight','Bold')
    xlim([0 3])
%     
%     s_times = msInfo.startTime;
%     s_times = s_times(~isnan(s_times));
%     
%     ms_sogg = sort([round(s_times)]);
%     a = 0; rate = [];
%     for i = 1:400
%         [N,edges] = histcounts(ms_sogg,[a-25 a+25]);
%         rate = [rate ((N/0.1)/length(s_times))];
%         a = a + 1;
%     end
%     rate_all(ii_p,:) = rate;
   
%     plot(rate)
%     xlabel('time (ms)')
%     ylabel('rate of microsaccades (ms/s)')
%     
%     pathToSave = 'C:/Users/sanjana/OneDrive - University of Rochester/Documents/Sanjana/Projects/schizophrenia/Figures/msPlots_new/patients/blankS';
%     
%     if ~exist(strcat(pathToSave), 'dir')
%         mkdir(strcat(pathToSave))
%     end
%     fullPath = strcat(pathToSave,'/%s');
%     strfName = title_str;
%     saveas(gcf,sprintf(fullPath, strfName), 'png')
%     close all
    
end