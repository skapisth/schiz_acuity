%% params
clc;clear all;
% subjects = {'PC-S01','PC-S02','PC-S04','PC-S07','PC-S12','PC-S16','FE-S01','FE-S08','FE-S09'};
% subjects = {'HUX4','HUX10','HUX12','HUX18','HUX21','HUX22','HUX23','Hux24','MP','PC-C06','FE-C05','FE-C17','FE-C19'};
% 'PC-C06','FE-C05','FE-C17','FE-C19'
subjects = {'HUX17'};
patient = 0;
huxC = 1;
newEyeris = 0;
%% subtract the mean from x and y
%% use 300ms chunks instead of 250
time_threshold = 250;
machine = {'D'};
figures = struct(...
                    'FIGURE_ON', 0,... %Show individual trial with traces in 2D space
                    'VIDEO', 0,... %Wills save video of FIGURE_ON figures
                    'FIXATION_ANALYSIS',0,... %Plots psychometric curves
                    'saveFixMat',1,...
                    'QUICK_ANALYSIS',0,... %Only plots psychometric curves
                    'CHECK_TRACES',0,... %will plot traces
                    'HUX_RUN',1,... %will discard trials differently
                    'FOLDED', 0,...
                    'FIXED_SIZE', 0,...
                    'COMPARE_SPANS',0); %0 = all spans, 1 = small spans, 2 = large spans

params = struct(...
                    'patient',0,...
                    'nBoots', 1,... %Number of boots for psychometric fits
                    'crowded', false, ... %Gets rewritten based on currentCond
                    'DMS',true, ...
                    'D', false, ...
                    'MS', false,...
                    'fixation',false,...
                    'machine',machine, ... %DPI "A" vs dDPI "D"
                    'session', '1', ... %Which number session
                    'spanMax', 100,... %The Maximum Span
                    'spanMin', 0,...%The Minimum Span
                    'compSpans', 0,...
                    'ecc',0,...
                    'samplingRate' , 1000/341,...
                    'stabilization','Unstabilized');
      
for ii_s = 1:length(subjects)
    params.subject = subjects{ii_s};
    title_str = char(subjects{ii_s});
    if patient
        pathToFile =  strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\patients\',char(subjects{ii_s}),'\unCrowded');     
        
        if length(subjects) > 1
            if ii_s > 3
                newEyeris = 1;
                Fs = 1000;
                params.samplingRate = 1000/Fs;
            else
                newEyeris = 0;
                Fs = 341;
                params.samplingRate = 1000/Fs;
            end
        else
            if newEyeris
                Fs = 1000;
                params.samplingRate = 1000/Fs;
            else
                Fs = 341;
                params.samplingRate = 1000/Fs;
            end

        end
    end
    if (patient == 0 && huxC == 1)
         pathToFile =  strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\controls\HUX_C\',char(subjects{ii_s}),'\unCrowded');
         newEyeris = 0;
         Fs = 341;
         params.samplingRate = 1000/Fs;
    end
    if (patient == 0 && huxC == 0)
        if ii_s > 9
            pathToFile =  strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\controls\',char(subjects{ii_s}),'\unCrowded');
        else
            pathToFile =  strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\controls\HUX_C\',char(subjects{ii_s}),'\unCrowded');
        end
         if ii_s > 10
            newEyeris = 1;
            Fs = 1000;
            params.samplingRate = 1000/Fs;
        else
            newEyeris = 0;
            Fs = 341;
            params.samplingRate = 1000/Fs;
%             newEyeris = 1;
%             Fs = 1000;
%             params.samplingRate = 1000/Fs;
        end
    end
    if newEyeris
        sample_threshold = time_threshold ;
    else
        sample_threshold = time_threshold / (1000/Fs); % use drift segments longer than this threshold in samples;
    end
%     if (strcmp(subjects{ii_s}, 'PC-S16') || strcmp(subjects{1}, 'PC-S07'))
%         if params.DMS
%             load(fullfile(pathToFile,'task_clean.mat'), 'all_clean');
%         elseif params.fixation
%             load(fullfile(pathToFile,'fix_clean.mat'), 'all_clean');
%         end
%         
%     end
    load(fullfile(pathToFile,'pptrials.mat'), 'pptrials');
    for ii = 1:length(pptrials)
        if isfield(pptrials{ii}, 'pixelAngle')
            params.pixelAngle(ii) = pptrials{1,ii}.pixelAngle;
            pptrials{1,ii}.pxAngle = pptrials{1,ii}.pixelAngle;
        else
            params.pixelAngle(ii) = pptrials{1,ii}.pxAngle;
        end
        pptrials{ii}.Unstabilized = 0;
    end
%     if (strcmp(subjects{1}, 'PC-S16')  || strcmp(subjects{1}, 'PC-S07'))
%         valid.dms = all_clean;
%     end
    
    trialChar = buildTrialCharStruct(pptrials,newEyeris);
    trialChar = add_fields_trialChar(trialChar,pptrials,params);
    em = [];
    if newEyeris
        [~, ~, valid, ~] = countingTrials_newEyeris(pptrials, em, params, figures);
    else
        [~, ~, valid, ~] = countingTrials(pptrials, em, params, figures);
    end
    
    
    
    condition = Psychometric_Graphing(params,params.ecc,...
        valid, trialChar, params.crowded, title_str);
    close all
    all_thresh(ii_s) = condition.thresh;
    
    
    all_long_drifts = struct();
    segidx = 0;
    idx = find(valid.fixation == 0);
    
    for val = 1:length(idx)
        pptrials_new{val} = pptrials{idx(val)};
    end
    for nT = 1:length(pptrials_new)
        
        if newEyeris
            timeOn = 1;
            timeOff = length(pptrials_new{1,nT}.x.position);
        else
            timeOn = round(pptrials_new{nT}.TimeTargetON/(params.samplingRate));
            timeOff = round(min(pptrials_new{nT}.TimeTargetOFF/(params.samplingRate)-1, pptrials_new{nT}.ResponseTime/(params.samplingRate)));
        end
        for di = 1:length(pptrials_new{1,nT}.drifts.duration)
            if pptrials_new{1,nT}.drifts.duration(di) >= timeOn && pptrials_new{1,nT}.drifts.duration(di) <= timeOff
                if pptrials_new{1,nT}.drifts.duration(di) >= sample_threshold
                    tmp_start = pptrials_new{1,nT}.drifts.start(di);
                    tmp_end = tmp_start + sample_threshold;
                    ms_in_trial = find(pptrials_new{1,nT}.microsaccades.start >= tmp_start & pptrials_new{1,nT}.microsaccades.start <= tmp_end);
                    if ~isempty(ms_in_trial) > 0
                        continue
                    end
                    if tmp_end > length(pptrials_new{1,nT}.x.position)
                        continue
                    end
                    if tmp_end > length(pptrials_new{1,nT}.y.position)
                        continue
                    end
                    tmp_x = pptrials_new{1,nT}.x.position(tmp_start:tmp_end);
                    tmp_y = pptrials_new{1,nT}.y.position(tmp_start:tmp_end);

                    euc_dist = sqrt((tmp_x-0).^2 + (tmp_y-0).^2);
                    mean_euc_dist = mean(euc_dist);

                    if mean_euc_dist > 30
                        continue;
                    end
    %                 
                    % check if there is nan value in the trace
                    if sum(isnan(tmp_x)) ~= 0 ||  sum(isnan(tmp_y)) ~= 0
                        [startIndex, stopIndex] = getIndicesFromBin(~isnan(tmp_x));
                        durations = stopIndex - startIndex;
                        use_subtrace = durations > sample_threshold; % which subtrace to use after removing nan
                        for ui = 1:length(use_subtrace)
                            if use_subtrace(ui) % save the subtrace
                                tmp_start = startIndex(ui);
                                tmp_end = stopIndex(ui);
                                tmp_x = pptrials_new{1,nT}.x.position(tmp_start:tmp_end);
                                tmp_y = pptrials_new{1,nT}.y.position(tmp_start:tmp_end);

                                segidx = segidx+1;
                                all_long_drifts(segidx).x = tmp_x;
                                all_long_drifts(segidx).y = tmp_y;
                                all_long_drifts(segidx).recordingIdx = nT;
                                all_long_drifts(segidx).duration = pptrials_AO{1,nT}.drifts.duration(di);

                            end
                        end
                    else

                        % save the trace to the struct
                        segidx = segidx+1;

                        all_long_drifts(segidx).x = tmp_x;
                        all_long_drifts(segidx).y = tmp_y;
                        all_long_drifts(segidx).recordingIdx = nT;
                        all_long_drifts(segidx).duration = pptrials_new{1,nT}.drifts.duration(di);
                    end
                end
            else
                continue
            end
        end
    end
all_long_drifts_clean = struct();
counter_save = 0;
for all = 1:length(all_long_drifts)
    plot(all_long_drifts(all).x,'r')
    hold on
    plot(all_long_drifts(all).y,'b')
    title(string(all))
    
    contType = input('keep trial(1)? \n Next Trial(enter)');
    cont = [];
    if contType == 1
        counter_save = counter_save +1;
        all_long_drifts_clean(counter_save).x = all_long_drifts(all).x;
        all_long_drifts_clean(counter_save).y = all_long_drifts(all).y;
    end
    clf
end
FName = sprintf(strcat('cleanD_struct_',  subjects{1}, '.mat'));
save(fullfile('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\controls\cleanDrifts\', FName), 'all_long_drifts_clean');
%     %% calculate DC
%% the third one here..

end

FName = sprintf(strcat('all_thresh_dms_C', '.mat'));
save(fullfile('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\controls\acuityThreshMats\', FName), 'all_thresh');

figure()
plot([all_thresh],[all_DC],'o','MarkerSize',10,'MarkerFaceColor','k')
xlabel('Acuity Threshold (arcmin)')
ylabel('Diffusion constant (arcmin^2/sec)')
set(gca,'fontsize',27,'FontWeight','Bold')
%    xlim([2,4])
% ylim([0,13])











%