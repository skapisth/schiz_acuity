function fixation_trials_analysis(valid,pptrials,short_seg_analysis,params,subject,ids)

    if ids == 0
        fixIdx = find(valid.fixation == 1);
    else 
        fixIdx = ids;
    end
    
    [xValues,yValues] = deal([]);
    
    [xVal_b1,xVal_b2,xVal_b3,xVal_b4,xVal_b5] = deal([]);
    [yVal_b1,yVal_b2,yVal_b3,yVal_b4,yVal_b5] = deal([]);
    
    [x_bcea_all,y_bcea_all] = deal([]);
    [x_all_bcea_b1,y_all_bcea_b1,...
        x_all_bcea_b2,y_all_bcea_b2,...
        x_all_bcea_b3,y_all_bcea_b3,...
        x_all_bcea_b4,y_all_bcea_b4,...
        x_all_bcea_b5,y_all_bcea_b5] = deal([]);
    [xOffset,yOffset] = deal([]);
    [msIdx,msIdx_forRates] = deal([]);
    counterFix = 1;
    counter = 1;
    count_empty_drifts = 0;
    
    bin1 = 1000*(1/params.samplingRate);
    bin2 = 2000*(1/params.samplingRate);
    bin3 = 3000*(1/params.samplingRate);
    bin4 = 4000*(1/params.samplingRate);
    bin5 = 5000*(1/params.samplingRate);
    
    [em.allTraces.xALL,em.allTraces.yALL] = deal([]);
    
    [em.allTraces.xBinned.bin1,em.allTraces.xBinned.bin2,em.allTraces.xBinned.bin3,...
        em.allTraces.xBinned.bin4,em.allTraces.xBinned.bin5] = deal([]);
    
    [em.allTraces.yBinned.bin1,em.allTraces.yBinned.bin2,em.allTraces.yBinned.bin3,...
        em.allTraces.yBinned.bin4,em.allTraces.yBinned.bin5] = deal([]);
    for i = fixIdx
        
        if params.newEyeris
            timeOn = 1;
            timeOff = length(pptrials{i}.x.position);
        else
            timeOn = round(pptrials{i}.TimeFixationON/params.samplingRate)+1;
            timeOff = floor(min(pptrials{i}.TimeFixationOFF/params.samplingRate, length(pptrials{i}.x.position)));
            
        end
        
        x = pptrials{i}.x.position(ceil(timeOn):floor(timeOff)) + pptrials{i}.xoffset * params.pixelAngle(i);
        y = pptrials{i}.y.position(ceil(timeOn):floor(timeOff)) + pptrials{i}.yoffset * params.pixelAngle(i);
        x_full = x;
        y_full = y;
        [x_full,y_full] = checkXYBound(x_full,y_full, 60);
        [ x, y] = checkXYBound(x, y, 60);
        findms = find(pptrials{i}.microsaccades.start > timeOn & pptrials{i}.microsaccades.start < timeOff);
        drift_dur_limit_ms = 250;
        
        if isempty(findms)
            pptrials{i}.drifts.start = pptrials{i}.drifts.start(pptrials{i}.drifts.start~=0);
            find_d_segs = pptrials{i}.drifts.start;
            if isempty(find_d_segs)
                count_empty_drifts = count_empty_drifts+1;
                flag = 0;
                continue;
            end
            for ec = 1:length(find_d_segs)
                pptrials{i}.drifts.duration = pptrials{i}.drifts.duration(pptrials{i}.drifts.duration~=0);
                pptrials{i}.drifts.start = pptrials{i}.drifts.start(pptrials{i}.drifts.start~=0);
                if isempty(pptrials{i}.drifts.duration)
                    flag = 0;
                    continue
                end
                d_dur = pptrials{i}.drifts.duration(ec);
                d_dur_ms = d_dur*params.samplingRate;
                if short_seg_analysis
                    needed_d_seg = drift_dur_limit_ms/params.samplingRate; %(300ms converted to samples)
                else
                    needed_d_seg = d_dur;
                end
                if d_dur_ms >=drift_dur_limit_ms
                    time_d_start = pptrials{i}.drifts.start(ec);
                    x = pptrials{i}.x.position(ceil(time_d_start):floor(time_d_start+needed_d_seg)) + pptrials{i}.xoffset * params.pixelAngle(i);
                    y = pptrials{i}.y.position(ceil(time_d_start):floor(time_d_start+needed_d_seg)) + pptrials{i}.yoffset * params.pixelAngle(i);
                    %                                 [ x, y] = checkXYArt(x, y, min(round(length(x)/100)),31);
                    [ x, y] = checkXYBound(x, y, 60);
                    flag = 1;
                    break;
                end
            end
        else
            ms_start_sample = pptrials{i}.microsaccades.start(findms);
            for ms_samp = 1:length(ms_start_sample)
                samples_before_ms = ms_start_sample(ms_samp) - 1;
                if isempty(pptrials{i}.drifts.duration)
                    flag = 0;
                    continue
                end
                if ms_samp > length(pptrials{i}.drifts.duration)
                    continue
                else
                    pptrials{i}.drifts.duration = pptrials{i}.drifts.duration(pptrials{i}.drifts.duration~=0);
                    if isempty(pptrials{i}.drifts.duration)
                        flag = 0;
                        continue
                    end
                    pptrials{i}.drifts.start = pptrials{i}.drifts.start(pptrials{i}.drifts.start~=0);
                    d_dur = pptrials{i}.drifts.duration(ms_samp);
                    d_dur_ms = d_dur*params.samplingRate;
                    if short_seg_analysis
                        needed_d_seg = drift_dur_limit_ms/params.samplingRate; %(300ms converted to samples)
                    else
                        needed_d_seg = d_dur;
                    end
                    if samples_before_ms >= needed_d_seg
                        if d_dur_ms >=drift_dur_limit_ms
                            time_d_start = pptrials{i}.drifts.start(ms_samp);
                            x = pptrials{i}.x.position(ceil(time_d_start):floor(time_d_start+needed_d_seg)) + pptrials{i}.xoffset * params.pixelAngle(i);
                            y = pptrials{i}.y.position(ceil(time_d_start):floor(time_d_start+needed_d_seg)) + pptrials{i}.yoffset * params.pixelAngle(i);
                            [ x, y] = checkXYBound(x, y, 60);
                            flag = 1;
                            break;
                        else
                            flag = 0;
                        end
                    else
                        flag = 0;
                        continue;
                    end
                end
            end
        end
        xDrift{1} = x;
        yDrift{1} = y;
       
        if ~isempty(pptrials{i}.drifts.start)
            xDriftAll = []; yDriftAll = [];
            for nLongDrift = 1:length(xDrift)
                [~, instSpX{nLongDrift,1},...
                    instSpY{nLongDrift,1},...
                    mn_speed{nLongDrift},...
                    driftAngle{nLongDrift,1},...
                    curvature{nLongDrift},...
                    varx{nLongDrift},...
                    vary{nLongDrift},...
                    span{nLongDrift}, ...
                    amplitude(nLongDrift)] = getDriftChar(xDrift{nLongDrift}, yDrift{nLongDrift}, 41, 1, Inf); %41
                [prlDistance(nLongDrift),...
                    prlDistanceX(nLongDrift),...
                    prlDistanceY(nLongDrift)] = get_PRL(x_full, y_full);
                xDriftAll = [xDrift{nLongDrift} xDriftAll];
                yDriftAll = [yDrift{nLongDrift} yDriftAll];
                x_bcea_all = [x_bcea_all,x_full];
                y_bcea_all = [y_bcea_all,y_full];
                
                x_all_bcea_b1 = [x_all_bcea_b1,x_full(1:bin1)];
                y_all_bcea_b1 = [y_all_bcea_b1,y_full(1:bin1)];
                
                x_all_bcea_b2 = [x_all_bcea_b2,x_full(bin1:bin2)];
                y_all_bcea_b2 = [y_all_bcea_b2,y_full(bin1:bin2)];
                
                x_all_bcea_b3 = [x_all_bcea_b3,x_full(bin2:bin3)];
                y_all_bcea_b3 = [y_all_bcea_b3,y_full(bin2:bin3)];
                
                x_all_bcea_b4 = [x_all_bcea_b4,x_full(bin3:bin4)];
                y_all_bcea_b4 = [y_all_bcea_b4,y_full(bin3:bin4)];
                
                if bin5 > length(x_full)
                    x_all_bcea_b5 = [x_all_bcea_b5, x_full(bin4+1:end)];
                    y_all_bcea_b5 = [y_all_bcea_b5, y_full(bin4+1:end)];
                else
                    x_all_bcea_b5 = [x_all_bcea_b5, x_full(bin4+1:bin5)];
                    y_all_bcea_b5 = [y_all_bcea_b5, y_full(bin4+1:bin5)];
                end
            end
            fixation.instSpX{counterFix} = instSpX;
            fixation.instSpY{counterFix} = instSpY;
            fixation.mn_speed{counterFix} = ([mn_speed{:}]);
            fixation.driftAngle{counterFix} = driftAngle;
            fixation.curvature{counterFix} = ([curvature{:}]);
            fixation.varx{counterFix} = [varx{:}];
            fixation.vary{counterFix} = [vary{:}];
            fixation.span{counterFix} = [span{:}];
            fixation.amplitude{counterFix} = amplitude(:);
            fixation.prlDistance{counterFix} = prlDistance;
            fixation.prlDistanceX{counterFix} = prlDistanceX;
            fixation.prlDistanceY{counterFix} = prlDistanceY;
            counterFix = counterFix + 1;
        end
                
        xValues = [xValues, x_full];
        yValues = [yValues, y_full];
        
        xVal_b1 = [xVal_b1, x_full(1:bin1)];
        yVal_b1 = [yVal_b1, y_full(1:bin1)];
        
        xVal_b2 = [xVal_b2, x_full(bin1+1:bin2)];
        yVal_b2 = [yVal_b2, y_full(bin1+1:bin2)];
        
        xVal_b3 = [xVal_b3, x_full(bin2+1:bin3)];
        yVal_b3 = [yVal_b3, y_full(bin2+1:bin3)];
        
        xVal_b4 = [xVal_b4, x_full(bin3+1:bin4)];
        yVal_b4 = [yVal_b4, y_full(bin3+1:bin4)];
        
        if bin5 > length(x_full)
            xVal_b5 = [xVal_b5, x_full(bin4+1:end)];
            yVal_b5 = [yVal_b5, y_full(bin4+1:end)];
        else
            xVal_b5 = [xVal_b5, x_full(bin4+1:bin5)];
            yVal_b5 = [yVal_b5, y_full(bin4+1:bin5)];
        end
        
        
        xOffset = [xOffset, pptrials{i}.xoffset * params.pixelAngle(i)];
        yOffset = [yOffset, pptrials{i}.yoffset * params.pixelAngle(i)];
        
        fixationInfo.position(counter).x = x;
        fixationInfo.position(counter).y = y;
        
        if ~isempty(pptrials{i}.microsaccades.start) && any(pptrials{i}.microsaccades.start) > 0
            msIdx(1,end+1) = i;
            msIdx_forRates(1,end+1) = i;
        else
            msIdx(1,end+1) = 0;
            msIdx_forRates(1,end+1) = i;
        end
        counter = counter + 1;
        
    end
    bcea = get_bcea(x_bcea_all(~isnan(x_bcea_all)),y_bcea_all(~isnan(y_bcea_all)));
    [bcea_b1] = get_bcea(x_all_bcea_b1(~isnan(x_all_bcea_b1)), y_all_bcea_b1(~isnan(y_all_bcea_b1)));
    [bcea_b2] = get_bcea(x_all_bcea_b2(~isnan(x_all_bcea_b2)), y_all_bcea_b2(~isnan(y_all_bcea_b2)));
    [bcea_b3] = get_bcea(x_all_bcea_b3(~isnan(x_all_bcea_b3)), y_all_bcea_b3(~isnan(y_all_bcea_b3)));
    [bcea_b4] = get_bcea(x_all_bcea_b4(~isnan(x_all_bcea_b4)), y_all_bcea_b4(~isnan(y_all_bcea_b4)));
    [bcea_b5] = get_bcea(x_all_bcea_b5(~isnan(x_all_bcea_b5)), y_all_bcea_b5(~isnan(y_all_bcea_b5)));
    
    fixation.bcea = bcea;
    fixation.bcea_b1 = bcea_b1;
    fixation.bcea_b2 = bcea_b2;
    fixation.bcea_b3 = bcea_b3;
    fixation.bcea_b4 = bcea_b4;
    fixation.bcea_b5 = bcea_b5;
    
    clear msInfo
    tempIdx = (find(msIdx > 0));
    [~, msInfo.msRates] = buildRates(pptrials(msIdx_forRates));
    counterms = 1;
    for i = tempIdx
         msInfo.startTime{counterms} = pptrials{msIdx(i)}.microsaccades.start(...
            pptrials{msIdx(i)}.microsaccades.start  > 0);
        msInfo.endTime{counterms} = msInfo.startTime{counterms} + ...
            pptrials{msIdx(i)}.microsaccades.duration( pptrials{msIdx(i)}.microsaccades.start  > 0);
%         msInfo.startTime{counterms} = pptrials{msIdx(i)}.microsaccades.start(...
%             pptrials{msIdx(i)}.microsaccades.start  > 0 & pptrials{msIdx(i)}.microsaccades.start  <= 500);
%         msInfo.endTime{counterms} = msInfo.startTime{counterms} + ...
%             pptrials{msIdx(i)}.microsaccades.duration( pptrials{msIdx(i)}.microsaccades.start  > 0 & pptrials{msIdx(i)}.microsaccades.start  <= 500);
        if isempty(msInfo.startTime{counterms})
            continue
        end
        if isempty(msInfo.endTime{counterms})
            continue
        end
        if  (msInfo.startTime{counterms}(end) > length(pptrials{msIdx(i)}.x.position)...
                || msInfo.startTime{counterms}(end) > length(pptrials{msIdx(i)}.y.position)...
                || msInfo.endTime{counterms}(end) > length(pptrials{msIdx(i)}.x.position)...
                || msInfo.endTime{counterms}(end) > length(pptrials{msIdx(i)}.y.position))
            continue
        else
            msInfo.startPos{counterms}(1,:) = pptrials{msIdx(i)}.x.position( msInfo.startTime{counterms});
            msInfo.startPos{counterms}(2,:) = pptrials{msIdx(i)}.y.position( msInfo.startTime{counterms});
            
            
            msInfo.endPos{counterms}(1,:) = pptrials{msIdx(i)}.x.position( msInfo.endTime{counterms});
            msInfo.endPos{counterms}(2,:) = pptrials{msIdx(i)}.y.position( msInfo.endTime{counterms});
            counterms = counterms + 1;
        end
    end
    if params.patient
        pathToSub = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\patients\',subject{1},'\','matFiles');
        if ~exist(pathToSub,'dir')
            mkdir(pathToSub);
        end
    else
        pathToSub = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\controls\',subject{1},'\','matFiles');
        if ~exist(pathToSub,'dir')
            mkdir(pathToSub);
        end
    end
    msFname = sprintf(strcat('ms_info_fix_',subject{1},'.mat'));
%     save(fullfile(pathToSub,msFname),'msInfo');
    
%     em.ecc_0.fixation = fixationInfo;
%     [ xValues, yValues] = checkXYArt(xValues, yValues, min(round(length(xValues)/100)),31); %Checks strange artifacts
    [ xValues, yValues] = checkXYBound(xValues, yValues, 60); %Checks boundries
    
%     [ xVal_b1, yVal_b1] = checkXYArt(xVal_b1, yVal_b1, min(round(length(xVal_b1)/100)),31); %Checks strange artifacts
    [ xVal_b1, yVal_b1] = checkXYBound(xVal_b1, yVal_b1, 60); %Checks boundries
    
%     [ xVal_b2, yVal_b2] = checkXYArt(xVal_b2, yVal_b2, min(round(length(xVal_b2)/100)),31); %Checks strange artifacts
    [ xVal_b2, yVal_b2] = checkXYBound(xVal_b2, yVal_b2, 60);
    
%     [ xVal_b3, yVal_b3] = checkXYArt(xVal_b3, yVal_b3, min(round(length(xVal_b3)/100)),31); %Checks strange artifacts
    [ xVal_b3, yVal_b3] = checkXYBound(xVal_b3, yVal_b3, 60);
    
%     [ xVal_b4, yVal_b4] = checkXYArt(xVal_b4, yVal_b4, min(round(length(xVal_b4)/100)),31); %Checks strange artifacts
    [ xVal_b4, yVal_b4] = checkXYBound(xVal_b4, yVal_b4, 60);
    
%     [ xVal_b5, yVal_b5] = checkXYArt(xVal_b5, yVal_b5, min(round(length(xVal_b5)/100)),31); %Checks strange artifacts
    [ xVal_b5, yVal_b5] = checkXYBound(xVal_b5, yVal_b5, 60);
    
    em.allTraces.xALL = [em.allTraces.xALL,xValues];
    em.allTraces.yALL = [em.allTraces.yALL,yValues];
    
    em.allTraces.xBinned.bin1 = [em.allTraces.xBinned.bin1,xVal_b1];
    em.allTraces.yBinned.bin1 = [em.allTraces.yBinned.bin1,yVal_b1];
    
    em.allTraces.xBinned.bin2 = [em.allTraces.xBinned.bin2,xVal_b2];
    em.allTraces.yBinned.bin2 = [em.allTraces.yBinned.bin2,yVal_b2];
    
    em.allTraces.xBinned.bin3 = [em.allTraces.xBinned.bin3,xVal_b3];
    em.allTraces.yBinned.bin3 = [em.allTraces.yBinned.bin3,yVal_b3];
    
    em.allTraces.xBinned.bin4 = [em.allTraces.xBinned.bin4,xVal_b4];
    em.allTraces.yBinned.bin4 = [em.allTraces.yBinned.bin4,yVal_b4];
    
    em.allTraces.xBinned.bin5 = [em.allTraces.xBinned.bin5,xVal_b5];
    em.allTraces.yBinned.bin5 = [em.allTraces.yBinned.bin5,yVal_b5];
    
    if params.save_heatmap_mats
        pathToSub = getPathToSaveMatfiles(params);
        em_Fname = sprintf(strcat('em_info_for_heatmaps_fix_',subject{1},'.mat'));
        save(fullfile(pathToSub,em_Fname),'em');
    end
    
%     [fixation.MeanDistanceFrom0, fixation.dAbs] = generateDirectionMap(xValues, yValues);
    
    counterFix = 1;
    for ii = fixIdx
        timeOn = ceil(pptrials{ii}.TimeFixationON/(params.samplingRate));
        timeOff = round(pptrials{ii}.TimeFixationOFF/(params.samplingRate));
        if timeOff > length(pptrials{ii}.x.position)
            timeOff = length(pptrials{ii}.x.position);
        end
        fixation.tracesX{counterFix} = pptrials{ii}.x.position(ceil(timeOn):floor(timeOff)) + (pptrials{ii}.pixelAngle * pptrials{ii}.xoffset);
        fixation.tracesY{counterFix} = pptrials{ii}.y.position(ceil(timeOn):floor(timeOff)) + (pptrials{ii}.pixelAngle * pptrials{ii}.yoffset);
        fixation.velocityX{counterFix} = pptrials{ii}.x.velocity;
        fixation.velocityY{counterFix} = pptrials{ii}.y.velocity;
        counterFix = counterFix + 1;
    end
    %             generateDriftVelocityMap(fixation.velocityX, fixation.velocityY,...
    %                 0, params, title_str, trialChar)
    fixFname = sprintf(strcat('fix_results_cut_drift_',subject{1},'.mat'));
    save(fullfile(pathToSub,fixFname),'fixation');
end
