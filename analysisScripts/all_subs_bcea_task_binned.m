function all_subs_bcea_task_binned(patients,controls, HUX)
    
    cP = jet(length(patients));
    cC = winter(length(controls));
    for ii_p = 1:length(patients)
        dir_path = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\patients\',patients{ii_p},'\matFiles\');
    
        file_to_read_avg = strcat('em_info_for_BSA_heatmaps_',patients{ii_p},'.mat');
        load(strcat(dir_path,file_to_read_avg))
        
        xT_b1_BSA = em.allTraces.xBinned.bin1;
        yT_b1_BSA = em.allTraces.yBinned.bin1;
        all_bcea_P(ii_p,1) = get_bcea(xT_b1_BSA/60,yT_b1_BSA/60);
        
        xT_b2_BSA = em.allTraces.xBinned.bin2;
        yT_b2_BSA = em.allTraces.yBinned.bin2;
        all_bcea_P(ii_p,2) = get_bcea(xT_b2_BSA/60,yT_b2_BSA/60);
        
        xT_b3_BSA = em.allTraces.xBinned.bin3;
        yT_b3_BSA = em.allTraces.yBinned.bin3;
        all_bcea_P(ii_p,3) = get_bcea(xT_b3_BSA/60,yT_b3_BSA/60);
        
        xT_b4_BSA = em.allTraces.xBinned.bin4;
        yT_b4_BSA = em.allTraces.yBinned.bin4;
        all_bcea_P(ii_p,4) = get_bcea(xT_b4_BSA/60,yT_b4_BSA/60);
        
        clear em
        file_to_read_avg = strcat('em_info_for_heatmaps_',patients{ii_p},'.mat');
        load(strcat(dir_path,file_to_read_avg))
      
        xT_b1_task = em.allTraces.xBinned.bin1;
        yT_b1_task = em.allTraces.yBinned.bin1;
        all_bcea_P(ii_p,5) = get_bcea(xT_b1_task/60,yT_b1_task/60);
        
        xT_b2_task = em.allTraces.xBinned.bin2;
        yT_b2_task = em.allTraces.yBinned.bin2;
        all_bcea_P(ii_p,6) = get_bcea(xT_b2_task/60,yT_b2_task/60);
        
        xT_b3_task = em.allTraces.xBinned.bin3;
        yT_b3_task = em.allTraces.yBinned.bin3;
        all_bcea_P(ii_p,7) = get_bcea(xT_b3_task/60,yT_b3_task/60);
        
        xT_b4_task = em.allTraces.xBinned.bin4;
        yT_b4_task = em.allTraces.yBinned.bin4;
        all_bcea_P(ii_p,8) = get_bcea(xT_b4_task/60,yT_b4_task/60);
        
        xT_b5_task = em.allTraces.xBinned.bin5;
        yT_b5_task = em.allTraces.yBinned.bin5;
        all_bcea_P(ii_p,9) = get_bcea(xT_b5_task/60,yT_b5_task/60);
        
%         figure(1)
%         plot([1,2,3,4,5,6,7,8,9],...
%             [all_bcea_P(ii_p,1), all_bcea_P(ii_p,2),all_bcea_P(ii_p,3),...
%            all_bcea_P(ii_p,4), all_bcea_P(ii_p,5),all_bcea_P(ii_p,6),...
%             all_bcea_P(ii_p,7), all_bcea_P(ii_p,8),all_bcea_P(ii_p,9)],...
%             'o','MarkerSize',6,'MarkerFaceColor','g')
%         hold on
        
    end
    for ii_c = 1:length(controls)
        
        if ii_c < 11 
           dir_path = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\controls\HUX_C\',controls{ii_c},'\matFiles\'); 
        else
            dir_path = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\controls\',controls{ii_c},'\matFiles\');
        end
        file_to_read_avg = strcat('em_info_for_BSA_heatmaps_',controls{ii_c},'.mat');
        load(strcat(dir_path,file_to_read_avg))
        
        xT_b1_BSA = em.allTraces.xBinned.bin1;
        yT_b1_BSA = em.allTraces.yBinned.bin1;
        all_bcea_C(ii_c,1) = get_bcea(xT_b1_BSA/60,yT_b1_BSA/60);
        
        xT_b2_BSA = em.allTraces.xBinned.bin2;
        yT_b2_BSA = em.allTraces.yBinned.bin2;
        all_bcea_C(ii_c,2) = get_bcea(xT_b2_BSA/60,yT_b2_BSA/60);
        
        xT_b3_BSA = em.allTraces.xBinned.bin3;
        yT_b3_BSA = em.allTraces.yBinned.bin3;
        all_bcea_C(ii_c,3) = get_bcea(xT_b3_BSA/60,yT_b3_BSA/60);
        
        xT_b4_BSA = em.allTraces.xBinned.bin4;
        yT_b4_BSA = em.allTraces.yBinned.bin4;
        all_bcea_C(ii_c,4) = get_bcea(xT_b4_BSA/60,yT_b4_BSA/60);
        
        clear em
        file_to_read_avg = strcat('em_info_for_heatmaps_',controls{ii_c},'.mat');
        load(strcat(dir_path,file_to_read_avg))
      
        xT_b1_task = em.allTraces.xBinned.bin1;
        yT_b1_task = em.allTraces.yBinned.bin1;
        all_bcea_C(ii_c,5) = get_bcea(xT_b1_task/60,yT_b1_task/60);
        
        xT_b2_task = em.allTraces.xBinned.bin2;
        yT_b2_task = em.allTraces.yBinned.bin2;
        all_bcea_C(ii_c,6) = get_bcea(xT_b2_task/60,yT_b2_task/60);
        
        xT_b3_task = em.allTraces.xBinned.bin3;
        yT_b3_task = em.allTraces.yBinned.bin3;
        all_bcea_C(ii_c,7) = get_bcea(xT_b3_task/60,yT_b3_task/60);
        
        xT_b4_task = em.allTraces.xBinned.bin4;
        yT_b4_task = em.allTraces.yBinned.bin4;
        all_bcea_C(ii_c,8) = get_bcea(xT_b4_task/60,yT_b4_task/60);
        
        xT_b5_task = em.allTraces.xBinned.bin5;
        yT_b5_task = em.allTraces.yBinned.bin5;
        all_bcea_C(ii_c,9) = get_bcea(xT_b5_task/60,yT_b5_task/60);
        
%         figure(1)
%         plot([1,2,3,4,5,6,7,8,9],...
%             [all_bcea_C(ii_c,1), all_bcea_C(ii_c,2),all_bcea_C(ii_c,3),...
%            all_bcea_C(ii_c,4), all_bcea_C(ii_c,5),all_bcea_C(ii_c,6),...
%             all_bcea_C(ii_c,7), all_bcea_C(ii_c,8),all_bcea_C(ii_c,9)],...
%             'o','MarkerSize',6,'MarkerFaceColor','m')
%         hold on
    end

    for ii_hc = 1:length(HUX)
        dir_path = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\controls\HUX_C\',HUX{ii_hc},'\matFiles\');
     
        file_to_read_avg = strcat('em_info_for_BSA_heatmaps_',HUX{ii_hc},'.mat');
        load(strcat(dir_path,file_to_read_avg))
        
        xT_b1_BSA = em.allTraces.xBinned.bin1;
        yT_b1_BSA = em.allTraces.yBinned.bin1;
        all_bcea_hC(ii_hc,1) = get_bcea(xT_b1_BSA/60,yT_b1_BSA/60);
        
        xT_b2_BSA = em.allTraces.xBinned.bin2;
        yT_b2_BSA = em.allTraces.yBinned.bin2;
        all_bcea_hC(ii_hc,2) = get_bcea(xT_b2_BSA/60,yT_b2_BSA/60);
        
        xT_b3_BSA = em.allTraces.xBinned.bin3;
        yT_b3_BSA = em.allTraces.yBinned.bin3;
        all_bcea_hC(ii_hc,3) = get_bcea(xT_b3_BSA/60,yT_b3_BSA/60);
        
        xT_b4_BSA = em.allTraces.xBinned.bin4;
        yT_b4_BSA = em.allTraces.yBinned.bin4;
        all_bcea_hC(ii_hc,4) = get_bcea(xT_b4_BSA/60,yT_b4_BSA/60);
        
        clear em
        file_to_read_avg = strcat('em_info_for_heatmaps_',HUX{ii_hc},'.mat');
        load(strcat(dir_path,file_to_read_avg))
      
        xT_b1_task = em.allTraces.xBinned.bin1;
        yT_b1_task = em.allTraces.yBinned.bin1;
        all_bcea_hC(ii_hc,5) = get_bcea(xT_b1_task/60,yT_b1_task/60);
        
        xT_b2_task = em.allTraces.xBinned.bin2;
        yT_b2_task = em.allTraces.yBinned.bin2;
        all_bcea_hC(ii_hc,6) = get_bcea(xT_b2_task/60,yT_b2_task/60);
        
        xT_b3_task = em.allTraces.xBinned.bin3;
        yT_b3_task = em.allTraces.yBinned.bin3;
        all_bcea_hC(ii_hc,7) = get_bcea(xT_b3_task/60,yT_b3_task/60);
        
        xT_b4_task = em.allTraces.xBinned.bin4;
        yT_b4_task = em.allTraces.yBinned.bin4;
        all_bcea_hC(ii_hc,8) = get_bcea(xT_b4_task/60,yT_b4_task/60);
        
        xT_b5_task = em.allTraces.xBinned.bin5;
        yT_b5_task = em.allTraces.yBinned.bin5;
        all_bcea_hC(ii_hc,9) = get_bcea(xT_b5_task/60,yT_b5_task/60);
        
        figure(1)
        plot([1,2,3,4,5,6,7,8,9],...
            [all_bcea_hC(ii_hc,1), all_bcea_hC(ii_hc,2),all_bcea_hC(ii_hc,3),...
           all_bcea_hC(ii_hc,4), all_bcea_hC(ii_hc,5),all_bcea_hC(ii_hc,6),...
            all_bcea_hC(ii_hc,7), all_bcea_hC(ii_hc,8),all_bcea_hC(ii_hc,9)],...
            'o','MarkerSize',6,'MarkerFaceColor','b')
        hold on
    end
    
    
    figure(1)
    std_err_bcea_P_b1 = std(all_bcea_P(:,1))/sqrt(length(all_bcea_P(:,1)));
    std_err_bcea_P_b2 = std(all_bcea_P(:,2))/sqrt(length(all_bcea_P(:,2)));
    std_err_bcea_P_b3 = std(all_bcea_P(:,3))/sqrt(length(all_bcea_P(:,3)));
    std_err_bcea_P_b4 = std(all_bcea_P(:,4))/sqrt(length(all_bcea_P(:,4)));
    std_err_bcea_P_b5 = std(all_bcea_P(:,5))/sqrt(length(all_bcea_P(:,5)));
    std_err_bcea_P_b6 = std(all_bcea_P(:,6))/sqrt(length(all_bcea_P(:,6)));
    std_err_bcea_P_b7 = std(all_bcea_P(:,7))/sqrt(length(all_bcea_P(:,7)));
    std_err_bcea_P_b8 = std(all_bcea_P(:,8))/sqrt(length(all_bcea_P(:,8)));
    std_err_bcea_P_b9 = std(all_bcea_P(:,9))/sqrt(length(all_bcea_P(:,9)));
    
    std_err_bcea_C_b1 = std(all_bcea_C(:,1))/sqrt(length(all_bcea_C(:,1)));
    std_err_bcea_C_b2 = std(all_bcea_C(:,2))/sqrt(length(all_bcea_C(:,2)));
    std_err_bcea_C_b3 = std(all_bcea_C(:,3))/sqrt(length(all_bcea_C(:,3)));
    std_err_bcea_C_b4 = std(all_bcea_C(:,4))/sqrt(length(all_bcea_C(:,4)));
    std_err_bcea_C_b5 = std(all_bcea_C(:,5))/sqrt(length(all_bcea_C(:,5)));
    std_err_bcea_C_b6 = std(all_bcea_C(:,6))/sqrt(length(all_bcea_C(:,6)));
    std_err_bcea_C_b7 = std(all_bcea_C(:,7))/sqrt(length(all_bcea_C(:,7)));
    std_err_bcea_C_b8 = std(all_bcea_C(:,8))/sqrt(length(all_bcea_C(:,8)));
    std_err_bcea_C_b9 = std(all_bcea_C(:,9))/sqrt(length(all_bcea_C(:,9)));
    
    std_err_bcea_hC_b1 = std(all_bcea_hC(:,1))/sqrt(length(all_bcea_hC(:,1)));
    std_err_bcea_hC_b2 = std(all_bcea_hC(:,2))/sqrt(length(all_bcea_hC(:,2)));
    std_err_bcea_hC_b3 = std(all_bcea_hC(:,3))/sqrt(length(all_bcea_hC(:,3)));
    std_err_bcea_hC_b4 = std(all_bcea_hC(:,4))/sqrt(length(all_bcea_hC(:,4)));
    std_err_bcea_hC_b5 = std(all_bcea_hC(:,5))/sqrt(length(all_bcea_hC(:,5)));
    std_err_bcea_hC_b6 = std(all_bcea_hC(:,6))/sqrt(length(all_bcea_hC(:,6)));
    std_err_bcea_hC_b7 = std(all_bcea_hC(:,7))/sqrt(length(all_bcea_hC(:,7)));
    std_err_bcea_hC_b8 = std(all_bcea_hC(:,8))/sqrt(length(all_bcea_hC(:,8)));
    std_err_bcea_hC_b9 = std(all_bcea_hC(:,9))/sqrt(length(all_bcea_hC(:,9)));
    
    hold on
    errorbar([mean(all_bcea_P(:,1)) mean(all_bcea_P(:,2)) mean(all_bcea_P(:,3))...
              mean(all_bcea_P(:,4)) mean(all_bcea_P(:,5)) mean(all_bcea_P(:,6))...
              mean(all_bcea_P(:,7)) mean(all_bcea_P(:,8)) mean(all_bcea_P(:,9))],...
             [std_err_bcea_P_b1 std_err_bcea_P_b2 std_err_bcea_P_b3...
              std_err_bcea_P_b4 std_err_bcea_P_b5 std_err_bcea_P_b6...
              std_err_bcea_P_b7 std_err_bcea_P_b8 std_err_bcea_P_b9],...
              'o', 'Color', 'g', 'LineStyle', '-','LineWidth',3)
   hold on
   errorbar([mean(all_bcea_C(:,1)) mean(all_bcea_C(:,2)) mean(all_bcea_C(:,3))...
              mean(all_bcea_C(:,4)) mean(all_bcea_C(:,5)) mean(all_bcea_C(:,6))...
              mean(all_bcea_C(:,7)) mean(all_bcea_C(:,8)) mean(all_bcea_C(:,9))],...
             [std_err_bcea_C_b1 std_err_bcea_C_b2 std_err_bcea_C_b3...
              std_err_bcea_C_b4 std_err_bcea_C_b5 std_err_bcea_C_b6...
              std_err_bcea_C_b7 std_err_bcea_C_b8 std_err_bcea_C_b9],...
              'o', 'Color', 'm', 'LineStyle', '-','LineWidth',3)
   hold on
   errorbar([mean(all_bcea_hC(:,1)) mean(all_bcea_hC(:,2)) mean(all_bcea_hC(:,3))...
              mean(all_bcea_hC(:,4)) mean(all_bcea_hC(:,5)) mean(all_bcea_hC(:,6))...
              mean(all_bcea_hC(:,7)) mean(all_bcea_hC(:,8)) mean(all_bcea_hC(:,9))],...
             [std_err_bcea_hC_b1 std_err_bcea_hC_b2 std_err_bcea_hC_b3...
              std_err_bcea_hC_b4 std_err_bcea_hC_b5 std_err_bcea_hC_b6...
              std_err_bcea_hC_b7 std_err_bcea_hC_b8 std_err_bcea_hC_b9],...
              'o', 'Color', 'b', 'LineStyle', '-','LineWidth',3)
   set(gca, 'XTick', [1:1:9], 'XTickLabel', {'50'; '150'; '250';'350';'450';'550';'650';'750';'850'}, 'FontSize', 12);
   xlim([0 10])
%    ylim([0 5])
   ylabel('bcea (deg^2)')
   xlabel('Time (ms)')
   xline(4.5,'--k','LineWidth',3)
   set(gca,'fontsize',13,'FontWeight','Bold')
%    set(gca, 'YScale', 'log')
   

end
