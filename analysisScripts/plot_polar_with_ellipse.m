function plot_polar_with_ellipse(patients,controls,tasktype)
    cP = distinguishable_colors(length(patients));%parula(length(patients));
    cC = distinguishable_colors(length(controls));
    pPlot = 1;
    scatPlot = 0;
    for ii_p = 1:length(patients)
        dir_path = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\patients\',patients{ii_p},'\matFiles\');
        if ii_p > 10
            dir_path = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\controls\',patients{ii_p},'\matFiles\');
        else
            dir_path = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\controls\HUX_C\',controls{ii_p},'\matFiles\');
        end
        if strcmp(tasktype,'fix')
            file_to_read = strcat('ms_info_fix_500ms_',controls{ii_p},'.mat');
        else
            file_to_read = strcat('ms_info_task_',patients{ii_p},'.mat');
        end
        load(strcat(dir_path,file_to_read))
        title_str = patients{ii_p};
        [~,theta,amp_1,...
            ~,...
            ~,~] = calc_amp_theta(msInfo,tasktype,scatPlot,title_str);
        if pPlot
            idx = find(amp_1> 5);
            amp_1 = amp_1(idx);
            theta = theta(idx);
            figure()
            p = polarplot(theta,amp_1,'o');
            p.Color = cP(ii_p,:);
            p.MarkerSize = 12;
            rlim([0 30])
            title(title_str);
            set(gca,'fontsize',13,'FontWeight','Bold')
            pathToSave = 'C:/Users/sanjana/OneDrive - University of Rochester/Documents/Sanjana/Projects/schizophrenia/Figures/ms_polarplots_task';
            
            if ~exist(strcat(pathToSave), 'dir')
                mkdir(strcat(pathToSave))
            end
            fullPath = strcat(pathToSave,'/%s');
            strfName = title_str;
            saveas(gcf,sprintf(fullPath, strfName), 'png')
            saveas(gcf,sprintf(fullPath, strfName), 'eps')
            close all
            hold on
            figure(2)
            [x,y] = pol2cart(p.ThetaData,p.RData);
            scatter(x,y,color)
            hold on
            stimuliSize = 4.2/2;
            width = 2 * stimuliSize;
            height = width * 5;
            centerX = (-stimuliSize);
            centerY = (-stimuliSize*5);
            rectangle('Position',[centerX, centerY, width, height],'LineWidth',3)
            hold on
            ellipseXY(x, y, 68, cP(ii_p,:), 0)
            xlim([-30 30])
            ylim([-30 30])
            xlabel('arcmins')
            ylabel('arcmins')
            hold on
            clear p
        end
        [inst_sp_x_P,inst_sp_y_P,raw_x,raw_y] = deal([]);
        if ii_p > 9
            dir_path = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\controls\',controls{ii_p},'\matFiles\');
        else
            dir_path = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\controls\HUX_C\',controls{ii_p},'\matFiles\');
        end
        file_to_read = strcat('em_info_for_drift_bias_task_',controls{ii_p},'.mat');
        load(strcat(dir_path,file_to_read))
        ln_em = numel(fieldnames(em.ecc_0));
        for t_size = 1:ln_em
            f_name = fieldnames(em.ecc_0);

            all_chars = eval(strcat('em.ecc_0.',f_name{t_size}));
            if ~isfield(all_chars,'instSpX')
                continue
            else
                for all = 1:length(all_chars.instSpX)
                    x = all_chars.instSpX{all};
                    y = all_chars.instSpY{all};
                    rawX = all_chars.x_d_bias{all};
                    rawY = all_chars.y_d_bias{all};
                    inst_sp_x_P = [inst_sp_x_P,x];
                    inst_sp_y_P = [inst_sp_y_P,y];
                    raw_x = [raw_x,rawX];
                    raw_y = [raw_y,rawY];
                end
            end

        end
        clear em;
        temp_inst_sp_x_P = inst_sp_x_P(inst_sp_x_P > -200 & inst_sp_x_P < 200);
        temp_inst_sp_x_P = inst_sp_x_P(inst_sp_y_P > -200 & inst_sp_y_P < 200);
        temp_inst_sp_y_P = inst_sp_y_P(inst_sp_x_P > -200 & inst_sp_x_P < 200);
        temp_inst_sp_y_P = inst_sp_y_P(inst_sp_y_P > -200 & inst_sp_y_P < 200);
%         [x,y] = pol2cart(temp_inst_sp_x_P(~isnan(temp_inst_sp_x_P)),temp_inst_sp_y_P(~isnan(temp_inst_sp_y_P)));
%         x = temp_inst_sp_x_P(~isnan(temp_inst_sp_x_P)); y = temp_inst_sp_y_P(~isnan(temp_inst_sp_x_P));
        figure(3)
        ellipseXY(temp_inst_sp_x_P(~isnan(temp_inst_sp_x_P)),temp_inst_sp_y_P(~isnan(temp_inst_sp_y_P)), 68,cP(ii_p,:), 0)
%         ellipseXY(x,y, 68,cP(ii_p,:), 0)
        hold on
        xlim([-120 120])
        ylim([-120 120])
        xlabel('arcmins/s')
        ylabel('arcmins/s')
    end
%     end
    for ii_c = 1:length(controls)
        
        if ii_c > 10
            dir_path = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\controls\',controls{ii_c},'\matFiles\');
        else
            dir_path = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\controls\HUX_C\',controls{ii_c},'\matFiles\');
        end
        if strcmp(tasktype,'fix')
            file_to_read = strcat('ms_info_fix_500ms_',controls{ii_c},'.mat');
        else
            file_to_read = strcat('ms_info_task_',controls{ii_c},'.mat');
        end
        load(strcat(dir_path,file_to_read))
        title_str = controls{ii_c};
        [~,theta,amp_1,...
            ~,...
            ~,~] = calc_amp_theta(msInfo,tasktype,scatPlot,title_str);
        if pPlot
            idx = find(amp_1> 5);
            amp_1 = amp_1(idx);
            theta = theta(idx);
            figure()
            p = polarplot(theta,amp_1,'o');
            p.Color = cC(ii_c,:);
            p.MarkerSize = 12;
            rlim([0 30])
            title(title_str);
            set(gca,'fontsize',13,'FontWeight','Bold')
            pathToSave = 'C:/Users/sanjana/OneDrive - University of Rochester/Documents/Sanjana/Projects/schizophrenia/Figures/ms_polarplots_task';
%             
            if ~exist(strcat(pathToSave), 'dir')
                mkdir(strcat(pathToSave))
            end
            fullPath = strcat(pathToSave,'/%s');
            strfName = title_str;
            saveas(gcf,sprintf(fullPath, strfName), 'png')
            saveas(gcf,sprintf(fullPath, strfName), 'eps')
            close all
%             hold on
%             figure(2)
%             [x,y] = pol2cart(p.ThetaData,p.RData);
% %             scatter(x,y,color)
%             hold on
%             stimuliSize = 4.2/2;
%             width = 2 * stimuliSize;
%             height = width * 5;
%             centerX = (-stimuliSize);
%             centerY = (-stimuliSize*5);
%             rectangle('Position',[centerX, centerY, width, height],'LineWidth',3)
%             hold on
%             ellipseXY(x, y, 68, cP(ii_p,:), 0)
%             xlim([-30 30])
%             ylim([-30 30])
%             xlabel('arcmins')
%             ylabel('arcmins')
%             hold on
            clear p
        end
    end
    
end
