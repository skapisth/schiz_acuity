function get_bias_prob(patients,controls,tasktype)
    cP = distinguishable_colors(length(patients));%parula(length(patients));
    cC = distinguishable_colors(length(controls));
    pPlot = 1;
    scatPlot = 0;
    
    prob_array_ms_P = zeros(length(patients),4);
    prob_array_d_P = zeros(length(patients),4);
    
    flag_Q_ms_P = zeros(length(patients),4);
    flag_Q_d_P = zeros(length(patients),4);
    
    prob_array_ms_C = zeros(length(controls),4);
    prob_array_d_C = zeros(length(controls),4);
    
    flag_Q_ms_C = zeros(length(controls),4);
    flag_Q_d_C = zeros(length(controls),4);
    
    
    
    for ii_p = 1:length(patients)
        dir_path = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\patients\',patients{ii_p},'\matFiles\');

        if strcmp(tasktype,'fix')
            file_to_read = strcat('ms_info_fix_500ms_',controls{ii_p},'.mat');
        else
            file_to_read = strcat('ms_info_task_',patients{ii_p},'.mat');
        end
        load(strcat(dir_path,file_to_read))
        title_str = patients{ii_p};
        [~,theta,amp_1,...
            ~,...
            ~,~] = calc_amp_theta(msInfo,tasktype,scatPlot,title_str);
        
        
        deg_theta_ms = rad2deg(theta);
        %Q1: 0 to 90, Q2: > 270 , Q3: 180-270, Q4: 90-180
        Q1_ln_ms = find(deg_theta_ms > 0 & deg_theta_ms < 90);
        Q2_ln_ms = find(deg_theta_ms < 0  & deg_theta_ms > -90);
        Q3_ln_ms = find(deg_theta_ms > -180 & deg_theta_ms < -90);
        Q4_ln_ms = find(deg_theta_ms > 90 & deg_theta_ms < 180);
        
        pQ1 = length(Q1_ln_ms)/length(theta);
        pQ2 = length(Q2_ln_ms)/length(theta);
        pQ3 = length(Q3_ln_ms)/length(theta);
        pQ4 = length(Q4_ln_ms)/length(theta);
        
        prob_array_ms_P(ii_p,1) = pQ1;
        prob_array_ms_P(ii_p,2) = pQ2;
        prob_array_ms_P(ii_p,3) = pQ3;
        prob_array_ms_P(ii_p,4) = pQ4;
        
        if pQ1 > 0.35
            flag_Q_ms_P(ii_p,1) = 1;
        elseif pQ2 > 0.35
            flag_Q_ms_P(ii_p,2) = 1;
        elseif pQ3 > 0.35
            flag_Q_ms_P(ii_p,3) = 1;
        elseif pQ4 > 0.35
            flag_Q_ms_P(ii_p,4) = 1;
        end
        
%         figure()
%         p = polarplot(theta,amp_1,'o');
%         p.Color = cP(ii_p,:);
%         rlim([0 30])
%         pathToSave = 'C:/Users/sanjana/OneDrive - University of Rochester/Documents/Sanjana/Projects/schizophrenia/Figures/ms_polarplots_task';
%         %
%         if ~exist(strcat(pathToSave), 'dir')
%             mkdir(strcat(pathToSave))
%         end
%         fullPath = strcat(pathToSave,'/%s');
%         strfName = title_str;
%         saveas(gcf,sprintf(fullPath, strfName), 'png')
%         saveas(gcf,sprintf(fullPath, strfName), 'eps')
%         close all
        %
%         clear p
        
        [inst_sp_x_P,inst_sp_y_P,raw_x,raw_y] = deal([]);
        dir_path = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\patients\',patients{ii_p},'\matFiles\');
        
        file_to_read = strcat('em_info_for_drift_bias_task_',patients{ii_p},'.mat');
        load(strcat(dir_path,file_to_read))
        ln_em = numel(fieldnames(em.ecc_0));
        for t_size = 1:ln_em
            f_name = fieldnames(em.ecc_0);

            all_chars = eval(strcat('em.ecc_0.',f_name{t_size}));
            if ~isfield(all_chars,'instSpX')
                continue
            else
                for all = 1:length(all_chars.instSpX)
                    x = all_chars.instSpX{all};
                    y = all_chars.instSpY{all};
                    rawX = all_chars.x_d_bias{all};
                    rawY = all_chars.y_d_bias{all};
                    inst_sp_x_P = [inst_sp_x_P,x];
                    inst_sp_y_P = [inst_sp_y_P,y];
                    raw_x = [raw_x,rawX];
                    raw_y = [raw_y,rawY];
                end
            end

        end
        clear em;
        temp_inst_sp_x_P = inst_sp_x_P(inst_sp_x_P > -200 & inst_sp_x_P < 200);
        temp_inst_sp_x_P = inst_sp_x_P(inst_sp_y_P > -200 & inst_sp_y_P < 200);
        temp_inst_sp_y_P = inst_sp_y_P(inst_sp_x_P > -200 & inst_sp_x_P < 200);
        temp_inst_sp_y_P = inst_sp_y_P(inst_sp_y_P > -200 & inst_sp_y_P < 200);

        nd_X = temp_inst_sp_x_P(~isnan(temp_inst_sp_x_P));
        nd_Y = temp_inst_sp_y_P(~isnan(temp_inst_sp_x_P));
        
        nd_X = inst_sp_x_P(~isnan(inst_sp_x_P));
        nd_Y = inst_sp_y_P(~isnan(inst_sp_x_P));
        
        
        rad_theta_vel = atan2(nd_Y,nd_X);
        deg_theta_vel = rad2deg(rad_theta_vel);
        
         %Q1: 0 to 90, Q2: > 270 , Q3: 180-270, Q4: 90-180
        Q1_ln_d = find(deg_theta_vel > 0 & deg_theta_vel < 90);
        Q2_ln_d = find(deg_theta_vel < 0  & deg_theta_vel > -90);
        Q3_ln_d = find(deg_theta_vel > -180 & deg_theta_vel < -90);
        Q4_ln_d = find(deg_theta_vel > 90 & deg_theta_vel < 180);
        
        pQ1 = length(Q1_ln_d)/length(deg_theta_vel);
        pQ2 = length(Q2_ln_d)/length(deg_theta_vel);
        pQ3 = length(Q3_ln_d)/length(deg_theta_vel);
        pQ4 = length(Q4_ln_d)/length(deg_theta_vel);
        
        prob_array_d_P(ii_p,1) = pQ1;
        prob_array_d_P(ii_p,2) = pQ2;
        prob_array_d_P(ii_p,3) = pQ3;
        prob_array_d_P(ii_p,4) = pQ4;
        
        if pQ1 > 0.35
            flag_Q_d_P(ii_p,1) = 1;
        elseif pQ2 > 0.35
            flag_Q_d_P(ii_p,2) = 1;
        elseif pQ3 > 0.35
            flag_Q_d_P(ii_p,3) = 1;
        elseif pQ4 > 0.35
            flag_Q_d_P(ii_p,4) = 1;
        end
        
%         amp = sqrt(inst_sp_x_P(~isnan(inst_sp_x_P)).^2+inst_sp_y_P(~isnan(inst_sp_x_P)).^2);
    end
    
    for ii_c = 1:length(controls)
        
        if ii_c > 9
            dir_path = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\controls\',controls{ii_c},'\matFiles\');
        else
            dir_path = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\controls\HUX_C\',controls{ii_c},'\matFiles\');
        end
        if strcmp(tasktype,'fix')
            file_to_read = strcat('ms_info_fix_500ms_',controls{ii_c},'.mat');
        else
            file_to_read = strcat('ms_info_task_',controls{ii_c},'.mat');
        end
        load(strcat(dir_path,file_to_read))
        title_str = controls{ii_c};
        [~,theta,amp_1,...
            ~,...
            ~,~] = calc_amp_theta(msInfo,tasktype,scatPlot,title_str);
        
        deg_theta_ms = rad2deg(theta);
        %Q1: 0 to 90, Q2: > 270 , Q3: 180-270, Q4: 90-180
        Q1_ln_ms = find(deg_theta_ms > 0 & deg_theta_ms < 90);
        Q2_ln_ms = find(deg_theta_ms < 0  & deg_theta_ms > -90);
        Q3_ln_ms = find(deg_theta_ms > -180 & deg_theta_ms < -90);
        Q4_ln_ms = find(deg_theta_ms > 90 & deg_theta_ms < 180);
        
        pQ1 = length(Q1_ln_ms)/length(theta);
        pQ2 = length(Q2_ln_ms)/length(theta);
        pQ3 = length(Q3_ln_ms)/length(theta);
        pQ4 = length(Q4_ln_ms)/length(theta);
        
        prob_array_ms_C(ii_c,1) = pQ1;
        prob_array_ms_C(ii_c,2) = pQ2;
        prob_array_ms_C(ii_c,3) = pQ3;
        prob_array_ms_C(ii_c,4) = pQ4;
        
        if pQ1 > 0.35
            flag_Q_ms_C(ii_c,1) = 1;
        elseif pQ2 > 0.35
            flag_Q_ms_C(ii_c,2) = 1;
        elseif pQ3 > 0.35
            flag_Q_ms_C(ii_c,3) = 1;
        elseif pQ4 > 0.35
            flag_Q_ms_C(ii_c,4) = 1;
        end
        
%         figure()
%         p = polarplot(theta,amp_1,'o');
%         p.Color = cC(ii_c,:);
%         rlim([0 30])
%         pathToSave = 'C:/Users/sanjana/OneDrive - University of Rochester/Documents/Sanjana/Projects/schizophrenia/Figures/ms_polarplots_task';
%         %
%         if ~exist(strcat(pathToSave), 'dir')
%             mkdir(strcat(pathToSave))
%         end
%         fullPath = strcat(pathToSave,'/%s');
%         strfName = title_str;
%         saveas(gcf,sprintf(fullPath, strfName), 'png')
%         saveas(gcf,sprintf(fullPath, strfName), 'eps')
%         close all
%         %
%         clear p
%         
    [inst_sp_x_C,inst_sp_y_C,raw_x,raw_y] = deal([]);
    if ii_c > 9
       dir_path = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\controls\',controls{ii_c},'\matFiles\');
    else
       dir_path = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\controls\HUX_C\',controls{ii_c},'\matFiles\');
    end
    
    file_to_read = strcat('em_info_for_drift_bias_task_',controls{ii_c},'.mat');
    load(strcat(dir_path,file_to_read))
    ln_em = numel(fieldnames(em.ecc_0));
    for t_size = 1:ln_em
        f_name = fieldnames(em.ecc_0);
        
        all_chars = eval(strcat('em.ecc_0.',f_name{t_size}));
        if ~isfield(all_chars,'instSpX')
            continue
        else
            for all = 1:length(all_chars.instSpX)
                x = all_chars.instSpX{all};
                y = all_chars.instSpY{all};
                rawX = all_chars.x_d_bias{all};
                rawY = all_chars.y_d_bias{all};
                inst_sp_x_C = [inst_sp_x_C,x];
                inst_sp_y_C = [inst_sp_y_C,y];
                raw_x = [raw_x,rawX];
                raw_y = [raw_y,rawY];
            end
        end
        
    end
    clear em;
    temp_inst_sp_x_C = inst_sp_x_C(inst_sp_x_C > -200 & inst_sp_x_C < 200);
    temp_inst_sp_x_C = inst_sp_x_C(inst_sp_y_C > -200 & inst_sp_y_C < 200);
    temp_inst_sp_y_C = inst_sp_y_C(inst_sp_x_C > -200 & inst_sp_x_C < 200);
    temp_inst_sp_y_C = inst_sp_y_C(inst_sp_y_C > -200 & inst_sp_y_C < 200);
    
    %         nd_X = temp_inst_sp_x_P(~isnan(temp_inst_sp_x_P));
    %         nd_Y = temp_inst_sp_y_P(~isnan(temp_inst_sp_x_P));
    %
    nd_X = inst_sp_x_C(~isnan(inst_sp_x_C));
    nd_Y = inst_sp_y_C(~isnan(inst_sp_x_C));
    
    
    rad_theta_vel = atan2(nd_Y,nd_X);
    deg_theta_vel = rad2deg(rad_theta_vel);
    
    %Q1: 0 to 90, Q2: > 270 , Q3: 180-270, Q4: 90-180
    Q1_ln_d = find(deg_theta_vel > 0 & deg_theta_vel < 90);
    Q2_ln_d = find(deg_theta_vel < 0  & deg_theta_vel > -90);
    Q3_ln_d = find(deg_theta_vel > -180 & deg_theta_vel < -90);
    Q4_ln_d = find(deg_theta_vel > 90 & deg_theta_vel < 180);
    
    pQ1 = length(Q1_ln_d)/length(deg_theta_vel);
    pQ2 = length(Q2_ln_d)/length(deg_theta_vel);
    pQ3 = length(Q3_ln_d)/length(deg_theta_vel);
    pQ4 = length(Q4_ln_d)/length(deg_theta_vel);
    
    prob_array_d_C(ii_c,1) = pQ1;
    prob_array_d_C(ii_c,2) = pQ2;
    prob_array_d_C(ii_c,3) = pQ3;
    prob_array_d_C(ii_c,4) = pQ4;
    
    if pQ1 > 0.35
        flag_Q_d_C(ii_c,1) = 1;
    elseif pQ2 > 0.35
        flag_Q_d_C(ii_c,2) = 1;
    elseif pQ3 > 0.35
        flag_Q_d_C(ii_c,3) = 1;
    elseif pQ4 > 0.35
        flag_Q_d_C(ii_c,4) = 1;
    end
    
%         amp = sqrt(ins
    end
    
end
