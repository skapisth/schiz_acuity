function get_offsets_in_x_y(patients,controls)
    [avg_x_all_P,avg_x_all_C,avg_y_all_P,avg_y_all_C] = deal([]);
    cP = distinguishable_colors(length(patients));
    cC = distinguishable_colors(length(controls));
    for ii_p = 1:length(patients)
        
        dir_path = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\patients\',patients{ii_p},'\matFiles\');
        file_to_read = strcat('em_info_for_heatmaps_',patients{ii_p},'.mat');
       
        load(strcat(dir_path,file_to_read))
        
        avg_x = round(nanmean(em.allTraces.xALL),2);
        avg_y = round(nanmean(em.allTraces.yALL),2);
        
        avg_x_all_P(ii_p) = avg_x;
        avg_y_all_P(ii_p) = avg_y;
        
        color = cP(ii_p,:);
        
        figure(1)
        plot([1],avg_x,'kp','MarkerSize',10,'MarkerFaceColor',color)
        hold on
        
        figure(2)
        plot([1],avg_y,'kp','MarkerSize',10,'MarkerFaceColor',color)
        hold on
        
                 
    end
    for ii_c = 1:length(controls)
        
        if ii_c < 10
            dir_path = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\controls\HUX_C\',controls{ii_c},'\matFiles\');
        else
            dir_path = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\controls\',controls{ii_c},'\matFiles\');
        end
        file_to_read = strcat('em_info_for_heatmaps_',controls{ii_c},'.mat');
        
        
        load(strcat(dir_path,file_to_read))
        
        avg_x = round(nanmean(em.allTraces.xALL),2);
        avg_y = round(nanmean(em.allTraces.yALL),2);
        
        avg_x_all_C(ii_c) = avg_x;
        avg_y_all_C(ii_c) = avg_y;
        
        color = cP(ii_p,:);
        
        figure(1)
        plot([2],avg_x,'kp','MarkerSize',10,'MarkerFaceColor',color)
        set(gca, 'XTick', [1:1:2], 'XTickLabel', {'patients'; 'controls'}, 'FontSize', 13);
        ylabel('Avg x [arcmin]')
        set(gca,'fontsize',13,'FontWeight','Bold')
        xlim([0 3])
        hold on
        
        figure(2)
        plot([2],avg_y,'kp','MarkerSize',10,'MarkerFaceColor',color)
        set(gca, 'XTick', [1:1:2], 'XTickLabel', {'patients'; 'controls'}, 'FontSize', 13);
        ylabel('Avg y [arcmin]')
        set(gca,'fontsize',13,'FontWeight','Bold')
        xlim([0 3])
        hold on
                 
    end
    ylim([-15 15])
    [px,hx] = ranksum(avg_x_all_P,avg_x_all_C)
    [py,hy] = ranksum(avg_y_all_P,avg_y_all_C)
end