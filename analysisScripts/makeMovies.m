em = [];
newEyeris = 1;
figures = struct(...
    'FIGURE_ON', 0,... %Show individual trial with traces in 2D space
    'VIDEO', 0,... %Wills save video of FIGURE_ON figures
    'FIXATION_ANALYSIS',0,... %Plots psychometric curves
    'saveFixMat',0,...
    'QUICK_ANALYSIS',0,... %Only plots psychometric curves
    'CHECK_TRACES',1,... %will plot traces
    'HUX_RUN',1,... %will discard trials differently
    'FOLDED', 0,...
    'FIXED_SIZE', 0,...
    'COMPARE_SPANS',0); %0 = all spans, 1 = small spans, 2 = large spans

machine = {'D'};

subject = {'FE-S08'};
condition = {'uncrowded'};

params = struct(...
    'newEyeris',newEyeris,...
    'subject',subject,...
    'patient',1,...
    'blankScreenAnalysis',0,...
    'nBoots', 1,... %Number of boots for psychometric fits
    'crowded', false, ... %Gets rewritten based on currentCond
    'DMS',false, ...
    'D', false, ...
    'MS', false,...
    'fixation',true,...
    'machine',machine, ... %DPI "A" vs dDPI "D"
    'session', '1', ... %Which number session
    'spanMax', 100,... %The Maximum Span
    'spanMin', 0,...%The Minimum Span
    'compSpans', 0,...
    'ecc',0,...
    'samplingRate' , 1000/1000,... % 1000/341 for old eyeris
    'stabilization','Unstabilized');

if params.patient
    pathToFile =  strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\patients\',char(subject),'\unCrowded');
else
    pathToFile =  strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\controls\HUX_C\',char(subject),'\unCrowded');
end
    
load(fullfile(pathToFile,'pptrials.mat'), 'pptrials');

[pptrials,params] = modify_pptrials(pptrials,subject,newEyeris,params);

if newEyeris
    params.samplingRate = 1000/pptrials{1}.eye_spf;
    [traces, counter, valid, em] = countingTrials_newEyeris(pptrials, em, params, figures);
else
    params.samplingRate = 1000/341;
    [traces, counter, valid, em] = countingTrials(pptrials, em, params, figures);
end

trialsNeeded = find(valid.dms == 1);

cleanTrials = {};
for aT = 1:length(trialsNeeded)
    cleanTrials{1,end+1} = pptrials{trialsNeeded(aT)};
end

[posX,posY,timeStamps] = deal([]);
for cT = 1:length(cleanTrials)
    
    if newEyeris
        posX = [posX, cleanTrials{cT}.x.position+cleanTrials{cT}.xoffset * params.pixelAngle(cT)];
        posY = [posY, cleanTrials{cT}.y.position+cleanTrials{cT}.yoffset * params.pixelAngle(cT)];
        timeStamps = [timeStamps, 1:1:length(cleanTrials{cT}.x.position)];
    else
         timeOn = round(cleanTrials{cT}.TimeTargetON/(params.samplingRate));
         timeOff = round(min(cleanTrials{cT}.TimeTargetOFF/(params.samplingRate)-1, cleanTrials{cT}.ResponseTime/(params.samplingRate)));  
         posX = [posX, cleanTrials{cT}.x.position(timeOn:timeOff)+cleanTrials{cT}.xoffset * params.pixelAngle(cT)];
         posY = [posY, cleanTrials{cT}.y.position(timeOn:timeOff)+cleanTrials{cT}.yoffset * params.pixelAngle(cT)];
         timeStamps = [timeStamps, (1:1:length(cleanTrials{cT}.x.position(timeOn:timeOff)))*3];
    end
    
end

%% For blankscreen 
if params.patient
    if newEyeris
        pathToFile = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\newEyeris_analysisScripts\patients\',char(subject),'\unCrowded\blankScreenData');
    else
        pathToFile =  strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\patients\',char(subject),'\unCrowded');
    end
else
    pathToFile =  strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\controls\HUX_C\',char(subject),'\unCrowded');
end
  
    
load(fullfile(pathToFile,'pptrials.mat'), 'pptrials');
blankS_pptrials = pptrials;


[blankS_pptrials,params] = modify_pptrials(blankS_pptrials,subject,newEyeris,params);

if newEyeris
    params.samplingRate = 1000/blankS_pptrials{1}.eye_spf;
    [traces, counter, valid, em] = countingTrials_newEyeris(blankS_pptrials, em, params, figures);
else
    %params.samplingRate = 1000/341;
    [traces, counter, valid, em] = countingTrials(blankS_pptrials, em, params, figures);
end

blankS_trialsNeeded = find(valid.dms == 1);

blankS_cleanTrials = {};
for bT = 1:length(blankS_trialsNeeded)
    blankS_cleanTrials{1,end+1} = blankS_pptrials{blankS_trialsNeeded(bT)};
end

[blankS_posX,blankS_posY,blankS_timeStamps] = deal([]);
for cBT = 1:length(blankS_cleanTrials)
    
    if newEyeris
        blankS_posX = [blankS_posX, blankS_cleanTrials{cBT}.x.position+blankS_cleanTrials{cBT}.xoffset * params.pixelAngle(cBT)];
        blankS_posY = [blankS_posY, blankS_cleanTrials{cBT}.y.position+blankS_cleanTrials{cBT}.yoffset * params.pixelAngle(cBT)];
        blankS_timeStamps = [blankS_timeStamps, 1:1:length(blankS_cleanTrials{cBT}.x.position)];
    else
        timeOn = round(blankS_cleanTrials{cBT}.TimeTargetON/(params.samplingRate));
        timeOn = timeOn - 400/params.samplingRate;
        timeOff = timeOn + 400/params.samplingRate;  
        blankS_posX = [posX, blankS_cleanTrials{cBT}.x.position(timeOn:timeOff)+blankS_cleanTrials{cBT}.xoffset * params.pixelAngle(cBT)];
        blankS_posY = [posY, blankS_cleanTrials{cBT}.y.position(timeOn:timeOff)+blankS_cleanTrials{cBT}.yoffset * params.pixelAngle(cBT)];
        blankS_timeStamps = [blankS_timeStamps, (1:1:length(blankS_cleanTrials{cBT}.x.position(timeOn:timeOff)))*3];
    end
    
end

generateHeatMapMovie_combined(subject,blankS_posX,blankS_posY,blankS_timeStamps,posX, posY,timeStamps)
% generateHeatMapMovie_one(posX, posY,timeStamps)

function [pptrials,params] = modify_pptrials(pptrials,subject,newEyeris,params)
    
    for ii = 1:length(pptrials)
        if isfield(pptrials{ii}, 'pixelAngle')
            params.pixelAngle(ii) = pptrials{1,ii}.pixelAngle;
            pptrials{1,ii}.pxAngle = pptrials{1,ii}.pixelAngle;
        else
            params.pixelAngle(ii) = pptrials{1,ii}.pxAngle;
        end
        if strcmp(subject{1}, 'FE-B01')
            pptrials{ii}.Unstabilized = 0;
        end
        if newEyeris
            pptrials{ii}.x.velocity = p_calculateVelocity(pptrials{ii}.x.position, 'prefilter', true, ...
                'postfilter', true, 'postvalue', true, 'order', 3, 'smoothing', bitor( double(round(51/1000*pptrials{ii}.eye_spf)), 1)  ) * pptrials{ii}.eye_spf / 1000;
            pptrials{ii}.y.velocity = p_calculateVelocity(pptrials{ii}.y.position, 'prefilter', true, ...
                'postfilter', true, 'postvalue', true, 'order', 3, 'smoothing', bitor( double(round(51/1000*pptrials{ii}.eye_spf)), 1)  ) * pptrials{ii}.eye_spf / 1000;
        end
    end
end
    

