function [traces, counter, valid, em] = countingTrials_old(pptrials, em, params, figures)%,numTooShort,numBlinks,numNoTracks,numSaccades,numMicroSaccades)
%COUNTING TRIALS discards trials based on parameters
%   Print details of what is being thrown in command window

numTotalTrials = length(pptrials);
dist_thresh = 30; %offset from recalibration
counter = buildCountingStruct;
valid = buildValidStruct(pptrials);
traces = [];
if params.D && strcmp(params.stabilization,'Unstabilized')
    
    targetDistThresh = 30; %maximum distance mean of trace is from 0 (can play with this for patients, it basically increases the threshold)
elseif params.D && strcmp(params.stabilization,'Stabilized')
    targetDistThresh = 50; %maximum distance mean of trace is from 0
else
    targetDistThresh = 50; %maximum distance mean of trace is from 0
end
%%%%%%can change to 60 for stabilized

output = NaN(length(pptrials));
%different counts for different events
for ii = 1:length(pptrials)
    output(ii) = 0;
    msInTrial = 0;
    sacInTrial = 0;
    msPartiallyInTrial = 0;
    %% Task Trial
    if  ceil(pptrials{ii}.TimeTargetON) > 0 %% TASK TRIAL
        if strcmp('D',params.machine)
            timeOn = round(pptrials{ii}.TimeTargetON/(params.samplingRate));
            timeOff = round(min(pptrials{ii}.TimeTargetOFF/(params.samplingRate)-1, pptrials{ii}.ResponseTime/(params.samplingRate)));
        else
            timeOn = round(pptrials{ii}.TimeTargetON);
            timeOff = round(min(pptrials{ii}.TimeTargetOFF, pptrials{ii}.ResponseTime));
            pptrials{ii}.pixelAngle = pptrials{ii}.pxAngle;
        end
        if floor(timeOff) > length(pptrials{ii}.x.position)
            timeOff = length(pptrials{ii}.x.position);
        end
         x = pptrials{ii}.x.position(ceil(timeOn):floor(timeOff)) + pptrials{ii}.xoffset * params.pixelAngle(ii);
         y = pptrials{ii}.y.position(ceil(timeOn):floor(timeOff)) + pptrials{ii}.yoffset * params.pixelAngle(ii);         
        em.xShift{ii} = x - single(pptrials{ii}.TargetEccentricity * pptrials{ii}.pxAngle);
        em.targetEcc(ii) = single(pptrials{ii}.TargetEccentricity * pptrials{ii}.pxAngle);
        em.x{ii} = x;
        em.y{ii} = y;
        em.eccTarget(ii) =  single(pptrials{ii}.TargetEccentricity * pptrials{ii}.pxAngle);
        em.span(ii) = quantile(sqrt((x-mean(x)).^2 + (y-mean(y)).^2), .95); %Caclulates Span
        em.meanXSpan(ii) = (mean(x)); %%mean span from 0, essentially causing target to be a specific distance
        em.actualEccTargetDist(ii) = double( mean(x)...
            - single(pptrials{ii}.TargetEccentricity * pptrials{ii}.pxAngle));
        span = em.span(ii);
        %Performance
        em.performance(ii) = pptrials{ii}.Correct;
        em.size(ii) = pptrials{ii}.pxAngle * double(pptrials{ii}.TargetStrokewidth);
    elseif ceil(pptrials{ii}.TimeFixationON) > 0
        %% Fixation Trial
        if strcmp('D',params.machine)
            if pptrials{ii}.TimeFixationON == 0
                pptrials{ii}.timesOFF = 1;
                valid.timesOFF(ii) = true;
                counter.timesOFF = counter.timesOFF+1;
                output(ii) = 1;
                continue;
            end
        end
        timeOn = ceil(pptrials{ii}.TimeFixationON/(params.samplingRate));
        timeOff = length(pptrials{ii}.x.position);%round(pptrials{ii}.TimeFixationOFF);
        
%          if find(pptrials{ii}.notracks.start>timeOn & pptrials{ii}.notracks.start<timeOff)
%             valid.notrack(ii) = true;
%             counter.NoTracks = counter.NoTracks+1;
%             output(ii) = 1;
%             continue;
%         end
        
        if strcmp('D',params.machine)
            x = pptrials{ii}.x.position((ceil(timeOn/(params.samplingRate))):timeOff) + params.pixelAngle(ii) * pptrials{ii}.xoffset;
            y = pptrials{ii}.y.position((ceil(timeOn/(params.samplingRate))):timeOff) + params.pixelAngle(ii) * pptrials{ii}.yoffset;
        else
            x = pptrials{ii}.x.position(timeOn:timeOff) + params.pixelAngle(ii) * pptrials{ii}.xoffset;
            y = pptrials{ii}.y.position(timeOn:timeOff) + params.pixelAngle(ii) * pptrials{ii}.yoffset;
        end
        em.span(ii) = quantile(sqrt((x-mean(x)).^2 + (y-mean(y)).^2), .95);
        em.meanXSpan(ii) = (mean(x)); %%mean span from 0, essentially causing target to be a specific distance
        em.actualEccTargetDist(ii) = double( mean(x)...
            - single(pptrials{ii}.TargetEccentricity * params.pixelAngle(ii))); %%Take the DIFFERENCE between the 2
        span = em.span(ii);
        valid.fixation(ii) = true;
        counter.Fixation = counter.Fixation+1;
        output(ii) = 1;
        continue;
    else
        %% Doesn't fall into the category of Fixation or Task Trial - sanity check
        valid.unknownToss = true;
        counter.unknownToss = counter.unknownToss + 1;
        output(ii) = 1;
        continue;
    end
    if ~figures.HUX_RUN
        %Checks for GAZE OFF CENTER
        if pptrials{ii}.TargetEccentricity > 0 || pptrials{ii}.TargetEccentricity < 0
            if mean(sqrt(x.^2 + y.^2)) > targetDistThresh %%%Gaze Off Center/Span too large
                pptrials{ii}.GazeOffCenter = 1;
                valid.gazeoffcenter(ii) = true;
                counter.GazeOffCenter = counter.GazeOffCenter+1;
                output(ii) = 1;
                continue;
            end
%                         targetDistThresh = 15;
%                         dist_thresh = 10;
            pptrials{ii}.ActualEccTarget = double(mean(x)); %%mean span from 0, essentially causing target to be a specific distance
            em.actualEccTarget(ii) = double(mean(x));
            stimSize = pptrials{ii}.pixelAngle * double(pptrials{ii}.TargetStrokewidth) * 2;
            uEcc = double(pptrials{ii}.TargetEccentricity * pptrials{ii}.pixelAngle);
            centerTarget = (rectangle('Position',[(-stimSize)+uEcc ...
                (-stimSize*5) ...
                (2*stimSize) ...
                (10*stimSize)]));
            pointsCenterTarget = bbox2points(centerTarget.Position);
            onTarget = find(x > pointsCenterTarget(1,1) & ...
                x < pointsCenterTarget(2,1) & ...
                y > pointsCenterTarget(1,2) & ...
                y < pointsCenterTarget(3,2));
            close all;
            if sum(length(onTarget)) > 5
                pptrials{ii}.LookingAtTarget = 1;
                valid.lookingAtTarget(ii) = true;
                counter.LookingAtTarget = counter.LookingAtTarget+1;
                output(ii) = 1;
                continue;
            else
                em.actualEccTarget(ii) = double(mean(x));
            end

        elseif mean(sqrt(x.^2 + y.^2)) > targetDistThresh %%%Gaze Off Center/Span too large
            pptrials{ii}.GazeOffCenter = 1;
            valid.gazeoffcenter(ii) = true;
            counter.GazeOffCenter = counter.GazeOffCenter+1;
            output(ii) = 1;
            continue;
        end
        %Checks for LARGE OFFSET
        if sum(abs(x) > dist_thresh) > 0 || ...
                sum(abs(y) > dist_thresh) > 0
            pptrials{ii}.BigOffset = 1;
            valid.bigoffset(ii) = true;
            counter.BigOffset = counter.BigOffset+1;
            output(ii) = 1;
            continue;
        end
    else
        huxOffset = 180;
%         if strcmp(params.subject,'HUX25')
%             huxOffset = 180;
%         end
        if sum(abs(x) > huxOffset) > 0 || ...
                sum(abs(y) > huxOffset) > 0
            pptrials{ii}.BigOffset = 1;
            valid.bigoffset(ii) = true;
            counter.BigOffset = counter.BigOffset+1;
            output(ii) = 1;
            continue;
        end

    end
    %% Checks for BLINKS
    if find(pptrials{ii}.blinks.start > timeOn & pptrials{ii}.blinks.start < timeOff)%%%BLINKS
        valid.blink(ii) = true;
        counter.Blinks = counter.Blinks+1;
        output(ii) = 1;
        continue;
    end

    %% Checks for NO TRACKS
    if find(pptrials{ii}.notracks.start>timeOn & pptrials{ii}.notracks.start<timeOff)
        valid.notrack(ii) = true;
        counter.NoTracks = counter.NoTracks+1;
        output(ii) = 1;
        continue;
    end
    if strcmp('D',params.machine)
        if  ~isempty(find(diff(pptrials{ii}.x.position(ceil(timeOn/(params.samplingRate)):floor(timeOff/(params.samplingRate)))) == 0, 1))
            valid.notrack(ii) = true;
            counter.NoTracks = counter.NoTracks+1;
            output(ii) = 1;
            continue;
        end
    end
    %% Checks for NO RESPONSE
    if pptrials{ii}.Correct == 3 && pptrials{ii}.TimeFixationON == 0 %%%NO RESPONSE
        valid.noresponse(ii) = true;
        counter.NoResponse = counter.NoResponse+1;
        output(ii) = 1;
        continue;
    end
    %% Checks for MANUAL DISCARD
    if  ~isempty(pptrials{ii}.invalid.start)
        valid.manualdiscard(ii) = true;
        counter.manualDiscard = counter.manualDiscard+1;
        output(ii) = 1;
        continue;
    end
    %% Checks for SACCADES
    %     if ~isempty(length(pptrials{ii}.saccades.start))
    %         for i = 1:length(pptrials{ii}.saccades.start)
    %             if (pptrials{ii}.saccades.start(i) >= timeOn) && ...
    %                     (pptrials{ii}.saccades.start(i)+pptrials{ii}.saccades.duration(i)<timeOff)
    %                 sacInTrial = 1;
    %             end
    %         end
    %         if sacInTrial
    %             valid.s(ii) = true;
    %             counter.Saccades = counter.Saccades+1;
    %             output(ii) = 1;
    %             continue;
    %         end
    %     end
    %% Makes sure there are no fixation trials
    if pptrials{ii}.TimeFixationOFF > 0
        valid.fixation(ii) = true;
        counter.Fixation = counter.Fixation+1;
        output(ii) = 1;
        continue;
    end
    %% Checks for Span Limits
    if span < params.spanMin || span > params.spanMax %%%IF Span is too large/too small
        valid.span(ii) = true;
        counter.Span = counter.Span+1;
        output(ii) = 1;
        continue;
    end
    %% Saccade Check
    sInTrial = 0;
    if ~isempty(length(pptrials{ii}.saccades.start))
        for i = 1:length(pptrials{ii}.saccades.start)
            sStartTime = pptrials{ii}.saccades.start(i);
            sDurationTime = pptrials{ii}.saccades.duration(i);
            if (timeOn <= sStartTime) && ...
                    (timeOff > sStartTime + sDurationTime)
                sInTrial = 1;
            elseif (timeOn >= sStartTime) && ...
                    (timeOn <  sStartTime + sDurationTime) || ...
                    (timeOff > sStartTime) && ...
                    (timeOff <= sStartTime + sDurationTime)
                sInTrial = 1;
            end
        end
        if sInTrial
            valid.s(ii) = true;
            counter.Saccades = counter.Saccades+1;
            counter.TotalTask = counter.TotalTask + 1;
            continue;
        end
    end
    %% Microsaccade Check
    if ~isempty(length(pptrials{ii}.microsaccades.start))
        for i = 1:length(pptrials{ii}.microsaccades.start)
            msStartTime = pptrials{ii}.microsaccades.start(i);
            msDurationTime = pptrials{ii}.microsaccades.duration(i);
            if (timeOn <= msStartTime) && ...
                    (timeOff > msStartTime + msDurationTime)
                msInTrial = 1;
            elseif (timeOn >= msStartTime) && ...
                    (timeOn <  msStartTime + msDurationTime) || ...
                    (timeOff > msStartTime) && ...
                    (timeOff <= msStartTime + msDurationTime)
                msPartiallyInTrial = 1;
            end
        end
        if msPartiallyInTrial
            valid.partialMS(ii) = true;
            valid.dms(ii) = true;
            counter.partialMS = counter.partialMS+1;
            output(ii) = 1;
            continue;
        elseif msInTrial
            valid.ms(ii) = true;
            valid.dms(ii) = true;
            counter.Microsaccades = counter.Microsaccades+1;
            counter.TotalTask = counter.TotalTask + 1;
            continue;
        end
    end
    %% Drift Check
    if ~isempty(pptrials{ii}.drifts)
        valid.d(ii) = true;
        valid.dms(ii) = true;
        counter.Drifts = counter.Drifts+1;
        counter.TotalTask = counter.TotalTask + 1;
        
        traces.x{counter.Drifts} = x;
        traces.y{counter.Drifts} = y;
        continue;
    else
        %% Didn't Fufill any other category
        valid.unknownToss = true;
        counter.unknownToss = counter.unknownToss + 1;
    end
end

if ~isfield(params,'OnlyTemp')
    fprintf('\nThere are %i in total trials prior to filter.\n\n', numTotalTrials)
    
    fprintf('There are %i trials that have incorrect start time.\n',counter.timesOFF)
    fprintf('There are %i trials that have gaze too far off center.\n',counter.GazeOffCenter)
    fprintf('There are %i trials that have looking at target (Ecc > 0).\n',counter.LookingAtTarget)
    fprintf('There are %i trials that have too short.\n',counter.TooShort)
    fprintf('There are %i trials that have large offset.\n',counter.BigOffset)
    fprintf('There are %i trials that have blinks.\n',counter.Blinks)
    fprintf('There are %i trials that have no track during stimulus.\n',counter.NoTracks)
    fprintf('There are %i trials that have no response.\n',counter.NoResponse)
    fprintf('There are %i trials that have saccades.\n',counter.Saccades)
    fprintf('There are %i trials that are fixation.\n',counter.Fixation)
    fprintf('There are %i trials that have large offset for fixation trials.\n',counter.BigOffsetFixation)
    fprintf('There are %i trials that do not fit within the span.\n',counter.Span)
    fprintf('There are %i trials that have microsaccades.\n',counter.Microsaccades)
    fprintf('There are %i trials that have microsaccades only partially in trail.\n',counter.partialMS)
    fprintf('There are %i trials that have drifts only.\n',counter.Drifts)
    fprintf('There are %i trials that have been manually discarded.\n',counter.manualDiscard)
    fprintf('There are %i trials that have been tossed for no category.\n',counter.unknownToss)
    fprintf('There are %i trials that have been tossed for being the wrong ecc.\n',counter.wrongecc)
    fprintf('There are %i trials after filter.\n',  sum(cell2mat(struct2cell(counter))) - counter.TotalTask)

    counter.TotalTrials = sum(cell2mat(struct2cell(counter))) - counter.TotalTask;
    
    data = struct2table(counter);

end

end



