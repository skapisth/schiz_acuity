function [ pptrials ] = autoPrune( pptrials, currentTrialId, em, threshold )
%Automatically prunes tags that are too short to be real
%   em = the type of eye movements (ie drifts, saccades, notracks...)
%   threshold = the minimum number of samples that are required for it
%   to be real

for ii = 1:length(pptrials{currentTrialId}.(em).duration)
    if pptrials{currentTrialId}.(em).duration(ii) < threshold
        pptrials{currentTrialId}.(em).start(ii) = 0;
        pptrials{currentTrialId}.(em).duration(ii) = 0;
    end
end
end