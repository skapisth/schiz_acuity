function trialChar = add_fields_trialChar(trialChar,pptrials,params)
    flds = fields(trialChar);
    for fi = 1:length(flds)
        trialChar.(flds{fi}) = cellfun(@(z) z(:).(flds{fi}), pptrials);
%         trialChar.(flds{fi}) = cell2mat(trialChar.(flds{fi}));
    end
    for ii = 1:length(pptrials)
        
        trialChar.TargetSize(ii) = double(pptrials{ii}.TargetStrokewidth)*2*pptrials{ii}.pxAngle;
%         trialChar.TargetEccentricity(ii) = {0};%round(double(pptrials{ii}.TargetEccentricity)*params.pixelAngle(ii)); %picks 1 rounded ecc measure
        
    end
    
end