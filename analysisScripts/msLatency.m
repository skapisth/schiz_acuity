function msLatency(patients,controls)
 
    scatPlot = 0;
    cP = jet(length(controls));
    cC = winter(length(patients));
    
    [avg_task_p,avg_task_c,avg_BS_p,avg_BS_c] = deal([]);
    [all_p_task,all_c_task,all_p_blankS,all_c_blankS] = deal({});
    for ii_p = 1:length(patients)
        dir_path = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\patients\',patients{ii_p},'\matFiles\');
        file_to_read = strcat('ms_info_task_',patients{ii_p},'.mat');
        
        load(strcat(dir_path,file_to_read))
        
        ms_start_task_p = msInfo.startTime(~isnan(msInfo.startTime));
        all_p_task{ii_p,1} = ms_start_task_p;
        
        clear msInfo
        file_to_read = strcat('ms_info_blankS_',patients{ii_p},'.mat');
        load(strcat(dir_path,file_to_read))
        
        ms_start_blankS_p = msInfo.startTime(~isnan(msInfo.startTime));
        all_p_blankS{ii_p,1} = ms_start_blankS_p;
        
        ss = {ms_start_blankS_p,ms_start_task_p};
        
        avg_task_p(1,end+1) = median(ms_start_task_p);
        avg_BS_p(1,end+1) = median(ms_start_blankS_p);
        subplot(3,3,ii_p)
        [theText, rawN, x] = MultipleHist(ss,'xlabel', 'Microsaccade latency (ms)', 'ylabel', 'proportion','legend',...
                {'blank screen period','task period'}, ...
                'samebins','noerror', 'binfactor',1, 'color','qualitative', 'proportion','smooth');
        
    end 
    clear ss
    for ii_c = 1:length(controls)
        if ii_c < 11
            dir_path = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\controls\HUX_C\',controls{ii_c},'\matFiles\');
        else
            dir_path = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\controls\',controls{ii_c},'\matFiles\');
        end
%         
        file_to_read = strcat('ms_info_task_',controls{ii_c},'.mat');
        load(strcat(dir_path,file_to_read))
        
        ms_start_task_c = msInfo.startTime(~isnan(msInfo.startTime));
        all_c_task{ii_c,1} = ms_start_task_c;
        
        file_to_read = strcat('ms_info_blankS_',controls{ii_c},'.mat');
        load(strcat(dir_path,file_to_read))
        
        ms_start_blankS_c = msInfo.startTime(~isnan(msInfo.startTime));
        all_c_blankS{ii_c,1} = ms_start_blankS_c;
        
        ss = {ms_start_blankS_c,ms_start_task_c};
        
        avg_task_c(1,end+1) = median(ms_start_task_c);
        avg_BS_c(1,end+1) = median(ms_start_blankS_c);
        figure(2)
        hold on
        subplot(5,3,ii_c)
        [theText, rawN, x] = MultipleHist(ss,'xlabel', 'Microsaccade latency (ms)', 'ylabel', 'proportion','legend',...
                {'blank screen period','task period'}, ...
                'samebins','noerror', 'binfactor',1, 'color','qualitative', 'proportion','smooth');
        
    end
   figure(3)
   hold on
   eb = {};
   [theText, rawN, x,eb] = getAvgdValsForHistogram(all_p_task,0,eb,'xlabel', 'Microsaccade latency (ms)', 'ylabel', 'proportion','legend',...
                {'patients', 'controls'}, ...
                'samebins','noerror', 'binfactor',1, 'color','r', 'proportion','smooth');
   hold on
%    figure(4)
   eb = {};
   [theText, rawN, x,eb] = getAvgdValsForHistogram(all_c_task,0,eb,'xlabel', 'Microsaccade latency (ms)', 'ylabel', 'proportion','legend',...
                {'patients', 'controls'}, ...
                'samebins','noerror', 'binfactor',1, 'color','qualitative', 'proportion','smooth');
            
            
   figure(4)
   hold on
   eb = {};
   [theText, rawN, x,eb] = getAvgdValsForHistogram(all_p_blankS,0,eb,'xlabel', 'Microsaccade latency (ms)', 'ylabel', 'proportion','legend',...
                {'patients', 'controls'}, ...
                'samebins','noerror', 'binfactor',1, 'color','r', 'proportion','smooth');
   hold on
   eb = {};
   [theText, rawN, x,eb] = getAvgdValsForHistogram(all_c_blankS,0,eb,'xlabel', 'Microsaccade latency (ms)', 'ylabel', 'proportion','legend',...
                {'patients', 'controls'}, ...
                'samebins','noerror', 'binfactor',1, 'color','qualitative', 'proportion','smooth');
%    
    end