function [em,counter,ii] = vals_for_heatmaps_task(em,counter,valueGroup,params,ii)
    if ii == 1
        [em.allTraces.xALL,em.allTraces.yALL] = deal([]);

        [em.allTraces.xBinned.bin1,em.allTraces.xBinned.bin2,em.allTraces.xBinned.bin3,...
            em.allTraces.xBinned.bin4,em.allTraces.xBinned.bin5] = deal([]);

        [em.allTraces.yBinned.bin1,em.allTraces.yBinned.bin2,em.allTraces.yBinned.bin3,...
            em.allTraces.yBinned.bin4,em.allTraces.yBinned.bin5] = deal([]);
        ii = 0;
    end
    
    bin1 = 100*(1/params.samplingRate);
    bin2 = 200*(1/params.samplingRate);
    bin3 = 300*(1/params.samplingRate);
    bin4 = 400*(1/params.samplingRate);
    bin5 = 500*(1/params.samplingRate);
    for numTraces = 1:length(em.(valueGroup.ecc).(valueGroup.strokeWidth).position)
        
        em.allTraces.x{counter} = em.(valueGroup.ecc).(valueGroup.strokeWidth).position(numTraces).x;
        em.allTraces.y{counter} = em.(valueGroup.ecc).(valueGroup.strokeWidth).position(numTraces).y;
        
%        [ em.allTraces.x{counter}, em.allTraces.y{counter}] = checkXYArt(em.allTraces.x{counter}, em.allTraces.y{counter}, min(round(length(em.allTraces.x{counter})/100)),31); %Checks strange artifacts
       [ em.allTraces.x{counter}, em.allTraces.y{counter}] = checkXYBound(em.allTraces.x{counter}, em.allTraces.y{counter}, 30); %Checks boundries
        
        em.allTraces.xALL = [em.allTraces.xALL em.allTraces.x{counter}];
        em.allTraces.yALL = [em.allTraces.yALL em.allTraces.y{counter}];
        
        em.allTraces.xBinned.bin1 = [em.allTraces.xBinned.bin1 em.allTraces.x{counter}(1:bin1)];
        em.allTraces.yBinned.bin1 = [em.allTraces.yBinned.bin1 em.allTraces.y{counter}(1:bin1)];
        
        em.allTraces.xBinned.bin2 = [em.allTraces.xBinned.bin2 em.allTraces.x{counter}(bin1+1:bin2)];
        em.allTraces.yBinned.bin2 = [em.allTraces.yBinned.bin2 em.allTraces.y{counter}(bin1+1:bin2)];
        
        em.allTraces.xBinned.bin3 = [em.allTraces.xBinned.bin3 em.allTraces.x{counter}(bin2+1:bin3)];
        em.allTraces.yBinned.bin3 = [em.allTraces.yBinned.bin3 em.allTraces.y{counter}(bin2+1:bin3)];
        
        
        
        if ~params.blankScreenAnalysis
            em.allTraces.xBinned.bin4 = [em.allTraces.xBinned.bin4 em.allTraces.x{counter}(bin3+1:bin4)];
            em.allTraces.yBinned.bin4 = [em.allTraces.yBinned.bin4 em.allTraces.y{counter}(bin3+1:bin4)];
            em.allTraces.xBinned.bin5 = [em.allTraces.xBinned.bin5 em.allTraces.x{counter}(bin4+1:end)];
            em.allTraces.yBinned.bin5 = [em.allTraces.yBinned.bin5 em.allTraces.y{counter}(bin4+1:end)];
        else
            em.allTraces.xBinned.bin4 = [em.allTraces.xBinned.bin4 em.allTraces.x{counter}(bin3+1:end)];
            em.allTraces.yBinned.bin4 = [em.allTraces.yBinned.bin4 em.allTraces.y{counter}(bin3+1:end)];
        end
        counter = counter+1;
    end
    
    
end