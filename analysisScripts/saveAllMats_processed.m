% subject = {'PC-S01','PC-S02','PC-S04','PC-S07','PC-S12','PC-S16','FE-S01','FE-S08','FE-S09','FE-S17','FE-S18','FE-S31'};
subject = {'FE-C17','FE-C19','FE-C33','FE-C36','FE-C44','FE-C48'};
machine = {'D'};
condition = {'uncrowded'};
newEyeris = 1;
params = struct(...
        'patient',0,...
        'blankScreenAnalysis',0,...
        'nBoots', 1,... %Number of boots for psychometric fits
        'crowded', false, ... %Gets rewritten based on currentCond
        'DMS',true, ...
        'D', false, ...
        'MS', false,...
        'fixation',false,...
        'machine',machine, ... %DPI "A" vs dDPI "D"
        'session', '1', ... %Which number session
        'spanMax', 100,... %The Maximum Span
        'spanMin', 0,...%The Minimum Span
        'compSpans', 0,...
        'ecc',0,...
        'samplingRate' , 1000/1000,... % 1000/341 for old eyeris
        'short_300ms_analysis',0,...
        'save_heatmap_mats',1,...
        'scanpath_analysis',0,...
        'drift_bias_mats',0,...
        'plot_2D_traces',0,...
        'stabilization','Unstabilized');

figures = struct(...
        'FIGURE_ON', 0,... %Show individual trial with traces in 2D space
        'VIDEO', 0,... %Wills save video of FIGURE_ON figures
        'FIXATION_ANALYSIS',0,... %Plots psychometric curves
        'saveFixMat',0,...
        'QUICK_ANALYSIS',0,... %Only plots psychometric curves
        'CHECK_TRACES',0,... %will plot traces
        'HUX_RUN',1,... %will discard trials differently
        'FOLDED', 0,...
        'FIXED_SIZE', 0,...
        'COMPARE_SPANS',0); %0 = all spans, 1 = small spans, 2 = large spans


for is = 1:length(subject)
    
    params.subject = subject{is};
    
    em = [];
    
    if params.patient
        if is > 3
            newEyeris = 1;
        else
            newEyeris = 0;
        end
         params.newEyeris = newEyeris;
        if params.blankScreenAnalysis
            if newEyeris
                pathToFile = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\newEyeris_analysisScripts\patients\',char(subject{is}),'\unCrowded\blankScreenData');
            else
                pathToFile =  strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\patients\',char(subject{is}),'\unCrowded');
            end
        else
            pathToFile =  strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\patients\',char(subject{is}),'\unCrowded');
        end
    else
        params.newEyeris = newEyeris;
        if params.blankScreenAnalysis
            if newEyeris
                pathToFile = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\newEyeris_analysisScripts\controls\',char(subject{is}),'\unCrowded\blankScreenData');
            else
                pathToFile =  strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\controls\HUX_C\',char(subject{is}),'\unCrowded');
            end
        else
            pathToFile =  strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\controls\',char(subject{is}),'\unCrowded');
        end
    end

    if ~params.blankScreenAnalysis
        if strcmp(subject{is}, 'PC-S16')% || strcmp(subject{1}, 'PC-S07'))
            if params.DMS
                load(fullfile(pathToFile,'task_clean.mat'), 'all_clean');
            elseif params.fixation
                load(fullfile(pathToFile,'fix_clean.mat'), 'all_clean');
            end

        end
    end
    load(fullfile(pathToFile,'pptrials.mat'), 'pptrials');

    % load(fullfile(pathToFile,'clean_data_PC-S12.mat'), 'all_clean');
    % pptrials = osRemoverInEM(pptrials);

    for ii = 1:length(pptrials)
        if isfield(pptrials{ii}, 'pixelAngle')
            params.pixelAngle(ii) = pptrials{1,ii}.pixelAngle;
            pptrials{1,ii}.pxAngle = pptrials{1,ii}.pixelAngle;
        else
            params.pixelAngle(ii) = pptrials{1,ii}.pxAngle;
        end
        if strcmp(subject{is}, 'FE-B01') 
            pptrials{ii}.Unstabilized = 0;
        end
        if newEyeris
            pptrials{ii}.x.velocity = p_calculateVelocity(pptrials{ii}.x.position, 'prefilter', true, ...
              'postfilter', true, 'postvalue', true, 'order', 3, 'smoothing', bitor( double(round(51/1000*pptrials{ii}.eye_spf)), 1)  ) * pptrials{ii}.eye_spf / 1000;
            pptrials{ii}.y.velocity = p_calculateVelocity(pptrials{ii}.y.position, 'prefilter', true, ...
              'postfilter', true, 'postvalue', true, 'order', 3, 'smoothing', bitor( double(round(51/1000*pptrials{ii}.eye_spf)), 1)  ) * pptrials{ii}.eye_spf / 1000;
        end
    end

    trialChar = buildTrialCharStruct(pptrials,newEyeris);
    trialChar = add_fields_trialChar(trialChar,pptrials,params);



    if newEyeris
        params.samplingRate = 1000/pptrials{1}.eye_spf;
        [traces, counter, valid, em] = countingTrials_newEyeris(pptrials, em, params, figures);
    else
        params.samplingRate = 1000/341;
        [traces, counter, valid, em] = countingTrials(pptrials, em, params, figures);
    end
    % 



    traces.swIdx = trialChar.TargetStrokewidth(valid.d);
    trialChar.Subject = char(subject{is});

    title_str = char(subject{is});

    if figures.FIXATION_ANALYSIS
        if strcmp(subject{is}, 'PC-S16') %|| strcmp(subject{1}, 'PC-S07'))
            ids = find(valid.fixation == 1 & all_clean == 1);
    %         valid.fixation = valid.fixation(ids);
        else
            ids = 0;
        end
        fixation_trials_analysis(valid,pptrials,1,params,subject{is},ids)
    end
    if ~params.blankScreenAnalysis
        if strcmp(subject{is}, 'PC-S16') % || strcmp(subject{1}, 'PC-S07'))
            valid.dms = all_clean;
        end
    end
    [em] = emAnalysis(em, pptrials, valid, trialChar,params, 0,figures,subject{is});
    % 
    pathToSub = getPathToSaveMatfiles(params);
%     if params.short_300ms_analysis
%         em_Fname = sprintf(strcat('em_info_cut_drift_',subject{is},'.mat'));
%         save(fullfile(pathToSub,em_Fname),'em');
%     end
end
% condition = Psychometric_Graphing(params,params.ecc,...
%                                   valid, trialChar, params.crowded, title_str);
% fprintf('Threshold SW(Size) for %s  = %.3f\n', trialChar.Subject,condition.thresh);


