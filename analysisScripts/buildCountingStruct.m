function counter = buildCountingStruct()

counter.TooShort = 0;
counter.BigOffset = 0;
counter.BigOffsetFixation = 0;
counter.Blinks = 0;
counter.NoTracks = 0;
counter.NoResponse = 0;
counter.manualDiscard = 0;
counter.Saccades = 0;
counter.Fixation = 0;
counter.Span = 0;
counter.Microsaccades = 0;
counter.Drifts = 0;
counter.TotalTask = 0; 
counter.GazeOffCenter = 0;
counter.timesOFF = 0;
counter.unknownToss = 0;
counter.partialMS = 0;
counter.SpanTooSmall = 0;
counter.LookingAtTarget = 0;
counter.wrongecc = 0;

end
