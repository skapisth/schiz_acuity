function call_ms_analysis(patients,controls)
 
    scatPlot = 0;
    cP = jet(length(controls));
    cC = winter(length(patients));
    [all_dist_start_toC, all_dist_end_toC,...
        rate_all,mean_amp_all] = deal([]);
    for ii_p = 1:length(patients)
%         dir_path = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\patients\',patients{ii_p},'\matFiles\');
%         
        if ii_p < 9
            dir_path = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\controls\HUX_C\',controls{ii_p},'\matFiles\');
        else
            dir_path = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\controls\',controls{ii_p},'\matFiles\');
        end
%         file_to_read = strcat('ms_info_blankS_',controls{ii_p},'.mat');
        file_to_read = strcat('ms_info_task_',controls{ii_p},'.mat');
%         file_to_read = strcat('ms_info_blankS_',patients{ii_p},'.mat');
%         file_to_read = strcat('ms_info_task_',patients{ii_p},'.mat');
        load(strcat(dir_path,file_to_read))
        
       
%         title_str = patients{ii_p};
        title_str = controls{ii_p};
        [all_dist_start_toC,all_dist_end_toC,...
            rate_all,mean_amp_all] = ms_Analysis_2023(msInfo,scatPlot,title_str,cP,ii_p,all_dist_start_toC,all_dist_end_toC,...
                                    rate_all,mean_amp_all);
    end  
   
%     hold on
    shadedErrorBar(1:400,rate_all(:,1:400),{@mean,@std});
    title('Temporal course of microsaccades','FontSize',20)
    xlabel('Time (ms)','FontSize',15)
    ylabel('Microsaccades/s','FontSize',15)
    xlim([0 400])
    ylim([-1 5])
    axis square
    set(gca,'fontsize',27,'FontWeight','Bold')
%      ylim([0 50])
    [p,h] = ranksum(all_dist_start_toC,all_dist_end_toC);

%     [all_dist_start_toC_C, all_dist_end_toC_C,...
%         rate_all_C,mean_amp_all_C] = deal([]);
%     for ii_C = 1:length(controls)
%         dir_path = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\controls\',controls{ii_C},'\matFiles\');
%         file_to_read = strcat('ms_info_blankS_',controls{ii_C},'.mat');
%         
%         load(strcat(dir_path,file_to_read))
%         
%        
%         title_str = patients{ii_C};
% %         title_str = controls{ii_p};
%         [all_dist_start_toC_C,all_dist_end_toC_C,...
%             rate_all_C,mean_amp_all_C] = ms_Analysis_2023(msInfo,scatPlot,title_str,ii_C,all_dist_start_toC_C,all_dist_end_toC_C,...
%                                     rate_all_C,mean_amp_all_C);
%     end  
%    
%     hold on
%     shadedErrorBar(1:400,rate_all_C(:,1:400),{@mean,@std});
%     title('Temporal course of microsaccades','FontSize',20)
%     xlabel('Time (ms)','FontSize',15)
%     ylabel('Microsaccades/s','FontSize',15)
%     xlim([0 400])
%     ylim([-1 7])
       
    end