function result = generateHeatMapSimple( xValues, yValues, varargin )
%Creates 2D Distribution Map of Traces
n_bins = 40;
axisValue = 40;
stimuliSize = 1;
offset = 0;
doubleTarget = 0;
condition = 0;
borders = 1;
rotateIm = 0;
% poiEndName = 'TimeTargetOFF';
% axisWindow = 60;
% filepath = pwd;
% newFileName = 'pptrials';
% conversionFactor = 1; %machine sampling rate conversion factor (ie DPI = 1, dDPI = 1000/330)
% trialId = 1:length(pptrials);

k = 1;
Properties = varargin;
while k <= length(Properties) && ischar(Properties{k})
    switch (Properties{k})
        case 'Bins'
            n_bins =  Properties{k + 1};
            Properties(k:k+1) = [];
        case 'StimulusSize'
            stimuliSize = Properties{k + 1};
            Properties(k:k+1) = [];
        case 'AxisValue'
            axisValue = Properties{k + 1};
            Properties(k:k+1) = [];
        case 'Offset'
            offset = Properties{k + 1};
            Properties(k:k+1) = [];
        case 'DoubleTargets'
            doubleTarget = Properties{k + 1};
            Properties(k:k+1) = [];
        case 'Uncrowded' %(should be 1 for U, 2 for Crowded, 4 = Fixation)
            condition = Properties{k + 1};
            Properties(k:k+1) = [];
        case 'Borders'
            borders = Properties{k + 1};
            Properties(k:k+1) = [];
        case 'Rotate'
            rotateIm = Properties{k + 1};
            Properties(k:k+1) = [];
        otherwise
            k = k + 1;
    end
end

limit.xmin = floor(min(xValues));
limit.xmax = ceil(max(xValues));
limit.ymin = floor(min(yValues));
limit.ymax = ceil(max(yValues));

%  limit.xmin = -60;
%         limit.xmax = 60;
%         limit.ymin = -60;
%         limit.ymax = 60;

result = MyHistogram2(xValues, yValues, [limit.xmin,limit.xmax,n_bins;limit.ymin,limit.ymax,n_bins]);
result = result./(max(max(result)));

if rotateIm ~= 0
    result = imrotate((result),rotateIm);
end

load('./MyColormaps.mat');
mycmap(1,:) = [1 1 1];
set(gcf, 'Colormap', mycmap)
% J = customcolormap([0 0.5 1], {'#000000','#ff0000','#808080'});
% set(gcf, 'Colormap', J)%       colormap(J); 


hold on
result(result==0)=NaN; %%change background to white
temp = pcolor(linspace(limit.xmin, limit.xmax, size(result, 1)),...
    linspace(limit.ymin, limit.ymax, size(result, 1)),...
    result');
% imrotate((temp.CData),90,'bilinear','crop');

% if rotateIm ~= 0
%     direction = [1 0 0];
%     rotate(temp,direction,rotateIm);
%     J = imrotate(temp.CData,rotateIm)
%     imshow(J)
% end
%             p = plot(0,0,'--ks','MarkerSize',16);
%width in arcminutes
if stimuliSize == 0
    centerX = 8;
    centerY = -8;
    width = 16;
    height = 16;
    rectangle('Position',[-centerX+stimuliSize, centerY, width, height],'LineWidth',3,'LineStyle','-')
else
    stimuliSize = stimuliSize/2;
    width = 2 * stimuliSize;
    height = width * 5;
    centerX = (-stimuliSize+offset);
    centerY = (-stimuliSize*5);
    rectangle('Position',[centerX, centerY, width, height],'LineWidth',2)
    
    if doubleTarget
        % if uEcc(numEcc)> 0
        rectangle('Position',[-centerX+stimuliSize, centerY, width, height],'LineWidth',2,'LineStyle','--')
        % end
    end
    
    %     spacing = 1.4;
    %     arcminEcc = uEcc * params.pixelAngle;
    if condition == 2
        
        rectangle('Position',[-(width + (width * 1.4)) + offset, centerY, width, height],'LineWidth',1) %Right
        
        rectangle('Position',[(width * 1.4) + offset, centerY, width, height], 'LineWidth',1) %Left
        
        rectangle('Position',[centerX, -(height + (height * 1.4)), width, height], 'LineWidth',1) % Bottom
        
        rectangle('Position',[centerX, (height * 1.4), width, height], 'LineWidth',1) %Top
        
    end
    if condition == 1 || condition == 2
%         rectangle('Position',[centerX, centerY, width, height],'LineWidth',3)
    end
    if condition == 4
        p = plot(0,0,'--ks','MarkerSize',stimuliSize);
        p(1).LineWidth = 3;
    end
end
% p(1).LineWidth = 3;

set(gca, 'FontSize', 12)
% xlabel('X [arcmin]')
% ylabel('Y [arcmin]')
if borders == 0
    %     set(gca,'XColor', 'none','YColor','none')
    set(gca,'xtick',[], 'ytick', []);
end
% axis tight
axis square
caxis([0 ceil(max(max(result)))])
shading interp;

axis([-axisValue axisValue -axisValue axisValue]);
% hold on
% b = line([-limit.xmin limit.xmax], [0 0],'LineStyle','--');
% c = plot([0 0], [-limit.ymin limit.ymax], '--k');
% uistack(b,'top') 
% uistack(c,'top') 

end

