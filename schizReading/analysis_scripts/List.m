function list = List()

% stimulus duration: stimOff-saccOn.
% this is because the stimulus duration 
% is calculated from the moment the eye lands.

list = eis_readData([], 'x');
list = eis_readData(list, 'y');

list = eis_readData(list, 'trigger', 'blink');
list = eis_readData(list, 'trigger', 'notrack');
        
%----------------------------------------------------------
% user variables
list = eis_readData(list, 'uservar','paragraph');
list = eis_readData(list, 'uservar','quiz');
list = eis_readData(list, 'uservar','splitPar');
list = eis_readData(list, 'uservar','timeImage2OFF');
list = eis_readData(list, 'uservar','hResolution');
list = eis_readData(list, 'uservar','vResolution');
list = eis_readData(list, 'uservar','rRate');
list = eis_readData(list, 'uservar','timeImageOFF');
list = eis_readData(list, 'uservar','timeImage2OFF');
list = eis_readData(list, 'uservar','fixTrial');
list = eis_readData(list, 'uservar','response');
list = eis_readData(list, 'uservar','timeResponded');
list = eis_readData(list, 'uservar','xOffset');
list = eis_readData(list, 'uservar','yOffset');
list = eis_readData(list, 'uservar','subject');



%----------------------------------------------------------
