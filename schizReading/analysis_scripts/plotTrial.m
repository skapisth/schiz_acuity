function plotTrial(xx, yy, par, msS, msE, kill, pxAngle)
% read image
img = imread(sprintf('./stimuli/DDPI/%s.bmp', par));
%size of the image
ySize = size(img,1);
xSize = size(img,2);
% plot image
% figure;imagesc(img, [ySize, xSize]);
% set(gcf,'unit','normalized','outerposition',[0 0 1 1]);
imshow(img)
hold on
%resize x and y
xx = (xx/pxAngle) + (xSize/2);
yy = (yy/pxAngle) + (ySize/2);
% remove no tracks and blinks
% kill = [];
% for j = 1:length(blinkS)
%     kill = [kill blinkS(j):blinkE(j)];
% end
% for j = 1:length(notrackS)
%     kill = [kill notrackS(j):notrackE(j)];
% end
kill = unique(kill);
xx(kill) = nan;
yy(kill) = nan;
xx(isoutlier(xx)) = nan;
yy(isoutlier(yy)) = nan;
plot(xx, yy, 'LineWidth', 1.5, 'color', 'b')
for i = 1:length(msS)
    plot([xx(msS(i)) xx(msE(i))], [yy(msS(i)) yy(msE(i))], 'LineWidth', 3, 'color', 'r')
end
set(gca, 'linewidth', 2, 'fontsize', 20, 'Layer','top')