clear; close all; clc;
msClasses = {'bg', 'cr', 'regIn', 'proIn', 'regBr', 'proBr'};
% get all files in the folder
ls = dir(fullfile('./FilteredTrials/', '*.mat'));
load('./ParList.mat');
mg = cell(1, length(parList));
for i = 1:length(ls)
    load(sprintf('./FilteredTrials/%s', ls(i).name))
    % compile words: in the order of paragraphs, pool all microsaccade
    % classes from all subjects
    for parIdx = 1:length(parList)
        % initiate cell aray for cat
        tmp = ft.words(strcmp(ft.img, parList{parIdx}));
        % if the paragraph was not tested
        if ~isempty(tmp)
            tmp = tmp{1};
        end
        % if no ms was available
        if ~isempty(tmp)
            % remove nans
            tmp(cellfun(@(z) any(isnan(z)), tmp)) = [];
            mg{parIdx} = [mg{parIdx} unique(tmp)];
        end
    end
    ls(i).name
end

%% calculate
for parIdx = 1:length(parList)
    [read(parIdx).words, ~, id] = unique(mg{parIdx});
    read(parIdx).counts = histcounts(id);
    figure('rend','painters','pos',[0 0 1500 1000]);
    % plot paragraph
    ax1 = subplot(2, 1, 1);
    imshow(sprintf('../stimuli/1200/%s.bmp', parList{parIdx}));
    pbaspect(ax1,[2 1 1])
    % plot counts
    ax2 = subplot(2, 1, 2);
    ct = read(parIdx).counts(read(parIdx).counts);
    wd = read(parIdx).words(read(parIdx).counts);
    bar((1:length(ct)), ct, 0.6, 'FaceColor', [0.3010, 0.7450, 0.9330],'EdgeColor', [0.3010, 0.7450, 0.9330])
    ylabel('Counts')
    xticks(1:length(ct))
    xticklabels(wd)
    pbaspect(ax2,[2 1 1])
    set(gca, 'linewidth', 3, 'fontsize', 28)
    xtickangle(30)
    title(sprintf('Paragraph%i', parIdx))
    %saveas(gca, sprintf('./MergeFigs/MsWords/Par%i.png', parIdx));
end