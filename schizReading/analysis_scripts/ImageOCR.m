clear;clc;close all
fig = 1;
list = dir('../stimuli/1200/');
% save char, word and line boundaries for each paragraph
if fig
    figure;
end
for i = 3:length(list)
    I = imread(sprintf('../stimuli/1200/%s', list(i).name));
    fName = strrep(list(i).name, '.bmp', '');
    %run ocr
    rOCR = ocr(I);
    ocrI.wordBox = rOCR.WordBoundingBoxes;
    ocrI.words = rOCR.Words;
    ocrI.charBox = rOCR.CharacterBoundingBoxes;
    % find line boxes
    tmp = ocrI.wordBox(:, 1);
    sIdx = [1; find(diff(tmp) < 0) + 1; length(tmp) + 1];
    for j = 1:length(sIdx) - 1
        % save which line each word belongs to
        ocrI.wordLine(sIdx(j):sIdx(j+1)) = j;
        ocrI.lineBox(j, (1:2)) = ocrI.wordBox(sIdx(j), (1:2));
        % find the end idx of the line
        stopIdx = ocrI.wordBox(sIdx(j+1)-1, 1) ...
            + ocrI.wordBox(sIdx(j+1)-1, 3);
        ocrI.lineBox(j, 3) = stopIdx - ocrI.wordBox(sIdx(j), 1);
        % take the max of height of charaters
        ocrI.lineBox(j, 4) = max(ocrI.wordBox((sIdx(j):(sIdx(j+1)-1)), 4));
    end
    % delete excess entry at the end
    ocrI.wordLine(end) = [];
    % seperate char
    a = num2cell(rOCR.Text);
    if ~exist(sprintf('./OCRResults'), 'dir')
        mkdir(sprintf('./OCRResults'))
    end
%     save(sprintf('./OCRResults/%s.mat', fName), 'rOCR')
    if fig
        objAntt = insertObjectAnnotation(I, 'rectangle', ...
            ocrI.wordBox, ' ', 'color','m');
        imshow(objAntt);
        input ''
        cla
    end
end