clc; clear; %close all;



subjects = {'PC-S04'};
patient = 1;
MaxMSaccAmp = 30;
DDPI = 1;
conversionFac = 1000/340;
samplingRate = 340;

if patient
    pt = 'C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\schizReading\data\patients';
    ptToSave = 'C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\schizReading\processed_pptrials\patients';
    if ~exist(ptToSave,'dir')
        mkdir(ptToSave);
    end
else
    pt = 'C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\schizReading\data\controls';
    ptToSave = 'C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\schizReading\processed_pptrials\controls';
    if ~exist(ptToSave,'dir')
        mkdir(ptToSave);
    end
end

fname1 = strcat(subjects{1},'.mat');
pathToData = fullfile(pt, subjects{1});
data = readdata(pathToData, List);
pptrials = preprocessORI(data, MaxMSaccAmp, samplingRate);
pptrials = osRemoverInEM(pptrials);
pptrials = convertSamplesToMs(pptrials, conversionFac);
save(fullfile(ptToSave, fname1), 'pptrials', 'data');


