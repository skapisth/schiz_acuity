%This text takes the inputs vt{ii}.x.position, vt{ii}.y.position, start times of all MS's, end
%times of all MS's, Pixel-to-Arcmin ration, and the index to which image we're on and then analyzes
%the microsaccade's behavior over the text itself, both in relation to the
%words and the characters.

%Make sure offset is added to x and y positions

function [msClass, inChar, words] = classifyMs(xx, yy, msSVec, msEVec, pxAngle, par, rm) %change this later


%IMG_EXP = 200; %expand outer part of image 500
BOX_EXP = 5; %expand bounding boxes 5
fig = 1;

img = imread(sprintf('./stimuli/DDPI/%s.bmp', par));
load(sprintf('./OCRResults/DDPI/%s', par))

%Size of images
ySize = size(img,1);
xSize = size(img,2);

%Create duaay images
wordMat = zeros(xSize,ySize);
charMat = zeros(xSize,ySize);

%Expand Vertically
% expend word boxes and use all four corners
rszWordBox = [rOCR.WordBoundingBoxes(:,1) ...
    rOCR.WordBoundingBoxes(:,2) - BOX_EXP ...
    (rOCR.WordBoundingBoxes(:,1) + rOCR.WordBoundingBoxes(:,3)) ...
    (rOCR.WordBoundingBoxes(:,2) + rOCR.WordBoundingBoxes(:,4) + BOX_EXP)];
%Coordinate system of boxes around each word. Different numbers/colors
%represent different words.
rszWordBox(rszWordBox<=0) = 1; %eliminate negatives

% project word boundaries onto images
for i = 1:length(rszWordBox)
    wordMat(rszWordBox(i, 1):rszWordBox(i, 3),rszWordBox(i, 2):rszWordBox(i, 4))= i;
end
wordMat = wordMat';
%% Character Matrix

%Matrix of bounding boxes of characters
rszCharBox = [rOCR.CharacterBoundingBoxes(:,1) rOCR.CharacterBoundingBoxes(:,2) - BOX_EXP ...
    (rOCR.CharacterBoundingBoxes(:,1) + rOCR.CharacterBoundingBoxes(:,3)) ...
    (rOCR.CharacterBoundingBoxes(:,2)+rOCR.CharacterBoundingBoxes(:,4)) + BOX_EXP];
% split into characters
Character = num2cell(num2cell(rOCR.Text));
% identify space
space = cell2mat(cellfun(@(z) strcmp(z, ' '), Character, 'UniformOutput', false));
charCont = 1; %lastCharIdx = [];
% 
%Indices to number of characters in each word
for i = 1:length(rOCR.Words)
    if i~=1
        charIdx = charCont:(charCont+length(rOCR.Words{i}) - 1);
        charCont = charIdx(end) + 2; %+2 to skip white spaces after each word
    else
        charIdx = charCont:length(rOCR.Words{i});
        charCont = charIdx(end) + 2; %+2 to skip white spaces after each word
    end
    %spaceIdx = [spaceIdx charIdx(end)+1]; %Index to which characters are spaces
    %lastCharIdx = [lastCharIdx charIdx(end)]; %Index to last character in each word
  
    %Equalize Y-Size of all constituent characters to words
    rszCharBox(charIdx,2) = rszWordBox(i,2);
    rszCharBox(charIdx,4) = rszWordBox(i,4);
end
% spaceIdx(end) = []; %No space after last word

%Create Character Matrix
for i = 1:length(rszCharBox)
    charMat(rszCharBox(i,1):rszCharBox(i,3), rszCharBox(i,2):rszCharBox(i,4)) = i;
end
%Eliminate Spaces
% for i = 1:length(spaceIdx)
%     charMat(rszCharBox(spaceIdx(i),1)+1:rszCharBox(spaceIdx(i),3)-1, rszCharBox(spaceIdx(i),2):rszCharBox(spaceIdx(i),4)) = 0;
% end
charMat(rszCharBox(space, 1):rszCharBox(space, 3), rszCharBox(space, 2):rszCharBox(space, 4)) = 0;
charMat = charMat';
%imagesc(charMat, 'AlphaData', .5)
   
%% Start/End points of microsaccades

% convert eye trace from angle to px
xx = (xx/pxAngle) + (xSize/2);
yy = (yy/pxAngle) + (ySize/2);
        
% start and end pos of ms
xSPos = round(xx(msSVec)); xEPos = round(xx(msEVec)); %Start and end points
ySPos = round(yy(msSVec)); yEPos = round(yy(msEVec)); %Start and end points
% change to 1 if 0
xSPos(xSPos<=0)=1; xEPos(xEPos<=0)=1;
ySPos(ySPos<=0)=1; yEPos(yEPos<=0)=1;

% plot xx and yy
xx(rm) = nan;
yy(rm) = nan;
if fig
    figure()
    imshow(img); hold on 
    %imagesc(charMat, 'AlphaData', .5);
    plot(xx, yy, 'Color', [0.3010, 0.7450, 0.9330 0.6], 'LineWidth', 2)
    set(gcf,'unit','normalized','outerposition',[0 0 1 1]);
end
% 1 = Background MS
% 2 = Corrective MS
% 3 = Regressive Within     4 = Progressive Within
% 5 = Regressive Branch     6 = Progressive Branch

wd = wordMat;
cr = charMat;
msClass = [];
inChar = [];
words = [];

% dicard ms out of bounds
kill = ySPos > (ySize) | xSPos>(xSize) | ... 
    yEPos>(ySize) | xEPos> (xSize);
if sum(kill) > 0
    ySPos(kill) = [];
    xSPos(kill) = [];
    yEPos(kill) = [];
    xEPos(kill) = [];
end

 %Loop through each MS
for j = 1:length(xSPos)
    % background
    if wd(yEPos(j),xEPos(j)) == 0
        fprintf('background \n')
        msClass = [msClass 1]; 
        words = [words {nan}];
        plot([xSPos(j) xEPos(j)], [ySPos(j) yEPos(j)], 'b', 'LineWidth', 2);
    %CORRECTIVE MICROSACCADES
    elseif  (wd(ySPos(j),xSPos(j)) == 0 && wd(yEPos(j),xEPos(j)) ~= 0) 
        words = [words rOCR.Words(wd(yEPos(j),xEPos(j)))];
        msClass = [msClass 2];
        fprintf('corrective: %s \n', cell2mat(rOCR.Words(wd(yEPos(j),xEPos(j)))))
        plot([xSPos(j) xEPos(j)], [ySPos(j) yEPos(j)], 'm', 'LineWidth', 2);
    elseif  (wd(ySPos(j),xSPos(j)) == wd(yEPos(j),xEPos(j))) && wd(ySPos(j),xSPos(j)) ~= 0 %WITHIN MICROSACCADES
        %% Characters spanned by Within MS
        %Adjusts for microsaccade landing in space between characters
        XEtmp = xEPos(j);  DUMMY1 = XEtmp;   DUMMY2 = XEtmp;  %tmp vars so we don't change actual data
        XStmp = xSPos(j);  DUMMY3 = XStmp;   DUMMY4 = XStmp;
        SKIP = 1:20; CNTR = 1; 
        %Adjust if end lands in the space between characters
        while cr(yEPos(j),XEtmp) == 0 
            if length(SKIP)<CNTR
                warning('Adjustment for spaces between characters exceeded 10 pixels. Code likely not working.');
            end
            if mod(SKIP(CNTR),2) == 0 %every other loop iteration, change
                DUMMY1 = DUMMY1+1; %add one
                XEtmp = DUMMY1;
            else
                DUMMY2 = DUMMY2-1; %minus 1
                XEtmp = DUMMY2;
            end
            CNTR = CNTR+1;
        end
        %Adjust if start lands in space between characters
        while cr(ySPos(j),XStmp) == 0 
            if length(SKIP) < CNTR
                warning('Adjustment for spaces between characters exceeds 10 pixels. Code likely not working.');
            end
            
            if mod(SKIP(CNTR),2) == 0
                DUMMY3 = DUMMY3 + 1;
                XStmp = DUMMY3;
            else
                DUMMY4 = DUMMY4 +1;
                XStmp = DUMMY4;
            end
            CNTR = CNTR + 1;
        end  
        %negative for regressive, positive for progressive
        inChar = [inChar (cr(yEPos(j),XEtmp)) - (cr(ySPos(j),XStmp))]; 
        if cr(ySPos(j),XStmp) > cr(yEPos(j),XEtmp)
            msClass = [msClass 3]; %REGRESSIVE
            words = [words rOCR.Words(wd(yEPos(j), xEPos(j)))];
            fprintf('regressive within: %s \n', cell2mat(rOCR.Words(wd(yEPos(j), xEPos(j)))))
            plot([xSPos(j) xEPos(j)], [ySPos(j) yEPos(j)], 'r', 'LineWidth', 2);
        elseif cr(ySPos(j),XStmp) < cr(yEPos(j),XEtmp)
            msClass = [msClass 4]; %PROGRESSIVE
            words = [words rOCR.Words(wd(yEPos(j), xEPos(j)))];
            fprintf('progressive within: %s \n', cell2mat(rOCR.Words(wd(yEPos(j), xEPos(j)))))
            plot([xSPos(j) xEPos(j)], [ySPos(j) yEPos(j)], 'r', 'LineWidth', 2);
        end
    elseif (wd(ySPos(j),xSPos(j)) ~= 0 && wd(yEPos(j),xEPos(j)) ~=0 ) && (wd(ySPos(j),xSPos(j)) ~= wd(yEPos(j),xEPos(j))) %BRANCHING MICROSACCADES
        if (wd(ySPos(j),xSPos(j)) > wd(yEPos(j),xEPos(j)))
            msClass = [msClass 5]; %REGRESSIVE
            % words  = [words strcat(rOCR.Words(wd(ySPos(j),xSPos(j))),'_',rOCR.Words(wd(yEPos(j),xEPos(j))))];
            words  = [words rOCR.Words(wd(yEPos(j),xEPos(j)))];
            fprintf('regressive branch \n')
            plot([xSPos(j) xEPos(j)], [ySPos(j) yEPos(j)], 'r', 'LineWidth', 2);
        elseif (wd(ySPos(j),xSPos(j)) < wd(yEPos(j),xEPos(j)))
            msClass = [msClass 6]; %PROGRESSIVE
            % words  = [words strcat(rOCR.Words(wd(ySPos(j),xSPos(j))),'_',rOCR.Words(wd(yEPos(j),xEPos(j))))];
            words  = [words rOCR.Words(wd(yEPos(j),xEPos(j)))];
            plot([xSPos(j) xEPos(j)], [ySPos(j) yEPos(j)], 'r', 'LineWidth', 2);
        end 
    end
end

%words = unique(words);
