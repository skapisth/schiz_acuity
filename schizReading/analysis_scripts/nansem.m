% Standard error of the mean
function [semval] = nansem(varargin)
vec = varargin{1};
try
    dim = varargin{2};
catch
    dim = 1;
end
% Recall that s.e.m. = std(x)/sqrt(length(x));
nonan_std = nanstd(vec, 0, dim);
sizeVec = size(vec);
if sizeVec(1) == 1
vec(isnan(vec)) = [];
elseif sizeVec(1) > 1
    vec(all(isnan(vec),2),:) = [];
end
nonan_len = size(vec, dim);
% Plug in values
semval = nonan_std / sqrt(nonan_len);

