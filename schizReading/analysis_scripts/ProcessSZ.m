%This text takes the inputs vt{ii}.x.position, vt{ii}.y.position, start times of all MS's, end
%times of all MS's, Pixel-to-Arcmin ration, and the index to which image we're on and then analyzes
%the microsaccade's behavior over the text itself, both in relation to the
%words and the characters.

%Make sure offset is added to x and y positions
clear; clc; close all;
% only variable needs to be changed
patient = false; % if patient or not
%--------------------------------------
% set variables
pxAngle = 0.5305;
reOrg = true;
switch patient
    case true
        fPath = 'C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\schizReading\processed_pptrials\patients';
    case false
        fPath = 'C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\schizReading\processed_pptrials\controls';
end

% get all files in the folder
ls = dir(fullfile(fPath, '*.mat'));
for i = 1:length(ls)
    load(sprintf('%s/%s', fPath, ls(i).name))
    vt = pptrials;
    fprintf(sprintf('analyzing %s\n', ls(i).name))
    % get subject's name
    subject = extractBefore(ls(i).name,'.mat');
    fprintf(sprintf('analyzing %s\n', subject))
    if ~exist(sprintf('./IndvFigs/%s', subject), 'dir')
        mkdir(sprintf('./IndvFigs/%s', subject))
    end
    % reorganize vts for ddpi data because of paragraph difference
    switch reOrg
        case true
            if ~strcmp(vt{7}.paragraph, 'Par_4_1')
                fprintf('Reorganizing vts \n')
                tmp = {vt{1:7}, vt{7}, vt{8:14}, vt{14}, vt{15:end}};
                tmp{7}.paragraph = 'Par_4_1'; tmp{8}.paragraph = 'Par_4_2';
                tmp{14}.paragraph = 'Par_7_1'; tmp{15}.paragraph = 'Par_7_2';
                vt = tmp; clear tmp;
                %save(sprintf('./ValidTrials/%s.mat', subject), 'vt')
            end
    end
    % run through all data
    ftIdx = 0;
    for all_ts = 1:length(vt)
        flag_ids(all_ts) = vt{all_ts}.fixTrial;
    end
    task_ids = find(flag_ids == 0);
    for j = task_ids%1:length(vt)
      
        ftIdx = ftIdx + 1;
        if strcmp(vt{j}.paragraph, 'Par_4_2')|| strcmp(vt{j}.paragraph, 'Par_7_2')
            % find index of interest in between two images
            saccIdx = (ddpiTime(vt{j}.timeImageOFF) < vt{j}.saccades.startSample) ...
                & (vt{j}.saccades.startSample < ddpiTime(vt{j}.timeImage2OFF));
            fixIdx = (ddpiTime(vt{j}.timeImageOFF) < vt{j}.fixations.startSample) ...
                & (vt{j}.fixations.startSample < ddpiTime(vt{j}.timeImage2OFF));
            msIdx = (ddpiTime(vt{j}.timeImageOFF) < vt{j}.microsaccades.startSample) ...
                & (vt{j}.microsaccades.startSample < ddpiTime(vt{j}.timeImage2OFF));
            blinkIdx = (ddpiTime(vt{j}.timeImageOFF) < vt{j}.blinks.startSample) ...
                & (vt{j}.blinks.startSample < ddpiTime(vt{j}.timeImage2OFF));
            notrackIdx = (ddpiTime(vt{j}.timeImageOFF) < vt{j}.notracks.startSample) ...
                & (vt{j}.notracks.startSample < ddpiTime(vt{j}.timeImage2OFF));
            % index into the actual time stamps and align them to the
            % beginning
            ft.saccS{ftIdx} = vt{j}.saccades.startSample(saccIdx) - ddpiTime(vt{j}.timeImageOFF) + 1;
            ft.saccE{ftIdx} = ft.saccS{ftIdx} + vt{j}.saccades.durationSample(saccIdx);
            ft.msS{ftIdx} = vt{j}.microsaccades.startSample(msIdx) - ddpiTime(vt{j}.timeImageOFF) + 1;
            ft.msE{ftIdx} = ft.msS{ftIdx} + vt{j}.microsaccades.durationSample(msIdx);
            ft.fixS{ftIdx} = vt{j}.fixations.startSample(fixIdx) - ddpiTime(vt{j}.timeImageOFF) + 1;
            ft.fixE{ftIdx} = ft.fixS{ftIdx} + vt{j}.fixations.durationSample(fixIdx);
            ft.xPos{ftIdx} = vt{j}.x.position(ddpiTime(vt{j}.timeImageOFF):ddpiTime(vt{j}.timeImage2OFF)) + vt{j}.xOffset * pxAngle;
            ft.yPos{ftIdx} = vt{j}.y.position(ddpiTime(vt{j}.timeImageOFF):ddpiTime(vt{j}.timeImage2OFF)) + vt{j}.yOffset * pxAngle;
            ft.readTrial{ftIdx} = 1;
        else %%%%%% START HERE
            saccIdx = vt{j}.saccades.startSample < ddpiTime(vt{j}.timeImageOFF);
            fixIdx = vt{j}.fixations.startSample < ddpiTime(vt{j}.timeImageOFF);
            msIdx = vt{j}.microsaccades.startSample < ddpiTime(vt{j}.timeImageOFF);
            blinkIdx = vt{j}.blinks.startSample < ddpiTime(vt{j}.timeImageOFF);
            notrackIdx = vt{j}.notracks.startSample < ddpiTime(vt{j}.timeImageOFF);
            ft.xPos{ftIdx} = vt{j}.x.position(1:ddpiTime(vt{j}.timeImageOFF)) + vt{j}.xOffset * pxAngle;
            ft.yPos{ftIdx} = vt{j}.y.position(1:ddpiTime(vt{j}.timeImageOFF)) + vt{j}.yOffset * pxAngle;
            ft.saccS{ftIdx} = vt{j}.saccades.startSample(saccIdx);
            ft.saccE{ftIdx} = vt{j}.saccades.startSample(saccIdx) + vt{j}.saccades.durationSample(saccIdx);
            % save blinks
            ft.blinkS{ftIdx} = vt{j}.blinks.startSample(blinkIdx);
            ft.blinkE{ftIdx} = vt{j}.blinks.startSample(blinkIdx) + vt{j}.blinks.durationSample(blinkIdx);
            % save no tracks
            ft.notrackS{ftIdx} = vt{j}.notracks.startSample(notrackIdx);
            ft.notrackE{ftIdx} = vt{j}.notracks.startSample(notrackIdx) + vt{j}.notracks.durationSample(notrackIdx);
            % save microsaccades
            ft.msS{ftIdx} = vt{j}.microsaccades.startSample(msIdx);
            ft.msE{ftIdx} = vt{j}.microsaccades.startSample(msIdx) + vt{j}.microsaccades.durationSample(msIdx);
            ft.readTrial{ftIdx} = 1;
            
        end

        % calculate sacc direction
        if isempty(ft.saccS{ftIdx})
            continue;
        end
        if (length(ft.yPos{ftIdx})<ft.saccS{ftIdx}(end) || length(ft.yPos{ftIdx})<ft.saccE{ftIdx}(end)...
                || length(ft.xPos{ftIdx})<ft.saccS{ftIdx}(end) || length(ft.xPos{ftIdx})<ft.saccE{ftIdx}(end))
            ft.saccS{ftIdx}(end) = [];
            ft.saccE{ftIdx}(end) = [];
        end
        saccYS = ft.yPos{ftIdx}(ft.saccS{ftIdx});
        saccYE = ft.yPos{ftIdx}(ft.saccE{ftIdx});
        saccXS = ft.xPos{ftIdx}(ft.saccS{ftIdx});
        saccXE = ft.xPos{ftIdx}(ft.saccE{ftIdx});
        ft.saccAng{ftIdx} = rad2deg(atan2((saccYE - saccYS),(saccXE - saccXS)));
        ft.saccDir{ftIdx} = deg2dir(ft.saccAng{ftIdx});
        
        ft.saccAmp{ftIdx} = vt{j}.saccades.amplitude(saccIdx);
        ft.saccRate(ftIdx) = (length(ft.saccS{ftIdx}))/(sum([vt{j}.fixations.duration(fixIdx) ...
            vt{j}.saccades.duration(saccIdx) vt{j}.blinks.duration(blinkIdx) vt{j}.microsaccades.duration(msIdx)]) / 1000);
        ft.saccTotal(ftIdx) = sum(saccIdx);
       
        if isempty(ft.msS{ftIdx})
            continue
        end
        % calculate ms direction
        if (length(ft.yPos{ftIdx})<ft.msS{ftIdx}(end) || length(ft.yPos{ftIdx})<ft.msE{ftIdx}(end)...
                || length(ft.xPos{ftIdx})<ft.msS{ftIdx}(end) || length(ft.xPos{ftIdx})<ft.msE{ftIdx}(end))
            ft.msS{ftIdx}(end) = [];
            ft.msE{ftIdx}(end) = [];
        end
        msYS = ft.yPos{ftIdx}(ft.msS{ftIdx});
        msYE = ft.yPos{ftIdx}(ft.msE{ftIdx});
        msXS = ft.xPos{ftIdx}(ft.msS{ftIdx});
        msXE = ft.xPos{ftIdx}(ft.msE{ftIdx});
        ft.msAng{ftIdx} = rad2deg(atan2((msYE - msYS),(msXE - msXS)));
        ft.msDir{ftIdx} = deg2dir(ft.msAng{ftIdx});
        
        ft.msAmp{ftIdx} = vt{j}.microsaccades.amplitude(msIdx);
        ft.msRate(ftIdx) = (length(ft.msS{ftIdx}))/(sum([vt{j}.fixations.duration(fixIdx) ...
            vt{j}.saccades.duration(saccIdx) vt{j}.blinks.duration(blinkIdx) vt{j}.microsaccades.duration(msIdx)]) / 1000);
        ft.msTotal(ftIdx) = sum(msIdx);
        %calculate proprtion of sacc directions in each trial
        for dirIdx = 1:4
            ft.saccDirProp{ftIdx}(dirIdx) = sum(ft.saccDir{ftIdx} == dirIdx)/ft.saccTotal(ftIdx);
            ft.msDirProp{ftIdx}(dirIdx) = sum(ft.msDir{ftIdx} == dirIdx)/ft.msTotal(ftIdx);
        end
        
        % save drift information
        ft.driftDur{ftIdx} = vt{j}.drifts.duration;
        % 0 - easy, 1 - hard
        if cell2mat(ft.readTrial(ftIdx)) && ~isempty(vt{j}.analysis.duration)
            ft.img{ftIdx} = vt{j}.paragraph;
            % concatenate all blinks and no tracks
            kill = [];
            if ftIdx <= length(ft.blinkS) 
                for k = 1:length(ft.blinkS{ftIdx})
                    kill = [kill ft.blinkS{ftIdx}(k):ft.blinkE{ftIdx}(k)];
                end
            end
            if ftIdx <= length(ft.notrackS)
                for k = 1:length(ft.notrackS{ftIdx})
                    kill = [kill ft.notrackS{ftIdx}(k):ft.notrackE{ftIdx}(k)];
                end
            end
            % plot entire eyetrace
            %plotTrial(ft.xPos{j}, ft.yPos{j}, ft.img{j}, ft.msS{j}, ft.msE{j}, kill, pxAngle)
            % interpret ms trials from reading task
            [ft.msClass{ftIdx}, ft.inChar{ftIdx}, ft.words{ftIdx}] = classifyMs(ft.xPos{ftIdx}, ft.yPos{ftIdx}, ft.msS{ftIdx}, ft.msE{ftIdx}, pxAngle, ft.img{ftIdx}, kill);
%             saveas(gca, sprintf('./IndvFigs/%s/%s.png', subject, ft.img{ftIdx}));
%             commandwindow
%             input ''
%             close
        else
            ft.img{j} = nan; ft.msClass{j} = nan; ft.inChar{j} = nan; ft.words{j} = nan;
        end
    end
    ft.patient = patient;
    ft.pxAngle = pxAngle;
    if ~exist(sprintf('./FilteredTrials'), 'dir')
        mkdir(sprintf('./FilteredTrials'))
    end
    save(sprintf('./FilteredTrials/%s.mat',subject), 'ft')
end
