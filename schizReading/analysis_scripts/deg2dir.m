% input an array of degrees and convert them to cardinal directions: 1 -
% left, 2 - up, 3 - right, 4 - down

function direction = deg2dir(degree)
direction = zeros(1, length(degree));
direction(degree >= 135 | degree < -135) = 1;    %left
direction(degree < 135 & degree >= 45)= 2;      %up
direction(degree < 45 & degree >= -45) = 3;      %right
direction(degree < -45 & degree >= -135) = 4;    %down
