%Make sure offset is added to x and y positions
clear; clc; close all;
read = 1;
ppl = {'patients', 'controls'};
ls = dir(fullfile('FilteredTrials', '*.mat'));

msClasses = {'bg', 'cr', 'regIn', 'proIn', 'regBr', 'proBr'};
vars = {'saccAmp', 'saccRate', 'msAmp', 'msRate', 'driftDur','msClass'};
for i = 1:length(ls)
    load(sprintf('./FilteredTrials/%s', ls(i).name))
    for j = 1:length(vars)
        if contains(vars{j}, 'Amp') || contains(vars{j}, 'Dur') || contains(vars{j}, 'Class')
            tmp = cellfun(@(z) z, ft.(vars{j})(cell2mat(ft.readTrial) == read), 'UniformOutput', false);
            tmp = cell2mat(tmp);
        else
            tmp = ft.(vars{j})(cell2mat(ft.readTrial) == read);
        end
        if j == 6
            merge_ms.total(i) = length(tmp);
            merge_ms.ms_bg(i) = length(find(tmp == 1));
            merge_ms.ms_corr(i) = length(find(tmp == 2));
            merge_ms.ms_reg_in(i) = length(find(tmp == 3));
            merge_ms.ms_prog_in(i) = length(find(tmp == 4));
            merge_ms.ms_reg_brnch(i) = length(find(tmp == 5));
            merge_ms.ms_prog_brnch(i) = length(find(tmp == 6));
            merge_ms.patient(i) = ft.patient;
        else
            tmp_mn = mean(tmp);
            tmp_sem = nansem(tmp, 2);
            merge.(sprintf('avg_%s', vars{j}))(i) = tmp_mn;
            merge.(sprintf('sem_%s', vars{j}))(i) = tmp_sem;
        end
    end
    merge.patient(i) = ft.patient;
end
cP = jet(2);
cC = winter(2);


for j = 1:length(vars)
    count_p = 0;
    count_c = 0;
    figure(j)
    hold on
    figure('rend','painters','pos',[0 0 600 700]);
    hold on
    [val_p,val_c] = deal([]);
    if strcmp(vars{j},'msClass')
        [ms_bg_P,ms_corr_P,ms_reg_in_P,ms_prog_in_P,ms_reg_brnch_P,ms_prog_brnch_P] = deal([]);
        [ms_bg_C,ms_corr_C,ms_reg_in_C,ms_prog_in_C,ms_reg_brnch_C,ms_prog_brnch_C] = deal([]);
        for all = 1:length(merge.patient)
            if merge_ms.patient(all)
                ms_bg_P(1,end+1) = merge_ms.ms_bg(all)/merge_ms.total(all);
                ms_corr_P(1,end+1) = merge_ms.ms_corr(all)/merge_ms.total(all);
                ms_reg_in_P(1,end+1) = merge_ms.ms_reg_in(all)/merge_ms.total(all);
                ms_prog_in_P(1,end+1) = merge_ms.ms_prog_in(all)/merge_ms.total(all);
                ms_reg_brnch_P(1,end+1) = merge_ms.ms_reg_brnch(all)/merge_ms.total(all);
                ms_prog_brnch_P(1,end+1) = merge_ms.ms_prog_brnch(all)/merge_ms.total(all);
                plot([1,2,3,4,5,6],[ms_bg_P(end),ms_corr_P(end),ms_reg_in_P(end),...
                    ms_prog_in_P(end),ms_reg_brnch_P(end),ms_prog_brnch_P(end)],...
                    'o','MarkerSize',10,'MarkerFaceColor','r')
                hold on
            else
                ms_bg_C(1,end+1) = merge_ms.ms_bg(all)/merge_ms.total(all);
                ms_corr_C(1,end+1) = merge_ms.ms_corr(all)/merge_ms.total(all);
                ms_reg_in_C(1,end+1) = merge_ms.ms_reg_in(all)/merge_ms.total(all);
                ms_prog_in_C(1,end+1) = merge_ms.ms_prog_in(all)/merge_ms.total(all);
                ms_reg_brnch_C(1,end+1) = merge_ms.ms_reg_brnch(all)/merge_ms.total(all);
                ms_prog_brnch_C(1,end+1) = merge_ms.ms_prog_brnch(all)/merge_ms.total(all);
                plot([1,2,3,4,5,6],[ms_bg_C(end),ms_corr_C(end),ms_reg_in_C(end),...
                    ms_prog_in_C(end),ms_reg_brnch_C(end),ms_prog_brnch_C(end)],...
                    'o','MarkerSize',10,'MarkerFaceColor','b')
                hold on
            end
        end
        sem_bg_P = std(ms_bg_P)/sqrt(length(ms_bg_P));
        sem_corr_P = std(ms_corr_P)/sqrt(length(ms_corr_P));
        sem_reg_in_P = std(ms_reg_in_P)/sqrt(length(ms_reg_in_P));
        sem_prog_in_P = std(ms_prog_in_P)/sqrt(length(ms_prog_in_P));
        sem_reg_brnch_P = std(ms_reg_brnch_P)/sqrt(length(ms_reg_brnch_P));
        sem_prog_brnch_P = std(ms_prog_brnch_P)/sqrt(length(ms_prog_brnch_P));
        
        sem_bg_C = std(ms_bg_C)/sqrt(length(ms_bg_C));
        sem_corr_C = std(ms_corr_C)/sqrt(length(ms_corr_C));
        sem_reg_in_C = std(ms_reg_in_C)/sqrt(length(ms_reg_in_C));
        sem_prog_in_C = std(ms_prog_in_C)/sqrt(length(ms_prog_in_C));
        sem_reg_brnch_C = std(ms_reg_brnch_C)/sqrt(length(ms_reg_brnch_C));
        sem_prog_brnch_C = std(ms_prog_brnch_C)/sqrt(length(ms_prog_brnch_C));
        
        errorbar([mean(ms_bg_P) mean(ms_corr_P)...
                  mean(ms_reg_in_P) mean(ms_prog_in_P)...
                  mean(ms_reg_brnch_P) mean(ms_prog_brnch_P)],...
                  [sem_bg_P sem_corr_P...
                  sem_reg_in_P sem_prog_in_P...
                  sem_reg_brnch_P sem_prog_brnch_P],...
            'o', 'Color', 'r', 'LineStyle', '-','LineWidth',3)
        set(gca, 'XTick', [1:1:6], 'XTickLabel', {'BG'; 'Corr ms'; 'Reg in'; 'Prog in'; 'Reg Branch';'Prog Branch'}, 'FontSize', 12);
        hold on
        errorbar([mean(ms_bg_C) mean(ms_corr_C)...
                  mean(ms_reg_in_C) mean(ms_prog_in_C)...
                  mean(ms_reg_brnch_C) mean(ms_prog_brnch_C)],...
                  [sem_bg_C sem_corr_C...
                  sem_reg_in_C sem_prog_in_C...
                  sem_reg_brnch_C sem_prog_brnch_C],...
            'o', 'Color', 'b', 'LineStyle', '-','LineWidth',3)
        set(gca, 'XTick', [1:1:6], 'XTickLabel', {'BG'; 'Corr ms'; 'Reg in'; 'Prog in'; 'Reg Branch';'Prog Branch'}, 'FontSize', 12);
        xlim([0 7])
    else
        for all = 1:length(merge.patient)
            if merge.patient(all)
                count_p = count_p+1;
                val_p(1,end+1) = merge.(sprintf('avg_%s', vars{j}))(all);
                plot([1],merge.(sprintf('avg_%s', vars{j}))(all),'o','MarkerSize',10,'MarkerFaceColor',cP(count_p,:))
                hold on
            else
                count_c = count_c+1;
                val_c(1,end+1) = merge.(sprintf('avg_%s', vars{j}))(all);
                plot([2],merge.(sprintf('avg_%s', vars{j}))(all),'o','MarkerSize',10,'MarkerFaceColor',cC(count_c,:))
                hold on
            end
        end
        val_sem_p = std(val_p)/sqrt(length(val_p));
        val_sem_c = std(val_c)/sqrt(length(val_c));
        errorbar([mean(val_p) mean(val_c)], [val_sem_p val_sem_c], ...
            'o', 'Color', 'k', 'LineStyle', '-','LineWidth',3)
        set(gca, 'XTick', [1:1:2], 'XTickLabel', {'Patients'; 'Controls'}, 'FontSize', 12);

        xlim([0 3])
    %     xtickangle(45)
        ylabel(vars{j})
    %     switch read
    %         case 0
    %             title('Fix Task')
    %         case 1
    %             title('Read Task')
    %     end
        set(gca, 'linewidth', 3, 'fontsize', 32)
        switch read
            case true
                if ~exist(sprintf('./MergeFigs/Read'), 'dir')
                    mkdir(sprintf('./MergeFigs/Read'))
                end
                saveas(gca, sprintf('./MergeFigs/Read/%s.png', vars{j}));
            case false
                if ~exist(sprintf('./MergeFigs/Fix'), 'dir')
                    mkdir(sprintf('./MergeFigs/Fix'))
                end
                saveas(gca, sprintf('./MergeFigs/Fix/%s.png', vars{j}));
                saveas(gca, sprintf('./MergeFigs/Fix/%s.eps', vars{j}));
        end
    end
end

% % plotting
% for j = 1:length(vars)
%     for k = 1:length(ppl)
%         summary.(sprintf('avg_%s', vars{j}))(:, 1)
%     end
% end
