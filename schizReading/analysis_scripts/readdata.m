
function data = readdata(pt,list)

% LIST is the list of variables to import for 
% the specific experiment.

fname = [pt,'/results.mat'];
data = eis_eisdir2mat(pt, list, fname);
save(fname, 'data');
 