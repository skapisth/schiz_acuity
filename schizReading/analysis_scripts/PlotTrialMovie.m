clc; clear; close all 

Name = 'PC-S01';
FIGURE_ON = 1;
DYNAMIC_PLOT = 1;
MOVIE_ON = 1;
CHECK_MS_DIR = 1; % to double check the direction classification of ms
PlotRawData = 0; % set at 1 to see the raw traces (with overshoots)
load(sprintf('../processed_pptrials/patients/%s',Name));
addition = 114; % for image plotting
% define the pixel to arcmin factor based on the screen resolution
PxArcmin = 0.5305;

MsRate = [];
Type = [];
MSLeftRate = [];
MSUpRate = [];
MSRigthRate = [];
MSDownRate = [];

for ii = 1:length(pptrials)
    
    % plot only the reading trials and skip the fixation trial
    if ~pptrials{ii}.fixTrial
        TotalTime = (sum([pptrials{ii}.fixations.durationSample ...
            pptrials{ii}.saccades.durationSample pptrials{ii}.blinks.durationSample pptrials{ii}.microsaccades.durationSample]));
        MsRate = [MsRate (length(pptrials{ii}.microsaccades.durationSample))/(TotalTime/ 1000)];
        % easy = 0 hard = 1
        
        
        MSEnd = pptrials{ii}.microsaccades.startSample + pptrials{ii}.microsaccades.durationSample -1 ;
        MSStart = pptrials{ii}.microsaccades.startSample;
        
        YmsS = pptrials{ii}.y.position(MSStart);
        YmsE = pptrials{ii}.y.position(MSEnd);
        XmsS = pptrials{ii}.x.position(MSStart);
        XmsE = pptrials{ii}.x.position(MSEnd);
        
        MSAngle = rad2deg(atan2((YmsE-YmsS),(XmsE-XmsS)));
        
        MSDirection = zeros(length(MSAngle),1)';
        MSDirection(find(MSAngle >= 135 | MSAngle < -135)) = 1; %left
        MSDirection(find(MSAngle < 135 & MSAngle >= 45)) = 2; %up
        MSDirection(find(MSAngle < 45 & MSAngle >= -45)) = 3; %right
        MSDirection(find(MSAngle < -45 & MSAngle >= -135)) = 4; %down
        
        %amount of MS in each direction
        MSLeftRate = [MSLeftRate length(find(MSDirection == 1))/(TotalTime/1000)];
        MSUpRate = [MSUpRate length(find(MSDirection == 2))/(TotalTime/1000)];
        MSRightRate = [MSRigthRate length(find(MSDirection == 3))/(TotalTime/1000)];
        MSDownRate = [MSDownRate length(find(MSDirection == 4))/(TotalTime/1000)];
        

        if CHECK_MS_DIR
            for jj = 1:length(pptrials{ii}.microsaccades.startSample)
                S = pptrials{ii}.microsaccades.startSample(jj);
                E = S + pptrials{ii}.microsaccades.durationSample(jj) - 1;
                
                plot(0,0, 'r*','LineWidth', 3)
                hold on
                plot([0 pptrials{ii}.x.position(E)-pptrials{ii}.x.position(S)],[0 pptrials{ii}.y.position(E)-pptrials{ii}.y.position(S)],...
                    'r','LineWidth', 3)
                title(sprintf('ms direction = %d (1 left 2 up 3 rigth 4 down)', MSDirection(jj)))
                axis([-30 30 -30 30])
                axis square
                input ''
                cla
            end
        end
        
        if FIGURE_ON
            %Load the image for the paragraph corresponding to the trial we are on (ii).
            figure('Position', [100, 100, 1200, 500]);
            Im = imread(sprintf('../stimuli/1200/%s.bmp', pptrials{ii}.paragraph));
            Im = Im(:,:,1);
            ImageSizey = size(Im,1);
            ImageSizex = size(Im,2);
            Im = flipud(Im);
            imagesc([-ImageSizex/2 ImageSizex/2], [-ImageSizey/2 ImageSizey/2],Im)
            axis('xy')
            colormap gray
            hold on
        
            xx = (pptrials{ii}.x.position(:)/PxArcmin) + pptrials{ii}.xOffset; %OFFSET is already in px
            yy = (pptrials{ii}.y.position(:)/PxArcmin) + pptrials{ii}.yOffset;
            
            if DYNAMIC_PLOT
                if MOVIE_ON
                    v = VideoWriter('example.avi');
                    v.FrameRate = 5;
                    v.Quality = 10;
                    open(v)
                end
                axis off
                xx_c = xx*NaN;
                for jj = 1:length(pptrials{ii}.microsaccades.startSample)
                    xx_c(pptrials{ii}.microsaccades.startSample(jj):pptrials{ii}.microsaccades.startSample(jj)+pptrials{ii}.microsaccades.durationSample(jj)-1) = 1;
                    xx(pptrials{ii}.microsaccades.startSample(jj):pptrials{ii}.microsaccades.startSample(jj)+pptrials{ii}.microsaccades.durationSample(jj)-2) ...
                        = xx(pptrials{ii}.microsaccades.startSample(jj));
                    yy(pptrials{ii}.microsaccades.startSample(jj):pptrials{ii}.microsaccades.startSample(jj)+pptrials{ii}.microsaccades.durationSample(jj)-2) ...
                        = yy(pptrials{ii}.microsaccades.startSample(jj));
                end
                for jj = 1:length(pptrials{ii}.saccades.startSample)
                    xx_c(pptrials{ii}.saccades.startSample(jj):pptrials{ii}.saccades.startSample(jj)+pptrials{ii}.saccades.durationSample(jj)-1) = 2;
                    xx(pptrials{ii}.saccades.startSample(jj):pptrials{ii}.saccades.startSample(jj)+pptrials{ii}.saccades.durationSample(jj)-2) = xx(pptrials{ii}.saccades.startSample(jj));
                    yy(pptrials{ii}.saccades.startSample(jj):pptrials{ii}.saccades.startSample(jj)+pptrials{ii}.saccades.durationSample(jj)-2) = yy(pptrials{ii}.saccades.startSample(jj));
                end
                for jj = 1:length(pptrials{ii}.drifts.startSample)
                    xx_c(pptrials{ii}.drifts.startSample(jj):pptrials{ii}.drifts.startSample(jj)+pptrials{ii}.drifts.durationSample(jj)-1) = 3;
                end
                Step = 50;
                cont = 0;
                for kk = 1:Step:length(xx)-Step
                    cont = cont + 1;
                    if (xx_c(kk)==3)
                        plot(xx(kk:kk+Step),yy(kk:kk+Step), 'r')
                    elseif (xx_c(kk)==2)
                        plot(xx(kk:kk+Step),yy(kk:kk+Step), 'b')
                    elseif (xx_c(kk)==1)
                        plot(xx(kk:kk+Step),yy(kk:kk+Step), 'm', 'LineWidth',3)
                    end
                    if cont == 10
                        frame = getframe;
                        writeVideo(v,frame);
                        cont = 0;
                    end
                end
                pptrials{ii}.discard = input('Discard?');
                close(v);
                input ''
                
                
                close all
                
                
            end
            
            
        else
            
            
            
            %
            %             %Plot drifts
            %             for jj = 1:length(vt{ii}.drifts.startSample);
            %                 S = vt{ii}.drifts.startSample(jj);
            %                 E = S + vt{ii}.drifts.durationSample(jj) - 1;
            %                 if ~PlotRawData
            %                     M_D = plot([xx(S) xx(E)],[yy(S) yy(E)], 'r', 'LineWidth', 1);
            %                 else
            %                     M_D = plot(xx(S:E),yy(S:E), 'r', 'LineWidth', 1);
            %                 end
            %             end
            
            
            
            %Plot microsaccades
            for jj = 1:length(vt{ii}.microsaccades.startSample);
                S = vt{ii}.microsaccades.startSample(jj);
                E = S + vt{ii}.microsaccades.durationSample(jj) - 1;
                if ~PlotRawData
                    plot(xx(E),yy(E), '^m', 'MarkerFaceColor', 'm','LineWidth', 2);
                    M_Ms = plot([xx(S) xx(E)],[yy(S) yy(E)], 'm', 'LineWidth', 2);
                else
                    M_Ms = plot(xx(S:E),yy(S:E), 'm', 'LineWidth', 2);
                end
            end
            
            %Plot Saccades
            for jj = 1:length(vt{ii}.saccades.startSample)
                S = vt{ii}.saccades.startSample(jj);
                E = S + vt{ii}.saccades.durationSample(jj) - 1;
                if ~PlotRawData
                    M_S = plot([xx(S) xx(E)],[yy(S) yy(E)], 'b', 'LineWidth', 1);
                else
                    M_S = plot(xx(S:E),yy(S:E), 'b', 'LineWidth', 1);
                end
            end
            
            % just to plot the bar with the scale on the imagge
            plot([4  4 + addition], [(ImageSizey/2)-3 (ImageSizey/2)-3],'k', 'LineWidth', 3) %is this correct?
            text(1, (ImageSizey/2)+5, sprintf('%.0f arcmin', round(addition*PxArcmin)), 'FontSize', 10) %is this correct?
            set(gca,'FontSize', 15)
            axis([-ImageSizex/2-5 ImageSizex/2+5 -ImageSizey/2-10 ImageSizey/2+10])
            xlabel('Pixel')
            input ''
            close all
        end
    end
end
save(sprintf('./ValidTrials/%s.mat', Name), 'vt');
