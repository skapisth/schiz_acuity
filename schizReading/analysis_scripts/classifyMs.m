%This text takes the inputs vt{ii}.x.position, vt{ii}.y.position, start times of all MS's, end
%times of all MS's, Pixel-to-Arcmin ration, and the index to which image we're on and then analyzes
%the microsaccade's behavior over the text itself, both in relation to the
%words and the characters.

%Make sure offset is added to x and y positions

function [msClass, inChar, words] = classifyMs(xx, yy, msSVec, msEVec, pxAngle, par, rm) %change this later

WORD_EXP = 10; %expand outer part of image
CHAR_EXP = 5; %expand bounding boxes
fig = true;



%Im = imread(sprintf('C:/Users/Natalya/Documents/APLab/ProcessVision/Passages/P%s.bmp',ii)); %read image
img = imread(sprintf('C:/Users/sanjana/OneDrive - University of Rochester/Documents/Sanjana/Projects/schizophrenia/schizReading/stimuli/1200/%s.bmp', par));
load(sprintf('C:/Users/sanjana/OneDrive - University of Rochester/Documents/Sanjana/Projects/schizophrenia/schizReading/OCRResults/DDPI/%s', par))
%% Image Matrix

%Size of images
ySize = size(img,1) + WORD_EXP;
xSize = size(img,2) + WORD_EXP;

%Create duaay images
wordMat = zeros(xSize,ySize);
charMat = zeros(xSize,ySize);

%Expand Vertically
% expend word boxes and use all four corners
wordBox = [rOCR.WordBoundingBoxes(:,1) - WORD_EXP ...
    rOCR.WordBoundingBoxes(:,2) - WORD_EXP * 2 ...
    (rOCR.WordBoundingBoxes(:,1) + rOCR.WordBoundingBoxes(:,3) + WORD_EXP) ...
    (rOCR.WordBoundingBoxes(:,2) + rOCR.WordBoundingBoxes(:,4) + WORD_EXP * 2)];
%Coordinate system of boxes around each word. Different numbers/colors
%represent different words.
wordBox(wordBox<=0) = 1; %eliminate negatives

% project word boundaries onto images
for i = 1:length(wordBox)
    wordMat(wordBox(i, 1):wordBox(i, 3),wordBox(i, 2):wordBox(i, 4)) = i;
end
wordMat = wordMat';
wordMat = flipud(wordMat);
%% Character Matrix

%Matrix of bounding boxes of characters
charBox = [rOCR.CharacterBoundingBoxes(:,1) - CHAR_EXP rOCR.CharacterBoundingBoxes(:,2) - CHAR_EXP * 2 ...
    (rOCR.CharacterBoundingBoxes(:,1) + rOCR.CharacterBoundingBoxes(:,3)) + CHAR_EXP ...
    (rOCR.CharacterBoundingBoxes(:,2) + rOCR.CharacterBoundingBoxes(:,4)) + CHAR_EXP * 2];
charBox(charBox<=0) = 1; %eliminate negatives
%Create Character Matrix
for i = 1:length(charBox)
    charMat(charBox(i,1):charBox(i,3), charBox(i,2):charBox(i,4)) = i;
end
charMat = charMat';
charMat = flipud(charMat);
%% Start/End points of microsaccades

% convert eye trace from angle to px
xx = (xx/pxAngle) + (xSize/2);
yy = (yy/pxAngle) + (ySize/2);
% start and end pos of ms
xSPos = round(xx(msSVec)); xEPos = round(xx(msEVec)); %Start and end points
ySPos = round(yy(msSVec)); yEPos = round(yy(msEVec)); %Start and end points
% plot xx and yy
xx(rm) = nan;
yy(rm) = nan;
if fig
    figure('Position', [100, 100, 1200, 500]); hold on
    img = flipud(img);
    imagesc([0 xSize], [0 ySize], img)
    axis('xy')
    % imagesc(charMat, 'AlphaData', .5);
    plot(xx, yy, 'Color', [0.3010, 0.7450, 0.9330 0.6], 'LineWidth', 2)
    set(gca,'FontSize', 15)
    axis([-5 xSize -5 ySize])
    xlabel('Pixel')
    % imagesc(charMat, 'AlphaData', .5)
    % set(gcf,'unit','normalized','outerposition',[0 0 1 1]);
end

% change to 1 if 0
xSPos(xSPos<=0)=1; xEPos(xEPos<=0)=1;
ySPos(ySPos<=0)=1; yEPos(yEPos<=0)=1;

% 1 = Background MS
% 2 = Corrective MS
% 3 = Regressive Within     4 = Progressive Within
% 5 = Regressive Branch     6 = Progressive Branch

wd = wordMat;
cr = charMat;
msClass = [];
inChar = [];
words = [];

% dicard ms out of bounds
kill = ySPos > ySize | xSPos>xSize| ...
    yEPos > ySize | xEPos > xSize;
if sum(kill) > 0
    ySPos(kill) = [];
    xSPos(kill) = [];
    yEPos(kill) = [];
    xEPos(kill) = [];
end
%Loop through each MS
for j = 1:length(xSPos)
    % background
    if wd(yEPos(j),xEPos(j)) == 0
        fprintf('background \n')
        msClass = [msClass 1];
        words = [words {nan}];
        plot([xSPos(j) xEPos(j)], [ySPos(j) yEPos(j)], 'b', 'LineWidth', 2);
        %CORRECTIVE MICROSACCADES
    elseif (wd(ySPos(j),xSPos(j)) == 0 && wd(yEPos(j),xEPos(j)) ~= 0)
        words = [words rOCR.Words(wd(yEPos(j),xEPos(j)))];
        msClass = [msClass 2];
        fprintf('corrective \n')
        plot([xSPos(j) xEPos(j)], [ySPos(j) yEPos(j)], 'm', 'LineWidth', 2);
    elseif  (wd(ySPos(j),xSPos(j)) == wd(yEPos(j),xEPos(j))) && wd(ySPos(j),xSPos(j)) ~= 0 %WITHIN MICROSACCADES
        %%negative for regressive, positive for progressive
        % inChar = [inChar (cr(yEPos(j),XEtmp)) - (cr(ySPos(j),XStmp))];
        tmpChar = (cr(yEPos(j), xEPos(j))) - (cr(ySPos(j), xSPos(j)));
        %if cr(ySPos(j),XStmp) > cr(yEPos(j),XEtmp)
        if tmpChar < 0
            msClass = [msClass 3]; %REGRESSIVE
            inChar = [inChar tmpChar];
            words = [words rOCR.Words(wd(yEPos(j), xEPos(j)))];
            fprintf('regressive within \n')
            plot([xSPos(j) xEPos(j)], [ySPos(j) yEPos(j)], 'r', 'LineWidth', 2);
%         elseif cr(ySPos(j),XStmp) < cr(yEPos(j),XEtmp)
        elseif tmpChar > 0
            msClass = [msClass 4]; %PROGRESSIVE
            inChar = [inChar tmpChar];
            words = [words rOCR.Words(wd(yEPos(j), xEPos(j)))];
            fprintf('progressive within \n')
            plot([xSPos(j) xEPos(j)], [ySPos(j) yEPos(j)], 'r', 'LineWidth', 2);
        end
    elseif (wd(ySPos(j),xSPos(j)) ~= 0 && wd(yEPos(j),xEPos(j)) ~=0 ) && (wd(ySPos(j),xSPos(j)) ~= wd(yEPos(j),xEPos(j))) %BRANCHING MICROSACCADES
        if (wd(ySPos(j),xSPos(j)) > wd(yEPos(j),xEPos(j)))
            msClass = [msClass 5]; %REGRESSIVE
%             words  = [words strcat(rOCR.Words(wd(ySPos(j),xSPos(j))),'_',rOCR.Words(wd(yEPos(j),xEPos(j))))];
            words  = [words rOCR.Words(wd(yEPos(j),xEPos(j)))];
            fprintf('regressive branch \n')
            plot([xSPos(j) xEPos(j)], [ySPos(j) yEPos(j)], 'r', 'LineWidth', 2);
        elseif (wd(ySPos(j),xSPos(j)) < wd(yEPos(j),xEPos(j)))
            msClass = [msClass 6]; %PROGRESSIVE
            % words  = [words strcat(rOCR.Words(wd(ySPos(j),xSPos(j))),'_',rOCR.Words(wd(yEPos(j),xEPos(j))))];
            words  = [words rOCR.Words(wd(yEPos(j),xEPos(j)))];
            fprintf('progressive branch \n')
            plot([xSPos(j) xEPos(j)], [ySPos(j) yEPos(j)], 'r', 'LineWidth', 2);
        end
    end
end
%words = unique(words);
